<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('roles')->delete();

        DB::table('roles')->insert(array(
            0 =>
            array(
                'actions' => '2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|29|30|31|32|33|34|35|36|37|38|39|40|41|97',
                'created_at' => '2021-06-29 05:53:35',
                'id' => 1,
                'module_id' => 1,
                'title' => 'Admin',
                'updated_at' => '2021-06-29 05:53:35',
            ),
            1 =>
            array(
                'actions' => '42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|92|93|94|95|96|97',
                'created_at' => '2021-06-29 11:52:18',
                'id' => 20,
                'module_id' => 2,
                'title' => 'Seller',
                'updated_at' => '2021-06-29 07:04:27',
            )

        ));
    }
}
