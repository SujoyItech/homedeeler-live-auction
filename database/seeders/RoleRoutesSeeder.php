<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleRoutesSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('role_routes')->delete();

        DB::table('role_routes')->insert(array(
            0 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 1,
                'module_id' => 0,
                'name' => 'Super Admin Home',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'superAdminHome',
            ),
            1 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 2,
                'module_id' => 1,
                'name' => 'Admin Home',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'adminHome',
            ),
            2 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 3,
                'module_id' => 1,
                'name' => 'Site Setting',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'siteSetting',
            ),
            3 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 4,
                'module_id' => 1,
                'name' => 'Payment Settings',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'paymentSettings',
            ),
            4 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 5,
                'module_id' => 1,
                'name' => 'Payment Setting Save',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'paymentSettingSave',
            ),
            5 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 6,
                'module_id' => 1,
                'name' => 'Logo Setting',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'logoSetting',
            ),
            6 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 7,
                'module_id' => 1,
                'name' => 'Social Setting',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'socialSetting',
            ),
            7 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 8,
                'module_id' => 1,
                'name' => 'Application Setting',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'applicationSetting',
            ),
            8 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 9,
                'module_id' => 1,
                'name' => 'Commission Rate Settings',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'commissionRateSettings',
            ),
            9 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 10,
                'module_id' => 1,
                'name' => 'Admin Settings Save',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'adminSettingsSave',
            ),
            10 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 11,
                'module_id' => 1,
                'name' => 'Command Settings',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'commandSettings',
            ),
            11 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 12,
                'module_id' => 1,
                'name' => 'Run Command',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'runCommand',
            ),
            12 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 13,
                'module_id' => 1,
                'name' => 'Create Command',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'createCommand',
            ),
            13 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 14,
                'module_id' => 1,
                'name' => 'Terms Condition Settings',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'termsConditionSettings',
            ),
            14 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 15,
                'module_id' => 1,
                'name' => 'Privacy Policy Settings',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'privacyPolicySettings',
            ),
            15 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 16,
                'module_id' => 1,
                'name' => 'About Us Settings',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'aboutUsSettings',
            ),
            16 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 17,
                'module_id' => 1,
                'name' => 'Help Center Settings',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'helpCenterSettings',
            ),
            17 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 18,
                'module_id' => 1,
                'name' => 'Why Choose Us Settings',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'whyChooseUsSettings',
            ),
            18 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 19,
                'module_id' => 1,
                'name' => 'Faqs Settings',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'faqsSettings',
            ),
            19 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 20,
                'module_id' => 1,
                'name' => 'Faqs Setting Save',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'faqsSettingSave',
            ),
            20 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 21,
                'module_id' => 1,
                'name' => 'Delete Faqs Item',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'deleteFaqsItem',
            ),
            21 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 22,
                'module_id' => 1,
                'name' => 'Get Faq Item By Id',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'getFaqItemById',
            ),
            22 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 23,
                'module_id' => 1,
                'name' => 'Roles',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'roles',
            ),
            23 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 24,
                'module_id' => 1,
                'name' => 'Save Role',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'saveRole',
            ),
            24 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 25,
                'module_id' => 1,
                'name' => 'Edit Role',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'editRole',
            ),
            25 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 26,
                'module_id' => 1,
                'name' => 'Delete Role',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'deleteRole',
            ),
            26 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 27,
                'module_id' => 1,
                'name' => 'Update Route List',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'updateRouteList',
            ),
            27 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 28,
                'module_id' => 1,
                'name' => 'Get Route By Type',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'getRouteByType',
            ),
            28 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 29,
                'module_id' => 1,
                'name' => 'Users',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'users',
            ),
            29 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 30,
                'module_id' => 1,
                'name' => 'Save User',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'saveUser',
            ),
            30 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 31,
                'module_id' => 1,
                'name' => 'Edit User',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'editUser',
            ),
            31 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 32,
                'module_id' => 1,
                'name' => 'Delete User',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'deleteUser',
            ),
            32 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 33,
                'module_id' => 1,
                'name' => 'User Slug Check',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'userSlugCheck',
            ),
            33 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 34,
                'module_id' => 1,
                'name' => 'Sellers',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'sellers',
            ),
            34 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 35,
                'module_id' => 1,
                'name' => 'Save Seller',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'saveSeller',
            ),
            35 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 36,
                'module_id' => 1,
                'name' => 'Edit Seller',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'editSeller',
            ),
            36 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 37,
                'module_id' => 1,
                'name' => 'Approve Seller',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'approveSeller',
            ),
            37 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 38,
                'module_id' => 1,
                'name' => 'Bidders',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'bidders',
            ),
            38 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 39,
                'module_id' => 1,
                'name' => 'Save Bidder',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'saveBidder',
            ),
            39 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 40,
                'module_id' => 1,
                'name' => 'Edit Bidder',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'editBidder',
            ),
            40 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 41,
                'module_id' => 1,
                'name' => 'Admin Logs',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'adminLogs',
            ),
            41 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 42,
                'module_id' => 2,
                'name' => 'Auctions',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'auctions',
            ),
            42 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 43,
                'module_id' => 2,
                'name' => 'Auction Details',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'auctionDetails',
            ),
            43 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 44,
                'module_id' => 2,
                'name' => 'Edit Auction',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'editAuction',
            ),
            44 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 45,
                'module_id' => 2,
                'name' => 'Init Auction',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'initAuction',
            ),
            45 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 46,
                'module_id' => 2,
                'name' => 'Save Auction',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'saveAuction',
            ),
            46 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 47,
                'module_id' => 2,
                'name' => 'Save Product',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'saveProduct',
            ),
            47 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 48,
                'module_id' => 2,
                'name' => 'Delete Product',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'deleteProduct',
            ),
            48 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 49,
                'module_id' => 2,
                'name' => 'Product Slug Check',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'productSlugCheck',
            ),
            49 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 50,
                'module_id' => 2,
                'name' => 'Product Status Change',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'productStatusChange',
            ),
            50 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 51,
                'module_id' => 2,
                'name' => 'Save Product Categories And Tags',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'saveProductCategoriesAndTags',
            ),
            51 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 52,
                'module_id' => 2,
                'name' => 'Save Product Combinations',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'saveProductCombinations',
            ),
            52 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 53,
                'module_id' => 2,
                'name' => 'Save Product Media',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'saveProductMedia',
            ),
            53 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 54,
                'module_id' => 2,
                'name' => 'Save Product Featured Media',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'saveProductFeaturedMedia',
            ),
            54 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 55,
                'module_id' => 2,
                'name' => 'Save Product Pricing Info',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'saveProductPricingInfo',
            ),
            55 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 56,
                'module_id' => 2,
                'name' => 'Make Auction Live',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'makeAuctionLive',
            ),
            56 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 57,
                'module_id' => 2,
                'name' => 'Save Auction Pricing Info',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'saveAuctionPricingInfo',
            ),
            57 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 58,
                'module_id' => 2,
                'name' => 'Auction Payment Make',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'auctionPaymentMake',
            ),
            58 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 59,
                'module_id' => 2,
                'name' => 'Auction Status Change',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'auctionStatusChange',
            ),
            59 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 60,
                'module_id' => 2,
                'name' => 'Messaging',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'messaging',
            ),
            60 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 61,
                'module_id' => 2,
                'name' => 'Get Event Messages',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'getEventMessages',
            ),
            61 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 62,
                'module_id' => 2,
                'name' => 'Show Message Details',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'showMessageDetails',
            ),
            62 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 63,
                'module_id' => 2,
                'name' => 'Send Message',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'sendMessage',
            ),
            63 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 64,
                'module_id' => 2,
                'name' => 'Current Auction List',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'currentAuctionList',
            ),
            64 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 65,
                'module_id' => 2,
                'name' => 'Live Auction List',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'liveAuctionList',
            ),
            65 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 66,
                'module_id' => 2,
                'name' => 'Complete Auction List',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'completeAuctionList',
            ),
            66 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 67,
                'module_id' => 2,
                'name' => 'My Auction List',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'myAuctionList',
            ),
            67 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 68,
                'module_id' => 2,
                'name' => 'Create Auction',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'createAuction',
            ),
            68 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 69,
                'module_id' => 2,
                'name' => 'Product Manager Home',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'productManagerHome',
            ),
            69 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 70,
                'module_id' => 2,
                'name' => 'Categories',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'categories',
            ),
            70 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 71,
                'module_id' => 2,
                'name' => 'Edit Category',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'editCategory',
            ),
            71 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 72,
                'module_id' => 2,
                'name' => 'Show Category',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'showCategory',
            ),
            72 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 73,
                'module_id' => 2,
                'name' => 'Save Category',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'saveCategory',
            ),
            73 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 74,
                'module_id' => 2,
                'name' => 'Delete Category',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'deleteCategory',
            ),
            74 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 75,
                'module_id' => 2,
                'name' => 'Category Order Update',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'categoryOrderUpdate',
            ),
            75 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 76,
                'module_id' => 2,
                'name' => 'Category Order Save',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'categoryOrderSave',
            ),
            76 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 77,
                'module_id' => 2,
                'name' => 'Category Slug Check',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'categorySlugCheck',
            ),
            77 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 78,
                'module_id' => 2,
                'name' => 'Brands',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'brands',
            ),
            78 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 79,
                'module_id' => 2,
                'name' => 'Edit Brand',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'editBrand',
            ),
            79 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 80,
                'module_id' => 2,
                'name' => 'Save Brand',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'saveBrand',
            ),
            80 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 81,
                'module_id' => 2,
                'name' => 'Brand Slug Check',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'brandSlugCheck',
            ),
            81 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 82,
                'module_id' => 2,
                'name' => 'Delete Brand',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'deleteBrand',
            ),
            82 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 83,
                'module_id' => 2,
                'name' => 'Tags',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'tags',
            ),
            83 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 84,
                'module_id' => 2,
                'name' => 'Edit Tag',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'editTag',
            ),
            84 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 85,
                'module_id' => 2,
                'name' => 'Store Tag',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'storeTag',
            ),
            85 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 86,
                'module_id' => 2,
                'name' => 'Delete Tag',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'deleteTag',
            ),
            86 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 87,
                'module_id' => 2,
                'name' => 'Combination Type',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'combinationType',
            ),
            87 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 88,
                'module_id' => 2,
                'name' => 'Edit Combination Type',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'editCombinationType',
            ),
            88 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 89,
                'module_id' => 2,
                'name' => 'Store Combination Type',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'storeCombinationType',
            ),
            89 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 90,
                'module_id' => 2,
                'name' => 'Delete Combination Type',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'deleteCombinationType',
            ),
            90 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 91,
                'module_id' => 2,
                'name' => 'Combination',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'combination',
            ),
            91 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 92,
                'module_id' => 2,
                'name' => 'Edit Combination',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'editCombination',
            ),
            92 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 93,
                'module_id' => 2,
                'name' => 'Store Combination',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'storeCombination',
            ),
            93 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 94,
                'module_id' => 2,
                'name' => 'Delete Combination',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'deleteCombination',
            ),
            94 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 95,
                'module_id' => 2,
                'name' => 'Seller Home',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'sellerHome',
            ),
            95 =>
            array(
                'created_at' => '2021-06-18 10:47:06',
                'id' => 96,
                'module_id' => 2,
                'name' => 'My Auctions',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'myAuctions',
            ),
            96 =>
            array(
                'created_at' => '2021-06-29 05:36:25',
                'id' => 97,
                'module_id' => 2,
                'name' => 'Transaction Histories',
                'updated_at' => '2021-06-29 07:04:16',
                'url' => 'transactionHistories',
            ),
        ));
    }
}
