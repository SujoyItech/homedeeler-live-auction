<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductBrandsTable extends Migration {

    public function up()
    {
        Schema::create('product_brands', function(Blueprint $table) {
            $table->integer('product_id');
            $table->integer('brand_id');
        });
    }

    public function down()
    {
        Schema::drop('product_brands');
    }
}
