<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateAuctionDetailsHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('auction_details_history', function (Blueprint $table) {
            $table->unsignedInteger('auction_id')->primary();
            $table->text('bidding_history')->nullable();
        });
        DB::statement("ALTER TABLE `auction_details_history` comment 'when a auction completed, all biding history kept here as json format'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auction_details_history');
    }
}
