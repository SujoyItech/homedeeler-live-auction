<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateAuctionPaymentTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('auction_payment_transactions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('auction_id')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->tinyInteger('purpose')->comment("offline_charge = 1, live_charge = 2, product_payment = 3, seller_payback = 4")->nullable();
            $table->tinyInteger('payment_method')->comment("card = 1, paypal = 2, btc = 3")->nullable();
            $table->decimal('amount',12,2)->nullable();
            $table->date('created_at')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->text('transaction_reference')->nullable();
            $table->tinyInteger('payment_status')->nullable();
        });
        DB::statement("ALTER TABLE `auction_payment_transactions` comment 'The transaction log for any payment is kept here defined by status (as constant definition)'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auction_payment_transactions');
    }
}
