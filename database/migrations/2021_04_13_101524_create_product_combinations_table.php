<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCombinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_combinations', function (Blueprint $table) {
            $table->id();
            $table->integer('product_id');
            $table->integer('combination_type_id')->nullable();
            $table->integer('combination_id')->nullable();
            $table->enum('media_type',[INTERNAL_IMAGE, EXTERNAL_IMAGE, VIDEO_URL, _360_URL])->nullable();
            $table->text('media_url')->nullable();
            $table->tinyInteger('status')->default(ACTIVE);
            $table->tinyInteger('is_featured')->default(INACTIVE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_combinations');
    }
}
