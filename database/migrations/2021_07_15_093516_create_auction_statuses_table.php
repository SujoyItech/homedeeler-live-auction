<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuctionStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auction_statuses', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('auction_id');
            $table->text('message')->nullable();
            $table->text('extra_message')->nullable();
            $table->string('image')->nullable();
            $table->string('file')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auction_statuses');
    }
}
