<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateAuctionPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('auction_payments', function (Blueprint $table) {
            $table->bigInteger('auction_id')->nullable();
            $table->decimal('offline_auction_charge',6,2)->nullable();
            $table->decimal('offline_auction_charge_paid',6,2)->nullable();
            $table->dateTime('offline_auction_charge_paid_time')->nullable();
            $table->decimal('live_auction_charge',6,2)->nullable();
            $table->decimal('live_auction_charge_paid',6,2)->nullable();
            $table->dateTime('live_auction_charge_paid_time')->nullable();
            $table->decimal('auction_winning_price',10,2)->nullable();
            $table->decimal('commission_price',10,2)->nullable();
            $table->decimal('processing_fee',7,2)->nullable();
            $table->decimal('total_price',10,2)->nullable();
            $table->decimal('recieved_amount_by_winner',10,0)->nullable();
            $table->dateTime('recieved_amount_by_winner_time')->nullable();
            $table->decimal('seller_paying_amount',10,2)->nullable();
            $table->dateTime('seller_paying_amount_time')->nullable();
            $table->boolean('is_live_charge_paid')->nullable();
            $table->boolean('is_winner_paid')->nullable();
            $table->boolean('is_seller_paid')->nullable();
        });
        DB::statement("ALTER TABLE `auction_payments` comment 'All types of payments information will kept here'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auction_payments');
    }
}
