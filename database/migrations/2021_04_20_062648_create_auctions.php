<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuctions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auctions', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('product_id')->nullable();
            $table->text('description')->nullable();
            $table->decimal('base_price',10,2)->nullable();
            $table->smallInteger('bid_increment')->comment("Default value come from admin settings")->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->tinyInteger('winnig_position')->default(1)->comment("Winner is 1; If absent second winner : 2 && so on...")->nullable();
            $table->bigInteger('winner_id')->nullable();
            $table->decimal('highest_bid',10,2)->nullable();
            $table->decimal('commission_price',10,2)->nullable();
            $table->decimal('processing_fee',7,2)->nullable();
            $table->decimal('total_price',10,2)->nullable();
            $table->boolean('has_live')->default(0)->nullable();
            $table->dateTime('live_start_time')->nullable();
            $table->integer('live_duration')->unsigned()->nullable()->comment('live auction time (in minutes)');
            $table->tinyInteger('live_extend_minutes')->comment("increses by admin setting. e.g. 5 > 10 > 15 ....")->nullable();
            $table->tinyInteger('live_extends_count')->comment("extended count (limit by admin settings) e.g. 1 > 2 > 3 ...")->nullable();
            $table->boolean('is_featured')->default(0)->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->tinyInteger('status')->comment("inactive = 0, active = 10, in_live = 11, processing = 20, closed = 30")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auctions');
    }
}
