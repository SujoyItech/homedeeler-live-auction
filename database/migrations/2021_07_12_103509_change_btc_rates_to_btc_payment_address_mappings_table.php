<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeBtcRatesToBtcPaymentAddressMappingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('btc_payment_address_mappings', function (Blueprint $table) {
            $table->decimal('btc_rates',18,18)->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('btc_payment_address_mappings', function (Blueprint $table) {
            $table->dropColumn('btc_rates');
        });
    }
}
