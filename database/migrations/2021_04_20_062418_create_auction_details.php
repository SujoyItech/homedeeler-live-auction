<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateAuctionDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('auction_details', function (Blueprint $table) {
            $table->bigInteger('auction_id')->nullable();
            $table->bigInteger('bidder_id')->nullable();
            $table->decimal('bid_price',10,2)->nullable();
            $table->dateTime('created_at')->nullable();
        });
        DB::statement("ALTER TABLE `auction_details` comment 'The bidding information before the auction procedure has been completed is kept here as in seperate row'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auction_details');
    }
}
