<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

    public function up()
    {
        Schema::create('products', function(Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->bigInteger('seller_id')->nullable();
            $table->string('slug', 255)->unique();
            $table->text('description')->nullable();
            $table->text('additional_info')->nullable();
            $table->boolean('is_new')->default(1);
            $table->tinyInteger('status')->default(0);
            $table->string('meta_title', 255)->nullable();
            $table->text('meta_keywords')->nullable();
            $table->text('meta_description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('products');
    }
}
