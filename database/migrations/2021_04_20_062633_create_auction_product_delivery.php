<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuctionProductDelivery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('auction_product_delivery', function (Blueprint $table) {
            $table->bigInteger('auction_id');
            $table->primary('auction_id');
            $table->text('delivery_information')->nullable();
            $table->bigInteger('delivered_to')->nullable();
            $table->bigInteger('delivered_by')->nullable();
            $table->tinyInteger('status')->comment("10 = requested, 20 = delivered, 30 = confirmed")->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('delivered_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auction_product_delivery');
    }
}
