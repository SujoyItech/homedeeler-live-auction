<?php

use Illuminate\Support\Facades\Route;


Route::any('auctions', 'Web\Admin\Auction\AuctionController@auction')->name('auctions');

Route::any('auction-details/{slug}', 'Web\Admin\Auction\AuctionController@auctionDetails')->name('auctionDetails');

Route::get('auction-edit/{slug}', 'Web\Admin\Auction\AuctionController@edit')->name('editAuction');
Route::post('auction-init', 'Web\Admin\Auction\AuctionController@initAuction')->name('initAuction');
Route::post('auction-save', 'Web\Admin\Auction\AuctionController@saveAuction')->name('saveAuction');
Route::post('product-save', 'Web\Admin\Product\ProductController@saveProduct')->name('saveProduct');
Route::post('product-delete', 'Web\Admin\Product\ProductController@delete')->name('deleteProduct');
Route::post('product-slug-check', 'Web\Admin\Product\ProductController@productSlugCheck')->name('productSlugCheck');
Route::post('product-status-change', 'Web\Admin\Product\ProductController@productStatusChange')->name('productStatusChange');
Route::post('product-categories-tags-save', 'Web\Admin\Product\ProductController@saveProductCategoriesAndTags')->name('saveProductCategoriesAndTags');
Route::post('product-combination-save', 'Web\Admin\Product\ProductController@saveProductCombinations')->name('saveProductCombinations');
Route::post('product-media-save', 'Web\Admin\Product\ProductController@saveProductMedia')->name('saveProductMedia');
Route::post('product-featured-media-save', 'Web\Admin\Product\ProductController@saveProductFeaturedMedia')->name('saveProductFeaturedMedia');
Route::post('product-pricing-info-save', 'Web\Admin\Product\ProductController@saveProductPricingInfo')->name('saveProductPricingInfo');

Route::post('view-auction-live-payment-page', 'Web\Admin\Auction\AuctionController@viewLiveAuctionPaymentPage')->name('viewLiveAuctionPaymentPage');
Route::post('make-auction-live', 'Web\Admin\Auction\AuctionController@makeAuctionLive')->name('makeAuctionLive');

Route::post('auction-pricing-info-save', 'Web\Admin\Auction\AuctionController@saveAuctionPricingInfo')->name('saveAuctionPricingInfo');
Route::post('auction-payment-make', 'Web\Admin\Auction\AuctionController@auctionPaymentMake')->name('auctionPaymentMake');
Route::post('auction-status-change', 'Web\Admin\Auction\AuctionController@auctionStatusChange')->name('auctionStatusChange');
Route::post('auction-status-history-update', 'Web\Admin\Auction\AuctionController@updateAuctionStatusHistory')->name('updateAuctionStatusHistory');

Route::post('auction-approve', 'Web\Admin\Auction\AuctionController@auctionApprove')->name('auctionApprove');

Route::get('messaging', 'Web\Admin\Messaging\MessagingController@messaging')->name('messaging');
Route::post('get-event-messages', 'Web\Admin\Messaging\MessagingController@getEventMessages')->name('getEventMessages');
Route::post('show-message-details', 'Web\Admin\Messaging\MessagingController@showMessageDetails')->name('showMessageDetails');
Route::post('send-message', 'Web\Admin\Messaging\MessagingController@sendMessage')->name('sendMessage');


Route::post('load-admin-live-auction-message', 'Web\Admin\Messaging\MessagingController@loadAdminLiveAuctionMessage')->name('loadAdminLiveAuctionMessage');
Route::post('send-admin-live-auction-reply', 'Web\Admin\Messaging\MessagingController@sendAdminLiveAuctionReply')->name('sendAdminLiveAuctionReply');

Route::post('load-seller-payment-modal-body', 'Web\Admin\Auction\AuctionController@loadSellerPaymentModalBody')->name('loadSellerPaymentModalBody');
Route::post('pay-auction-price-to-seller', 'Web\Admin\Auction\AuctionController@payAuctionPriceToSeller')->name('payAuctionPriceToSeller');


Route::any('current-auction-list', 'Web\Admin\Auction\AuctionController@currentAuctionList')->name('currentAuctionList');
Route::any('live-auction-list', 'Web\Admin\Auction\AuctionController@liveAuctionList')->name('liveAuctionList');
Route::any('complete-auction-list', 'Web\Admin\Auction\AuctionController@completeAuctionList')->name('completeAuctionList');

Route::any('my-auction-list', 'Web\Admin\Auction\AuctionController@myAuctionList')->name('myAuctionList');
Route::get('create-auction', 'Web\Admin\Auction\AuctionController@createAuction')->name('createAuction');

Route::any('transaction-histories', 'TransactionController@transactionHistories')->name('transactionHistories');

Route::post('create-token', 'AgoraVideoController@createToken')->name('createToken');

Route::get('auction-live-streaming/{slug}', 'AgoraVideoController@auctionLiveStreaming')->name('auctionLiveStreaming');
Route::post('auction-live-streaming-stop', 'AgoraVideoController@auctionLiveStreamingStop')->name('auctionLiveStreamingStop');
