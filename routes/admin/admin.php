<?php

use Illuminate\Support\Facades\Route;

Route::get('admin-home', 'Web\Admin\DashboardController@adminHome')->name('adminHome');

/*==============================================Settings===============================================*/
Route::get('site-settings', 'Web\Admin\Settings\SettingsController@siteSetting')->name('siteSetting');
Route::get('payment-settings', 'Web\Admin\Settings\SettingsController@paymentSettings')->name('paymentSettings');
Route::get('admin-payment-settings-save', 'Web\Admin\Settings\SettingsController@paymentSettingSave')->name('paymentSettingSave');

Route::get('coin-payment-settings', 'Web\Admin\Settings\SettingsController@coinPaymentSettings')->name('coinPaymentSettings');
Route::get('coin-payment-settings-save', 'Web\Admin\Settings\SettingsController@coinPaymentSettingSave')->name('coinPaymentSettingSave');

Route::get('logo-settings', 'Web\Admin\Settings\SettingsController@logoSetting')->name('logoSetting');
Route::get('social-settings', 'Web\Admin\Settings\SettingsController@socialSetting')->name('socialSetting');
Route::get('application-settings', 'Web\Admin\Settings\SettingsController@applicationSetting')->name('applicationSetting');
Route::get('commission-rate-settings', 'Web\Admin\Settings\SettingsController@commissionRateSettings')->name('commissionRateSettings');

Route::post('admin-settings-save', 'Web\Admin\Settings\SettingsController@adminSettingsSave')->name('adminSettingsSave');
Route::get('commands-settings', 'Web\Admin\Settings\SettingsController@commandSettings')->name('commandSettings');
Route::post('runCommand', 'Web\Admin\Settings\SettingsController@runCommand')->name('runCommand');

Route::post('create-command', 'Web\Admin\Settings\SettingsController@createCommand')->name('createCommand');

Route::get('terms-condition-settings', 'Web\Admin\Settings\SettingsController@termsConditionSettings')->name('termsConditionSettings');
Route::get('privacy-policy-settings', 'Web\Admin\Settings\SettingsController@privacyPolicySettings')->name('privacyPolicySettings');
Route::get('about-us-settings', 'Web\Admin\Settings\SettingsController@aboutUsSettings')->name('aboutUsSettings');
Route::get('help-center-settings', 'Web\Admin\Settings\SettingsController@helpCenterSettings')->name('helpCenterSettings');
Route::get('why-choose-us-settings', 'Web\Admin\Settings\SettingsController@whyChooseUsSettings')->name('whyChooseUsSettings');

Route::get('faqs-settings', 'Web\Admin\Settings\SettingsController@faqsSettings')->name('faqsSettings');
Route::post('faqs-setting-save', 'Web\Admin\Settings\SettingsController@faqsSettingSave')->name('faqsSettingSave');
Route::post('faqs-item-delete', 'Web\Admin\Settings\SettingsController@deleteFaqsItem')->name('deleteFaqsItem');
Route::post('faqs-item-by-id', 'Web\Admin\Settings\SettingsController@getFaqItemById')->name('getFaqItemById');


/*==============================================Settings===============================================*/

/*==============================================Roles && Permissions=======================================*/
Route::get('roles', 'Web\Admin\Role\RolePermissionController@index')->name('roles');
Route::post('role-save', 'Web\Admin\Role\RolePermissionController@store')->name('saveRole');
Route::post('role-edit', 'Web\Admin\Role\RolePermissionController@edit')->name('editRole');
Route::post('role-delete', 'Web\Admin\Role\RolePermissionController@delete')->name('deleteRole');
Route::get('update-routing-list', 'Web\Admin\Role\RouteController@updateRouteList')->name('updateRouteList');
Route::post('get-routes-by-type', 'Web\Admin\Role\RouteController@getRouteByType')->name('getRouteByType');
/*==============================================Roles && Permissions=======================================*/


/*==============================================Users===============================================*/
Route::get('users', 'Web\Admin\User\UserController@users')->name('users');
Route::post('user-save', 'Web\Admin\User\UserController@saveUser')->name('saveUser');
Route::post('user-edit', 'Web\Admin\User\UserController@editUser')->name('editUser');
Route::post('user-delete', 'Web\Admin\User\UserController@delete')->name('deleteUser');
Route::post('user-slug-check', 'Web\Admin\User\UserController@userSlugCheck')->name('userSlugCheck');

Route::get('user-details/{user_id}', 'Web\Admin\User\UserController@userDetails')->name('userDetails');

Route::post('verify-user', 'Web\Admin\User\UserController@verifyUser')->name('verifyUser');
Route::post('user-status-change', 'Web\Admin\User\UserController@userStatusChange')->name('userStatusChange');
/*==============================================Users===============================================*/

Route::get('sellers', 'Web\Admin\User\UserController@sellers')->name('sellers');
Route::post('seller-save', 'Web\Admin\User\UserController@saveSeller')->name('saveSeller');
Route::post('seller-edit', 'Web\Admin\User\UserController@editSeller')->name('editSeller');
Route::post('seller-approve', 'Web\Admin\User\UserController@approveSeller')->name('approveSeller');

Route::get('bidders', 'Web\Admin\User\UserController@bidders')->name('bidders');
Route::post('bidder-save', 'Web\Admin\User\UserController@saveBidder')->name('saveBidder');
Route::post('bidder-edit', 'Web\Admin\User\UserController@editBidder')->name('editBidder');

/*==============================================User Brands=======================================*/
// Logs
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('adminLogs');

Route::get('test-notification', 'HomeController@triggerTestNotification')->name('triggerTestNotification');

Route::get('general-messages', 'Web\Admin\Messaging\GeneralMessagingController@generalMessages')->name('generalMessages');
Route::post('delete-general-messages', 'Web\Admin\Messaging\GeneralMessagingController@deleteGeneralMessage')->name('deleteGeneralMessage');
Route::post('delete-general-messages', 'Web\Admin\Messaging\GeneralMessagingController@deleteGeneralMessage')->name('deleteGeneralMessage');
Route::post('reply-general-messages', 'Web\Admin\Messaging\GeneralMessagingController@replyGeneralMessage')->name('replyGeneralMessage');
