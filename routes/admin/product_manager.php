<?php
use Illuminate\Support\Facades\Route;

Route::get('product-manager-home', 'Web\Admin\Product\DashboardController@index')->name('productManagerHome');

Route::get('category/{id?}','Web\Admin\Product\CategoryController@index')->name('categories');
Route::post('category-edit','Web\Admin\Product\CategoryController@edit')->name('editCategory');
Route::post('category-show','Web\Admin\Product\CategoryController@show')->name('showCategory');
Route::post('category-save','Web\Admin\Product\CategoryController@store')->name('saveCategory');
Route::post('category-delete','Web\Admin\Product\CategoryController@delete')->name('deleteCategory');
Route::post('category-order-update','Web\Admin\Product\CategoryController@categoryOrderUpdate')->name('categoryOrderUpdate');
Route::post('category-order-save','Web\Admin\Product\CategoryController@categoryOrderSave')->name('categoryOrderSave');
Route::post('category-slug-check','Web\Admin\Product\CategoryController@categorySlugCheck')->name('categorySlugCheck');

Route::get('brand','Web\Admin\Product\BrandController@index')->name('brands');
Route::post('brand-edit','Web\Admin\Product\BrandController@edit')->name('editBrand');
Route::post('brand-save','Web\Admin\Product\BrandController@store')->name('saveBrand');
Route::post('brand-slug-check','Web\Admin\Product\BrandController@brandSlugCheck')->name('brandSlugCheck');
Route::post('brand-delete','Web\Admin\Product\BrandController@delete')->name('deleteBrand');

Route::get('tag','Web\Admin\Product\TagController@index')->name('tags');
Route::post('tag-edit','Web\Admin\Product\TagController@edit')->name('editTag');
Route::post('tag-save','Web\Admin\Product\TagController@store')->name('storeTag');
Route::post('tag-delete','Web\Admin\Product\TagController@delete')->name('deleteTag');

Route::get('combination-type','Web\Admin\Product\CombinationTypeController@index')->name('combinationType');
Route::post('combination-type-edit','Web\Admin\Product\CombinationTypeController@edit')->name('editCombinationType');
Route::post('combination-type-save','Web\Admin\Product\CombinationTypeController@store')->name('storeCombinationType');
Route::post('combination-type-delete','Web\Admin\Product\CombinationTypeController@delete')->name('deleteCombinationType');

Route::get('combination','Web\Admin\Product\CombinationController@index')->name('combination');
Route::post('combination-edit','Web\Admin\Product\CombinationController@edit')->name('editCombination');
Route::post('combination-save','Web\Admin\Product\CombinationController@store')->name('storeCombination');
Route::post('combination-delete','Web\Admin\Product\CombinationController@delete')->name('deleteCombination');


