<?php

use Illuminate\Support\Facades\Route;

Route::get('seller-home', 'Web\Admin\Seller\HomeController@index')->name('sellerHome');

Route::any('my-auctions', 'Web\Admin\Auction\AuctionController@myAuctions')->name('myAuctions');
