<?php
use Illuminate\Support\Facades\Route;
Route::get('auction/{slug}', 'Web\FrontEnd\AuctionController@auction')->name('auction');
Route::get('auction-bidding-list/{auction_id}', 'Web\FrontEnd\AuctionController@auctionBiddingList')->name('auctionBiddingList');
Route::any('auction-list/{search_key?}', 'Web\FrontEnd\AuctionController@auctionList')->name('auctionList');

Route::get('get-primary-category-auction-list', 'Web\FrontEnd\AuctionController@getPrimaryCategoryAuctionList')->name('getPrimaryCategoryAuctionList');
Route::get('get-featured-auction-list', 'Web\FrontEnd\AuctionController@getFeaturedAuctionList')->name('getFeaturedAuctionList');
Route::get('get-today-auction-list', 'Web\FrontEnd\AuctionController@getTodayAuctionList')->name('getTodayAuctionList');
Route::get('get-electronics-art-auction-list', 'Web\FrontEnd\AuctionController@getElectronicAndArtAuctionList')->name('getElectronicAndArtAuctionList');
Route::get('get-real-estate-auction-list', 'Web\FrontEnd\AuctionController@getRealEstateAuctionList')->name('getRealEstateAuctionList');

Route::post('submit-bid', 'Web\FrontEnd\AuctionController@submitBid')->name('submitBid');

Route::post('auction-message-list', 'Web\FrontEnd\MessageController@getUserAuctionMessageList')->name('getUserAuctionMessageList');
Route::post('auction-message-send', 'Web\FrontEnd\MessageController@sendAuctionMessage')->name('sendAuctionMessage');
Route::post('send-live-auction-message', 'Web\FrontEnd\MessageController@sendLiveMessageReply')->name('sendLiveMessageReply');
Route::post('load-live-auction-message', 'Web\FrontEnd\MessageController@loadLiveAuctionMessage')->name('loadLiveAuctionMessage');

Route::post('send-delivery-request', 'Web\FrontEnd\AuctionController@sendDeliveryRequest')->name('sendDeliveryRequest');
Route::post('send-product-receive-confirmation', 'Web\FrontEnd\AuctionController@sendProductReceiveConfirmation')->name('sendProductReceiveConfirmation');

Route::post('view-own-auction-payment-page', 'Web\FrontEnd\AuctionController@viewOwnAuctionPaymentPage')->name('viewOwnAuctionPaymentPage');
Route::post('live-charge-payment-by-btc', 'Web\FrontEnd\AuctionController@liveChargePaymentByBtc')->name('liveChargePaymentByBtc');

Route::post('load-auction-status-body', 'Web\FrontEnd\AuctionController@loadAuctionStatusBody')->name('loadAuctionStatusBody');






