<?php
use Illuminate\Support\Facades\Route;
Route::get('dashboard','Web\FrontEnd\ProfileController@dashboard')->name('userDashboard');

Route::get('add-auction-to-favourite/{auction_id?}', 'Web\FrontEnd\AuctionController@addAuctionToFavourite')->name('addAuctionToFavourite');

Route::get('my-profile', 'Web\FrontEnd\ProfileController@myProfile')->name('myProfile');

Route::get('my-won-item','Web\FrontEnd\ProfileController@myWonItems')->name('myWonItems');

Route::get('my-bids','Web\FrontEnd\ProfileController@myBids')->name('myBids');

Route::get('my-favourites','Web\FrontEnd\ProfileController@myFavourite')->name('myFavourite');

Route::get('my-notification','Web\FrontEnd\ProfileController@myNotification')->name('myNotification');

Route::get('user-profile-edit/{type}','Web\FrontEnd\ProfileController@editUserProfile')->name('editUserProfile');
Route::post('user-profile-picture-update','Web\FrontEnd\ProfileController@userProfilePictureUpdate')->name('userProfilePictureUpdate');
Route::post('user-profile-update','Web\FrontEnd\ProfileController@userProfileUpdate')->name('userProfileUpdate');
Route::post('user-password-update','Web\FrontEnd\ProfileController@userPasswordUpdate')->name('userPasswordUpdate');

Route::get('user-profile-update-view','Web\FrontEnd\ProfileController@getProfileUpdateView')->name('getProfileUpdateView');




