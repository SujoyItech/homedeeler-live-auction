function cropImage(file, props, callback) {
    let reader = new FileReader();
    reader.onload = function (e) {
        $("#cropper-image").attr("src", e.target.result);
    };
    reader.readAsDataURL(file);

    $("#imageCroperModal").modal("show");

    $("#imageCroperModal").on("shown.bs.modal", function (e) {
        setTimeout(initCropper(file, props, callback), 500);
    });
}

function initCropper(file, props, callback) {
    const image = $("#cropper-image");
    image.cropper({
        aspectRatio:
            props.hasOwnProperty("aspectRatio") === true
                ? props.aspectRatio
                : 1 / 1,
        autoCropArea:
            props.hasOwnProperty("autoCropArea") === true
                ? props.autoCropArea
                : 0.85,
        strict: props.hasOwnProperty("strict") === true ? props.strict : false,
        guides: props.hasOwnProperty("guides") === true ? props.guides : false,
        highlight:
            props.hasOwnProperty("highlight") === true
                ? props.highlight
                : false,
        dragCrop:
            props.hasOwnProperty("dragCrop") === true ? props.dragCrop : false,
        movable:
            props.hasOwnProperty("movable") === true ? props.movable : true,
        resizable:
            props.hasOwnProperty("resizable") === true ? props.resizable : true,
        mouseWheelZoom:
            props.hasOwnProperty("mouseWheelZoom") === true
                ? props.mouseWheelZoom
                : false,
        imageSmoothingEnabled: true,
        imageSmoothingQuality: "high",
    });
    $(document).on("click", "#move", function () {
        image.cropper("setDragMode", "move");
    });
    $(document).on("click", "#zoomIn", function () {
        image.cropper("zoom", 0.1);
    });
    $(document).on("click", "#zoomOut", function () {
        image.cropper("zoom", -0.1);
    });
    $(document).on("click", "#getCroppedCanvas", function () {
        const croppedImage = image.cropper("getCroppedCanvas", {
            height: 640,
            width: 640,
            fillColor: "#fff",
            imageSmoothingEnabled: true,
            imageSmoothingQuality: "high",
        });
        if (croppedImage) {
            const extension = file.name.split(".").pop();
            const newImage = croppedImage.toDataURL(extension);
            callback(newImage);
            $("#imageCroperModal").modal("hide");
        } else {
            callback(false);
        }
    });

    $("#imageCroperModal").on("hidden.bs.modal", function (e) {
        image.cropper("destroy");
    });
}
