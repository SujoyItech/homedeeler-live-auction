function callEcho(ws_port, key){
    Pusher.logToConsole = true;
    window.Echo = new Echo({
        broadcaster: 'pusher',
        wsHost: window.location.hostname,
        wsPort: ws_port,
        key: key,
        forceTLS: false,
        disableStats: true,
        enabledTransports: ['ws', 'wss']
    });
    Pusher.logToConsole = true;
}

function receiveNotification(auction_channel_name,event_name,callback){
    Echo.channel(auction_channel_name).listen('.'+event_name, (response) => {
        callback(response);
    });
}

function triggerNotification(channel_name,event_name,data,ws_port, key){
    let pusher = new Pusher(key, {
        wsHost: window.location.hostname,
        wsPort: ws_port,
        forceTLS: false,
        disableStats: true,
        enabledTransports: ['ws', 'wss']
    });
    let channel = pusher.subscribe(channel_name);
    channel.bind("pusher:subscription_succeeded", () => {
        var triggered = channel.trigger(event_name,data);
    });
}
