<div id="sellerPaymentModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content" id="sellerPaymentModalContent">
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        resetValidation('status-update-form')
        submitOperation(function (response){
            if (response.success == true){
                swalRedirect("{{Request::url()}}",response.message,'success');
            }else {
                swalError(response.message);
            }
        },'update-status')
    });
    $(document).on('change','.custom-file-input',function (){
        $('.custom-file-label').html(`<span class="text-success"><i class="fa fa-check"></i> Selected </span>`)
    })
</script>

