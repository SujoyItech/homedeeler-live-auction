<div class="card-box">
    @if (isset($product_images) && !empty($product_images[0]))
        <div class="row mb-3">
            <div class="col-lg-12">
                <div class="tab-content pt-0">
                    @foreach($product_images as $image_view)
                    <div class="tab-pane @if($image_view->is_featured == TRUE) active show @endif" id="product-{{$image_view->id}}-item">
                        <img src="{{check_storage_image_exists($image_view->media_url)}}" alt="" class="img-fluid mx-auto d-block rounded" style="min-height: 300px;">
                    </div>
                    @endforeach
                </div>

                <ul class="nav nav-pills nav-justified">
                    @foreach($product_images as $image_list)
                        <li class="nav-item">
                            <a href="#product-{{$image_list->id}}-item" data-toggle="tab" aria-expanded="false" class="nav-link product-thumb @if($image_list->is_featured == TRUE) active show @endif">
                                <img src="{{check_storage_image_exists($image_list->media_url)}}" alt="" class="img-fluid mx-auto" width="50">
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif

    <h4 class="card-title">{{$auction_details->name ?? ''}}</h4>
    <span><strong>{{__('Start date: ')}} </strong>{{$auction_details->start_date ?? ''}}</span><br>
    <span><strong>{{__('End date: ')}} </strong>{{$auction_details->end_date ?? ''}}</span><br>
    <span><strong>{{__('Base Price: ')}} </strong>{{$auction_details->base_price ?? ''}}</span><br>
    <span><strong>{{__('Bid Increments: ')}}</strong> {{$auction_details->bid_increment ?? ''}}</span><br>
    <span><strong>{{__('Highest Bid: ')}}</strong> {{$auction_details->highest_bid ?? ''}}</span><br>
    <span><strong>{{__('Live Enabled ? : ')}}</strong> {{$auction_details->has_live == true ? 'Yes': 'No'}}</span><br>
    <span><strong>{{__('Live Start Time : ')}}</strong> {{$auction_details->live_start_time}}</span><br>
    <span><strong>{{__('Live Duration : ')}}</strong> {{$auction_details->live_duration}}</span><br>
</div>

