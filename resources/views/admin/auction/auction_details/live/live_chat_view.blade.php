@if(isset($live_messages))
    @foreach($live_messages as $message)
        <li class="clearfix">
            <div class="chat-avatar">
                <img src="{{$message->user_image ?? ''}}" class="rounded" onerror='this.src="{{adminAsset('images/users/avatar.png')}}"'/>
            </div>
            <div class="conversation-text">
                <div class="ctext-wrap">
                    <i>{{$message->user_name}}</i>
                    <i><small>{{$message->created_at}}</small></i>
                    @if(isset($message->message))
                        <p class="mb-2">{{$message->message ?? ''}}</p>
                    @endif
                    @if(isset($message->message_image) && !empty($message->message_image))
                        <span><img src="{{$message->message_image}}" class="thumbnail" width="300"></span><br>
                    @endif
                </div>

            </div>
        </li>
    @endforeach
@endif
