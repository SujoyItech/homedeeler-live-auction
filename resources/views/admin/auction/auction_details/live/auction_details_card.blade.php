<div class="widget-rounded-circle card-box">
    <div class="row">
        <div class="col-md-2">
            <div class="widget-rounded-circle card-box">
                <div class="avatar-lg rounded-circle bg-soft-success">
                    <img src="{{check_storage_image_exists($product_images->media_url ?? '')}}" alt="" height="30" class="img-fluid">
                </div>
            </div>
        </div>
        <div class="col-md-10">
            <div class="widget-rounded-circle card-box">
                <span>{{__('Title')}} : {{$auction_details->name}}</span><br>
                <span>{{__('Base Price')}} : {{getMoney($auction_details->base_price)}}</span><br>
                <span>{{__('Bid Increment')}} : {{getMoney($auction_details->bid_increment)}}</span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="widget-rounded-circle card-box">
                <div class="text-left ml-2">
                    <span>{{__('Start Date')}} : {{$auction_details->start_date}}</span><br>
                    <span>{{__('End Date')}} : {{$auction_details->end_date}}</span>
                </div>
            </div>
        </div>
        <div class="col-md-6">
             <div class="widget-rounded-circle card-box">
                <div class="text-left ml-2">
                    <span>{{__('Live Start')}} : {{$auction_details->live_start_time}}</span><br>
                    <span>{{__('Live Duration')}} : {{$auction_details->live_duration}} mins</span><br>
                    @if (!empty($auction_details->winner_name))
                        <span>{{__('Winner Name')}} : {{$auction_details->winner_name}}</span>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
