<div class="card ajax-load">
    <div class="card-body py-2 px-3 border-bottom border-light">
        <div class="media py-1">
            <img src="{{getUserProfileImage()}}" class="mr-2 rounded-circle" height="36" onerror='this.src="{{adminAsset('images/users/avatar.png')}}"'>
            <div class="media-body">
                <h5 class="mt-0 mb-0 font-15">
                    <a href="#" class="text-reset">{{\Illuminate\Support\Facades\Auth::user()->name}}</a>
                </h5>
                <p class="mt-1 mb-0 text-muted font-12">
                    <small class="mdi mdi-circle text-success"></small> {{__('Online')}}
                </p>
            </div>
        </div>
    </div>
    <div class="card-body">
        <ul class="conversation-list custom-scrollbar-css" id="live_auction_message_list">

        </ul>

        <div class="row">
            <div class="col">
                <div class="mt-2 bg-light p-3 rounded">
                    <form method="post" action="{{route('sendAdminLiveAuctionReply')}}" id="send_message_id" class="send_message_class">
                        <div class="row">
                            <div class="col mb-2 mb-sm-0">
                                <input type="hidden" name="user_id" value="{{\Illuminate\Support\Facades\Auth::user()->id}}">
                                <input type="hidden" name="user_name" value="{{\Illuminate\Support\Facades\Auth::user()->name}}">
                                <input type="hidden" name="user_image" value="{{getUserAvatar(\Illuminate\Support\Facades\Auth::user())}}">
                                <input type="hidden" name="auction_id" value="{{$auction_details->id}}">
                                <input type="file" name="message_image" id="message-pic" class="d-none">
                                <input type="text" class="form-control border-0 reply_message" placeholder="Enter your text" name="message" id="message">
                                <div class="invalid-feedback">
                                    {{__('Please enter your messsage')}}
                                </div>
                            </div>
                            <div class="col-sm-auto">
                                <label for="message-pic" class="btn btn-light btn-sm message_files_label"><i class="fe-paperclip"></i></label>
                                <button type="submit" class="btn btn-dark send_live_reply"><i class='fe-send'></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function (){
        submitOperation(sendLiveAuctionReply,'send_live_reply');
    })

    function sendLiveAuctionReply(response){
        $('.reply_message').val('');
        $('#message-pic').val('');
        $('.message_files_label').html(`<i class="fe-paperclip"></i>`);
        $('.message_files_label').removeClass('text-success');
    }

    $('#message-pic').on('change',function (){
        let file = $(this)[0].files;
        if (file.length > 0){
            $('.message_files_label').html(`<i class="fa fa-check"></i>`);
            $('.message_files_label').removeClass('text-success');
        }
    });

</script>

@push('custom-script')
    <script src="{{adminAsset('js/notification.js')}}"></script>
    <script>
        $(document).ready(function (){
            const live_auction_message_channel_name = 'live_auction_message_'+"{{$auction_details->id ?? ''}}"
            receiveNotification(live_auction_message_channel_name,'auction_message',function (response){
                const body = response.body;
                const user_image = body.user_image;
                const user_name = body.user_name;
                const message = body.hasOwnProperty('message') ? body.message : '';
                const message_image = body.hasOwnProperty('message_image') ? body.message_image : '';
                const time = body.created_at;
                let chat_body = prepareAdminLiveChat(user_image,user_name,message,message_image,time);
                $('.conversation-list').append(chat_body);
                scrollSmoothToBottom('live_auction_message_list');
            });
            loadLiveAuctionMessage();
        });

        function loadLiveAuctionMessage(){
            let data = {
                auction_id : "{{$auction_details->id}}"
            };
            makeAjaxPostText(data,"{{route('loadAdminLiveAuctionMessage')}}",null).done(function (response){
                $('.conversation-list').html(response);
                scrollSmoothToBottom('live_auction_message_list');
            })
        }

        function prepareAdminLiveChat(user_image,user_name,message,message_image,time){
            const chat_image = message_image.length > 0 ? `<span><img src="${message_image}" class="img-thumbnail" width="300"></span><br>` : ``;

            const chat_view = `<li class="clearfix">
                                    <div class="chat-avatar">
                                        <img src="${user_image}" class="rounded" onerror='this.src="{{adminAsset('images/users/avatar.png')}}"'/>
                                    </div>
                                    <div class="conversation-text">
                                        <div class="ctext-wrap">
                                            <i>${user_name}</i>
                                            <i><small>${time}</small></i>
                                            <p class="mb-2">${message}</p>
                                            ${chat_image}
                                        </div>
                                    </div>
                               </li>`;
            return chat_view;
        }

        function scrollSmoothToBottom (id) {
            let div = document.getElementById(id);
            $('#' + id).animate({
                scrollTop: div.scrollHeight
            }, 500);
        }
    </script>
@endpush

