<div class="card">
    <div class="card-body">
        <h4 class="header-title mb-3">{{__('Track Order')}}</h4>
        <div class="track-order-list">
            <ul class="list-unstyled">
                @if (isset($auction_status_histories) && !empty($auction_status_histories[0]))
                    @foreach ( $auction_status_histories as $auction_history)
                        <li class="completed">
                            <h5 class="mt-0 mb-1">{{AUCTION_STATUS[$auction_history->status]}}</h5>
                            <h6 class="mt-0 mb-1">{{$auction_history->message}}</h6>
                            <p class="text-muted">{{\Carbon\Carbon::parse($auction_history->created_at)->format('F jS, Y')}} <small class="text-muted">{{\Carbon\Carbon::parse($auction_history->created_at)->format('g:i A')}}</small> </p>
                        </li>
                    @endforeach

                @endif
            </ul>
        </div>
    </div>
</div>
