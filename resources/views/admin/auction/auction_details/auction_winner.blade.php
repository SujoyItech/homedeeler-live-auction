<div class="card">
    <div class="card-body">
        <h4 class="header-title mb-3">{{__('Auction Winner')}}</h4>
        <div class="row">
            <div class="col-md-6">
                <h5>{{__('Name')}}: {{$winner->name ?? ''}}</h5>
                <h5>{{__('Email')}}: {{$winner->email ?? ''}}</h5>
                <h5>{{__('Phone')}}: {{$winner->phone ?? ''}}</h5>
            </div>
            <div class="col-md-6">
                <h5>{{__('Country')}}: {{$winner->country ?? ''}}</h5>
                <h5>{{__('City')}}: {{$winner->city ?? ''}}</h5>
                <h5>{{__('Address')}}: {{$winner->address ?? ''}}</h5>
            </div>
        </div>

    </div>
</div>
