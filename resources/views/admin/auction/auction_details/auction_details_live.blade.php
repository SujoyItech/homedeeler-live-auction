@extends('admin.layouts.app',['menu'=>'auctions','sub_menu'=>'auctions'])
@section('content')
    <div class="col-md-6 col-xl-6 col-md-6">
        @include('admin.auction.auction_details.live.auction_details_card')
    </div>
    <div class="col-md-6 col-xl-6 col-md-6">
        @include('admin.auction.auction_details.status_history')
    </div>
    <div class="col-lg-6 col-xl-6 col-md-6 col-sm-12 col-xs-12">
        @include('admin.auction.auction_details.live.live_messages')
    </div>
    <div class="col-lg-6 col-xl-6 col-md-6 col-sm-12 col-xs-12">
        @include('admin.auction.auction_details.bidder_list')
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.dropify').dropify();
            var data_url = "{{route('auctionDetails',['slug'=>$auction_details->slug ?? ''])}}";
            var data_column =  [
                {"data": "created_at", visible: false},
                {"data": "icon",orderable: false, searchable: false},
                {"data": "bidder_name"},
                {"data": "bid_price"},
                {"data": "created_at"}
            ];
            renderDataTable($('#bidding_table'),data_url,data_column);
            const live_bid_channel_name = 'live_auction_message_'+"{{$auction_details->id ?? ''}}"
            receiveNotification(live_bid_channel_name,'bid_event',function (response){
               renderDataTable($('#bidding_table'),data_url,data_column);
            });
        });
    </script>
    @stack('custom-script')
@endsection

