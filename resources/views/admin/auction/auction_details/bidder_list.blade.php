<div class="card-box">
    <h4 class="header-title">{{__('Bidding List')}}</h4>
    <p class="sub-header">
        {{__('Here goes the bidding list')}}
    </p>

    <div class="table-responsive">
        <table id="bidding_table" class="table table-sm dt-responsive nowrap w-100">
            <thead>
            <tr>
                <th></th>
                <th>{{__('Image')}}</th>
                <th>{{__('Name')}}</th>
                <th>{{__('Price')}}</th>
                <th>{{__('Time')}}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
