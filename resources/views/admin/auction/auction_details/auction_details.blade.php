@extends('admin.layouts.app',['menu'=>'auctions','sub_menu'=>'auctions'])
@section('content')
    <div class="col-lg-4 col-xl-4 col-md-4 col-sm-12 col-xs-12 add_edit">
        @include('admin.auction.auction_details.auction_details_show')
    </div>
    <div class="col-lg-8 col-xl-8 col-md-8 col-sm-12 col-xs-12 list">
        @if (isset($winner))
            @include('admin.auction.auction_details.auction_winner')
        @endif
        @include('admin.auction.auction_details.status_history')
        @include('admin.auction.auction_details.bidder_list')
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.dropify').dropify();
            var data_url = "{{route('auctionDetails',['slug'=>$auction_details->slug ?? ''])}}";
            var data_column =  [
                {"data": "created_at", visible: false},
                {"data": "icon",orderable: false, searchable: false},
                {"data": "bidder_name"},
                {"data": "bid_price"},
                {"data": "created_at"}
            ];
            renderDataTable($('#bidding_table'),data_url,data_column);
        });
    </script>
@endsection

