@extends('admin.layouts.app',['menu'=>'auctions','sub_menu'=>'auctions'])
@section('style')
    <link href="{{adminAsset('libs/summernote/summernote-bs4.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{adminAsset('libs/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{adminAsset('multiselect/tagsinput.css')}}" rel="stylesheet" type="text/css">
    <link href="{{adminAsset('libs/mohithg-switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="col-12">
        <div class="row">
            <div class="col-8 text-left">
                <h3><b>{{__('Auction information of ')}}{{$auction->name}} ({{$auction->slug}})</b></h3>
            </div>
        </div>
    </div><br>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
        @include('admin.auction.add_product.general_info')
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
        @include('admin.auction.add_product.combinations')
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
        @include('admin.auction.add_product.auction_price_info')
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
        @include('admin.auction.add_product.categories_tag')
    </div>

@endsection
@section('script')
    {{--    for gallery--}}
    <script src="{{adminAsset('libs/summernote/summernote-bs4.min.js')}}"></script>=
    <script src="{{adminAsset('libs/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{adminAsset('js/pages/gallery.init.js')}}"></script>
    <script src="{{adminAsset('libs/mohithg-switchery/switchery.min.js')}}"></script>
    <script src="{{adminAsset('libs/multiselect/js/jquery.multi-select.js')}}"></script>
    <script src="{{adminAsset('multiselect/tagsinput.js')}}"></script>


    {{-----------------------------------------}}

    <script>
        submitOperation(submitRedirectResponse, 'submit_auction_info');
        submitOperation(submitResponse, 'submit-general');
        submitOperation(submitResponse, 'submit-category');
        submitOperation(submitResponse, 'submit-pricing');
        submitOperation(submitRedirectResponse, 'submit-auction-pricing');
        function submitResponse(response, this_form){
            if (response.success === true) {
                swalSuccess(response.message);
                this_form.removeClass('was-validated');
            } else {
                swalError(response.message);
            }
        }
        function submitRedirectResponse(response, this_form){
            if (response.success === true) {
                swalRedirect("{{route('editAuction',['slug'=>$auction->slug])}}",response.message,'success');
            } else {
                swalError(response.message);
            }
        }
    </script>
    @stack('custom-script')
@endsection

