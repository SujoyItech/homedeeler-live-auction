@include('admin.auction.add_product.combination.media_modal')
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md-12">
                <h4>{{__('Colors and images combination')}}</h4>
            </div>
        </div>
    </div>
    <div class="card-body">
        @if (isset($combinationTypes[0]) && !empty($combinationTypes[0]))
            <form novalidate method="post" id="combination-form-{{$combinationTypes[0]->id}}"
                  action="{{route('saveProductCombinations')}}"
                  class="combination-form mt-2">
                <input type="hidden" name="product_id" value="{{$auction->product_id ?? ''}}">
                <input type="hidden" name="combination_type_id" value="1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <h5 class="card-title">{{$combinationTypes[0]->name}}</h5>
                            <select class="selectpicker" multiple="true" name="combination_ids[]">
                                @foreach($combinations as $comb)
                                    @if($comb->combination_type_id == 1)
                                        <option
                                            @foreach($productCombinations as $prodComb)
                                            @if($prodComb->combination_id == $comb->id)
                                            selected
                                            @endif
                                            @endforeach
                                            value="{{$comb->id}}">{{$comb->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-dark save-combination mt-2"  data-style="zoom-in"><i class="fa fa-save"></i> {{__('Save')}}</button>
                    </div>
                </div>
            </form>
            <hr>

            <div class="row mb-3" id="color-combination-div">
                @include('admin.auction.add_product.combination.media_combinations')
            </div>
            <div class="row mb-3">
                <div class="col-md-12 color_modal">
                    <div class="text-left">
                        <a href="javascript:" onclick="showMediaModal('','')" class="btn btn-secondary">
                            <icon class="fa fa-plus"></icon> {{__('Add more media')}}</a>
                    </div>
                </div>
            </div>
        @endif

    </div>
</div>

<script>
    $(document).ready(function () {
        resetValidation('combination-form');
        submitOperation(submitResponse, 'save-combination');
        resetValidation('product-media-form');
        submitOperation(submitResponse, 'submit-media');

        function submitResponse(response, this_form) {
            if (response.success === true) {
                swalSuccess(response.message);
                if (this_form !== undefined) {
                    this_form.removeClass('was-validated');
                }
                if (response.data.html) {
                    $("#color-combination-div").html(response.data.html);
                    $("#media-modal").modal("hide");
                }
            } else {
                swalError(response.message);
            }
        }



        tippy(".tippy");
        $(document).on('change', '[name="product_combination_id"]', function () {
            let data = {product_combination_id: $(this).val(), product_id: $(this).data('product_id')};
            let save_url = '{{route('saveProductFeaturedMedia')}}';
            makeAjaxPost(data, save_url).done(function (response) {
                submitResponse(response);
            });
        })
    });

    function showMediaModal(productCombination, combinationName) {
        //cleanup
        $(".clean-input").val('');
        clearDropifyWithSelector("#image");
        $("#image").attr("data-default-file", "");
        $("#image").attr("required", "");
        $("#media_type").val(productCombination.media_type).change();

        if (productCombination) {
            if (combinationName) {
                $("#comb-head").text('(' + combinationName + ')');
            } else {
                $("#comb-head").text('(' + '{{$auction->name ?? ''}}' + ')');
            }

            $("#p-c-id-in").val(productCombination.id);
            let media_type = productCombination.media_type;

            if (media_type === '{{INTERNAL_IMAGE}}') {
                $("#url-div").hide();
                $("#media_url").attr("disabled", "");
                let imagePath = '{{\Illuminate\Support\Facades\Storage::url(':url')}}'.replace(':url', productCombination.media_url);
                reloadUploader(imagePath);

            } else if (media_type === null) {
                $("#uploader-div").show();
                let imageElem = $("#image");
                $("#media_type").val('{{INTERNAL_IMAGE}}').change();
                imageElem.removeAttr("disabled");

                $("#url-div").hide();
                $("#media_url").attr("disabled", "");

            } else {
                $("#uploader-div").hide();
                let imageElem = $("#image");
                imageElem.attr("disabled", "");

                let mediaUrlElem = $("#media_url");
                mediaUrlElem.removeAttr("disabled");
                mediaUrlElem.val(productCombination.media_url);
                $("#url-div").show();
            }
        } else {
            $("#comb-head").text('(' + '{{$auction->name ?? ''}}' + ')');

            $("#p-c-id-in").val("");
            $("#url-div").hide();
            $("#uploader-div").show();
            let imageElem = $("#image");
            $("#media_type").val('{{INTERNAL_IMAGE}}').change();
            imageElem.removeAttr("disabled");
        }
        $("#media-modal").modal("show");
    }

    function reloadUploader($filePath = '') {
        $("#uploader-div .dropify-wrapper").remove();
        let prevImageElem = $("#image");
        prevImageElem.remove();

        let uploaderDiv = $("#uploader-div");
        uploaderDiv.append(`<input type="file" name="image" class="clean-input" id="image"
                                       data-default-file="">`);

        let imageElem = $("#image");
        imageElem.attr("data-default-file", $filePath);
        imageElem.dropify();
        uploaderDiv.show();
    }

</script>
