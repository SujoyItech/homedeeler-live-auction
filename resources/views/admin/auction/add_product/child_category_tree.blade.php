@foreach($sub_categories as $sub_category)
    @if(isset($sub_category['children']) && !empty($sub_category['children'][0]))
        <option @if(in_array($sub_category['id'],$productCategories)) selected @endif value="{{$sub_category['id']}}" style="margin-left: {{$tree_number*20}}px" class="product-category-tree">{{$sub_category['name']['en']}}</option>
        @include('admin.auction.add_product.child_category_tree',['sub_categories'=>$sub_category['children'],'tree_number' => $tree_number+1])
    @else
        <option @if(in_array($sub_category['id'],$productCategories)) selected @endif value="{{$sub_category['id']}}" style="margin-left: {{$tree_number*20}}px" class="product-category-tree">{{$sub_category['name']['en']}}</option>
    @endif
@endforeach
