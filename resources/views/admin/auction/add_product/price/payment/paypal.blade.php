<form class="paypal_payment_class" id="paypal_payment_id" action="{{ route('liveChargePaymentByPaypal') }}" method="post">
    @csrf
    <div class="card-box">
        <div class="">
            <div class="row">
                <input type="hidden" name="auction_id" class="auction_id_payment" value="{{$auction_id}}">
                <input type="hidden" name="live_start_time" class="live_start_time_payment" value="{{$live_start_time}}">
                <input type="hidden" name="live_duration" class="live_duration_payment" value="{{$live_duration}}">
                <input type="hidden" name="payment_type" value="{{PAYMENT_TYPE_LIVE_CHARGE}}">
                <div class="col-12 text-center">
                    <button class="btn btn-primary btn-block submit_paypal_payment" type="submit"><i class="fab fa-paypal"></i> {{__('Pay With Paypal')}}</button>
                </div>
            </div>
        </div>
    </div>
</form>

