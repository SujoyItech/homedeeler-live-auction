<div class="text-center">
    <input type="hidden" name="auction_id" class="auction_id_payment" value="{{$auction_id}}">
    <input type="hidden" name="live_start_time" class="live_start_time_payment" value="{{$live_start_time}}">
    <input type="hidden" name="live_duration" class="live_duration_payment" value="{{$live_duration}}">
    <input type="hidden" name="payment_type" value="{{PAYMENT_TYPE_LIVE_CHARGE}}" class="payment_type">
    <div class="ajax-load"></div>
    <div id="dropin-container"></div>
    <button id="submit-button" class="btn btn-info btn-sm">{{__('Request payment method')}}</button>
</div>
<script>
    $(document).ready(async function (){
        await brainTreeOperationCall();
    });

    async function brainTreeOperationCall() {
        $.getScript("https://js.braintreegateway.com/web/dropin/1.8.1/js/dropin.min.js", function() {
            let button = document.querySelector('#submit-button');
            braintree.dropin.create({
                authorization: "{{Braintree\ClientToken::generate()}}",
                container: '#dropin-container'
            }, function (createErr, instance) {
                button.addEventListener('click', function () {
                    $('#loadingBuffer').removeClass('d-none');
                    instance.requestPaymentMethod(function (err, payload) {
                        const submit_url = "{{route('liveChargePaymentByBrainTree')}}";
                        const formData = {
                            auction_id : $('.auction_id_payment').val(),
                            live_start_time : $('.live_start_time_payment').val(),
                            live_duration : $('.live_duration_payment').val(),
                            payment_type : $('.payment_type').val(),
                            payload : payload
                        };
                        $('#submit-button').addClass('d-none');
                        makeAjaxPost(formData,submit_url,null).done(function (response){
                            if (response.success) {
                                swalRedirect("{{route('editAuction',['slug'=>$auction->slug])}}",response.message,'success')
                            } else {
                                swalError(response.message);
                            }
                        });
                    });
                });

            });
        });
    }
</script>
