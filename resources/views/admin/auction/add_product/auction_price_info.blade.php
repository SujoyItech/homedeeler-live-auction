<link href="{{adminAsset('libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
<div class="card">
    <div class="card-header">
        <h4>{{__('Auction bidding information')}}</h4>
    </div>
    <div class="card-body">
        <form novalidate action="{{route('saveAuctionPricingInfo')}}" method="post" id="auction-pricing-form-id" class="auction-pricing-form-class">
            <input type="hidden" name="id" id="auction_id" value="{{$auction->id ?? ''}}">
            <div class="form-group">
                <label for="base_price">{{__('Starting Bid Price')}}</label>
                <input type="number" class="form-control" name="base_price" id=base_pricename" placeholder="{{__('Starting Bid Price')}}" value="{{$auction->base_price ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group">
                <label for="bid_increment">{{__('Bidding Price Increment')}}</label>
                <input type="number" class="form-control" name="bid_increment" id="bid_increment" placeholder="{{__('Bid Increment')}}" value="{{$auction->bid_increment ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>

            <div class="form-group">
                <h5 class="card-title">{{__('Offline Auction Duration')}}</h5>
                <div class="input-group">
                    <input type="text" class="form-control" name="start_date" id="start_date" placeholder="{{__('Start Date')}}" value="{{$auction->start_date ?? ''}}" required>
                    <input type="text" class="form-control" name="end_date" id="end_date" disabled placeholder="{{__('End Date')}}" value="{{$auction->end_date ?? ''}}" required>
                </div>
                <span class="text-danger date_error d-none"></span>
            </div>

            <button class="btn btn-dark submit-auction-pricing" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
        </form>
        <hr>
        <div class="form-group">
            <h5 class="card-title">{{__('Live Auction')}} &nbsp;&nbsp;&nbsp;
                <input type="checkbox" @if($auction->has_live === ACTIVE) checked @endif
                data-plugin="switchery" name="has_live" id="has_live" data-id="{{$auction->id ?? ''}}"
                       data-live-charge="{{$auction->auction_payment->is_live_charge_paid ?? ''}}" data-color="#1bb99a" @if ($auction->start_date == '' || $auction->end_date == '')
                           disabled
                       @endif/></h5>
        </div>
        <div class="live-auction @if($auction->has_live === INACTIVE) d-none @endif">
            <div class="form-group">
                <label for="live_start_time">{{__('Live Start Time')}}</label>
                <input type="text" class="form-control" name="live_start_time" id="live_start_time" readonly
                    @if (isset($auction->auction_payment->is_live_charge_paid) && $auction->auction_payment->is_live_charge_paid == TRUE)
                        value="{{$auction->live_start_time ?? ''}}"
                    @else
                        value="{{$auction->end_date ?? ''}}"
                    @endif >
                <span class="text-danger d-none" id="live_start_time_error">{{__('Live start time field can\'t be empty.')}}</span>
            </div>
            <div class="form-group">
                <label for="live_duration">{{__('Live Duration')}}</label>
                <select class="form-control live_duration" name="live_duration"
                    @if (isset($auction->auction_payment->is_live_charge_paid) && $auction->auction_payment->is_live_charge_paid == TRUE)
                        disabled
                    @endif >
                    @foreach(live_duration() as $key => $value)
                        <option value="{{$key}}" {{is_selected($key,$auction->live_duration ?? '')}}>{{$value}}</option>
                    @endforeach
                </select>
            </div>
            @if (isset($auction->auction_payment->is_live_charge_paid) && $auction->auction_payment->is_live_charge_paid == true)
                <span class="badge badge-success">{{__('Payment Completed')}}</span>
            @else
                <span class="badge badge-warning mb-2">{{__('Complete your payment first to active this.')}}</span><br>
                <button class="btn btn-danger" data-style="zoom-in" type="button" id="makePayment"><i class="fa fa-money-check"></i> {{__('Make Payment to Activate Live')}}</button>
            @endif
        </div>
    </div>
</div>

@include('admin.auction.add_product.price.add_payment_modal')


<script src="{{adminAsset('libs/flatpickr/flatpickr.min.js')}}"></script>
<script>
    $(document).ready(function () {
        resetValidation('auction-pricing-form-class');
       flatpickr('#start_date',{
           enableTime: true,
           dateFormat: "Y-m-d H:i",
       });
       flatpickr('#end_date',{
           enableTime: true,
           dateFormat: "Y-m-d H:i",
       });

       $('#start_date').on('change',function (){
           if ($(this).val.length > 0){
               $('#end_date').prop('disabled',false);
           }
       });
        $('#end_date').on('change',function (){
           if ($(this).val.length > 0){
               const start_date = $('#start_date').val();
               const end_date = $('#end_date').val();
              if (start_date.length == 0){
                  $('.date_error').removeClass('d-none').html('Start date can\'t be empty.');
              }else if (start_date > end_date){
                  $('.date_error').removeClass('d-none').html('End date must be greater than or equal to start date.');
                  $('#end_date').val('');
              }else {
                  $('.date_error').addClass('d-none');
              }
           }
       })

        $(document).on('click','#makePayment',function (){
            Ladda.bind(this);
            let load = $(this).ladda();
            let live_start_time = $('#live_start_time').val();
            console.log(live_start_time);
            let live_duration = $('.live_duration').val();
            let auction_id = $('#auction_id').val();
            if (live_start_time.length == 0){
                $('#live_start_time_error').removeClass('d-none');
            }else {
                const auction_data = {
                    auction_id : auction_id,
                    live_start_time : live_start_time,
                    live_duration : live_duration,
                }
                makeAjaxPostText(auction_data,"{{route('viewLiveAuctionPaymentPage')}}",load).done(function (response){
                    $('#add_payment_body').html(response);
                    $('#addPaymentModal').modal('show');
                    load.ladda('stop');
                })

            }
        });

    });
    $(document).on('change','#has_live',function (){
        let url = "{{route('makeAuctionLive')}}";
        let data = {
            id : $(this).data('id')
        }
        if ($(this).prop('checked') == true){
            if ($(this).data('live-charge') == true){
                data.type = 'enable';
                makeAjaxPost(data,"{{route('makeAuctionLive')}}").done(function (response){
                    if (response.success == true){
                        swalSuccess(response.message);
                        $('.live-auction').removeClass('d-none');
                    }else {
                        swalError(response.message);
                    }
                });
            }else {
                $('.live-auction').removeClass('d-none');
            }

        }else {
            if ($(this).data('live-charge') == true){
                data.type = 'disable';
                makeAjaxPost(data,"{{route('makeAuctionLive')}}").done(function (response){
                    if (response.success == true){
                        swalSuccess(response.message);
                        $('.live-auction').addClass('d-none');
                    }else {
                        swalError(response.message);
                    }
                });
            }else {
                $('.live-auction').addClass('d-none');
            }
        }
    });
</script>
