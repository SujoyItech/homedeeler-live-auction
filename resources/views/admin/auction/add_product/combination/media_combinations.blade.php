@foreach($productCombinations as $prodComb)
    @if($prodComb->media_type or $prodComb->combination_type_id == COLOR_ID)
        <div class="col-md-4">
            <div class="text-center tippy" data-plugin="tippy"
                 data-tippy-followCursor="true"
                 data-tippy-arrow="true"
                 data-tippy-placement=""
                 data-tippy-animation="fade"
                 data-tippy-theme="gradient"
                 title="{{__('Click to edit')}}">
                <img onclick="showMediaModal({{$prodComb}}, '{!! isset($prodComb->combination->name) ? $prodComb->combination->name : '' !!}')"
                    class="avatar-lg zoom mb-1" src="{{get_product_combination_thumbnail_and_media_url($prodComb)['thumbnail']}}"
                     onerror='this.src="{{adminAsset('images/no-image.png')}}"'>
                <br/>
                <span>{!! !empty($prodComb->combination->name) ? $prodComb->combination->name : $prodComb->product->name !!}</span>
                <div class="radio mb-3 ml-3">
                    <input type="radio" name="product_combination_id" data-product_id="{{$prodComb->product_id}}" id="{{$prodComb->id}}"
                           value="{{$prodComb->id}}" {{$prodComb->is_featured == ACTIVE ? 'checked' : ''}}>
                    <label for="{{$prodComb->id}}"> {{__('Featured')}} </label>
                </div>
            </div>
        </div>
    @endif
@endforeach
