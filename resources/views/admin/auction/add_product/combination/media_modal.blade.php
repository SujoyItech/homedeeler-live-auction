@php($settings = __options(['application_settings']))
<link href="{{adminAsset('libs/cropper/cropper.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{adminAsset('css/cropper_style.css')}}" rel="stylesheet" type="text/css" />

<div id="media-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel"
     aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content p-3">
            <div class="modal-header">
                <h4 class="modal-title" id="standard-modalLabel">{{__('Media manager')}} <span id="comb-head"></span></h4>
                <button type="button" class="close" onclick="clearFormValidation()" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form novalidate method="post" id="product-media-form" action="{{route('saveProductMedia')}}"
                      class="product-media-form">
                    <input type="hidden" name="product_id" value="{{$auction->product_id ?? ''}}">
                    {{--    <input type="hidden" id="c-t-id-in" name="combination_type_id" value="">--}}
                    {{--    <input type="hidden" id="c-id-in" name="combination_id" value="">--}}
                    <input type="hidden" id="p-c-id-in" name="id" value="">
                    <div class="form-group" id="media-type-div">
                        <h5 class="card-title">{{__('Select Media type')}}</h5>
                        <select class="form-control" id="media_type" name="media_type">
                            <option value="">--{{__('Select')}}--</option>
                            @foreach(get_product_media_type() as $key => $value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                        <div class="valid-feedback">
                            {{__('Looks good!')}}
                        </div>
                    </div>
                    <div class="form-group" id="url-div">
                        <h5 class="card-title">{{__('Enter Url')}}</h5>
                        <input type="text" id="media_url" name="media_url" class="clean-input form-control" placeholder="">
                        <div class="valid-feedback">
                            {{__('Looks good!')}}
                        </div>
                    </div>
                    <div class="form-group" id="uploader-div">
                        <label class="col-form-label"
                        for="image">{{__('Upload an image. Max size: ')}}
                            {{ $settings->product_image_max_size_in_mb ?? 1}}
                            {{__(' MB and Types: jpg, jpeg, png (required)')}}</label>
                        <div class="valid-feedback">
                            {{__('Looks good!')}}
                        </div>
                        <input type="hidden" id="image_url" name="image_url" value="">
                        <input type="file" name="image" class="dropify clean-input" id="image"
                               data-default-file=""
                               data-max-file-size="{{ $settings->product_image_max_size_in_mb ?? 1}}M" >
                    </div>
                    <button type="submit" class="btn btn-dark submit-media"  data-style="zoom-in">Save</button>
                </form>
            </div>

        </div>
    </div>
</div>
@include('admin.cropper_modal')

<script>
    $(document).ready(function (){
        $(document).on('change','#media_type',function (){
            clearFormValidation();
            let media_type = $(this).val();
            if(media_type === '{{INTERNAL_IMAGE}}') {
                $("#url-div").hide();
                $("#media_url").attr("disabled","");

                let imageElem = $("#image");
                imageElem.removeAttr("disabled");
                //imageElem.dropify();
                $("#uploader-div").show();

            } else if (media_type === "") {
                $("#uploader-div").hide();
                let imageElem = $("#image");
                imageElem.attr("disabled","");

                $("#url-div").hide();
                $("#media_url").attr("disabled","");

            } else {
                $("#uploader-div").hide();
                let imageElem = $("#image");
                imageElem.attr("disabled","");

                let mediaUrlElem = $("#media_url");
                mediaUrlElem.removeAttr("disabled");
                $("#url-div").show();
            }
        });
    });

    function clearFormValidation() {
        $("#product-media-form").removeClass('was-validated');
        $('.invalid-feedback').remove();
        $("#product-media-form select").removeClass("is-invalid");
        $("#product-media-form input").removeClass("is-invalid");
    }
</script>
@push('custom-script')
    <script src="{{adminAsset('libs/cropper/cropper.min.js')}}"></script>
    <script src="{{adminAsset('js/image_upload.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('#image').removeAttr('required');
            $(document).on("change", "#image", function (event) {
                if (event.target.files && event.target.files[0]) {
                    const props = {
                        aspectRatio : 1 / 1,
                        maxHeight : 600,
                        maxWidth : 300,
                    }
                    cropImage(event.target.files[0],props,(image)=>{
                        $('#image_url').val(image);
                        resetPreview('image',image,'Image.jpg');
                    });
                } else {
                    console.log("Image upload failed.");
                }
            });
        });

        function resetPreview(name, src, fname=''){
            let input 	 = $('input[name="'+name+'"]');
            let wrapper  = input.closest('.dropify-wrapper');
            let preview  = wrapper.find('.dropify-preview');
            let filename = wrapper.find('.dropify-filename-inner');
            let render 	 = wrapper.find('.dropify-render').html('');

            input.val('').attr('title', fname);
            wrapper.removeClass('has-error').addClass('has-preview');
            filename.html(fname);

            render.append($('<img />').attr('src', src).css('max-height', input.data('height') || ''));
            preview.fadeIn();
        }

    </script>
@endpush

