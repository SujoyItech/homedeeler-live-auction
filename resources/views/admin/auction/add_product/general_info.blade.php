
<form novalidate class="product-general-form" method="post" action="{{route('saveProduct')}}" id="product-general-form">
    <input type="hidden" name="id" value="{{$auction->product_id ?? ''}}">
    <input type="hidden" name="auction_id" value="{{$auction->id ?? ''}}">
    <div class="card">
        <div class="card-header">
            <h4>{{__('General information')}}</h4>
        </div>
        <div class="card-body">
            <div class="form-group">
                <h5 class="card-title">{{__('Name (required)')}}</h5>
                <input type="text" id="name" name="name" class="form-control" value="{{$auction->name ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>

            <div class="form-group">
                <h5 class="card-title">{{__('Estimated Price range')}}</h5>
                <div class="input-group">
                    <input type="number" id="price_range_from" name="price_range_from" class="form-control" placeholder="{{__('Start')}}" value="{{$auction->price_range_from ?? ''}}">
                    <input type="number" id="price_range_to" name="price_range_to" class="form-control" placeholder="{{__('End')}}" value="{{$auction->price_range_to ?? ''}}">
                </div>
            </div>
            <div class="form-group">
                <h5 class="card-title">{{__('Service charge')}} (%)</h5>
                <input type="number" id="service_charge" name="service_charge" class="form-control" placeholder="{{__('Service charge')}}" value="{{$auction->service_charge ?? ''}}">
            </div>
            <div class="row mb-2">
                <div class="col-md-12">
                    <button type="button" class="btn btn-sm btn-outline-dark mb-1" id="description_modal_button"><i class="fa fa-file"></i> {{__('Description')}}</button>
                </div>
                <div class="col-md-12">
                    <button type="button" class="btn btn-sm btn-outline-dark mb-1" id="shipping_payment_modal_button"><i class="fa fa-file"></i> {{__('Shipping & Payment')}}</button>
                </div>
                <div class="col-md-12">
                    <button type="button" class="btn btn-sm btn-outline-dark mb-1" id="terms_condition_modal_button"><i class="fa fa-file"></i> {{__('Terms Conditions')}}</button>
                </div>
            </div>

            <div class="form-group">
                <h5 class="card-title">{{__('Is New ?')}} &nbsp;&nbsp;&nbsp;<input type="checkbox" @if (isset($auction)) @if($auction->is_new === ACTIVE) checked @endif @else checked @endif data-plugin="switchery" name="is_new" id="is_new" data-color="#1bb99a"/></h5>
            </div>
            <div class="form-group">
                <h5 class="card-title">{{__('Is Featured ?')}} &nbsp;&nbsp;&nbsp;<input type="checkbox" @if (isset($auction)) @if($auction->is_featured === ACTIVE) checked @endif @else checked @endif data-plugin="switchery" name="is_featured" id="is_featured" data-color="#1bb99a"/></h5>
            </div>
            {{-- <div class="form-group">
                <h5 class="card-title">{{__('Status')}} &nbsp;&nbsp;&nbsp;<input type="checkbox" @if($auction->status === ACTIVE) checked @endif data-plugin="switchery" name="status" id="status" data-color="#1bb99a"/></h5>
            </div> --}}
            <button class="btn btn-dark submit-general" type="submit" data-style="zoom-in"><i class="fa fa-save"></i> {{__('Save')}}</button>
        </div>
    </div>
</form>
<input type="hidden" name="is_live_charge_paid" id="is_live_charge_paid" value="{{$auction->auction_payment->is_live_charge_paid ?? ''}}">
@include('admin.auction.modals.description')
@include('admin.auction.modals.shipping_payments')
@include('admin.auction.modals.terms_conditions')

<script>
    $(document).ready(function (){
        resetValidation('product-general-form');
    });
    $(document).on('click','#description_modal_button',function (){
        $('#description-modal').modal('show');
    });
    $(document).on('click','#shipping_payment_modal_button',function (){
        $('#shipping_payment-modal').modal('show');
    });
    $(document).on('click','#terms_condition_modal_button',function (){
        $('#terms_condition-modal').modal('show');
    });
</script>

