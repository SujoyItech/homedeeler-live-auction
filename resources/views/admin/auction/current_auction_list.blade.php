@extends('admin.layouts.app',['menu'=>'auction','sub_menu'=>'currentAuctionList'])
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h4><i class="fas fa-boxes"></i> {{__('Current Auction List')}}</h4>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form method="post" id="search_auction_form">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="reference">{{__('Reference')}}</label>
                            <input type="text" name="slug" class="form-control">
                        </div>
                        <div class="form-group col-md-3">
                            <label>{{__('Name/Description')}}</label>
                            <input type="text" name="name_description" placeholder="" class="form-control">
                        </div>
                        <div class="form-group col-md-2">
                            <label>{{__('Special cases')}}</label>
                            <select name="special_case" class="form-control">
                                <option value="">{{__('none')}}</option>
                                <option value="n_ct">{{__('No categories')}}</option>
                                <option value="n_t">{{__('No tags')}}</option>
                                <option value="n_cr">{{__('No colors')}}</option>
                                <option value="n_m">{{__('No media')}}</option>
                            </select>
                        </div>
                        <div class="form-group col-md-1">
                            <label>{{__('Active')}}</label>
                            <select name="status" class="form-control">
                                <option value="">{{__('All')}}</option>
                                <option value="{{ACTIVE}}">{{__('Active')}}</option>
                                <option value="{{INACTIVE}}">{{__('Inactive')}}</option>
                            </select>
                        </div>
                        <div class="col-md-4 listing-search">
                            <div class="col-8 px-0">
                                <button type="submit" class="btn btn-primary btn-block search_auction"><i class="fa fa-search"></i>&nbsp; {{__('Apply Filter')}}</button>
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-soft-secondary btn-block text-dark clear_search"><i class="fe-refresh-ccw"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <table id="auctionTable" class="table table-sm table-striped dt-responsive">
                        <thead>
                        <tr>
                            <th></th>
                            <th>{{__('Image')}}</th>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Duration')}}</th>
                            <th>{{__('Features')}}</th>
                            <th>{{__('Base Price')}}</th>
                            <th>{{__('Seller')}}</th> {{--View only for admin and on click seller information modal--}}
                            <th>{{__('Live')}}</th>
                            <th>{{__('Status')}}</th>
                            <th>{{__('Action')}}</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('.dropify').dropify();
            let data_url = "{{route('currentAuctionList')}}";
            let data_column = [
                {"data": "id", visible: false},
                {"data": "icon", orderable: false, searchable: false},
                {"data": "name",orderable:true},
                {"data": "features",orderable: false, searchable: false},
                {"data": "base_price"},
                {"data": "seller_name"},
                {"data": "duration",orderable: false, searchable: false},
                {"data": "live",orderable: false, searchable: false},
                {"data": "status"},
                {"data": "action", orderable: false, searchable: false}
            ];
            renderDataTable($('#auctionTable'), data_url, data_column,[[0, "desc"]],false);
            $(document).on('click','.search_auction',function (){
                let form_id = $(this).closest('form').attr('id');
                let this_form = $('#' + form_id);
                $(this_form).on('submit', function (e) {
                    if (!e.isDefaultPrevented()) {
                        e.preventDefault();
                        let formData = $(this).serializeArray();
                        renderDataTableWithData($('#auctionTable'),data_url,data_column,formData,[[0, "desc"]],false);
                    }
                });
            })

            $(document).on('click','.clear_search',function (){
                let this_form = $(this).closest('form')[0];
                this_form.reset();
                renderDataTable($('#auctionTable'), data_url, data_column,[[0, "desc"]],false);
            });

            $(document).on('click','.status_change',function (e) {
                Ladda.bind(this);
                let load = $(this).ladda();
                let id = $(this).data('id');
                let status = $(this).data('type');
                let status_url = "{{route('auctionStatusChange')}}";
                swalConfirm("Do you really want to change this ?").then(function (s) {
                    if(s.value){
                        let data = {
                            id : id,
                            status : status
                        };
                        makeAjaxPost(data, status_url, load).done(function (response) {
                            if (response.success == true){
                                swalSuccess(response.message);
                                renderDataTable($('#auctionTable'), data_url, data_column,[[0, "desc"]],false);
                            }else {
                                swalError(response.message);
                            }
                        });
                    }else{
                        load.ladda('stop');
                    }
                })
            });

        });
    </script>

@endsection
