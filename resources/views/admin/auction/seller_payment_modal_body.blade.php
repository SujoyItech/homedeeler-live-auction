<div class="modal-header pl-4 pb-0">
    <h4 class="modal-title text-center">{{__('Pay To Seller')}}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="pl-4 pr-4 mt-1">
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <p>{{__('Product Price: ')}} {{getMoney($auction_details->auction_winning_price ?? 0)}}</p>
                <p>{{__('Processing Fee: ')}} {{getMoney($auction_details->processing_fee ?? 0)}}</p>
                <p>{{__('Total Product Price: ')}} {{getMoney($auction_details->auction_winning_price ?? 0)}}
                    + {{getMoney($auction_details->processing_fee ?? 0)}}
                    = {{getMoney($auction_details->total_price ?? 0)}}</p>
                <p>{{__('Received Price From Bidder: ')}} {{getMoney((float)$auction_details->recieved_amount_by_winner ?? 0)}}</p>
                <p>{{__('Admin commission: ')}} {{getMoney($auction_details->commission_price ?? 0)}}</p>
                <p>{{__('Seller paying amount: ')}} {{getMoney($auction_details->total_price ?? 0)}}
                    - {{getMoney($auction_details->commission_price ?? 0)}}
                    = {{getMoney($auction_details->seller_paying_amount ?? 0)}}</p>
                <form method="POST" action="{{route('payAuctionPriceToSeller')}}" enctype="multipart/form-data" class="seller_payment_form_class" id="seller_payment_form_id">
                    <div class="form-group">
                        <label>{{__('Document Upload')}}</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="document" id="document">
                                <label class="custom-file-label" for="document"><i class="fa fa-paperclip"></i> {{__('Choose file')}}</label>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="auction_id" value="{{$auction_details->id}}">
                    <input type="hidden" name="amount" value="{{$auction_details->seller_paying_amount}}">
                    <button class="btn btn-dark btn-block submit_seller_pay" type="submit">
                        {{__('Pay ')}} {{getMoney($auction_details->seller_paying_amount ?? 0)}} {{(' To Seller')}}
                    </button>
                </form>

            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-light text-dark waves-effect" data-dismiss="modal">{{__('Cancel')}}</button>
</div>
<script>
    submitOperation((response)=>{
        if (response.success == true){
            swalSuccess(response.message);
            setTimeout(()=>{
               window.location.reload();
            },500);
        }else {
            swalError(response.message);
        }
    },'submit_seller_pay');

    $(document).on('change','.custom-file-input',function (){
        $('.custom-file-label').html(`<span class="text-success"><i class="fa fa-check"></i> Selected </span>`)
    })
</script>
