<div id="addAuctionModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form novalidate class="create-auction-form" method="post" action="{{route('initAuction')}}" id="create_auction">
                <div class="modal-header pl-4 pb-0">
                    <h4 class="modal-title text-center">{{__('New auction')}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <hr>
                <div class="pl-4 pr-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="control-label">{{__('Product Name')}} <span class="text-danger">*</span></label>
                                <input type="text" id="name" name="name" class="form-control" required placeholder="{{__('Product Name')}}">
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="slug">{{__('Slug')}}<span class="text-danger">*</span></label>
                                <input type="text" class="form-control check_slug_validity" data-slugforid="name" data-exceptvalueid="id" data-slugvalidateurl="{{route('productSlugCheck')}}" name="slug" id="slug" required>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light text-dark waves-effect" data-dismiss="modal">{{__('Cancel')}}</button>
                    <button type="submit" class="create-auction btn btn-dark waves-effect waves-light"><i class="fa fa-save"></i> {{__('Create auction')}}</button>
                </div>
            </form>

        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        resetValidation('create-auction-form')
        checkSlugVlaidity();
    });
</script>

