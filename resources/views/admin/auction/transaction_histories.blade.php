@extends('admin.layouts.app',['menu'=>'auction','sub_menu'=>'transactionHistories'])
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h4><i class="fas fa-boxes"></i> {{__('Transaction Histories')}}</h4>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="transactionHistoriesTable" class="table table-sm table-striped dt-responsive">
                        <thead>
                        <tr>
                            <th>{{__('Date')}}</th>
                            <th>{{__('Auction Name')}}</th>
                            <th>{{__('User Name')}}</th>
                            <th>{{__('Purpose')}}</th>
                            <th>{{__('Payment Method')}}</th>
                            <th>{{__('Amount')}}</th>
                            <th>{{__('Status')}}</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            let data_url = "{{route('transactionHistories')}}";
            let data_column = [
                {"data": "created_at"},
                {"data": "auction_name"},
                {"data": "user_name"},
                {"data": "purpose"},
                {"data": "payment_method"},
                {"data": "amount"},
                {"data": "payment_status"},
            ];
            renderDataTable($('#transactionHistoriesTable'), data_url, data_column);
        });
    </script>

@endsection
