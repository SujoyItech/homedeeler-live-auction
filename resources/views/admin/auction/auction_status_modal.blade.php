<div id="statusUpdateModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form novalidate class="status-update-form" method="post" action="{{route('updateAuctionStatusHistory')}}" id="status_update_id" enctype="multipart/form-data">
                <div class="modal-header pl-4 pb-0">
                    <h4 class="modal-title text-center">{{__('In Shipment')}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <hr>
                <div class="pl-4 pr-4">
                    <div class="row">
                        <input type="hidden" name="id" id="auction_status_id">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message" class="control-label">{{__('Message')}}</label>
                                <textarea class="form-control" name="message" rows="2" placeholder="{{__('Write here.')}}"></textarea>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-0">
                                <label>{{__('Document Upload')}}</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="file" id="file">
                                        <label class="custom-file-label" for="file"><i class="fa fa-paperclip"></i> {{__('Choose file')}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light text-dark waves-effect" data-dismiss="modal">{{__('Cancel')}}</button>
                    <button type="submit" class="update-status btn btn-dark waves-effect waves-light"><i class="fa fa-save"></i> {{__('In Shipment')}}</button>
                </div>
            </form>

        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        resetValidation('status-update-form')
        submitOperation(function (response){
            if (response.success == true){
                swalRedirect("{{Request::url()}}",response.message,'success');
            }else {
                swalError(response.message);
            }
        },'update-status')
    });
    $(document).on('change','.custom-file-input',function (){
        $('.custom-file-label').html(`<span class="text-success"><i class="fa fa-check"></i> Selected </span>`)
    })
</script>

