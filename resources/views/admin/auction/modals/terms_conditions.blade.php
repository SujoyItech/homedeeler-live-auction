<!-- terms_condition modal content -->
<div id="terms_condition-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="terms_condition-modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="terms_condition-modalLabel">{{__('Terms & Conditions')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="{{route('saveAuction')}}" novalidate class="terms_condition_form" method="POST" id="terms_condition_form_id">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <input type="hidden" name="id" value="{{$auction->id}}">
                                <textarea name="terms_condition" id="summernote-terms-condition" class="terms_condition">{!! $auction->terms_condition ?? '' !!}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary submit_auction_info"><i class="fa fa-save"></i> {{__('Save')}}</button>
                        <button type="button" class="btn btn-light" data-dismiss="modal">{{__('Close')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function (){
        $('#summernote-terms-condition').summernote({
            placeholder: "{{__('Write something about terms and conditions..')}}",
            tabsize: 2,
            height: 300
        });
    });
</script>
