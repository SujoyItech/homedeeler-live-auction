<!-- shipping_payment modal content -->
<div id="shipping_payment-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="shipping_payment-modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="shipping_payment-modalLabel">{{__('Shipping & Payments')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="{{route('saveAuction')}}" novalidate class="shipping_payment_form" method="POST" id="shipping_payment_form_id">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <input type="hidden" name="id" value="{{$auction->id}}">
                                <textarea name="shipping_payment" id="summernote-shipping-payment" class="shipping_payment">{!! $auction->shipping_payment ?? '' !!}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary submit_auction_info"><i class="fa fa-save"></i> {{__('Save')}}</button>
                        <button type="button" class="btn btn-light" data-dismiss="modal">{{__('Close')}}</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    $(document).ready(function (){
        $('#summernote-shipping-payment').summernote({
            placeholder: "{{__('Write something about shipping and payments..')}}",
            tabsize: 2,
            height: 300
        });
    })
</script>
