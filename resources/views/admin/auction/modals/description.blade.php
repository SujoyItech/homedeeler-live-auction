<link href="{{adminAsset('libs/summernote/summernote-bs4.min.css')}}" rel="stylesheet" type="text/css" />
<!-- description modal content -->
<div id="description-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="description-modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="description-modalLabel">{{__('Description')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="{{route('saveAuction')}}" novalidate class="description_form" method="POST" id="description_form_id">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <input type="hidden" name="id" value="{{$auction->id}}">
                                <textarea name="description" id="summernote-description" class="description">{!! $auction->description ?? '' !!}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary submit_auction_info"><i class="fa fa-save"></i> {{__('Save')}}</button>
                        <button type="button" class="btn btn-light" data-dismiss="modal">{{__('Close')}}</button>
                    </div>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="{{adminAsset('libs/summernote/summernote-bs4.min.js')}}"></script>
<script>
    $(document).ready(function (){
        $('#summernote-description').summernote({
            placeholder: "{{__('Write some description here..')}}",
            tabsize: 2,
            height: 300
        });
    })
</script>
