@extends('admin.layouts.app',['menu'=>'auctions','sub_menu'=>'auctions'])
@section('content')
    <div class="col-lg-7 col-xl-7 col-md-7 col-sm-12 col-xs-12">
        @include('admin.auction.auction_details.live.auction_details_card')
        @include('admin.auction.live_streaming.host')
    </div>
    <div class="col-lg-5 col-xl-5 col-md-5 col-sm-12 col-xs-12">
        @include('admin.auction.auction_details.live.live_messages')
    </div>
@endsection
@section('script')
    @stack('custom-script');

@endsection

