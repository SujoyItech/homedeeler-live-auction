 <link rel="stylesheet" type="text/css" href="{{adminAsset('agora/css/style.css')}}"/>
 <div class="card">
     <div class="card-body">
         <div id="buttons-container" class="row justify-content-center mt-3">
             {{-- <div class="col-md-2 text-center">
                 <button id="start-btn"  type="button" class="btn btn-block btn-success btn-lg">
                     <i id="exit-icon" class="fas fa-phone"></i>
                 </button>
             </div> --}}
             <div id="audio-controls" class="col-md-2 text-center btn-group">
                 <button id="mic-btn" type="button" class="btn btn-block btn-dark btn-lg">
                     <i id="mic-icon" class="fas fa-microphone"></i>
                 </button>
                 <button id="mic-dropdown" type="button" class="btn btn-lg btn-dark dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     <span class="sr-only">{{__('Toggle Dropdown')}}</span>
                 </button>
                 <div id="mic-list" class="dropdown-menu dropdown-menu-right">
                 </div>
             </div>
             <div id="video-controls" class="col-md-2 text-center btn-group">
                 <button id="video-btn"  type="button" class="btn btn-block btn-dark btn-lg">
                     <i id="video-icon" class="fas fa-video"></i>
                 </button>
                 <button id="cam-dropdown" type="button" class="btn btn-lg btn-dark dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     <span class="sr-only">{{__('Toggle Dropdown')}}</span>
                 </button>
                 <div id="camera-list" class="dropdown-menu dropdown-menu-right">
                 </div>
             </div>
             <div class="col-md-2 text-center">
                 <button id="exit-btn"  type="button" class="btn btn-block btn-danger btn-lg">
                     <i id="exit-icon" class="fas fa-phone-slash"></i>
                 </button>
             </div>
         </div>
         <div id="full-screen-video" class="d-flex align-items-center justify-content-center">
             <button class="btn btn-dark btn-xlg justify-content-center" id="start-btn"><i class="fas fa-video" aria-hidden="true"></i> {{__('Go Live')}}</button>
             <div class="justify-content-center d-none" id="loadingBuffer">
                 <div class="spinner-border text-dark"  style="width: 3rem; height: 3rem;" role="status">
                     <span class="sr-only">Loading...</span>
                 </div>
             </div>
         </div>
     </div>
 </div>

@push('custom-script')
    <script src="https://cdn.agora.io/sdk/web/AgoraRTCSDK-2.8.0.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script>
        // use tokens for added security
        showStartLiveBtn();
        $(document).on('click','#start-btn',function (){
            startStreaming();
            showInLiveBtn();
            startLoading();
        });

        $(document).on('click','#exit-btn',function (){
            leaveChannel();
        });

        /**
         * Agora Broadcast Client
         */

        const agoraAppId = "{{env('AGORA_APP_ID')}}"; // set app id
        const channelName = "{{$auction_details->slug}}"; // set channel name

        // create client instance
        const client = AgoraRTC.createClient({ mode: "live", codec: "vp8" }); // h264 better detail at a higher motion
        let mainStreamId; // reference to main stream

        // set video profile
        // [full list: https://docs.agora.io/en/Interactive%20Broadcast/videoProfile_web?platform=Web#video-profile-table]
        const cameraVideoProfile = "480p_1"; // 960 × 720 @ 30fps  & 750kbs

        // keep track of streams
        const localStreams = {
            uid: "",
            camera: {
                camId: "",
                micId: "",
                stream: {}
            }
        };

        // keep track of devices
        const devices = {
            cameras: [],
            mics: []
        };

        let externalBroadcastUrl = "";

        // default config for rtmp
        const defaultConfigRTMP = {
            width: 640,
            height: 360,
            videoBitrate: 400,
            videoFramerate: 15,
            lowLatency: false,
            audioSampleRate: 48000,
            audioBitrate: 48,
            audioChannels: 1,
            videoGop: 30,
            videoCodecProfile: 100,
            userCount: 0,
            userConfigExtraInfo: {},
            backgroundColor: 0x000000,
            transcodingUsers: []
        };

        // set log level:
        // -- .DEBUG for dev
        // -- .NONE for prod
        AgoraRTC.Logger.setLogLevel(AgoraRTC.Logger.DEBUG);

        function startStreaming(){
            // init Agora SDK
            client.init(
                agoraAppId,
                async function() {
                    console.log("AgoraRTC client initialized");
                    await joinChannel(); // join channel upon successfull init
                },
                function(err) {
                    console.log("[ERROR] : AgoraRTC client init failed", err);
                }
            );

            // client callbacks
            client.on("stream-published", function(evt) {
                showInLiveBtn();
                console.log("Publish local stream successfully");
            });

            // when a remote stream is added
            client.on("stream-added", function(evt) {
                console.log("new stream added: " + evt.stream.getId());
            });

            client.on("stream-removed", function(evt) {
                var stream = evt.stream;
                stream.stop(); // stop the stream
                stream.close(); // clean up and close the camera stream
                console.log("Remote stream is removed " + stream.getId());
                showStartLiveBtn();
            });

            //live transcoding events..
            client.on("liveStreamingStarted", function(evt) {
                console.log("Live streaming started");
            });

            client.on("liveStreamingFailed", function(evt) {
                console.log("Live streaming failed");
                showStartLiveBtn();
            });

            client.on("liveStreamingStopped", function(evt) {
                console.log("Live streaming stopped");
                showStartLiveBtn();
            });

            client.on("liveTranscodingUpdated", function(evt) {
                console.log("Live streaming updated");
            });

            // ingested live stream
            client.on("streamInjectedStatus", function(evt) {
                console.log("Injected Steram Status Updated");
                console.log(JSON.stringify(evt));
            });

            // when a remote stream leaves the channel
            client.on("peer-leave", function(evt) {
                console.log("Remote stream has left the channel: " + evt.stream.getId());
                showStartLiveBtn();
            });

            // show mute icon whenever a remote has muted their mic
            client.on("mute-audio", function(evt) {
                console.log("Mute Audio for: " + evt.uid);
            });

            client.on("unmute-audio", function(evt) {
                console.log("Unmute Audio for: " + evt.uid);
            });

            // show user icon whenever a remote has disabled their video
            client.on("mute-video", function(evt) {
                console.log("Mute Video for: " + evt.uid);
            });

            client.on("unmute-video", function(evt) {
                console.log("Unmute Video for: " + evt.uid);
            });
        }

        function showStartLiveBtn(){
            $('#buttons-container').addClass('d-none');
            $('#start-btn').removeClass('d-none');
        }
        function showInLiveBtn(){
            $('#buttons-container').removeClass('d-none');
            $('#start-btn').addClass('d-none');
        }

        function startLoading(){
            $('#loadingBuffer').removeClass('d-none');
        }

        function removeLoading(){
            $('#loadingBuffer').addClass('d-none');
        }

        // join a channel
        async function joinChannel() {
            const userID = parseInt("{{Auth::user()->id}}");
            const token = await fetchToken("{{$auction_details->id}}",1);
            console.log(token);
             // set to null to auto generate uid on successfull connection
            // set the role
            client.setClientRole(
                "host",
                function() {
                    console.log("Client role set as host.");
                },
                function(e) {
                    console.log("setClientRole failed", e);
                }
            );

            // client.join(token, 'allThingsRTCLiveStream', 0, function(uid) {
            client.join(
                token,
                channelName,
                userID,
                function(uid) {
                    createCameraStream(uid, {});
                    localStreams.uid = uid; // keep track of the stream uid
                    console.log("User " + uid + " joined channel successfully");
                    removeLoading();
                },
                function(err) {
                    console.log("[ERROR] : join channel failed", err);
                }
            );
        }
        // video streams for channel


        function leaveChannel() {
            client.leave(
                function() {
                    console.log("client leaves channel");
                    localStreams.camera.stream.stop(); // stop the camera stream playback
                    localStreams.camera.stream.close(); // clean up and close the camera stream
                    client.unpublish(localStreams.camera.stream); // unpublish the camera stream
                    //disable the UI elements
                    showStartLiveBtn();
                    leaveChannelAction("{{$auction_details->id}}");
                },
                function(err) {
                    console.log("client leave failed ", err); //error handling
                }
            );
        }

        function fetchToken(auction_id, tokenRole) {

            return new Promise(function (resolve) {
                axios.post('{{route('createToken')}}', {
                    auction_id: auction_id,
                    role: tokenRole
                }, {
                    headers: {
                        'Content-Type': 'application/json; charset=UTF-8'
                    }
                })
                    .then(function (response) {
                        const token = response.data.data.token;
                        resolve(token);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            })
        }

        function leaveChannelAction($auction_id){
            const data = {
                auction_id : $auction_id
            }
            makeAjaxPost(data,"{{route('auctionLiveStreamingStop')}}").done(function (response){
                if (response.success == true){
                    Toast.fire({type: 'success', text: response.message});
                }else {
                    Toast.fire({type: 'warning', text: response.message});
                }
            })
        }
    </script>
{{--    <script src="{{adminAsset('agora/js/agora-broadcast-client.js')}}"></script>--}}

    <script src="{{adminAsset('agora/js/host-streaming.js')}}"></script>
    <script src="{{adminAsset('agora/js/agora-screen-client.js')}}"></script>
    <script src="{{adminAsset('agora/js/ui.js')}}"></script>

@endpush



