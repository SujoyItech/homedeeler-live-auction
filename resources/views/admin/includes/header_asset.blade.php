<!-- App css -->
<script src="{{adminAsset('vendors/jQuery.min.js')}}"></script>
<link href="{{adminAsset('css/bootstrap-saas.min.css')}}" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
{{--<link href="{{adminAsset('css/bootstrap-saas-dark.css')}}" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" disabled />--}}

<!-- Other Plugins -->
{{--<link href="{{adminAsset('libs/selectize/css/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css" />--}}
{{--<link href="{{adminAsset('libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />--}}
<link href="{{adminAsset('libs/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{adminAsset('libs/dropify/css/dropify.min.css')}}" rel="stylesheet" type="text/css" />
<script src="{{adminAsset('libs/dropify/js/dropify.min.js')}}"></script>
<link href="{{adminAsset('libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
<script src="{{adminAsset('libs/flatpickr/flatpickr.min.js')}}"></script>


<link href="{{adminAsset('css/app-saas.css')}}" rel="stylesheet" type="text/css" id="app-default-stylesheet" />
{{--<link href="{{adminAsset('css/app-saas-dark.css')}}" rel="stylesheet" type="text/css" id="app-dark-stylesheet" disabled />--}}



<!-- icons -->
<link href="{{adminAsset('icons/feathericon.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{adminAsset('icons/fontawesome5.min.css')}}" rel="stylesheet" type="text/css" />
{{--<link href="{{adminAsset('icons/materialdesignicon.min.css')}}" rel="stylesheet" type="text/css" />--}}

<link href="{{adminAsset('vendors/ladda/ladda-themeless.min.css')}}" rel="stylesheet">
<script src="{{adminAsset('vendors/sweetalert/sweetalert2.all.min.js')}}"></script>
<link href="{{adminAsset('vendors/sweetalert/sweetalert.css')}}" rel="stylesheet">
<link href="{{adminAsset('libs/toastr/build/toastr.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{adminAsset('vendors/DataTables/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet newest" href="{{adminAsset('css/custom.css')}}">
<link href="{{adminAsset('css/icons.min.css')}}" rel="stylesheet" type="text/css" />


@yield('style')
