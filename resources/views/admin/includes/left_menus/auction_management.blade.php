@if(check_permission(MODULE_SUPER_ADMIN)|| check_permission(MODULE_USER_ADMIN))
    <h5 class="menu-title"><i class="fas fa-boxes fa-2x"></i> {{__('Admin')}}</h5>
    <hr class="my-1">
    <ul class="nav flex-column">
        @if (check_role(SUPER_ADMIN))
            {!! menuLiAppend('superAdminHome', 'Dashboard', 'fa fa-box', $sub_menu, '') !!}
        @endif
        @if (check_role(USER_ADMIN))
            {!! menuLiAppend('adminHome', 'Dashboard', 'fa fa-box', $sub_menu, '') !!}
        @endif
        {!! menuLiAppend('categories', 'Categories', 'fa fa-box', $sub_menu, '', TRUE) !!}
        {!! menuLiAppend('brands', __('Brands'), 'fas fa-store', $sub_menu, 'brands', TRUE) !!}
        {!! menuLiAppend('tags', __('Tags'), 'fas fa-tag', $sub_menu, 'tags', TRUE) !!}
        {!! menuLiAppend('combinationType', __('Combination Type'), 'fas fa-tags', $sub_menu, 'combinationType', FALSE) !!}
        {!! menuLiAppend('combination', __('Combination'), 'fas fa-tags', $sub_menu, 'combination', FALSE) !!}
        {!! menuLiAppend('auctions', __('Auction'), 'fa fa-box', $sub_menu, 'auctions',TRUE) !!}
        {!! menuLiAppend('messaging', __('Messaging'), 'fa fa-envelope', $sub_menu, 'messaging',TRUE) !!}
        {!! menuLiAppend('generalMessages', __('General Messages'), 'fa fa-envelope', $sub_menu, 'generalMessages',TRUE) !!}
        {!! menuLiAppend('currentAuctionList', 'Current Auctions', 'fa fa-box', $sub_menu, 'currentAuctionList', TRUE) !!}
        {!! menuLiAppend('liveAuctionList', 'Live Auctions', 'fa fa-box', $sub_menu, 'liveAuctionList', TRUE) !!}
        {!! menuLiAppend('completeAuctionList', 'Completed Auctions', 'fa fa-box', $sub_menu, 'completeAuctionList', TRUE) !!}
        {!! menuLiAppend('transactionHistories', 'Transactions', 'fa fa-box', $sub_menu, 'transactionHistories') !!}
        {!! menuLiAppend('viewAllNotifications', 'My Notifications', 'fa fa-box', $sub_menu, 'viewAllNotifications') !!}
    </ul>
@endif

@if(check_permission(MODULE_USER))
    @if (check_role(USER_SELLER))
        <h5 class="menu-title"><i class="fas fa-boxes fa-2x"></i> {{__('Auctioneers')}}</h5>
        <hr class="my-1">
        <ul class="nav flex-column">
            <li class="nav-item @if(!empty($sub_menu) && $sub_menu == 'dashboard') menuitem-active @endif">
                <a class="nav-link @if(!empty($sub_menu) && $sub_menu == 'dashboard') menuitem-active @endif" href="{{route('sellerHome')}}"><i class="fa fa-home"></i>&nbsp;{{__('Dashboard')}}</a>
            </li>
            {!! menuLiAppend('myAuctionList', __('Auction'), 'fa fa-box', $sub_menu, 'myAuctionList',TRUE) !!}
            {!! menuLiAppend('messaging', __('Messaging'), 'fa fa-envelope', $sub_menu, 'messaging',TRUE) !!}
            {!! menuLiAppend('currentAuctionList', 'Current Auctions', 'fa fa-box', $sub_menu, 'currentAuctionList', TRUE) !!}
            {!! menuLiAppend('liveAuctionList', 'Live Auctions', 'fa fa-box', $sub_menu, 'liveAuctionList', TRUE) !!}
            {!! menuLiAppend('completeAuctionList', 'Completed Auctions', 'fa fa-box', $sub_menu, 'completeAuctionList', TRUE) !!}
            {!! menuLiAppend('transactionHistories', 'Transactions', 'fa fa-box', $sub_menu, 'transactionHistories') !!}
            {!! menuLiAppend('viewAllNotifications', 'My Notifications', 'fa fa-box', $sub_menu, 'viewAllNotifications') !!}
        </ul>
    @endif
@endif

