@extends('admin.layouts.app',['menu'=>'home','sub_menu'=>'home'])
@section('content')
    <div class="col-md-6 col-xl-3">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                <div class="col-4">
                    <div class="avatar-lg rounded-circle bg-soft-primary">
                        <i class="fe-user font-22 avatar-title text-primary"></i>
                    </div>
                </div>
                <div class="col-8">
                    <div class="text-right">
                        <h3 class="mt-1"><span data-plugin="counterup">{{$user_count->total_seller ?? 0}}</span></h3>
                        <p class="text-muted mb-1 text-truncate">{{__('Total Seller')}}</p>
                    </div>
                </div>
            </div> <!-- end row-->
        </div> <!-- end widget-rounded-circle-->
    </div> <!-- end col-->
    <div class="col-md-6 col-xl-3">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                <div class="col-4">
                    <div class="avatar-lg rounded-circle bg-soft-danger">
                        <i class="fe-user font-22 avatar-title text-danger"></i>
                    </div>
                </div>
                <div class="col-8">
                    <div class="text-right">
                        <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$user_count->unapproved_seller ?? 0}}</span></h3>
                        <p class="text-muted mb-1 text-truncate">{{__('Unapproved Seller')}}</p>
                    </div>
                </div>
            </div> <!-- end row-->
        </div> <!-- end widget-rounded-circle-->
    </div> <!-- end col-->
    <div class="col-md-6 col-xl-3">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                <div class="col-4">
                    <div class="avatar-lg rounded-circle bg-soft-warning">
                        <i class="fe-user font-22 avatar-title text-warning"></i>
                    </div>
                </div>
                <div class="col-8">
                    <div class="text-right">
                        <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$user_count->total_bidder ?? 0}}</span></h3>
                        <p class="text-muted mb-1 text-truncate">{{__('Total Bidder')}}</p>
                    </div>
                </div>
            </div> <!-- end row-->
        </div> <!-- end widget-rounded-circle-->
    </div> <!-- end col-->
    <div class="col-md-6 col-xl-3">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                <div class="col-4">
                    <div class="avatar-lg rounded-circle bg-soft-warning">
                        <i class="fe-user font-22 avatar-title text-warning"></i>
                    </div>
                </div>
                <div class="col-8">
                    <div class="text-right">
                        <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$user_count->unapproved_bidder ?? 0}}</span></h3>
                        <p class="text-muted mb-1 text-truncate">{{__('Unapproved Bidder')}}</p>
                    </div>
                </div>
            </div> <!-- end row-->
        </div> <!-- end widget-rounded-circle-->
    </div> <!-- end col-->
    <div class="col-md-6 col-xl-3">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                <div class="col-4">
                    <div class="avatar-lg rounded-circle bg-soft-primary">
                        <i class="fe-heart font-22 avatar-title text-primary"></i>
                    </div>
                </div>
                <div class="col-8">
                    <div class="text-right">
                        <h3 class="mt-1"><span data-plugin="counterup">{{$auction_count->total_product ?? 0}}</span></h3>
                        <p class="text-muted mb-1 text-truncate">{{__('Total Product')}}</p>
                    </div>
                </div>
            </div> <!-- end row-->
        </div> <!-- end widget-rounded-circle-->
    </div> <!-- end col-->
    <div class="col-md-6 col-xl-3">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                <div class="col-4">
                    <div class="avatar-lg rounded-circle bg-soft-success">
                        <i class="fe-heart font-22 avatar-title text-success"></i>
                    </div>
                </div>
                <div class="col-8">
                    <div class="text-right">
                        <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$auction_count->total_product_in_live ?? 0}}</span></h3>
                        <p class="text-muted mb-1 text-truncate">{{__('Products In Live')}}</p>
                    </div>
                </div>
            </div> <!-- end row-->
        </div> <!-- end widget-rounded-circle-->
    </div> <!-- end col-->
    <div class="col-md-6 col-xl-3">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                <div class="col-4">
                    <div class="avatar-lg rounded-circle bg-soft-danger">
                        <i class="fe-heart font-22 avatar-title text-danger"></i>
                    </div>
                </div>
                <div class="col-8">
                    <div class="text-right">
                        <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$auction_count->total_active_product ?? 0}}</span></h3>
                        <p class="text-muted mb-1 text-truncate">{{__('Active Products')}}</p>
                    </div>
                </div>
            </div> <!-- end row-->
        </div> <!-- end widget-rounded-circle-->
    </div> <!-- end col-->
    <div class="col-md-6 col-xl-3">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                <div class="col-4">
                    <div class="avatar-lg rounded-circle bg-soft-primary">
                        <i class="fe-heart font-22 avatar-title text-primary"></i>
                    </div>
                </div>
                <div class="col-8">
                    <div class="text-right">
                        <h3 class="mt-1"><span data-plugin="counterup">{{$auction_count->total_deliverable_product ?? 0}}</span></h3>
                        <p class="text-muted mb-1 text-truncate">{{__('Deliverable Products')}}</p>
                    </div>
                </div>
            </div> <!-- end row-->
        </div> <!-- end widget-rounded-circle-->
    </div> <!-- end col-->
    <div class="col-md-6 col-xl-3">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                <div class="col-4">
                    <div class="avatar-lg rounded-circle bg-soft-danger">
                        <i class="fe-eye font-22 avatar-title text-danger"></i>
                    </div>
                </div>
                <div class="col-8">
                    <div class="text-right">
                        <h3 class="text-dark mt-1">$<span data-plugin="counterup">{{$balance_count->total_price ?? 0}}</span></h3>
                        <p class="text-muted mb-1 text-truncate">{{__('Total Balance')}}</p>
                    </div>
                </div>
            </div> <!-- end row-->
        </div> <!-- end widget-rounded-circle-->
    </div> <!-- end col-->
    <div class="col-md-6 col-xl-3">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                <div class="col-4">
                    <div class="avatar-lg rounded-circle bg-soft-warning">
                        <i class="fe-eye font-22 avatar-title text-warning"></i>
                    </div>
                </div>
                <div class="col-8">
                    <div class="text-right">
                        <h3 class="text-dark mt-1">$<span data-plugin="counterup">{{$balance_count->total_seller_payable ?? 0}}</span></h3>
                        <p class="text-muted mb-1 text-truncate">{{__('Seller Payable amount')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{adminAsset('libs/jquery.counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{adminAsset('libs/waypoints/lib/jquery.waypoints.min.js')}}"></script>
@endsection

