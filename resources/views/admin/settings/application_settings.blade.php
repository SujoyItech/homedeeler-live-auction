@extends('admin.layouts.app',['menu'=>'settings','sub_menu'=>'applicationSettings'])
@section('content')
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('adminSettingsSave')}}" name="application_settings" class="parsley-examples" method="POST" id="application_setting" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="name">{{__('Product Image Max Size in MB')}}<span class="text-danger">*</span></label>
                                <input type="text" name="product_image_max_size_in_mb" required value="{{ $settings->product_image_max_size_in_mb ?? ''}}"
                                       placeholder="Ex: 2" class="form-control set-settings" id="product_image_max_size_in_mb">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div
@endsection
@section('script')
    <script>
        $('.set-settings').on('blur',function (){
            if ($(this).val().length !== 0){
                var input_name = $(this).attr('name');
                var submit_url = "{{route('adminSettingsSave')}}";
                var option_group = "application_settings";
                var formData = new FormData();
                formData.append('option_type', 'text');
                formData.append('option_group', option_group);
                formData.append('option_key', input_name);
                formData.append('option_value', $(this).val());
                makeAjaxPostFile(formData,submit_url,null).done(function (response){
                    if (response.success == true){
                        $(".form-control").removeClass('is-valid');
                        $(".form-control").removeClass('is-invalid');
                        $(this_field).removeClass('is-invalid');
                        $(this_field).next().removeClass('invalid-feedback');
                        $(this_field).next().addClass('valid-feedback');
                        $(this_field).addClass('is-valid').siblings('.valid-feedback').text('{{__('Looks sdfdsfsdf good!')}}');
                    }else{
                        $(".form-control").removeClass('is-valid');
                        $(".form-control").removeClass('is-invalid');
                        $(this_field).removeClass('is-valid');
                        $(this_field).next().removeClass('valid-feedback');
                        $(this_field).next().addClass('invalid-feedback');
                        $(this_field).addClass('is-invalid').siblings('.invalid-feedback').text('{{__('Looks bad!')}}');
                    }
                })
            }

        })

    </script>
@endsection
