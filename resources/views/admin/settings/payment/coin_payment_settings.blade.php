@extends('admin.layouts.app',['menu'=>'settings','sub_menu'=>'socialSettings'])
@section('content')
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('adminSettingsSave')}}" name="site_settings" method="POST" id="social_setting" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="coin_payment_public_key">{{__('Coin Payment Public Key')}}</label>
                                <input type="text" name="coin_payment_public_key" value="{{ $settings->coin_payment_public_key ?? ''}}"
                                       placeholder="{{__('Coin Payment Public Key')}}" class="form-control coin_payment_settings" id="coin_payment_public_key">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="coin_payment_private_key">{{__('Coin Payment Private Key')}}</label>
                                <input type="text" name="coin_payment_private_key" value="{{ $settings->coin_payment_private_key ?? ''}}"
                                       placeholder="{{__('Coin Payment Private Key')}}" class="form-control coin_payment_settings" id="coin_payment_private_key">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="coin_payment_minimum_confirms">{{__('Minimum Confirms')}}</label>
                                <input type="number" name="coin_payment_minimum_confirms" value="{{ $settings->coin_payment_minimum_confirms ?? ''}}"
                                       placeholder="{{__('Minimum Confirms')}}" class="form-control coin_payment_settings" id="coin_payment_minimum_confirms">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="coin_payment_expiration_time">{{__('Expiration Time (MIN)')}}</label>
                                <input type="number" name="coin_payment_expiration_time" value="{{ $settings->coin_payment_expiration_time ?? ''}}"
                                       placeholder="{{__('Expiration Time')}}" class="form-control coin_payment_settings" id="coin_payment_expiration_time">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('.coin_payment_settings').on('blur',function (){
            if ($(this).val().length !== 0){
                var input_name = $(this).attr('name');
                var submit_url = "{{route('adminSettingsSave')}}";
                var option_group = "coin_payment_settings";
                var formData = new FormData();
                formData.append('option_type', 'text');
                formData.append('option_group', option_group);
                formData.append('option_key', input_name);
                formData.append('option_value', $(this).val());
                makeAjaxPostFile(formData,submit_url,null).done(function (response){

                })
            }

        })

    </script>
@endsection
