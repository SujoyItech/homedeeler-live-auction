@extends('admin.layouts.app',['menu'=>'settings','sub_menu'=>'paymentSettings'])
@section('content')
    <div class="col-md-12">
        <div class="page-title-box">
            <h4 class="page-title"><i class="fa fa-cogs"></i> {{__('Payment settings')}}</h4>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card-box">
            <div class="text-center mb-3">
                <div class="radio radio-info form-check-inline">
                    <input type="radio" id="brainTree" value="brain_tree" name="payment_method" @if(isset($settings->payment_method) && $settings->payment_method == 'brain_tree') checked @endif class="set-payment-setting">
                    <label for="brainTree"> {{__('Braintree')}} </label>
                </div>
                <div class="radio radio-info form-check-inline">
                    <input type="radio" id="stripe" value="stripe" name="payment_method" @if(isset($settings->payment_method) && $settings->payment_method == 'stripe') checked @endif class="set-payment-setting">
                    <label for="stripe"> {{__('Stripe')}} </label>
                </div>
                <div class="radio radio-info form-check-inline">
                    <input type="radio" id="paypal" value="paypal" name="payment_method" @if(isset($settings->payment_method) && $settings->payment_method == 'paypal') checked @endif class="set-payment-setting">
                    <label for="paypal"> {{__('Paypal')}} </label>
                </div>
            </div>
            <div class="show_payment_settings brain_tree @if(isset($settings->payment_method) && $settings->payment_method == 'brain_tree') @else d-none @endif">
                @include('admin.settings.payment.brain_tree')
            </div>
            <div class="show_payment_settings stripe  @if(isset($settings->payment_method) && $settings->payment_method == 'stripe') @else d-none @endif">
                @include('admin.settings.payment.stripe')
            </div>
            <div class="show_payment_settings paypal @if(isset($settings->payment_method) && $settings->payment_method == 'paypal') @else d-none @endif">
                @include('admin.settings.payment.paypal')
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $('.set-settings').on('blur',function (){
            if ($(this).val().length !== 0){
                let input_name = $(this).attr('name');
                let submit_url = "{{route('adminSettingsSave')}}";
                let option_group = "payment_settings";
                let formData = new FormData();
                formData.append('option_type', 'text');
                formData.append('option_group', option_group);
                formData.append('option_key', input_name);
                formData.append('option_value', $(this).val());
                makeAjaxPostFile(formData,submit_url,null).done(function (response){
                    if (response.success == true){
                        $(".form-control").removeClass('is-valid');
                        $(".form-control").removeClass('is-invalid');

                    }else{
                        $(".form-control").removeClass('is-valid');
                        $(".form-control").removeClass('is-invalid');
                    }
                })
            }

        });

        $('.set-payment-setting').on('change',function (){
            if ($(this).val().length !== 0){
                let input_name = $(this).attr('name');
                let active_class = $(this).val();
                let submit_url = "{{route('adminSettingsSave')}}";
                let option_group = "payment_settings";
                let formData = new FormData();
                formData.append('option_type', 'text');
                formData.append('option_group', option_group);
                formData.append('option_key', input_name);
                formData.append('option_value', $(this).val());
                makeAjaxPostFile(formData,submit_url,null).done(function (response){
                    if (response.success == true){
                        $('.show_payment_settings').addClass('d-none');
                        $('.'+active_class).removeClass('d-none');
                    }
                })
            }
        })

    </script>
@endsection
