@extends('admin.layouts.app')
@section('content')
    <div class="col-8">
        <div class="card-box">
            <h4 class="header-title">{{__('Language Add/Edit')}}</h4>
            <hr/>
            <form class="needs-validation" action="{{route('addLanguage')}}" method="get">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group mb-3">
                            <label for="validationCustom03">{{__('Language')}}</label>
                            <select class="form-control" id="language" required name="language">
                                <option value="">{{__('Select')}}</option>
                                @foreach(langName() as $key=>$value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <button class="btn btn-dark waves-effect waves-light mt-3" type="submit"><i class="fa fa-plus"></i> {{__('Add language')}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-4">
        <div class="card-box">
            <h4 class="header-title">{{__('Attribute Add/Edit')}}</h4>
            <hr/>
            <form class="needs-validation" novalidate>
                <div class="form-group mb-3">
                    <label for="validationCustom03">Attribute Group</label>
                    <select class="form-control" id="validationCustom03" required>
                        <option value="">Select</option>
                        <option value="active">Color</option>
                        <option value="inactive">Display Type</option>
                    </select>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <div class="form-group mb-3">
                    <label for="validationCustom03">Status</label>
                    <select class="form-control" id="validationCustom03" required>
                        <option value="active">Active</option>
                        <option value="inactive">Inactive</option>
                    </select>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <div class="my-3">
                    <input type="file" data-plugins="dropify" data-default-file=""  />
                </div>
                <button class="btn btn-dark waves-effect waves-light" type="submit">Submit form</button>
            </form>
        </div>
    </div>
@endsection
