@extends('admin.layouts.app',['menu'=>'settings','sub_menu'=>'commissionRateSettings'])
@section('style')
    <link href="{{adminAsset('libs/clockpicker/jquery-clockpicker.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <form action="{{route('adminSettingsSave')}}" name="application_settings" method="POST" id="application_setting" enctype="multipart/form-data">
        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <div class="form-group mb-3">
                        <label>{{__('Commission rate')}}</label>
                        <div class="input-group">
                            <input type="number" class="form-control set-settings" name="commission_rate" placeholder="{{__('Commission rate')}}" value="{{$settings->commission_rate ?? ''}}">
                            <div class="input-group-append">
                                <span class="input-group-text" id="commission_rate_percentage">%</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <label>{{__('Service Charge')}}</label>
                        <div class="input-group">
                            <input type="number" class="form-control set-settings" name="service_charge" placeholder="{{__('Service Charge')}}" value="{{$settings->service_charge ?? ''}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-box">
                    <label>{{__('Pick Time')}}</label>
                    <div class="form-group mb-3 row">
                        <div class="input-group col-md-6">
                            <div class="input-group clockpicker" data-placement="top" data-align="top" data-autoclose="true">
                                <input type="text" class="form-control set-settings-clock" value="{{$settings->pick_start ?? ''}}" placeholder="{{__('From:')}}" name="pick_start">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="mdi mdi-clock-outline"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group col-md-6">
                            <div class="input-group clockpicker" data-placement="top" data-align="top" data-autoclose="true">
                                <input type="text" class="form-control set-settings-clock" value="{{$settings->pick_end ?? ''}}" placeholder="{{__('To:')}}" name="pick_end">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="mdi mdi-clock-outline"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <label>{{__('Off Pick Time')}}</label>
                    <div class="form-group mb-3 row">
                        <div class="input-group col-md-6">
                            <div class="input-group clockpicker" data-placement="top" data-align="top" data-autoclose="true">
                                <input type="text" class="form-control set-settings-clock" value="{{$settings->off_pick_start ?? ''}}" placeholder="{{__('From:')}}" name="off_pick_start">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="mdi mdi-clock-outline"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group col-md-6">
                            <div class="input-group clockpicker" data-placement="top" data-align="top" data-autoclose="true">
                                <input type="text" class="form-control set-settings-clock" value="{{$settings->off_pick_end ?? ''}}" placeholder="{{__('To:')}}" name="off_pick_end">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="mdi mdi-clock-outline"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card-box">
                    <div class="form-group">
                        <label for="">{{__('Last bidding extends duration')}}</label>
                        <div class="input-group">
                            <input type="number" class="form-control set-settings" value="{{$settings->last_bidding_extends_duration ?? ''}}"  name="last_bidding_extends_duration">
                            <div class="input-group-append">
                                <span class="input-group-text">{{__('Minutes')}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">{{__('Last bidding extends times')}}</label>
                        <div class="input-group">
                            <input type="number" class="form-control set-settings" value="{{$settings->last_bidding_extends_times ?? ''}}"  name="last_bidding_extends_times">
                            <div class="input-group-append">
                                <span class="input-group-text">{{__('Times')}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card-box">
                            <h4 class="mb-3 header-title">{{__('Live auction room rent for Pick time: ')}}</h4>
                            <div class="form-group row mb-3">
                                <label for="pick30Min" class="col-3 col-form-label">{{__('3O Mins')}}</label>
                                <div class="col-9">
                                    <input type="number" class="form-control set-settings" value="{{$settings->pick_30_min ?? ''}}" id="pick30Min" name="pick_30_min">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label for="pick60Min" class="col-3 col-form-label">{{__('60 Mins')}}</label>
                                <div class="col-9">
                                    <input type="number" class="form-control set-settings" value="{{$settings->pick_60_min ?? ''}}" id="pick60Min" name="pick_60_min">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label for="pick90Min" class="col-3 col-form-label">{{__('90 Mins')}}</label>
                                <div class="col-9">
                                    <input type="number" class="form-control set-settings" value="{{$settings->pick_90_min ?? ''}}" id="pick90Min" name="pick_90_min">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label for="pick1200Min" class="col-3 col-form-label">{{__('120 Mins')}}</label>
                                <div class="col-9">
                                    <input type="number" class="form-control set-settings" value="{{$settings->pick_120_min ?? ''}}" id="pick120Min" name="pick_120_min">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label for="pick1500Min" class="col-3 col-form-label">{{__('150 Mins')}}</label>
                                <div class="col-9">
                                    <input type="number" class="form-control set-settings" value="{{$settings->pick_150_min ?? ''}}" id="pick150Min" name="pick_150_min">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label for="pick180Min" class="col-3 col-form-label">{{__('180 Mins')}}</label>
                                <div class="col-9">
                                    <input type="number" class="form-control set-settings" value="{{$settings->pick_180_min ?? ''}}" id="pick180Min" name="pick_180_min">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card-box">
                            <h4 class="mb-3 header-title">{{__('Live auction room rent for Off Pick time: ')}}</h4>
                            <div class="form-group row mb-3">
                                <label for="offPick30Min" class="col-3 col-form-label">{{__('3O Mins')}}</label>
                                <div class="col-9">
                                    <input type="number" class="form-control set-settings" value="{{$settings->off_pick_30_min ?? ''}}" id="offPick30Min" name="off_pick_30_min">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label for="offPick60Min" class="col-3 col-form-label">{{__('60 Mins')}}</label>
                                <div class="col-9">
                                    <input type="number" class="form-control set-settings" value="{{$settings->off_pick_60_min ?? ''}}" id="offPick60Min" name="off_pick_60_min">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label for="offPick90Min" class="col-3 col-form-label">{{__('90 Mins')}}</label>
                                <div class="col-9">
                                    <input type="number" class="form-control set-settings" value="{{$settings->off_pick_90_min ?? ''}}" id="offPick90Min" name="off_pick_90_min">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label for="offPick1200Min" class="col-3 col-form-label">{{__('120 Mins')}}</label>
                                <div class="col-9">
                                    <input type="number" class="form-control set-settings" value="{{$settings->off_pick_120_min ?? ''}}" id="offPick120Min" name="off_pick_120_min">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label for="offPick1500Min" class="col-3 col-form-label">{{__('150 Mins')}}</label>
                                <div class="col-9">
                                    <input type="number" class="form-control set-settings" value="{{$settings->off_pick_150_min ?? ''}}" id="offPick150Min" name="off_pick_150_min">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label for="pick180Min" class="col-3 col-form-label">{{__('180 Mins')}}</label>
                                <div class="col-9">
                                    <input type="number" class="form-control set-settings" value="{{$settings->off_pick_180_min ?? ''}}" id="pick180Min" name="off_pick_180_min">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
@section('script')
    <script src="{{adminAsset('libs/clockpicker/jquery-clockpicker.min.js')}}"></script>

    <script>
        $('.clockpicker').clockpicker();
        $('.set-settings').on('input',function (){
            if ($(this).val().length !== 0){
                var input_name = $(this).attr('name');
                var submit_url = "{{route('adminSettingsSave')}}";
                var option_group = "commission_settings";
                var formData = new FormData();
                formData.append('option_type', 'text');
                formData.append('option_group', option_group);
                formData.append('option_key', input_name);
                formData.append('option_value', $(this).val());
                makeAjaxPostFile(formData,submit_url,null).done(function (response){

                });
            }

        });
        $('.set-settings-clock').on('change',function (){
            if ($(this).val().length !== 0){
                var input_name = $(this).attr('name');
                var submit_url = "{{route('adminSettingsSave')}}";
                var option_group = "commission_settings";
                var formData = new FormData();
                formData.append('option_type', 'text');
                formData.append('option_group', option_group);
                formData.append('option_key', input_name);
                formData.append('option_value', $(this).val());
                makeAjaxPostFile(formData,submit_url,null).done(function (response){
                });
            }

        })

    </script>
@endsection
