 <div class="modal fade" id="imageCroperModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{__('File crop modal')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="img-container">
                                <img
                                    id="cropper-image"
                                    src=""
                                    alt="Picture"
                                    style="max-width: 50%!important"
                                />
                            </div>
                            <button class="btn btn-success" id="getCroppedCanvas">{{__('Crop Image')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
