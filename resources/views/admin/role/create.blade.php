<div class="card ajax-load">
    <div class="card-body">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h4 class="card-title">{{__('Role Add/Edit')}}</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <button class="btn btn-sm btn-info m-1 text-light update_route"><i class="fa fa-sync"></i> {{__('Update route list')}}</button>
                <button class="btn btn-sm btn-info m-1 text-light application_command" data-style="zoom-in" data-type="generate:routelist"><i class="fa fa-sync"></i> {{__('Generate route list')}}</button>
            </div>
        </div>

        <form class="role-form" novalidate method="post" action="{{route('saveRole')}}" id="role_form">
            <div class="form-group">
                <label>{{__('Module')}}</label>
                <select class="form-control" name="module_id" required id="userType">
                    <option value="">{{__('Select module')}}</option>
                    @foreach(MODULES as $module_id => $module_name)
                        <option value="{{$module_id}}" {{is_selected($role->module_id ?? '', $module_id)}}>{{__($module_name)}}</option>
                    @endforeach
                </select>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group">
                <label>{{__('Title')}}</label>
                <input type="text" name="title" class="form-control" value="{{$role->title ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            @php($role_action = isset($role) && !empty($role) ? explode('|',$role->actions) : [])
            <div class="form-group">
                <label>{{__('Select route')}}</label>
                <select multiple="multiple"  class="multi-select" id="my_multi_select1" name="routes[]" data-plugin="multiselect" required rows="100">
                    @if(isset($routes) && !empty($routes))
                        @foreach($routes as $route)
                            <option value="{{$route->id}}" {{in_array($route->id,$role_action) ? 'selected': ''}}>{{$route->name}}</option>
                        @endforeach
                    @endif
                </select>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <input type="hidden" name="id" value="{{$role->id ?? ''}}" id="role_id">
            <button class="btn btn-dark waves-effect waves-light submit_basic" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
            <button class="btn btn-outline-secondary waves-effect waves-light reset_from float-right" type="button" onclick="reset_form(false,resetAction);"><i class="fas fa-sync-alt"></i> {{__('Reset')}}</button>
        </form>
    </div>
</div>
<script src="{{adminAsset('libs/multiselect/js/jquery.multi-select.js')}}"></script>
<script src="{{adminAsset('js/pages/form-advanced.init.js')}}"></script>
<script>
    function resetAction(){
        $('#my_multi_select1').html('');
        $('#my_multi_select1').multiSelect('refresh');
    }
    $(document).on('click','.application_command',function (){
        Ladda.bind(this);
        var load = $(this).ladda();
        var type = $(this).data('type');
        swalConfirm("Do you really want to run this command ?").then(function (s) {
            if(s.value){
                var command_url = "{{route('runCommand')}}";
                var data = {
                    type : type
                };
                makeAjaxPost(data, command_url, load).done(function (response) {
                    if (response.success == true){
                        swalRedirect("{{Request::url()}}",response.message,'success');
                    }else {
                        swalError(response.message);
                    }
                    load.ladda('stop');
                });
            }else{
                load.ladda('stop');
            }
        })
    });
</script>
