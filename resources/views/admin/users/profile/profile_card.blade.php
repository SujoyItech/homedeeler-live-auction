<div class="card-box text-center">
    <img src="{{getUserAvatar($user)}}" class="rounded-circle avatar-xxl img-thumbnail"
         alt="profile-image">
    <h4 class="mb-0">{{$user->name ?? ''}}</h4>
    <p class="text-muted mb-0">{{ROLES[$user->role]}}</p>
    <p class="text-muted">{{__('Balance: ')}} {{$user->balance}}</p>
    @if($user->role == USER_SELLER)
        @if($user->is_seller == IS_SELLER )
            <span class="badge badge-success">{{__('Approved')}}</span>
        @else
            <button class="btn btn-xs btn-success approve_seller" data-id="{{$user->id}}" data-type="{{ACTIVE}}"> {{__('Approve')}}</button>
        @endif
    @endif
    @if($user->admin_verified == ACTIVE )
        <span class="badge badge-info">{{__('Verified')}}</span>
    @else
        <button class="btn btn-xs btn-info verify_user" data-id="{{$user->id}}" data-type="{{ACTIVE}}"> {{__('Verify')}}</button>
    @endif

    <div class="text-left mt-3">
        <h4 class="font-13 text-uppercase">{{__('About :')}}</h4>
        <hr>
        <p class="text-muted mb-2 font-13"><strong>{{__('Full Name :')}}</strong> <span class="ml-2">{{$user->name ?? ''}}</span></p>
        <p class="text-muted mb-2 font-13"><strong>{{__('Mobile :')}}</strong><span class="ml-2">{{$user->phone ?? '--'}}</span></p>
        <p class="text-muted mb-2 font-13"><strong>{{__('Email :')}}</strong> <span class="ml-2 ">{{$user->email ?? ''}}</span></p>
        <p class="text-muted mb-2 font-13"><strong>{{__('Date of birth :')}}</strong> <span class="ml-2 ">{{$user->date_of_birth ?? 'DD-MM-YYYY'}}</span></p>
        <p class="text-muted mb-2 font-13"><strong>{{__('Timezone :')}}</strong> <span class="ml-2 ">{{$user->time_zone ?? ''}}</span></p>
    </div>
</div> <!-- end card-box -->

<script>

    $(document).on('click','.approve_seller',function (){
        Ladda.bind(this);
        let load = $(this).ladda();
        let id = $(this).data('id');
        let is_seller = $(this).data('type');
        let confirm_text = '';
        if (is_seller === "{{ACTIVE}}"){
            confirm_text = '{{__('Do you want to approve this user as seller?')}}'
        }else {
            confirm_text = '{{__('Do you want to reject this user as seller ?')}}'
        }
        swalConfirm(confirm_text).then(function (s) {
            if(s.value){
                var data = {
                    id : id,
                    is_seller : is_seller
                };
                makeAjaxPost(data, "{{route('approveSeller')}}", load).done(function (response) {
                    if (response.success == true){
                        swalSuccess(response.message);
                        renderDataTable($('#seller_table'),data_url,data_column);
                    }else {
                        swalError(response.message);
                    }
                });
            }else{
                load.ladda('stop');
            }
        })
    });


    $(document).on('click','.verify_user',function (){
        Ladda.bind(this);
        let load = $(this).ladda();
        let id = $(this).data('id');
        let verify_status = $(this).data('type');
        let confirm_text = '';
        if (verify_status === "{{ACTIVE}}"){
            confirm_text = '{{__('Do you want to verify this user?')}}'
        }else {
            confirm_text = '{{__('Do you want to reject this user verification ?')}}'
        }
        swalConfirm(confirm_text).then(function (s) {
            if(s.value){
                var data = {
                    id : id,
                    admin_verified : verify_status
                };
                makeAjaxPost(data, "{{route('verifyUser')}}", load).done(function (response) {
                    if (response.success == true){
                        swalSuccess(response.message);
                        renderDataTable($('#seller_table'),data_url,data_column);
                    }else {
                        swalError(response.message);
                    }
                });
            }else{
                load.ladda('stop');
            }
        })
    })
</script>
