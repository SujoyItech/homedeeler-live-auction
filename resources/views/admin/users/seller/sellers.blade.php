@extends('admin.layouts.app',['menu'=>'users','sub_menu'=>'sellers'])
@section('content')
    <div class="col-lg-4 col-xl-4 col-md-4 col-sm-12 col-xs-12 add_edit">
        @include('admin.users.seller.seller_add')
    </div>
    <div class="col-lg-8 col-xl-8 col-md-8 col-sm-12 col-xs-12 list">
        @include('admin.users.seller.seller_list')
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            var data_url = "{{route('sellers')}}";
            var data_column = [
                {"data": "id", visible: false},
                {"data": "image", orderable: false, searchable: false},
                {"data": "name"},
                {"data": "contact_info"},
                {"data": "approve_status"},
                {"data": "verify_status"},
                {"data": "status"},
                {"data": "action",orderable: false, searchable: false}
            ];
            renderDataTable($('#seller_table'),data_url,data_column);
            submitOperation(submitResponse, 'submit_basic');
            editOperation(editResponse,"{{route('editSeller')}}");
            statusChangeOperation(function (response){
                if (response.success === true){
                    swalSuccess(response.message);
                }else {
                    swalError(response.message);
                }
                renderDataTable($('#bidder_table'), data_url, data_column);
            },'status_change',"{{route('userStatusChange')}}")

            function submitResponse(response, this_form){
                if (response.success == true) {
                    swalSuccess(response.message);
                    $('form :input').val('');
                    this_form.removeClass('was-validated')
                    renderDataTable($('#seller_table'),data_url,data_column);
                } else {
                    swalError(response.message);
                }
            }

            function editResponse(){
                submitOperation(submitResponse, 'submit_basic');
            }


            $(document).on('click','.approve_status_change',function (){
                Ladda.bind(this);
                let load = $(this).ladda();
                let id = $(this).data('id');
                let is_seller = $(this).data('type');
                let confirm_text = '';
                if (is_seller === "{{ACTIVE}}"){
                    confirm_text = '{{__('Do you want to approve this user as seller?')}}'
                }else {
                    confirm_text = '{{__('Do you want to reject this user as seller ?')}}'
                }
                swalConfirm(confirm_text).then(function (s) {
                    if(s.value){
                        var data = {
                            id : id,
                            is_seller : is_seller
                        };
                        makeAjaxPost(data, "{{route('approveSeller')}}", load).done(function (response) {
                            if (response.success == true){
                                swalSuccess(response.message);
                                renderDataTable($('#seller_table'),data_url,data_column);
                            }else {
                                swalError(response.message);
                            }
                        });
                    }else{
                        load.ladda('stop');
                    }
                })
            });

            $(document).on('click','.verify_status_change',function (){
                Ladda.bind(this);
                let load = $(this).ladda();
                let id = $(this).data('id');
                let verify_status = $(this).data('type');
                let confirm_text = '';
                if (verify_status === "{{ACTIVE}}"){
                    confirm_text = '{{__('Do you want to verify this user?')}}'
                }else {
                    confirm_text = '{{__('Do you want to reject this user verification ?')}}'
                }
                swalConfirm(confirm_text).then(function (s) {
                    if(s.value){
                        var data = {
                            id : id,
                            admin_verified : verify_status
                        };
                        makeAjaxPost(data, "{{route('verifyUser')}}", load).done(function (response) {
                            if (response.success == true){
                                swalSuccess(response.message);
                                renderDataTable($('#seller_table'),data_url,data_column);
                            }else {
                                swalError(response.message);
                            }
                        });
                    }else{
                        load.ladda('stop');
                    }
                })
            })

        });

    </script>
@endsection
