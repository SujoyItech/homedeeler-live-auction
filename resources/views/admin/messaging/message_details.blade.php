<div class="card ajax-load">
    <div class="card-body py-2 px-3 border-bottom border-light">
        <div class="media py-1">
            <img src="{{isset($message->sender_image) ? asset(get_image_path().'/'.$message->sender_image ?? '') : ''}}" class="mr-2 rounded-circle" height="36" onerror='this.src="{{adminAsset('images/users/avatar.png')}}"'>
            <div class="media-body">
                <h5 class="mt-0 mb-0 font-15">
                    <a href="#" class="text-reset">{{$message->sender_name ?? ''}}</a>
                </h5>
                <p class="mt-1 mb-0 text-muted font-12">
                    <small class="mdi mdi-circle text-success"></small> {{__('Online')}}
                </p>
            </div>
        </div>
    </div>
    <div class="card-body">
        <ul class="conversation-list custom-scrollbar-css" id="conversationListId">
            @if (isset($message_details) && !empty($message_details[0]))
                @foreach($message_details as $message_chat)
                    <li class="clearfix @if ($message_chat['sender'] !== $my_id) odd @endif">
                        <div class="chat-avatar">
                            <img src="{{asset(get_image_path().'/'.$message_chat['sender_image'])}}" class="rounded" onerror='this.src="{{adminAsset('images/users/avatar.png')}}"' />
                        </div>
                        <div class="conversation-text">
                            <div class="ctext-wrap">
                                <i>{{$message_chat['sender_name']}}</i>
                                <p>
                                    {{$message_chat['message']}}
                                </p>
                            </div>
                        </div>
                    </li>
                @endforeach
            @endif
        </ul>
        @if (isset($message_details) && !empty($message_details[0]))
            <div class="row">
                <div class="col">
                    <div class="mt-2 bg-light p-3 rounded">
                        <form method="post" action="{{route('sendMessage')}}" id="send_message_id" class="send_message_class">
                            <div class="row">
                                <div class="col mb-2 mb-sm-0">
                                    <input type="hidden" name="messaging_id" id="messaging_id" value="{{$message->id ?? ''}}">
                                    <input type="hidden" name="sender" id="sender" value="{{$message->receiver ?? ''}}">
                                    <input type="text" class="form-control border-0" placeholder="Enter your text" required name="message" id="message">
                                    <div class="invalid-feedback">
                                        {{__('Please enter your messsage')}}
                                    </div>
                                </div>
                                <div class="col-sm-auto">
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-dark chat-send btn-block"><i class='fe-send'></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <input type="hidden" name="receiver_name" id="sender_name" value="{{$message->receiver_name ?? ''}}">
                        <input type="hidden" name="receiver_image" id="sender_image" value="{{$message->receiver_image ?? ''}}">
                    </div>
                </div>
            </div>
        @else
            <div class="text-center">
                <span class="badge badge-danger p-1 font-12">{{__('No message received yet.')}}</span>
            </div>
        @endif
    </div>
</div>
<script>
    $(document).ready(function (){
        var count = parseInt("{{$count ?? 0}}");
        $("#conversationListId").scrollTop($("#conversationListId")[0].scrollHeight);
        $('#conversationListId').scroll(function(){
            if ($('#conversationListId').scrollTop() == 0){
                const data ={
                    auction_id : "{{$message->event_id ?? ''}}",
                    page : count
                }
               makeAjaxPostText(data,"{{route('loadAuctionMessagesByScroll')}}").done(function (response){
                   if (response.success == true){
                       const messages = response.data.message;
                       const message_array = messages.map(function (item){
                           const type = item.sender == "{{$my_id}}" ? '' : 'odd';
                           const image = "{{asset(get_image_path().'/')}}"+item.sender_image;
                           const default_image = "{{adminAsset('images/users/avatar.png')}}";
                           const name = item.sender_name;
                           const message = item.message;

                           return `<li class="clearfix ${type}">
                           <div class="chat-avatar">
                               <img src="${image}" class="rounded" onerror='this.src="${default_image}"' />
                           </div>
                           <div class="conversation-text">
                               <div class="ctext-wrap">
                                   <i>${name}</i>
                                   <p>${message}</p>
                               </div>
                           </div>
                       </li>`;
                       }).join('');

                       count ++;
                       $('#conversationListId').prepend(message_array);
                   }
               });
            }
        });
    })
    submitOperation(messageResponse,'chat-send');
    function messageResponse(response,this_form){
        if (response.success == true){
            let sender_name = $('#sender_name').val();
            let sender_image = $('#sender_image').val();
            let sender_message = $('#message').val();

            let sender_img_url = "{{asset(get_image_path())}}"+'/'+sender_image;
            let sender_default_img_url = "{{adminAsset('images/users/avatar.png')}}";

            let create_chat =  `<li class="clearfix">
                            <div class="chat-avatar">
                                <img src="${sender_img_url}" class="rounded" onerror='this.src="${sender_default_img_url}"' />
                                <i></i>
                            </div>
                            <div class="conversation-text">
                                <div class="ctext-wrap">
                                    <i>${sender_name}</i>
                                    <p>${sender_message}</p>
                                </div>
                            </div>
                        </li>`
            $('#conversationListId').append(create_chat);
            $('#conversationListId').scrollTop($('#conversationListId')[0].scrollHeight);
            this_form.find('#message').val('');
        }
    }


</script>



