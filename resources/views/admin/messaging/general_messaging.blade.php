@extends('admin.layouts.app',['menu'=>'auctions','sub_menu'=>'generalMessages'])
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-12">
                    <h4><i class="fas fa-boxes"></i> {{__('Message List')}}</h4>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="messageTable" class="table table-sm table-striped dt-responsive">
                    <thead>
                        <tr>
                            <th></th>
                            <th>{{__('Date')}}</th>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Email')}}</th>
                            <th>{{__('Message')}}</th>
                            <th>{{__('Status')}}</th>
                            <th>{{__('Action')}}</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Standard modal content -->
<div id="message-reply-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="standard-modalLabel">{{__('Reply Message')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form class="message-reply-form" novalidate id="message-reply-form-id" method="POST"
                    action="{{route('replyGeneralMessage')}}">
                    <input type="hidden" name="id" id="message_reply_id">
                    <div class="form-group">
                        <label for="subject">{{__('Subject')}}</label>
                        <input type="text" class="form-control" name="subject" required placeholder="{{__('Subject')}}">
                    </div>
                    <div class="form-group">
                        <label for="message">{{__('Message')}}</label>
                        <textarea class="form-control" name="message" required
                            placeholder="{{__('Write message here')}}"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary send_message"><i class="fa fa-reply"></i>
                            {{__('Reply')}}</button>
                        <button type="button" class="btn btn-light" data-dismiss="modal">{{__('Close')}}</button>
                    </div>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('script')
<script>
    $(document).ready(function() {
        resetValidation('message-reply-form')
        var data_url = "{{route('generalMessages')}}";
        var delete_url = "{{route('deleteGeneralMessage')}}";
        var data_column = [{
                "data": "id",
                visible: false
            },
            {
                "data": "created_at"
            },
            {
                "data": "name"
            },
            {
                "data": "email"
            },
            {
                "data": "message"
            },
            {
                "data": "status"
            },
            {
                "data": "action",
                orderable: false,
                searchable: false
            }
        ];
        renderDataTable($('#messageTable'), data_url, data_column, [
            [0, "desc"]
        ]);
        deleteOperation(function(response) {
            if (response.success == true) {
                swalSuccess(response.message);
                renderDataTable($('#messageTable'), data_url, data_column, [
                    [0, "desc"]
                ]);
            } else {
                swalError(response.message);
            }
        }, 'delete_item', delete_url);

    });

    $(document).on('click', '.reply_message', function() {
        let id = $(this).data('id');
        $('#message_reply_id').val(id);
        $('#message-reply-modal').modal('show');
    });

    submitOperation(submitResponse, 'send_message');

    function submitResponse(response, this_form) {
        if (response.success === true) {
            swalRedirect("{{Request::url()}}", response.message, 'success');
        } else {
            swalError(response.message);
        }
    }
</script>

@endsection
