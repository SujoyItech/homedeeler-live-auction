@extends('admin.layouts.app',['menu'=>'auction','sub_menu'=>'messaging'])
@section('content')
    <div class="col-md-4 col-xl-4 col-lg-4">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-4">{{__('Messaging')}}</h4>
                <ul class="nav nav-pills navtab-bg nav-justified">
                    <li class="nav-item">
                        <a href="#auction" data-toggle="tab" aria-expanded="true" class="nav-link show_tab active" data-type="auction">
                            {{__('Auction')}}
                        </a>
                    </li>
                    @if(\Illuminate\Support\Facades\Auth::user()->module_id == MODULE_SUPER_ADMIN || \Illuminate\Support\Facades\Auth::user()->module_id == MODULE_SUPER_ADMIN )
                        <li class="nav-item">
                            <a href="#admin" data-toggle="tab" aria-expanded="false" class="nav-link show_tab" data-type="admin">
                                {{__('Admin')}}
                            </a>
                        </li>
                    @endif
                </ul>
                <div class="tab-content">
                    <div class="tab-pane show active" id="auction">
                        <div class="row">
                            <div class="col">
                                <div class="auction_message custom-scrollbar-css">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="admin">
                        <div class="row">
                            <div class="col">
                                <div class="admin_message custom-scrollbar-css">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-8 col-xl-8 col-lg-8 show_message_details">

    </div>
@endsection

@section('script')
    <script src="{{adminAsset('js/notification.js')}}"></script>
    <script>
        $(document).ready(function (){
            getAuctionMessages();
        });

        $(document).on('click','.show_tab',function (){
            if ($(this).data('type') == 'auction'){
                getAuctionMessages();
            }else if ($(this).data('type') == 'admin'){
                getAdminMessages();
            }
        });

        $(document).on('click','.show_message',function (){
            let message_id = $(this).data('message-id');
            let data = {
                message_id : message_id,
            }
            $(this).find('.auction_seen_count').hide();
            $(this).find('.auction_seen').addClass('text-muted');
            $('.media').removeClass('bg-soft-success');
            $(this).find('.media').addClass('bg-soft-success');
            add_loader(false);
            makeAjaxPostText(data, "{{route('showMessageDetails')}}").done (function (response) {
                remove_loader();
                $('.show_message_details').html(response);
            });
        })

        function getAuctionMessages(){
            let type = 'auction';
            let data = {
                type : type
            }
            makeAjaxPost(data,"{{route('getEventMessages')}}").done(function (response){
                let auction_messages = response.event_messages;
                let auction_message_view = auction_messages.map(function (item,index){
                    let sender_image = item.sender_image ? "{{asset(get_image_path())}}"+"/"+item.sender_image : "{{adminAsset('images/users/avatar.png')}}";
                    let default_image = "{{adminAsset('images/users/avatar.png')}}";
                    if (item.seen == 0) {
                        return `<a href="javascript:void(0);" class="text-body show_message ${index == 0 ? 'first_auction_message' : ''}" data-message-id="${item.id}">
                                        <div class="media ${index == 0 ? 'bg-soft-success' : ''} border-radius-1 p-2">
                                            <img src="${sender_image}" class="mr-2 rounded-circle" height="42" onerror='this.src="${default_image}"' />
                                            <div class="media-body">
                                                <h5 class="mt-0 mb-0 auction_seen font-15">
                                                    ${item.auction_name}
                                                </h5>
                                                <p class="mt-1 mb-0 auction_seen font-12">
                                                    <span class="w-75">${item.sender_name}</span>
                                                </p>
                                            </div>
                                        </div>
                                    </a>`;
                    }else {
                        return `<a href="javascript:void(0);" class="text-body show_message ${index == 0 ? 'first_message' : ''}" data-message-id="${item.id}">
                            <div class="media ${index == 0 ? 'bg-soft-success' : ''} border-radius-1 p-2">
                                <img src="${sender_image}" class="mr-2 rounded-circle" height="42" onerror='this.src="${default_image}"' />
                                <div class="media-body">
                                    <h5 class="mt-0 mb-0 text-muted font-15">
                                        ${item.auction_name}
                                    </h5>
                                    <p class="mt-1 mb-0 text-muted font-12">
                                        <span class="w-75">${item.sender_name}</span>
                                    </p>
                                </div>
                            </div>
                        </a>`
                    }

                }).join('');
                $('.auction_message').html(auction_message_view);
                let data = {
                    message_id : $('.first_auction_message').data('message-id')
                }
                makeAjaxPostText(data, "{{route('showMessageDetails')}}").done (function (response) {
                    $('.show_message_details').html(response);
                    $('#conversationListId').scrollTop($('#conversationListId')[0].scrollHeight);
                });
            })
        }

        function getAdminMessages(){
            let type = 'admin';
            let data = {
                type : type
            }
            makeAjaxPost(data,"{{route('getEventMessages')}}").done(function (response){
                let admin_messages = response.event_messages;
                let admin_message_view = admin_messages.map(function (item,index){
                    let sender_image = item.sender_image ? "{{asset(get_image_path())}}"+"/"+item.sender_image : "{{adminAsset('images/users/avatar.png')}}"
                    if (item.seen == 0) {
                        return ` <a href="javascript:void(0);" class="text-body show_message ${index == 0 ? 'first_admin_message' : ''}" data-message-id="${item.id}">
                                        <div class="media ${index == 0 ? 'bg-soft-success' : ''} border-radius-1 p-2">
                                            <img src="${sender_image}" class="mr-2 rounded-circle" height="42" alt="" />
                                            <div class="media-body">
                                                <h5 class="mt-0 mb-0 auction_seen font-15">
                                                    ${item.sender_name}
                                                </h5>
                                            </div>
                                        </div>
                                    </a>`;
                    }else {
                        return `<a href="javascript:void(0);" class="text-body show_message ${index == 0 ? 'first_message' : ''}" data-message-id="${item.id}">
                            <div class="media ${index == 0 ? 'bg-soft-success' : ''} border-radius-1 p-2">
                                <img src="${sender_image}" class="mr-2 rounded-circle" height="42" alt="" />
                                <div class="media-body">
                                    <h5 class="mt-0 mb-0 text-muted font-15">
                                        ${item.sender_name}
                                    </h5>
                                </div>
                            </div>
                        </a>`
                    }

                }).join('');

                $('.admin_message').html(admin_message_view);
                let data = {
                    message_id : $('.first_admin_message').data('message-id')
                }
                makeAjaxPostText(data, "{{route('showMessageDetails')}}").done (function (response) {
                    $('.show_message_details').html(response);
                    $('#conversationListId').scrollTop($('#conversationListId')[0].scrollHeight);
                });
            })
        }

    </script>

@endsection
