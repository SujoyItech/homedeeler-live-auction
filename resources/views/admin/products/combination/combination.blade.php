@extends('admin.layouts.app',['menu'=>'product','sub_menu'=>'combination'])
@section('content')
    <div class="col-lg-4 col-xl-4 col-md-4 col-sm-12 col-xs-12 add_edit">
        @include('admin.products.combination.combination_add')
    </div>
    <div class="col-lg-8 col-xl-8 col-md-8 col-sm-12 col-xs-12 list">
        @include('admin.products.combination.combination_list')
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            var data_url = "{{route('combination')}}";
            var delete_url = "{{route('deleteCombination')}}";
            var edit_url = "{{route('editCombination')}}";
            var data_column = [
                {"data": "combination_type"},
                {"data": "name"},
                {"data": "class_name"},
                {"data": "color_code"},
                {"data": "action",orderable: false, searchable: false}
            ];
            renderDataTable($('#combination_table'),data_url,data_column);
            submitOperation(submitResponse, 'submit_basic');
            deleteOperation(deleteResponse,'delete_item',delete_url);
            editOperation(editResponse,edit_url);

            function submitResponse(response, this_form){
                if (response.success == true) {
                    swalSuccess(response.message);
                    resetLanguage('#name');
                    $('form :input').val('');
                    this_form.removeClass('was-validated')
                    renderDataTable($('#combination_table'),data_url,data_column);
                } else {
                    swalError(response.message);
                }
            }

            function editResponse(){
                submitOperation(submitResponse, 'submit_basic');
            }

            function deleteResponse(response){
                if(response.success == false) {
                    swalError(response.message);
                } else {
                    swalSuccess(response.message);
                    renderDataTable($('#combination_table'),data_url,data_column);
                }
            }
        });
    </script>
@endsection
