@extends('admin.layouts.app',['menu'=>'home','sub_menu'=>'home'])
@section('content')
    <div class="col-md-12">
        <h4>{{__('Product Admin')}}</h4>
    </div>
@endsection
@section('script')
    <script src="{{adminAsset('libs/jquery.counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{adminAsset('libs/waypoints/lib/jquery.waypoints.min.js')}}"></script>
@endsection

