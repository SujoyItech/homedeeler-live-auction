<div class="card ajax-load">
    <div class="card-body">
        <h4 class="card-title">{{__('Combination type Add/Edit')}}</h4>
        <div class="language-selector">
            @php
                $locales = config('app.locales');
            @endphp
            <div class="btn-group btn-group-sm" role="group" data-toggle="buttons">
                @foreach ($locales as $index => $locale)
                    <label class="btn {{ $index == 0 ? 'active btn-primary' : 'btn-secondary' }} {{ !isset($tag) ? ( $index != 0 ? 'd-none' : '' ) : ''}} language-btn-label">
                        <input type="radio" class="selected_language" name="i18n_selector" id="{{$locale}}" autocomplete="off" {{ $index == 0 ? 'checked' : '' }}> <span class="text-uppercase">{{$locale}}</span>
                    </label>
                @endforeach
            </div>
        </div>
        <form class="combination-type-form" novalidate method="post" action="{{route('storeCombinationType')}}" id="combination_type_form">
            <div class="form-group mb-3">
                @foreach ($locales as $index => $locale)
                    <div class="language_class {{ $index != 0 ? 'd-none' : '' }}" id="name{{$locale}}" data-type="{{$locale}}">
                        <label for="name">{{__('Name')}} ({{$locale}})  @if($index == 0) <span class="text-danger">*</span> @endif</label>
                        <input type="text" class="form-control" id="{{ $index == 0 ? 'name' : '' }}" name="name[{{$locale}}]"  placeholder="{{__('Color/Size')}}" value="{{$combination_type->translations['name'][$locale] ?? ''}}" {{ $index == 0 ? 'required' : '' }}>
                    </div>
                @endforeach
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <input type="hidden" name="id" value="{{$combination_type->id ?? ''}}">
            <button class="btn btn-dark waves-effect waves-light submit_basic" data-style="zoom-in" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
            <button class="btn btn-outline-secondary waves-effect waves-light reset_from float-right" type="button" onclick="reset_form(false,function (){})"><i class="fas fa-sync-alt"></i> {{__('Reset')}}</button>
        </form>
    </div>
</div>

<script>
    $(document).ready(function (){
        resetValidation('combination-type-form');
        addLanguage('#name');
    });

</script>

