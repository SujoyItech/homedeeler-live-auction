<!DOCTYPE html>
<html lang="en">
@php($settings = __options(['site_settings', 'logo_settings', 'social_settings']))
@php($menu = isset($menu) && !empty($menu) ? $menu : '')
@php($sub_menu = isset($sub_menu) && !empty($sub_menu)  ? $sub_menu : '')

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="laraframe" content="{{ csrf_token() }}"/>

    <title>
        {{$settings->app_title ?? env('APP_NAME')}}
        @if (trim($__env->yieldContent('title')))
            :: @yield('title')
        @endif
    </title>
    @include('web.includes.header_asset')
    <link rel="shortcut icon" href="{{adminAsset('images/favicon.svg')}}" type="image/x-icon">
</head>

<body>
<!--============= ScrollToTop Section Starts Here =============-->
<div class="overlayer" id="overlayer">
    <div class="loader">
        <div class="loader-inner"></div>
    </div>
</div>
<a href="#" class="scrollToTop"><i class="fas fa-angle-up"></i></a>
<div class="overlay"></div>
<!--============= ScrollToTop Section Ends Here =============-->
<!--============= Header Section Starts Here =============-->
@include('web.includes.header')
<!--============= Header Section Ends Here =============-->
<!--============= Cart Section Starts Here =============-->
{{--@include('web.includes.cart_sidebar')--}}
<!--============= Cart Section Ends Here =============-->
@yield('content')
<!--============= Footer Section Starts Here =============-->
@include('web.includes.footer')
<!--============= Footer Section Ends Here =============-->
@include('web.includes.footer_asset')
</body>
</html>
