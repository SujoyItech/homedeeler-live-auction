<section class="car-auction-section padding-bottom padding-top pos-rel oh">
{{--    <div class="car-bg"><img src="{{webAsset('images/auction/car/car-bg.png')}}" alt="car"></div>--}}
    <div class="container">
        <div class="section-header-3">
            <div class="left">
                <div class="thumb">
                    <img src="{{webAsset('images/header-icons/car-1.png')}}" alt="header-icons">
                </div>
                <div class="title-area">
                    <h2 class="title">{{__('Featured Items')}}</h2>
                    <p>{{__('Featured item list goes here.')}}</p>
                </div>
            </div>
            <a href="{{route('auctionList',['type'=>'featured'])}}" class="normal-button">{{__('View All')}}</a>
        </div>
        <div class="row justify-content-center mb-30-none" id="featured_auction">
            @if(isset($featured_items))
                @foreach($featured_items as $auction_list)
                    <div class="col-sm-10 col-md-6 col-lg-3 col-md-4">
                        @include('web.auctions.auction_card')
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</section>
