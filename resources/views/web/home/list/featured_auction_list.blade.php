@if(isset($auction_lists))
    @foreach($auction_lists as $auction_list)
        <div class="col-sm-10 col-md-6 col-lg-3 col-md-4">
            @include('web.auctions.auction_card')
        </div>
    @endforeach
@endif
