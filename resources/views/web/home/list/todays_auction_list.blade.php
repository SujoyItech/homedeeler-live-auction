@if(isset($auction_lists))
    @foreach($auction_lists as $auction_list)
        <div class="col-lg-6">
            <div class="auction-item-3">
                <div class="auction-thumb">
                    <a href="{{route('auction',['slug'=>$auction_list->slug])}}"><img src="{{check_storage_image_exists($auction_list->product_image)}}" onerror='this.src="{{webAsset('images/auction/product/10.png')}}"' alt="product"></a>
                    <a href="{{route('auction',['slug'=>$auction_list->slug])}}" class="bid"><i class="flaticon-auction"></i></a>
                </div>
                <div class="auction-content">
                    <h6 class="title">
                        <a href="{{route('auction',['slug'=>$auction_list->slug])}}">{{$auction_list->name}}</a>
                    </h6>
                    <div class="bid-amount">
                        <div class="icon">
                            <i class="flaticon-auction"></i>
                        </div>
                        <div class="amount-content">
                            <div class="current">{{__('Current Bid')}}</div>
                            <div class="amount">{{getMoney($auction_list->highest_bid)}}</div>
                        </div>
                    </div>
                    <div class="bids-area">
                        {{__('Total Bids :')}} <span class="total-bids">{{$auction_list->total_bids ?? 0}} {{__('Bids')}}</span>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif
