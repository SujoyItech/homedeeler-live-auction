@if(isset($electronic_categories) && !empty($electronic_categories['products']))
    <div class="col-xl-6 col-lg-8 mb-50">
        <div class="section-header-2">
            <div class="left">
                <div class="thumb">
                    <img src="{{webAsset('images/header-icons/camera-1.png')}}" alt="header-icons">
                </div>
                <h2 class="title">{{__('Electronics')}}</h2>
            </div>
            <div class="slider-nav">
                <a href="javascript:void(0);" class="electro-prev"><i class="flaticon-left-arrow"></i></a>
                <a href="javascript:void(0);" class="electro-next active"><i class="flaticon-right-arrow"></i></a>
            </div>
        </div>
        <div class="auction-slider-1 owl-carousel owl-theme mb-30-none">
            <div class="slide-item">
                @php($electronics_index = 0)
                @foreach($electronic_categories['products'] as $key=>$auction_list)
                    @if($key < 4)
                        @php($electronics_index++)
                        @include('web.auctions.auction_card_slide')
                    @else
                        @break(TRUE)
                    @endif
                @endforeach
            </div>
            <div class="slide-item">
                @foreach($electronic_categories['products'] as $key => $auction_list)
                    @if($key > $electronics_index)
                        @include('web.auctions.auction_card_slide')
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endif
@if(isset($art_categories) && !empty($art_categories['products']))
    <div class="col-xl-6 col-lg-8 mb-50">
        <div class="section-header-2">
            <div class="left">
                <div class="thumb">
                    <img src="{{webAsset('images/header-icons/art-1.png')}}" alt="header-icons">
                </div>
                <h2 class="title">{{__('Art')}}</h2>
            </div>
            <div class="slider-nav">
                <a href="javascript:void(0);" class="art-prev"><i class="flaticon-left-arrow"></i></a>
                <a href="javascript:void(0);" class="art-next active"><i class="flaticon-right-arrow"></i></a>
            </div>
        </div>
        <div class="auction-slider-2 owl-carousel owl-theme mb-30-none">
            @php($art_index = 0)
            <div class="slide-item">
                @foreach($art_categories['products'] as $key => $auction_list)
                    @if($key < 4)
                        @php($art_index++)
                        @include('web.auctions.auction_card_slide')
                    @else
                        @break(TRUE)
                    @endif
                @endforeach
            </div>
            <div class="slide-item">
                @foreach($art_categories['products'] as $key => $auction_list)
                    @if($key > $art_index)
                        @include('web.auctions.auction_card_slide')
                    @endif
                @endforeach
            </div>
        </div>
    </div>

@endif

<script>
    $('.auction-slider-1').owlCarousel({
        // loop:true,
        nav:false,
        dots: false,
        items:1,
        autoplay:false,
        autoHeight: false,
        margin: 30,
    });
    var owlOne = $('.auction-slider-1');
    owlOne.owlCarousel();
    // Go to the next item
    $('.electro-next').on('click', function() {
        owlOne.trigger('next.owl.carousel');
    })
    // Go to the previous item
    $('.electro-prev').on('click', function() {
        owlOne.trigger('prev.owl.carousel', [300]);
    })
    //Auction Slider
    $('.auction-slider-2').owlCarousel({
        // loop:true,
        nav:false,
        dots: false,
        items:1,
        autoplay:false,
        autoHeight: false,
        margin: 30,
    });
    var owlTwo = $('.auction-slider-2');
    owlTwo.owlCarousel();
    // Go to the next item
    $('.art-next').on('click', function() {
        owlTwo.trigger('next.owl.carousel');
    })
    // Go to the next item
    $('.art-prev').on('click', function() {
        owlTwo.trigger('prev.owl.carousel');
    });


</script>
