<div class="section-header-3">
    <div class="left">
        <div class="thumb">
            <img src="{{asset(get_image_path('category').'/'.$category_image)}}" alt="auction" onerror='this.src = "{{adminAsset('images/no-image.jpg')}}"' alt="header-icons">
        </div>
        <div class="title-area">
            <h2 class="title">{{$category_name}}</h2>
            <p>{{__('Find auctions for Homes, Condos, Residential & Commercial Properties.')}}</p>
        </div>
    </div>
    <a href="{{route('auctionList',['category_id'=>$category_id])}}" class="normal-button">{{__('View All')}}</a>
</div>
<div class="auction-slider-4 owl-theme owl-carousel">
    @if(isset($products))
        @foreach($products as $auction_list)
            @include('web.auctions.auction_card_real_estate')
        @endforeach
    @endif
</div>
<div class="slider-nav real-style ml-auto">
    <a href="javascript:void(0);" class="real-prev"><i class="flaticon-left-arrow"></i></a>
    <div class="pagination"></div>
    <a href="javascript:void(0);" class="real-next active"><i class="flaticon-right-arrow"></i></a>
</div>
<script>
    $('.auction-slider-4').owlCarousel({
        // loop: true,
        nav: false,
        dots: true,
        items: 1,
        autoplay: false,
        autoHeight: false,
        margin: 30,
        responsive:{
            768:{
                items: 2,
            },
            992:{
                items: 1,
            },
        }
    });
    var owlFour = $('.auction-slider-4');
    owlFour.owlCarousel();
    // Go to the next item
    $('.real-next').on('click', function() {
        owlFour.trigger('next.owl.carousel');
    })
    // Go to the previous item
    $('.real-prev').on('click', function() {
        owlFour.trigger('prev.owl.carousel', [300]);
    })
</script>
