@extends('web.layouts.app')
@section('content')
    <!--============= Banner Section Starts Here =============-->
    @include('web.home.banner')
    <!--============= Banner Section Ends Here =============-->

    <div class="browse-section ash-bg">
        @include('web.home.categories')
        @include('web.home.how_to_start')
        @if(isset($featured_items) && !empty($featured_items[0]))
            @include('web.home.featured')
        @endif
    </div>

{{--    <div class="browse-section ash-bg" id="primary_category_auction"></div>--}}

    <!--============= Popular Auction Section Starts Here =============-->
    @if(isset($today_auctions) && !empty($today_auctions[0]))
        @include('web.home.today_auction')
    @endif
    <!--============= Popular Auction Section Ends Here =============-->

    <!--============= Coins and Bullion Auction Section Starts Here =============-->
{{--    @include('web.home.coin')--}}
    <!--============= Coins and Bullion Auction Section Ends Here =============-->


    <!--============= Real Estate Section Starts Here =============-->
    @include('web.home.real_states')
    <!--============= Real Estate Section Starts Here =============-->


    <!--============= Art Auction Section Starts Here =============-->
    @include('web.home.electronics_art')
    <!--============= Art Auction Section Ends Here =============-->

    <!--============= Client Section Starts Here =============-->
    {{--    @include('web.home.clients')--}}
    <!--============= Client Section Ends Here =============-->


@endsection

@section('script')
    <script>
        $(document).ready(function (){
            loadElectronicsAndArtData();
            loadRealEstateData();
            countDownEndAuction();
        })
        function loadElectronicsAndArtData(){
            makeAjaxText("{{route('getElectronicAndArtAuctionList')}}").done(function (response){
                $('#electronics_art_list').html(response);
                countDownEndAuction();
            });
        }
        function loadRealEstateData(){
            makeAjaxText("{{route('getRealEstateAuctionList')}}").done(function (response){
                $('#real_estate_id').html(response);
            })
        }
    </script>
@endsection
