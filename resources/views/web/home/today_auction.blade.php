<section class="popular-auction padding-top pos-rel">
    <div class="popular-bg bg_img" data-background="{{webAsset('images/banner/banner-bg-3.png')}}"></div>
    <div class="container">
        <div class="section-header cl-black">
            <h2 class="title">{{__('Closing Within 24 Hours')}}</h2>
            <p>{{__('Bid and win great deals,Our auction process is simple, efficient, and transparent.')}}</p>
            <a href="{{route('auctionList',['type'=>'today_auctions'])}}" class="normal-button text-dark">{{__('View All')}}</a>
        </div>
        <div class="popular-auction-wrapper">
            <div class="row justify-content-center mb-30-none">
                @if(isset($today_auctions))
                    @foreach($today_auctions as $auction_list)
                        <div class="col-lg-6">
                            <div class="auction-item-3">
                                <div class="auction-thumb">
                                    <a href="{{route('auction',['slug'=>$auction_list->slug])}}"><img src="{{check_storage_image_exists($auction_list->product_image)}}" onerror='this.src="{{webAsset('images/auction/product/10.png')}}"' alt="product"></a>
                                    <a href="{{route('auction',['slug'=>$auction_list->slug])}}" class="bid"><i class="flaticon-auction"></i></a>
                                </div>
                                <div class="auction-content">
                                    <h6 class="title">
                                        <a href="{{route('auction',['slug'=>$auction_list->slug])}}">{{$auction_list->name}}</a>
                                    </h6>
                                    <div class="bid-amount">
                                        <div class="icon">
                                            <i class="flaticon-auction"></i>
                                        </div>
                                        <div class="amount-content">
                                            <div class="current">{{__('Current Bid')}}</div>
                                            <div class="amount">{{getMoney($auction_list->highest_bid)}}</div>
                                        </div>
                                    </div>
                                    <div class="bids-area">
                                        {{__('Total Bids :')}} <span class="total-bids">{{$auction_list->total_bids ?? 0}} {{__('Bids')}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</section>
