<div class="browse-slider-section mt--140" id="primary_category_section">
    <div class="container">
        <div class="section-header-2 cl-white mb-4">
            <div class="left">
                <h6 class="title pl-0 w-100">{{__('Browse the highlights')}}</h6>
            </div>
            <div class="slider-nav">
                <a href="javascript:void(0);" class="bro-prev"><i class="flaticon-left-arrow"></i></a>
                <a href="javascript:void(0);" class="bro-next active"><i class="flaticon-right-arrow"></i></a>
            </div>
        </div>
        <div class="m--15">
            <div class="browse-slider owl-theme owl-carousel">
                @if(isset($categories))
                    @foreach($categories as $category)
                        <a href="{{route('auctionList',['category_id'=>$category->id])}}" class="browse-item">
                            <img src="{{asset(get_image_path('category').'/'.$category->icon)}}" alt="auction" onerror='this.src = "{{adminAsset('images/no-image.jpg')}}"'>
                            <span class="info">{{$category->name}}</span>
                        </a>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
