<section class="banner-section bg_img mb-3" data-background="{{webAsset('images/banner/banner.jpg')}}">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-lg-6 col-xl-6">
                <div class="banner-content cl-white">
                    <h5 class="cate">{{__('Next Generation Auction')}}</h5>
                    <h1 class="title"><span class="d-xl-block">{{__('Find Your')}}</span> {{__('Next Deal!')}}</h1>
                    <p>
                        {{__('Online Auction is where everyone goes to shop, sell,and give, while discovering variety and affordability.')}}
                    </p>
                    <a href="{{route('auctionList')}}" class="custom-button yellow btn-large">{{__('Get Started')}}</a>
                </div>
            </div>
            <div class="d-none d-lg-block col-lg-6">
                <div class="banner-thumb-2">
                    <img src="{{webAsset('images/banner/banner-1.png')}}" alt="banner">
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="banner-shape d-none d-lg-block">
        <img src="{{webAsset('css/img/banner-shape.png')}}" alt="css">
    </div> --}}
</section>
