@if(isset($category_products))
    @foreach($category_products as $category_product)
        @if(!empty($category_product['products'][0]))
            <section class="jewelry-auction-section padding-bottom padding-top pos-rel" id="primary_category_auction">
                <div class="container">
                    <div class="section-header-3">
                        <div class="left">
                            <div class="thumb">
                                <img src="{{asset(get_image_path('category').'/'.$category_product['category_image'])}}" alt="auction" onerror='this.src = "{{adminAsset('images/no-image.jpg')}}"' alt="header-icons">
                            </div>
                            <div class="title-area">
                                <h2 class="title">{{$category_product['category_name']}}</h2>
                                <p>{{__('Online ')}} {{$category_product['category_name']}} {{__(' auctions where you can bid now and save money')}}'</p>
                            </div>
                        </div>
                        <a href="{{route('auctionList',['category_id'=>$category_product['category_id']])}}" class="normal-button">{{__('View All')}}</a>
                    </div>
                    <div class="row justify-content-center mb-30-none">
                        @if(isset($category_product['products']))
                            @foreach($category_product['products'] as $auction_list)
                                <div class="col-sm-10 col-md-6 col-lg-4">
                                    @include('web.auctions.auction_card')
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </section>
        @endif
    @endforeach
@endif

