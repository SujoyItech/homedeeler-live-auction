<section class="coins-and-bullion-auction-section padding-bottom padding-top pos-rel pb-max-xl-0">
    <div class="jewelry-bg d-none d-xl-block"><img src="{{webAsset('images/auction/coins/coin-bg.png')}}" alt="coin"></div>
    <div class="container">
        <div class="section-header-3">
            <div class="left">
                <div class="thumb">
                    <img src="{{webAsset('images/header-icons/coin-1.png')}}" alt="header-icons">
                </div>
                <div class="title-area">
                    <h2 class="title">Coins & Bullion</h2>
                    <p>Discover rare, foreign, & ancient coins that are worth collecting</p>
                </div>
            </div>
            <a href="javascript:void(0);" class="normal-button">View All</a>
        </div>
        <div class="row justify-content-center mb-30-none">
            <div class="col-sm-10 col-md-6 col-lg-4">
                <div class="auction-item-2">
                    <div class="auction-thumb">
                        <a href="./product-details.html"><img src="{{webAsset('images/auction/coins/auction-1.jpg')}}" alt="coins"></a>
                        <a href="javascript:void(0);" class="rating"><i class="far fa-star"></i></a>
                        <a href="javascript:void(0);" class="bid"><i class="flaticon-auction"></i></a>
                    </div>
                    <div class="auction-content">
                        <h6 class="title">
                            <a href="./product-details.html">Ancient & World Coins</a>
                        </h6>
                        <div class="bid-area">
                            <div class="bid-amount">
                                <div class="icon">
                                    <i class="flaticon-auction"></i>
                                </div>
                                <div class="amount-content">
                                    <div class="current">Current Bid</div>
                                    <div class="amount">$876.00</div>
                                </div>
                            </div>
                            <div class="bid-amount">
                                <div class="icon">
                                    <i class="flaticon-money"></i>
                                </div>
                                <div class="amount-content">
                                    <div class="current">Buy Now</div>
                                    <div class="amount">$5,00.00</div>
                                </div>
                            </div>
                        </div>
                        <div class="countdown-area">
                            <div class="countdown">
                                <div id="bid_counter17"></div>
                            </div>
                            <span class="total-bids">30 Bids</span>
                        </div>
                        <div class="text-center">
                            <a href="javascript:void(0);" class="custom-button">Submit a bid</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-10 col-md-6 col-lg-4">
                <div class="auction-item-2">
                    <div class="auction-thumb">
                        <a href="./product-details.html"><img src="{{webAsset('images/auction/coins/auction-2.jpg')}}" alt="coins"></a>
                        <a href="javascript:void(0);" class="rating"><i class="far fa-star"></i></a>
                        <a href="javascript:void(0);" class="bid"><i class="flaticon-auction"></i></a>
                    </div>
                    <div class="auction-content">
                        <h6 class="title">
                            <a href="./product-details.html">2018 Hyundai Sonata</a>
                        </h6>
                        <div class="bid-area">
                            <div class="bid-amount">
                                <div class="icon">
                                    <i class="flaticon-auction"></i>
                                </div>
                                <div class="amount-content">
                                    <div class="current">Current Bid</div>
                                    <div class="amount">$876.00</div>
                                </div>
                            </div>
                            <div class="bid-amount">
                                <div class="icon">
                                    <i class="flaticon-money"></i>
                                </div>
                                <div class="amount-content">
                                    <div class="current">Buy Now</div>
                                    <div class="amount">$5,00.00</div>
                                </div>
                            </div>
                        </div>
                        <div class="countdown-area">
                            <div class="countdown">
                                <div id="bid_counter18"></div>
                            </div>
                            <span class="total-bids">30 Bids</span>
                        </div>
                        <div class="text-center">
                            <a href="javascript:void(0);" class="custom-button">Submit a bid</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-10 col-md-6 col-lg-4">
                <div class="auction-item-2">
                    <div class="auction-thumb">
                        <a href="./product-details.html"><img src="{{webAsset('images/auction/coins/auction-3.jpg')}}" alt="coins"></a>
                        <a href="javascript:void(0);" class="rating"><i class="far fa-star"></i></a>
                        <a href="javascript:void(0);" class="bid"><i class="flaticon-auction"></i></a>
                    </div>
                    <div class="auction-content">
                        <h6 class="title">
                            <a href="./product-details.html">2018 Hyundai Sonata</a>
                        </h6>
                        <div class="bid-area">
                            <div class="bid-amount">
                                <div class="icon">
                                    <i class="flaticon-auction"></i>
                                </div>
                                <div class="amount-content">
                                    <div class="current">Current Bid</div>
                                    <div class="amount">$876.00</div>
                                </div>
                            </div>
                            <div class="bid-amount">
                                <div class="icon">
                                    <i class="flaticon-money"></i>
                                </div>
                                <div class="amount-content">
                                    <div class="current">Buy Now</div>
                                    <div class="amount">$5,00.00</div>
                                </div>
                            </div>
                        </div>
                        <div class="countdown-area">
                            <div class="countdown">
                                <div id="bid_counter19"></div>
                            </div>
                            <span class="total-bids">30 Bids</span>
                        </div>
                        <div class="text-center">
                            <a href="javascript:void(0);" class="custom-button">Submit a bid</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
