<section class="how-section padding-top my-3">
    <div class="container">
        <div class="how-wrapper section-bg">
            <div class="section-header text-lg-left">
                <h2 class="title text-light">How it works</h2>
                <p class="text-light">Easy 3 steps to win</p>
            </div>
            <div class="row justify-content-center mb--40">
                <div class="col-md-6 col-lg-4">
                    <div class="how-item">
                        <div class="how-thumb">
                            <img src="{{webAsset('images/how/how1.png')}}" alt="how">
                        </div>
                        <div class="how-content">
                            <h4 class="title text-light">Sign Up</h4>
                            <p class="text-light">No Credit Card Required</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="how-item">
                        <div class="how-thumb">
                            <img src="{{webAsset('images/how/how2.png')}}" alt="how">
                        </div>
                        <div class="how-content">
                            <h4 class="title text-light">Bid</h4>
                            <p class="text-light">Bidding is free Only pay if you win</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="how-item">
                        <div class="how-thumb">
                            <img src="{{webAsset('images/how/how3.png')}}" alt="how">
                        </div>
                        <div class="how-content">
                            <h4 class="title text-light">Win</h4>
                            <p class="text-light">Fun - Excitement - Great deals</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
