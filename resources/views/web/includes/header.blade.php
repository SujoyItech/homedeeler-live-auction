<header>
    <div class="header-top">
        <div class="container">
            <div class="header-top-wrapper">
                <ul class="customer-support">
                    <li>
                        <i class="fas fa-globe"></i>
                        <select name="language" class="select-bar">
                            <option value="en">{{__('En')}}</option>
                            <option value="no">{{__('No')}}</option>
                        </select>
                    </li>
                </ul>
                <ul class="cart-button-area">
                    <li class="text-right">
                        <form class="search-form mt-2" method="GET" action="{{route('auctionList')}}">
                            <input type="text" name="search_key" placeholder="Search for brand, model....">
                            <button type="submit"><i class="fas fa-search"></i></button>
                        </form>
                        <div class="search-bar d-md-none">
                            <a href="javascript:void(0);"><i class="fas fa-search"></i></a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="header-bottom">
        <div class="container">
            <div class="header-wrapper">
                <div class="logo">
                    <a href="{{route('home')}}">
                        <img src="{{adminAsset('images/logo-light.png')}}" alt="" height="">
                    </a>
                </div>
                <ul class="menu ml-auto">
                    <li>
                        <a href="{{route('home')}}">{{__('Home')}}</a>
                    </li>
                    <li>
                        <a href="{{route('auctionList')}}">{{__('Auctions')}}</a>
                    </li>
                    <li class="category_menu">
                    </li>
                    <li>
                        <a href="{{route('contact')}}">{{__('Contact')}}</a>
                    </li>
                    <li>
                        <a href="{{route('userDashboard')}}" class="user-button"><i class="flaticon-user"></i></a>
                    </li>
                </ul>
                <div class="header-bar d-lg-none">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </div>
</header>
<script>
    $(document).ready(function (){
        generateCategoryTree("{{route('showCategoryMenuToWeb')}}",function (response){
            let html = '<a href="javascript:void()">{{__('Categories')}}</a>';
            html += response;
            $('.category_menu').html(html);
        });
    })
</script>
