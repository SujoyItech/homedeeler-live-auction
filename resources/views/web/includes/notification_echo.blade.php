<script src="https://js.pusher.com/3.0/pusher.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/laravel-echo/1.8.1/echo.iife.min.js"></script>
<script src="{{adminAsset('js/notification.js')}}"></script>

<script>
    $(document).ready(function (){
        callEcho("{{env('LARAVEL_WEBSOCKETS_PORT',6020)}}", "{{env('PUSHER_APP_KEY', 'test')}}");
        receiveNotification('auction_send_in_live','auction-live',function (response){
            let title = response.title;
            let body = response.body;
            let auction_id = body.auction_id;
            if (auction_id.length > 0){
                Toast.fire({type: 'success', text: title});
                auction_id.forEach(function (item){
                    if ($('.check_for_auction_details_page_'+item).data('id')){
                        location.reload();
                    }else {
                        let class_name = '.'+'auction_in_live_'+item;
                        $(class_name).removeClass('d-none');
                    }

                });
            }
        });
        receiveNotification('auction_live_end','auction-live',function (response){
            let title = response.title;
            let body = response.body;
            let auction_id = body.auction_id;
            if (auction_id.length > 0){
                Toast.fire({type: 'success', text: title});
                auction_id.forEach(function (item){
                    if ($('.check_for_auction_details_page_'+item).data('id')){
                        location.reload();
                    }else {
                        let class_name = '.'+'auction_in_live_'+item;
                        $(class_name).addClass('d-none');
                    }

                });
            }
        });
    });


</script>
