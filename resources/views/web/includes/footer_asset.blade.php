{{--webAsset--}}

<script src="{{webAsset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{webAsset('js/modernizr-3.6.0.min.js')}}"></script>
<script src="{{webAsset('js/plugins.js')}}"></script>
<script src="{{webAsset('js/bootstrap.min.js')}}"></script>
<script src="{{webAsset('js/isotope.pkgd.min.js')}}"></script>
<script src="{{webAsset('js/wow.min.js')}}"></script>
<script src="{{webAsset('js/waypoints.js')}}"></script>
<script src="{{webAsset('js/nice-select.js')}}"></script>
<script src="{{webAsset('js/counterup.min.js')}}"></script>
<script src="{{webAsset('js/owl.min.js')}}"></script>
<script src="{{webAsset('js/magnific-popup.min.js')}}"></script>
<script src="{{webAsset('js/yscountdown.min.js')}}"></script>
<script src="{{webAsset('js/jquery-ui.min.js')}}"></script>
<script src="{{webAsset('js/main.js')}}"></script>
<script src="{{webAsset('js/homedeeler.js')}}"></script>
<script src="{{adminAsset('js/LaraframeScript.js')}}"></script>
<script src="{{adminAsset('js/crud.js')}}"></script>
<script src="{{adminAsset('vendors/ladda/spin.min.js')}}"></script>
<script src="{{adminAsset('vendors/ladda/ladda.min.js')}}"></script>
<script src="{{adminAsset('vendors/ladda/ladda.jquery.min.js')}}"></script>
<script src="{{adminAsset('libs/toastr/build/toastr.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/fontawesome.min.js"></script>
@yield('script')
<script>
    @if(Session::has('dismiss') && !empty(Session::get('dismiss')))
        Toast.fire({type: 'warning', text: '{{Session::get('dismiss')}}'});
    @endif
    @if(Session::has('success') && !empty(Session::get('success')))
        Toast.fire({type: 'success', text: '{{Session::get('success')}}'});
    @endif
</script>
<script>

</script>

