{{--webAsset--}}
<script src="{{webAsset('js/jquery-3.3.1.min.js')}}"></script>
<link rel="stylesheet" href="{{webAsset('css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{webAsset('css/all.min.css')}}">
<link rel="stylesheet" href="{{webAsset('css/animate.css')}}">
<link rel="stylesheet" href="{{webAsset('css/nice-select.css')}}">
<link rel="stylesheet" href="{{webAsset('css/owl.min.css')}}">
<link rel="stylesheet" href="{{webAsset('css/magnific-popup.css')}}">
<link rel="stylesheet" href="{{webAsset('css/flaticon.css')}}">
<link rel="stylesheet" href="{{webAsset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{webAsset('css/main.css')}}">
<link rel="stylesheet" href="{{webAsset('css/scroll.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/fontawesome.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<link href="{{adminAsset('vendors/ladda/ladda-themeless.min.css')}}" rel="stylesheet">
<script src="{{adminAsset('vendors/sweetalert/sweetalert2.all.min.js')}}"></script>
<link href="{{adminAsset('vendors/sweetalert/sweetalert.css')}}" rel="stylesheet">
<link href="{{adminAsset('libs/toastr/build/toastr.min.css')}}" rel="stylesheet" type="text/css" />

