@php($settings = __options(['site_settings','social_settings']))
<footer class="bg_img pt-3 oh" data-background="{{webAsset('images/footer/footer-bg.jpg')}}">
    @include('web.includes.notification_echo')
    <div class="footer-top-shape">
{{--        <img src="{{webAsset('css/img/footer-top-shape.png')}}" alt="css">--}}
    </div>
    <div class="anime-wrapper">
        <div class="anime-1 plus-anime">
            <img src="{{webAsset('images/footer/p1.png')}}" alt="footer">
        </div>
        <div class="anime-2 plus-anime">
            <img src="{{webAsset('images/footer/p2.png')}}" alt="footer">
        </div>
        <div class="anime-3 plus-anime">
            <img src="{{webAsset('images/footer/p3.png')}}" alt="footer">
        </div>
        <div class="anime-5 zigzag">
            <img src="{{webAsset('images/footer/c2.png')}}" alt="footer">
        </div>
        <div class="anime-6 zigzag">
            <img src="{{webAsset('images/footer/c3.png')}}" alt="footer">
        </div>
        <div class="anime-7 zigzag">
            <img src="{{webAsset('images/footer/c4.png')}}" alt="footer">
        </div>
    </div>
{{--    <div class="newslater-wrapper">--}}
{{--        <div class="container">--}}
{{--            <div class="newslater-area call-wrapper bg_img">--}}
{{--                <div class="section-header">--}}
{{--                    <h3 class="title">Register for Free & Start Bidding Now!</h3>--}}
{{--                    <p>From cars to diamonds to iPhones, we have it all.</p>--}}
{{--                </div>--}}
{{--                <a href="{{route('userRegister')}}" class="custom-register-button">Register</a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <div class="footer-top py-4">
        <div class="container">
            <div class="row mb--60">
                <div class="col-sm-6 col-lg-4">
                    <div class="footer-widget widget-links">
                        <h5 class="title">{{__('Auction Categories')}}</h5>
                        @php($categories = \App\Models\Product\Category::where(['parent_id'=>0,'status'=>STATUS_ACTIVE])->get())
                        <ul class="links-list">
                            @if(isset($categories))
                                @foreach($categories as $category)
                                    <li>
                                        <a href="{{route('auctionList',['category_id'=>$category->id])}}">{{$category->name}}</a>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="footer-widget widget-links">
                        <h5 class="title">{{__('We\'re Here to Help')}}</h5>
                        <ul class="links-list">
                            <li>
                                <a href="{{route('myProfile')}}">{{__('Your Account')}}</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">Safe and Secure</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">Shipping Information</a>
                            </li>
                            <li>
                                <a href="{{route('contact')}}">{{__('Contact Us')}}</a>
                            </li>
                            <li>
                                <a href="{{route('faqs')}}">{{__('Help & FAQ')}}</a>
                            </li>
                            <li>
                                <a href="{{route('login')}}">{{__('Admin Login')}}</a>
                            </li>
                            <li>
                                <a href="{{route('userLogin')}}">{{__('User Login')}}</a>
                            </li>
                            <li>
                                <a href="{{route('userLogin',['type'=>'seller'])}}">{{__('Seller Login')}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="footer-widget widget-follow">
                        <h5 class="title">{{__('Follow Us')}}</h5>
                        <ul class="links-list">
                            <li>
                                <a href="javascript:void(0);"><i class="fas fa-phone-alt"></i>{{$settings->contact_number ?? ''}}</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"><i class="fas fa-envelope-open-text"></i>{{$settings->contact_email ?? ''}}</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"><i class="fas fa-location-arrow"></i>{{$settings->address ?? ''}}</a>
                            </li>
                        </ul>
                        <ul class="social-icons">
                            <li>
                                <a href="{{$settings->facebook_link ?? ''}}" class="active"><i class="fab fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="{{$settings->twitter_link ?? ''}}"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="{{$settings->instagram_link ?? ''}};"><i class="fab fa-instagram"></i></a>
                            </li>
                            <li>
                                <a href="{{$settings->linkedin_link ?? ''}}"><i class="fab fa-linkedin-in"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="copyright-area">
                <div class="footer-bottom-wrapper">
                    <div class="logo">
                        <a href="index.html"><img src="{{adminAsset('images/logo-light.png')}}" alt="logo"></a>
                    </div>
                    <ul class="gateway-area">
                        <li>
                            <a href="javascript:void(0);"><img src="{{webAsset('images/footer/paypal.png')}}" alt="footer"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0);"><img src="{{webAsset('images/footer/visa.png')}}" alt="footer"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0);"><img src="{{webAsset('images/footer/discover.png')}}" alt="footer"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0);"><img src="{{webAsset('images/footer/mastercard.png')}}" alt="footer"></a>
                        </li>
                    </ul>
                    <div class="copyright"><p>&copy; Copyright 2021 | <a href="#">Homedeeler</a> By <a href="#">itech Soft Solutions</a></p></div>
                </div>
            </div>
        </div>
    </div>
</footer>
