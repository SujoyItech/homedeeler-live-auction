{{--{!! App\Http\Controllers\Web\Admin\Product\CategoryController::makeCategoryTreeDropdown($categoryTrees)!!}--}}
<ul class="submenu">
    @foreach ($categoryTrees as $val)
        @if(isset($val->children) && !empty($val->children[0]))
            <li>
                <a href="{{route('auctionList',['category_id'=>$val->id])}}">{{$val->name}}</a>
                <ul class="submenu">
                    @foreach ($val->children as $child)
                        <li>
                            <a href="{{route('auctionList',['category_id'=>$child->id])}}">{{$child->name}}</a>
                        </li>
                    @endforeach
                </ul>
            </li>
        @else
            <li>
                <a href="{{route('auctionList',['category_id'=>$val->id])}}">{{$val->name}}</a>
            </li>
        @endif
    @endforeach
</ul>
