@extends('web.layouts.app')
@section('content')

    <!--============= Hero Section Starts Here =============-->
    <div class="hero-section">
        <div class="bg_img hero-bg" data-background="{{webAsset('images/banner/auction-bg.jpg')}}"></div>
    </div>
    <!--============= Hero Section Ends Here =============-->
    <section class="account-section mt--240">
        <div class="container">
            <div class="account-wrapper mb-30">
                <div class="left-side">
                    <div class="section-header">
                        <h4 class="title">{{!empty($type) && $type == 'seller' ? __('Homedeeler Seller Registration') : __('Homedeeler User Registration')}}</h4>
                    </div>
                    <form class="login-form" method="post" action="{{route('userRegistrationSave')}}"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="is_seller"
                               value="{{!empty($type) && $type == 'seller' ? ACTIVE : INACTIVE}}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mb-30">
                                    <label for="name">
                                        <i class="far fa-user"></i><span class="text-danger">*</span>
                                    </label>
                                    <input type="text" name="name" id="name" placeholder="{{__('Name')}}"
                                           class="form-control {{$errors->first('name') ? 'border-danger' : ''}}"
                                           value="{{old('name')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mb-30">
                                    <label><i class="far fa-envelope"></i><span
                                            class="text-danger">*</span></label>
                                    <input type="email" name="email" placeholder="{{__('Email')}}"
                                           class="form-control {{$errors->first('email') ? 'border-danger' : ''}}"
                                           value="{{old('email')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="password"><i
                                            class="fas fa-lock"></i><span class="text-danger">*</span></label>
                                    <input type="password" id="password" placeholder="{{__('Enter your password')}}"
                                           class="form-control {{$errors->first('password') ? 'border-danger' : ''}}"
                                           name="password">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="password_confirmation"><i
                                            class="fas fa-lock"></i><span class="text-danger">*</span></label>
                                    <input type="password" id="password_confirmation"
                                           class="form-control {{$errors->first('password_confirmation') ? 'border-danger' : ''}}"
                                           placeholder="{{__('Confirm password')}}" name="password_confirmation">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="nid">{{__('Nid')}}</label>
                                    <input type="text" name="nid" id="nid"
                                           class="form-control {{$errors->first('nid') ? 'border-danger' : ''}}"
                                           value="{{old('nid')}}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{__('Nid Picture')}}</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="form-control custom-file-input"
                                                   name="nid_picture"
                                                   id="nid_picture">
                                            <label class="custom-file-label nid_picture_label" for="nid_picture"><i
                                                    class="fa fa-paperclip"></i> {{__('Upload NID Picture')}}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <input type="hidden" name="profile_photo_path" id="profile_photo_path_id">
                                <button class="btn btn-dark btn-block btn-sm" id="cameraOpen" type="button"><i
                                        class="fa fa-camera"></i> {{__('Take a Selfie')}}</button>
                            </div>
                        </div>
                        @if(Session::has('errors'))
                            @if($errors->first('name'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button data-style="zoom-in" type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    {!! $errors->first('name') !!}
                                </div>
                            @elseif($errors->first('email'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button data-style="zoom-in" type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    {!! $errors->first('email') !!}
                                </div>
                            @elseif($errors->first('password'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button data-style="zoom-in" type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    {!! $errors->first('password') !!}
                                </div>
                            @elseif($errors->first('password_confirmation'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button data-style="zoom-in" type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    {!! $errors->first('password_confirmation') !!}
                                </div>
                            @elseif($errors->first('nid'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button data-style="zoom-in" type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    {!! $errors->first('nid') !!}
                                </div>
                            @endif

                        @endif
                        <div class="form-group mb-0">
                            <button type="submit" class="custom-button">{{__('Sign Up')}}</button>
                        </div>
                    </form>
                </div>
                <div class="right-side cl-white">
                    <div class="section-header mb-0">
                        <h4 class="title mt-0">{{__('Already Registered?')}}</h4>
                        <p>{{__('Sign in and create your Account')}}</p>
                        <a href="{{route('userLogin')}}" class="custom-button transparent">{{__('Sign In')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('web.auth.image_capture_modal')
@endsection
@section('script')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css"
          integrity="sha512-In/+MILhf6UMDJU4ZhDL0R0fEpsp4D3Le23m6+ujDWXwl3whwpucJG1PEmI3B07nyJx+875ccs+yX2CqQJUxUw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"
            integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript" src="https://unpkg.com/webcam-easy/dist/webcam-easy.min.js"></script>
    <script>
        $(document).ready(function () {
            const webcamElement = document.getElementById('webcam');
            const canvasElement = document.getElementById('canvas');
            const snapSoundElement = document.getElementById('snapSound');
            const webcam = new Webcam(webcamElement, 'user', canvasElement, snapSoundElement);
            $(document).on('click', '#cameraOpen', function () {
                $('#image-capture-modal').modal('show');
                webcam.start()
                    .then(result => {
                        console.log("webcam started");
                    })
                    .catch(err => {
                        console.log(err);
                    });
            })
            $(document).on('click', '#click_shot', function () {
                let picture = webcam.snap();
                $('#profile_photo_path_id').val(picture);
                $('#image-capture-modal').modal('hide');
                $('#cameraOpen').html(`<i class="fa fa-check"></i> Selected`);
                webcam.stop();
            });
            $(document).on('click', '#close', function () {
                webcam.stop();
                $('#image-capture-modal').modal('hide');
            });
            $(document).on('click', '#flip', function () {
                webcam.flip();
                webcam.start();
            });
        });

        $(document).on('change', '#nid_picture', function () {
            $('.nid_picture_label').html(`<i class="fa fa-check"></i> Selected`);
        });

    </script>
@endsection
