@extends('web.layouts.app')
@section('content')
    <!--============= Hero Section Starts Here =============-->
    <div class="hero-section">
        <div class="bg_img hero-bg" data-background="{{webAsset('images/banner/auction-bg.jpg')}}"></div>
    </div>
    <!--============= Hero Section Ends Here =============-->
    <section class="account-section padding-bottom mt--240">
        <div class="container">
            <div class="account-wrapper">
                <div class="left-side">
                    <div class="section-header">
                        <h2 class="title">{{__('HI, THERE')}}</h2>
                        <p>{{__('You can log in to your home deeler account here.')}}</p>
                    </div>
                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button data-style="zoom-in" type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            {!! Session::get('message') !!}
                        </div>
                    @endif
                    @if(Session::has('dismiss'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button data-style="zoom-in" type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            {{Session::get('dismiss')}}
                        </div>
                    @endif
                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button data-style="zoom-in" type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            {{Session::get('success')}}
                        </div>
                    @endif
                    <ul class="login-with">
                        <li>
                            <a href="{{url('auth/facebook')}}"><i class="fab fa-facebook"></i>{{__('Log in with Facebook')}}</a>
                        </li>
                        <li>
                            <a href="{{url('auth/google')}}"><i class="fab fa-google-plus"></i>{{__('Log in with Google')}}</a>
                        </li>
                    </ul>
                    <div class="or">
                        <span>Or</span>
                    </div>
                    <form class="login-form" novalidate method="post" action="{{route('postUserLogin')}}">
                        @csrf
                        <div class="form-group mb-30">
                            <label for="email"><i class="far fa-envelope"></i></label>
                            <input type="email" name="email" id="email" placeholder="Email Address">
                            @if($errors->first('email'))
                                <span class="text-danger mt-1">{{$errors->first('email')}}</span> @endif
                        </div>
                        <div class="form-group">
                            <label for="login-pass"><i class="fas fa-lock"></i></label>
                            <input type="password" id="login-pass" placeholder="{{__('Enter your password')}}" name="password">
                            <span class="pass-type"><i class="fas fa-eye"></i></span>
                            @if($errors->first('password'))
                                <span class="text-danger mt-1">{{$errors->first('password')}}</span> @endif
                        </div>
                        <div class="form-group">
                            <a href="{{route('forgetPassword')}}">{{__('Forgot Password?')}}</a>
                        </div>
                        <div class="form-group mb-0">
                            <button type="submit" class="custom-button">{{__('LOG IN')}}</button>
                        </div>
                    </form>
                </div>
                <div class="right-side cl-white">
                    <div class="section-header mb-0">
                        <h3 class="title mt-0">{{__('NEW HERE?')}}</h3>
                        <p>{{__('Sign up and create your Account')}}</p>
                        @if(!empty($type))
                            <a href="{{route('userRegister',['type'=>$type])}}" class="custom-button transparent">{{__('Sign Up')}}</a>
                        @else
                            <a href="{{route('userRegister')}}" class="custom-button transparent">{{__('Sign Up')}}</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        resetValidation('login-form')
    </script>
@endsection
