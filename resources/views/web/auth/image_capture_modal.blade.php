<div class="modal fade" id="image-capture-modal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="video-section">
                        <video id="webcam" autoplay playsinline height="330" width="435"></video>
                        <canvas id="canvas" class="d-none"></canvas>
                        <div class="text-center">
                            <button type="button" class="btn btn-sm btn-dark m-1" id="click_shot"><i
                                    class="fa fa-camera"></i></button>
                            <button type="button" class="btn btn-sm btn-danger m-1" id="close"><i
                                    class="fa fa-window-close"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


