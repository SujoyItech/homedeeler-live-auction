<form method="post" action="{{ route('postLoginModal') }}" id="login-form-id" enctype="multipart/form-data" class="login-form-class">
    <div class="form-group mb-30">
        <input type="email" name="email" id="email" placeholder="Email Address">
    </div>
    <div class="form-group">
        <input type="password" id="password" placeholder="{{__('Enter your password')}}" name="password">
    </div>
    <div class="form-group">
        <a href="{{route('forgetPassword')}}">{{__('Forgot Password?')}}</a>
    </div>
    <div class="submit-btn">
        <button type="submit" class="btn btn-dark login_btn">{{__('Sign in')}}</button>
    </div>
    <div class="text-center mt-2">
        <ul class="login-with">
            <li>
                <a href="{{url('auth/facebook')}}"><i class="fab fa-facebook"></i>{{__('Log in with Facebook')}}</a>
            </li>
            <li>
                <a href="{{url('auth/google')}}"><i class="fab fa-google-plus"></i>{{__('Log in with Google')}}</a>
            </li>
        </ul>
    </div>
    <div class="col-12 sign-b">
        <p>{{__('Need An Account?')}} <a href="javascript:void(0)" class="signup_btn">{{__('Sign Up')}}</a></p>
    </div>
</form>

<script>
    $(document).on('click','.signup_btn',function (e){
        $('#loginModalLabel').html('Sing Up');
        $('#login').addClass('d-none');
        $('#register').removeClass('d-none');
    })
</script>
