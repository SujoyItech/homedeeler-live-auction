<form method="post" action="{{route('userRegistrationModal')}}" class="registration-form-class" id="registration-form-id">

    <input type="hidden" name="is_seller" value="{{!empty($type) && $type == 'seller' ? ACTIVE : INACTIVE}}">
    <div class="form-group">
        <label for="name">{{__('Name')}}</label>
        <input type="text" name="name" id="name" placeholder="{{__('Name')}}">
    </div>
    <div class="form-group">
        <label for="email">{{__('Email')}}</label>
        <input type="email" name="email" placeholder="{{__('Email')}}">
    </div>
    <div class="form-group">
        <label for="password">{{__('Password')}}</label>
        <input type="password" id="password" placeholder="{{__('Enter your password')}}" name="password">
    </div>
    <div class="form-group">
        <label for="password_confirmation">{{__('Confirm Password')}}</label>
        <input type="password" id="password_confirmation" placeholder="{{__('Confirm password')}}" name="password_confirmation">
    </div>
    <div class="form-group">
        <input type="hidden" name="profile_photo_path" id="profile_photo_path_id">
        <label>{{__('Profile Picture')}}</label>
        <button class="btn btn-dark btn-block btn-sm" id="cameraOpen" type="button">
            <i class="fa fa-camera"></i> {{__('Take a photo')}}</button>
    </div>
    <div class="form-group">
        <label>{{__('Nid Picture')}}</label>
        <div class="input-group">
            <div class="custom-file">
                <input type="file" class="custom-file-input" name="nid_picture" id="nid_picture">
                <label class="custom-file-label nid_picture_label" for="nid_picture"><i class="fa fa-paperclip"></i> {{__('Choose file')}}
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <a href="{{route('forgetPassword')}}">{{__('Forgot Password?')}}</a>
    </div>
    <div class="submit-btn mb-3">
        <button type="submit" class="btn btn-dark register_btn">{{__('Sign Up')}}</button>
    </div>
    <div class="col-12 sign-b">
        <p>{{__('Already have an account?')}}
            <a href="javascript:void(0);" class="login_btn">{{__('Return to Sign In')}}</a></p>
    </div>
</form>
@include('web.auth.image_capture_modal')
<script type="text/javascript" src="https://unpkg.com/webcam-easy/dist/webcam-easy.min.js"></script>
<script>
    $(document).on('click', '.login_btn', function (e) {
        $('#loginModalLabel').html('Login');
        $('#login').removeClass('d-none');
        $('#register').addClass('d-none');
    });
    $(document).ready(function () {
        const webcamElement = document.getElementById('webcam');
        const canvasElement = document.getElementById('canvas');
        const snapSoundElement = document.getElementById('snapSound');
        const webcam = new Webcam(webcamElement, 'user', canvasElement, snapSoundElement);
        $(document).on('click', '#cameraOpen', function () {
            $('#image-capture-modal').modal('show');
            webcam.start()
                .then(result => {
                    console.log("webcam started");
                })
                .catch(err => {
                    console.log(err);
                });
        })
        $(document).on('click', '#click_shot', function () {
            let picture = webcam.snap();
            let audioElement = document.createElement('audio');
            audioElement.setAttribute('src', '{{adminAsset('audio/audio.mp3')}}');
            audioElement.play();
            $('#profile_photo_path_id').val(picture);
            $('#image-capture-modal').modal('hide');
            $('#cameraOpen').html(`<i class="fa fa-check"></i> Selected`);
            webcam.stop();
        });
        $(document).on('click', '#close', function () {
            webcam.stop();
            $('#image-capture-modal').modal('hide');
        });
        $(document).on('click', '#flip', function () {
            webcam.flip();
            webcam.start();
        });

        $(document).on('change','#nid_picture',function (){
            $('.nid_picture_label').html(`<i class="fa fa-check"></i> Selected`);
        });
    });
</script>
