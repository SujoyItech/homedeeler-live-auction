@php($settings = __options(['admin_settings']))
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="loginModalLabel">{{__('Sign In')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="validation-errors-auth">
                </div>
                <div id="login">
                    @include('web.auth.login_modal_login')
                </div>
                <div id="register" class="d-none">
                    @include('web.auth.login_modal_signup')
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function (){
        submitOperationWithValidation(submitLoginResponse,'login_btn',validationErrorForAuth);
        submitOperationWithValidation(submitRegisterResponse,'register_btn',validationErrorForAuth);
    })

    function submitLoginResponse(response){
        if (response.success == true){
            swalRedirect("{{Request::url()}}",response.message,'success');
        }else{
            swalError(response.message);
        }
    }

    function submitRegisterResponse(response){
        if (response.success == true){
            swalRedirect("{{Request::url()}}",response.message,'success')
        }else{
            swalError(response.message);
        }
    }

    function validationErrorForAuth(response){
        let html = '';
        $.each(response.responseJSON.errors, function(key,value) {
            html += `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    ${value[0]}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>`;
        });
        $('.validation-errors-auth').html(html);
    }
</script>

