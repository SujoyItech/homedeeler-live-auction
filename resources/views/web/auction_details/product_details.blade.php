<div class="contact-wrapper px-1 py-3 " id="auction_details_id" data-id="{{$auction->id}}">
    <div class="col-lg-12 col-md-12 col-sm-12 mt-2">
        <h5 class="title">{{$auction->name}}</h5>
        {{__('Estimated Value')}} {{getMoney($auction->price_range_from)}} - {{getMoney($auction->price_range_to)}}

        @if ($auction->status < AUCTION_END)
            {!! $auction->is_new ? '<span class="badge badge-dark">NEW</span>' : '' !!}
            <div class="countdown mt-3">
                <h5 class="auction_end_date" id="auction_end_date_id" data-value="{{$auction->live_final_end_time}}">{{$auction->live_final_end_time ?? ''}}</h5>
            </div>
        @else
            <br>
            <span class="badge badge-danger p-1">{{__('Auction Ended')}}</span>
        @endif
        <h5 class="mt-2 price">{{__('Current Bid')}} : {{getMoney($auction->highest_bid)}}</h5>
        <h6 class="my-2 info">{{__('Total Bids')}}: <span id="bid_count">{{$bid_counts ?? 0}}</span></h6>
        <h6 class="my-2 info">{{__('Service Charge')}} : {{$auction->service_charge}}%</h6>
        <h6 class="my-2 info">{{__('Bid increment')}} : {{getMoney($auction->bid_increment)}}</h6>

        <div class="product-details-content">
            @php($check_bid = \App\Models\Auction\AuctionDetail::where(['auction_id' => $auction->id ?? '', 'bidder_id'=> \Illuminate\Support\Facades\Auth::user()->id ?? ''])->orderBy('created_at','desc')->first())
            @if(isset($check_bid))
                <h5 class="my-2 info text-success">{{__('My Bid')}} : {{getMoney($check_bid->bid_price)}}</h5>
            @else
                @if($auction->status < AUCTION_END && $auction->live_final_end_time >= \Carbon\Carbon::now())
                    <div class="product-bid-area">
                        <form class="product-bid-form" novalidate id="product-bid-form-id" action="{{route('submitBid')}}" method="post">
                            <input type="hidden" name="auction_id" value="{{$auction->id}}">
                            <input type="hidden" name="bidder_id" value="{{Auth::user()->id ?? ''}}">
                            <div class="form-group mr-3">
                                <select class="bid_input bid_input_option" name="bid_price_options">
                                    <option value="">{{__('Select Bid Amount')}}</option>
                                    @php($highest_bid = !empty($auction->highest_bid) ? $auction->highest_bid : $auction->base_price)
                                    @for($i=1;$i<=10;$i++)
                                        @php($bid = $highest_bid + $auction->bid_increment*$i)
                                        <option value="{{$bid}}">{{getMoney($bid)}}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="number" name="bid_price" class="bid_input" placeholder="Enter Custom Bid">
                            </div>
                            <div class="form-group ml-2">
                                @if(isset(Auth::user()->id))
                                    @if(\Illuminate\Support\Facades\Auth::user()->admin_verified == TRUE)
                                        <button type="submit" class="custom-button submit_bid" data-style="zoom-in">{{__('Submit a bid')}}</button>
                                    @else
                                        @if(getUserProfileImage() == '' || Auth::user()->nid_picture == '')
                                            <button class="custom-button profile_update_page">{{__('Update Profile to bid')}}</button>
                                        @else
                                            <button type="button" class="custom-button admin_verification" data-style="zoom-in">{{__('Submit a bid')}}</button>
                                        @endif
                                    @endif
                                @else
                                    <button type="button" class="custom-button login" data-style="zoom-in"><i class="fa fa-sign-in-alt"></i> {{__('Login to bid')}}</button>
                                @endif
                            </div>

                        </form>
                        <div class="validation-errors">

                        </div>
                    </div>
                    @endif
                @endif
            <div class="buy-now-area justify-center">
                <div class="share-area">
                    <span>{{__('Share with ')}}</span>
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/sharer/sharer.php?u={{route('auction',['slug'=>$auction->slug])}}"><i class="fab fa-facebook-f"></i></a>
                        </li>
                        <li>
                            <a href="https://twitter.com/intent/tweet?text=my share text&amp;url={{route('auction',['slug'=>$auction->slug])}}"><i class="fab fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{route('auction',['slug'=>$auction->slug])}}&amp;"><i class="fab fa-linkedin-in"></i></a>
                        </li>
                        <li>
                            <a href="https://wa.me/?text={{route('auction',['slug'=>$auction->slug])}}"><i class="fab fa-whatsapp"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@include('web.auth.login_modal')

@push('custom-script')
<script>
    $(document).on('click','.login',function (){
        $('#login-modal').modal('show');
    })
    $(document).ready(function (){
        let auction_channel_name = 'live_auction_message_'+"{{$auction->id ?? ''}}"
        receiveNotification(auction_channel_name,'bid_event',function (response){
            let body = response.body;
            const id = $('#auction_details_id').data('id');
            if (body.auction_id == id){
                Toast.fire({type: 'success', text: body.message});
                location.reload();
            }
        });

        submitOperationWithValidation(submitResponse,'submit_bid',validationError);
        function submitResponse(response){
            if (response.success == true){
                swalRedirect("{{Request::url()}}",response.message,'success');
            }else {
                swalError(response.message);
            }
        }
        function validationError(response){
            let html = '';
            $.each(response.responseJSON.errors, function(key,value) {
                html += `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    ${value[0]}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>`;
            });
            $('.validation-errors').html(html);
        }

        submitOperation(function (response){
            if (response.success == true){
                swalRedirect("{{route('auction',['slug'=>$auction->slug])}}",response.message,'success');
            }else {
                swalError(response.message);
            }
        },'submit_profile')
    });

    $(document).on('change','.bid_input_option',function (){
        if ($(this).val() == 'others'){
            $('.bid_input').val('');
        }else {
            $('.bid_input').val($(this).val());
        }
    });
</script>
@endpush


