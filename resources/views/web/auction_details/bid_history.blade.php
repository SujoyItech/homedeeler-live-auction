<div class="tab-pane fade show" id="history">
    <div class="history-wrapper">
        <div class="item">
            <div class="history-table-area">
                <table class="history-table">
                    <thead>
                    <tr>
                        <th>{{__('Bidder')}}</th>
                        <th>{{__('date')}}</th>
                        <th>{{__('time')}}</th>
                        <th>{{__('unit price')}}</th>
                    </tr>
                    </thead>
                    <tbody id="data-wrapper">
                    </tbody>
                </table>
                <div class="auto-load text-center mt-2">{{__('Loading..')}}</div>
                <div class="text-center mb-3 mt-4">
                    <a href="javascript:void(0)" class="button-3 load_more">{{__('Load More')}}</a>
                </div>
            </div>
        </div>
    </div>
</div>
