<link rel="stylesheet" type="text/css" href="{{adminAsset('agora/css/style.css')}}"/>
<style>
    #onscreen_button{
    position: absolute;
    top: 80%;
    font-size: 35px;
    color: #6e59599e;
    z-index: 1;
    }
</style>
<div class="card">
    <div id="full-screen-video" class="d-flex align-items-center justify-content-center">
        <button class="btn btn-dark btn-xlg justify-content-center" id="startLive"><i class="fas fa-video" aria-hidden="true"></i> {{__('Enter Live')}}</button>

        <div class="justify-content-center d-none" id="loadingBuffer">
            <div class="spinner-border text-dark"  style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <span class="" id="onscreen_button"><img src="{{adminAsset('images/logo-light.png')}}"></span>
    </div>
</div>
@push('custom-script')
    <script src="https://cdn.agora.io/sdk/web/AgoraRTCSDK-2.8.0.js"></script>
    <script>
        const agoraAppId = "{{env('AGORA_APP_ID')}}"; // set app id
        const channelName = "{{$auction->slug}}"; // set channel name
        const options = {
            client : null
        }
        //
        $(document).on('click','#startLive',function (){
            $('#startLive').addClass('d-none');
            $('#loadingBuffer').removeClass('d-none');
            startStreaming();
        });

        /**
         * Agora Broadcast Client
         */
        function startStreaming(){
            // create client
            const client = AgoraRTC.createClient({ mode: "live", codec: "vp8" }); // vp8 to work across mobile devices
            options.client = client;
            // set log level:
            // -- .DEBUG for dev
            // -- .NONE for prod
            AgoraRTC.Logger.setLogLevel(AgoraRTC.Logger.DEBUG);

            client.init(
                agoraAppId,
                function() {
                    $("#watch-live-overlay").remove();
                    console.log("AgoraRTC client initialized");
                    joinChannel(); // join channel upon successfull init
                },
                function(err) {
                    console.log("[ERROR] : AgoraRTC client init failed", err);
                }
            );

            client.on("stream-published", function(evt) {
                console.log("Publish local stream successfully");
            });

            // connect remote streams
            client.on("stream-added", function(evt) {
                let stream = evt.stream;
                let streamId = stream.getId();
                console.log("New stream added: " + streamId);
                console.log("Subscribing to remote stream:" + streamId);
                // Subscribe to the stream.
                client.subscribe(stream, function(err) {
                    console.log("[ERROR] : subscribe stream failed", err);
                });
            });

            client.on("stream-removed", function(evt) {
                let stream = evt.stream;
                stream.stop(); // stop the stream
                stream.close(); // clean up and close the camera stream
                console.log("Remote stream is removed " + stream.getId());
                $('#loadingBuffer').removeClass('d-none');
                leaveChannel();
            });

            client.on("stream-subscribed", function(evt) {
                let remoteStream = evt.stream;
                remoteStream.play("full-screen-video");
                $('#loadingBuffer').addClass('d-none');
                console.log(
                    "Successfully subscribed to remote stream: " + remoteStream.getId()
                );
            });

            // remove the remote-container when a user leaves the channel
            client.on("peer-leave", function(evt) {
                console.log("Remote stream has left the channel: " + evt.uid);
                evt.stream.stop(); // stop the stream
                leaveChannel();
            });

            // show mute icon whenever a remote has muted their mic
            client.on("mute-audio", function(evt) {
                let remoteId = evt.uid;
            });

            client.on("unmute-audio", function(evt) {
                let remoteId = evt.uid;
            });

            // show user icon whenever a remote has disabled their video
            client.on("mute-video", function(evt) {
                let remoteId = evt.uid;
            });

            client.on("unmute-video", function(evt) {
                let remoteId = evt.uid;
            });

            // ingested live stream
            client.on("streamInjectedStatus", function(evt) {
                console.log("Injected Steram Status Updated");
                // evt.stream.play('full-screen-video');
                console.log(JSON.stringify(evt));
            });

            // join a channel
            function joinChannel() {
                const token = "{{$auction->streaming_token}}";
                const userID = null;

                // set the role
                client.setClientRole(
                    "audience",
                    function() {
                        console.log("Client role set to audience");
                    },
                    function(e) {
                        console.log("setClientRole failed", e);
                    }
                );

                client.join(
                    token,
                    channelName,
                    userID,
                    function(uid) {
                        console.log("User " + uid + " join channel successfully");
                    },
                    function(err) {
                        console.log("[ERROR] : join channel failed", err);
                        $('#startLive').removeClass('d-none');
                        $('#loadingBuffer').addClass('d-none');
                    }
                );
            }
        }

        function leaveChannel() {
            options.client.leave(
                function() {
                    window.location.reload();
                    console.log("client leaves channel");
                },
                function(err) {
                    console.log("client leave failed ", err); //error handling
                }
            );
        }

    </script>
@endpush



