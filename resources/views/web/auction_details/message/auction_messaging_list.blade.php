@if(isset($messages))
    @foreach($messages as $message)
        @if($message['sender'] == \Illuminate\Support\Facades\Auth::user()->id)
            <div class="client-item w-100 p-2">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-1 pl-0">
                                <img src="{{asset(get_image_path('user').'/'.$message['profile_photo_path'])}}" alt="client" class="rounded-circle avatar-lg mb-2" width="40" height="40" onerror='this.src="{{asset(get_image_path().'avatar.png')}}"'>
                            </div>
                            <div class="col-11 pl-0">
                                <b class="p-0 ml-1">{{$message['name']}}</b><br>
                                <span class="mr-1 ml-1">{{$message['message']}}</span>
                            </div>
                        </div>
                    </div>
            </div>
        @else
            <div class="client-item w-100 p-2">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-11 text-right pr-0">
                                <b class="p-0 ml-1">{{$message['name']}}</b><br>
                                <span class="mr-1 ml-1">{{$message['message']}}</span>
                            </div>
                            <div class="col-1 pr-1 text-left">
                                <img src="{{asset(get_image_path('user').'/'.$message['profile_photo_path'])}}" alt="client" class="rounded-circle avatar-lg mb-2" width="40" height="40" onerror='this.src="{{asset(get_image_path().'avatar.png')}}"'>
                            </div>
                        </div>
                    </div>
            </div>
        @endif
    @endforeach
@endif
