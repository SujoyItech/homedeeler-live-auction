@if(isset($live_messages) && !empty($live_messages[0]))
    @foreach($live_messages as $message)
    <div class="chat">
        <img src="{{$message->user_image ?? ''}}" alt="" class="chat_avatar" onerror='this.src="{{asset(get_image_path().'avatar.png')}}"'>
        <div class="chat_content" @if(isset($message->message_image) && !empty($message->message_image)) style="width: 60%" @endif>
            @if(isset($message->message) && !empty($message->message))
            <div class="chat-message">
                <span>{{$message->message}}</span><br />
                @if(isset($message->message_image) && !empty($message->message_image))
                    <img src="{{$message->message_image}}" />
                @endif
            </div>
            <div class="chat-time">
                <i><small>{{$message->created_at}}</small></i>
            </div>
            @endif
        </div>
    </div>
    @endforeach
@endif

