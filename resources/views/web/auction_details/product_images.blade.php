{{--@dd($product_images)--}}
<div class="product-details-slider-top-wrapper">
    <div class="product-details-slider owl-theme owl-carousel" id="sync1">
        @if(isset($product_images))
            @foreach($product_images as $product_image)
                @if(!empty($product_image->media_url))
                    <div class="slide-top-item">
                        <div class="slide-inner">
                            <img src="{{check_storage_image_exists($product_image->media_url)}}" onerror='this.src="{{adminAsset('images/no-image.jpg')}}"' alt="product" alt="product">
                        </div>
                    </div>
                @endif
            @endforeach
        @endif
    </div>
</div>
<div class="product-details-slider-wrapper">
    <div class="product-bottom-slider owl-theme owl-carousel" id="sync2">
        @if(isset($product_images))
            @foreach($product_images as $product_image)
                @if(!empty($product_image->media_url))
                    <div class="slide-bottom-item">
                        <div class="slide-inner">
                            <img src="{{check_storage_image_exists($product_image->media_url)}}" onerror='this.src="{{adminAsset('images/no-image.jpg')}}"' alt="product" alt="product">
                        </div>
                    </div>
                @endif
            @endforeach
        @endif
    </div>
    <span class="det-prev det-nav"><i class="fas fa-angle-left"></i></span>
    <span class="det-next det-nav active"><i class="fas fa-angle-right"></i></span>
</div>
