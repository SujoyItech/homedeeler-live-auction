<div class="row">
    <div class="col-md-5">
        <div class="dash-pro-item mb-30 dashboard-widget">
            <form
                method="post"
                id="auction-message-id"
                class="auction-message-form"
                action="{{ route('sendAuctionMessage') }}"
            >
                <div class="form-group">
                    <input
                        type="hidden"
                        name="id"
                        id="messaging_id"
                        value="{{$auction_messaging->id ?? ''}}"
                    />
                    <input
                        type="hidden"
                        name="event_id"
                        id="event_id"
                        value="{{$auction->id}}"
                    />
                    <input
                        type="hidden"
                        name="receiver"
                        id="receiver"
                        value="{{$auction->seller_id}}"
                    />
                    <textarea
                        class="form-control auction_message"
                        rows="2"
                        name="message"
                        id="message"
                        placeholder="Ask about this auction" required
                    ></textarea>
                    <button
                        class="btn btn-dark btn-sm btn-block send_message mt-3"
                        type="submit"
                    >
                        <i class="fa fa-share-square"></i> {{ __("Send") }}
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-7">
        <div class="dashboard-widget">
            <div class="row justify-content-center custom-scrollbar-css show_message_list" id="auctionMessageListId"></div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var count = 0;
        $("#auctionMessageListId").scroll(function () {
            if ($("#auctionMessageListId").scrollTop() == 0) {
                const data = {
                    auction_id: "{{$auction->id}}",
                    page: count,
                };
                makeAjaxPostText(
                    data,
                    "{{route('loadAuctionMessagesByScroll')}}"
                ).done(function (response) {
                    if (response.success == true) {
                        const messages = response.data.message;
                        const message_array = messages
                            .map(function (item) {
                                const type =
                                    item.sender ==
                                    "{{\Illuminate\Support\Facades\Auth::user()->id ?? ''}}"
                                        ? ""
                                        : "odd";
                                const image =
                                    "{{asset(get_image_path().'/')}}" +
                                    item.sender_image;
                                const default_image =
                                    "{{adminAsset('images/users/avatar.png')}}";
                                const name = item.sender_name;
                                const message = item.message;

                                if (
                                    item.sender ==
                                    "{{\Illuminate\Support\Facades\Auth::user()->id}}"
                                ) {
                                    return `<div class="client-item w-100 p-2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="content text-left">
                                            <img src="${image}" alt="client" class="rounded-circle avatar-lg mb-2" width="50" height="50" onerror='this.src="${default_image}"'><span class="p-0 ml-1"></span><br>
                                            <span class="mr-1 ml-1">${message}</span>
                                    </div>
                                </div>
                            </div>
                         </div>`;
                                } else {
                                    return `<div class="client-item w-100 p-2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="content text-right">
                                            <img src="${image}" alt="client" class="rounded-circle avatar-lg mb-2 w-10" width="50" height="50" onerror='this.src="${default_image}"'><span class="p-0 ml-1"></span><br>
                                            <span class="ml-1 mr-1">${message}</span>
                                    </div>
                                </div>
                            </div>
                         </div>`;
                                }
                            })
                            .join("");

                        $("#auctionMessageListId").prepend(message_array);
                        count++;
                    }
                });
            }
        });
    });
</script>
