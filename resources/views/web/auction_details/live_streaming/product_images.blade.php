<link href="{{webAsset('css/live_chat.css')}}" rel="stylesheet" type="text/css" />

<div class="product-details-slider-top-wrapper">
    <div class="product-details-slider owl-theme owl-carousel" id="sync1">
        @if(isset($product_images))
            @foreach($product_images as $product_image)
                @if(!empty($product_image->media_url))
                    @php($image_url = check_storage_image_exists($product_image->media_url))
                    <div class="slide-top-item">
                        <div class="slide-inner">
                            <div class="wrapper" style="background: url('{{$image_url}}') center/cover no-repeat;">
                                <div class="chitchats dark_overlay">
                                    @if(isset(\Illuminate\Support\Facades\Auth::user()->id))
                                        <form method="post" action="{{route('sendLiveMessageReply')}}" class="live_message_form_class">
                                            <div class="input-group mb-0">
                                                <input type="hidden" name="user_id" value="{{\Illuminate\Support\Facades\Auth::user()->id}}">
                                                <input type="hidden" name="user_name" value="{{\Illuminate\Support\Facades\Auth::user()->name}}">
                                                <input type="hidden" name="user_image" value="{{getUserAvatar(\Illuminate\Support\Facades\Auth::user())}}">
                                                <input type="hidden" name="auction_id" value="{{$auction->id}}">
                                                <input type="text" class="form-control reply_message" name="message" placeholder="{{__('Reply')}}">
                                            </div>
                                        </form>
                                    @endif
                                    <!-- chats start -->
                                    <div class="live-chat-list">

                                    </div>
                                    <!-- chats end -->
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        @endif
    </div>
</div>
<div class="product-details-slider-wrapper">
    <div class="product-bottom-slider owl-theme owl-carousel" id="sync2">
        @if(isset($product_images))
            @foreach($product_images as $product_image)
                @if(!empty($product_image->media_url))
                    <div class="slide-bottom-item">
                        <div class="slide-inner">
                            <img src="{{check_storage_image_exists($product_image->media_url)}}" onerror='this.src="{{adminAsset('images/no-image.jpg')}}"' alt="product" alt="product">
                        </div>
                    </div>
                @endif
            @endforeach
        @endif
    </div>
    <span class="det-prev det-nav"><i class="fas fa-angle-left"></i></span>
    <span class="det-next det-nav active"><i class="fas fa-angle-right"></i></span>
</div>
@push('custom-script')
<script>
    $(document).on('click','.login',function (){
        $('#login-modal').modal('show');
    })
    $(document).on('change','.bid_input_option',function (){
        if ($(this).val() == 'others'){
            $('.bid_input').val('');
        }else {
            $('.bid_input').val($(this).val());
        }
    });
    $(document).ready(function (){
        const live_auction_message_channel_name = 'live_auction_message_'+"{{$auction->id}}"
        receiveNotification(live_auction_message_channel_name,'auction_message',function (response){
            const body = response.body;
            const user_image = body.user_image;
            const user_name = body.user_name;
            const message = body.hasOwnProperty('message') ? body.message : '';
            const message_image = body.hasOwnProperty('message_image') ? body.message_image : '';
            const time = body.created_at;
            let chat_body = prepareLiveChat(user_image,user_name,message,message_image,time);
            $('.live-chat-list').append(chat_body);
        });
        loadLiveAuctionMessage();
    });

    $('.live_message_form_class').on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            let formData = new FormData(this);
            $('.reply_message').val('');
            const submit_url = $(this).attr('action');
            makeAjaxPostFile(formData, submit_url).done(function (response) {

            });
        }
    });

    function loadLiveAuctionMessage(){
        let data = {
            auction_id : "{{$auction->id}}"
        };
        makeAjaxPostText(data,"{{route('loadLiveAuctionMessage')}}",null).done(function (response){
            $('.live-chat-list').html(response);
        });
    }

    function prepareLiveChat(user_image,user_name,message,message_image,time){
        const default_image = "{{adminAsset('images/users/avatar.png')}}";
        const image_style = message_image.length > 0 ? `style="width: 60%"` : ``;
        const image_content = message_image.length > 0 ? `<img src="${message_image}"` : ``;
        let html = `<div class="chat">
                        <img src="${user_image}" alt="" class="chat_avatar" onerror='this.src="${default_image}"'>
                        <div class="chat_content" ${image_style}>
                           <div class="chat-message">`
        if(message.length > 0){
            html += `<span>${message}</span><br>`;
        }
        if(image_content.length > 0){
            html += `<span>${image_content}</span><br>`;
        }
        html += `</div>
                <div class="chat-time"><i><small>${time}</small></i></div>
            </div>
        </div>`;
        return html;
    }

</script>
@endpush
