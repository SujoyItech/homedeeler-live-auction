<div class="mt-2">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 mt-3">
            <h5 class="title">{{$auction->name}} <span class="badge badge-danger">{{__('Live')}}</span></h5>
            {{__('Estimated Value')}} {{getMoney($auction->price_range_from)}} - {{getMoney($auction->price_range_to)}}
            @if ($auction->status < AUCTION_END)
                {!! $auction->is_new ? '<span class="badge badge-dark">NEW</span>' : '' !!}
                 <div class="countdown mt-3">
                    <h5 class="auction_end_date" id="auction_end_date_id" data-value="{{$auction->live_final_end_time}}">{{$auction->live_final_end_time ?? ''}}</h5>
                </div>
            @else
              <br>
              <span class="badge badge-danger p-1">{{__('Auction Ended')}}</span>
            @endif
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 mt-3">
            <h5 class="mb-2 price">{{__('Current Bid')}} <span id="current_bid">{{getMoney($auction->highest_bid)}}</span></h5>
            <h6 class="mb-2 info">{{__('Total Bids')}}: <span id="bid_count">{{$bid_counts ?? 0}}</span></h6>
            <h6 class="mb-2 info">{{__('Service Charge')}} {{$auction->service_charge}}%</h6>
            <h6 class="mb-2 info">{{__('Bid increment')}} : {{getMoney($auction->bid_increment)}}</h6>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 mt-3">
            @php($highest_bid = !empty($auction->highest_bid) ? $auction->highest_bid : $auction->base_price)
            @php($bid_increment = !empty($auction->bid_increment) ? $auction->bid_increment : 0)
            <div class="product-details-content">
                @php($check_bid = \App\Models\Auction\AuctionDetail::where(['auction_id'=>$auction->id ?? '' , 'bidder_id'=> \Illuminate\Support\Facades\Auth::user()->id ?? ''])->orderBy('created_at','desc')->first())
                @if(isset($check_bid))
                    <h5 class="my-2 text-center text-success">{{__('My Bid')}} : {{getMoney($check_bid->bid_price)}}</h5>
                @else
                    @if($auction->status < AUCTION_LIVE_END && $auction->live_final_end_time >= \Carbon\Carbon::now())
                        <form class="product-bid-form" novalidate id="product-bid-form-id" action="{{route('submitBid')}}" method="post">
                            <input type="hidden" name="auction_id" value="{{$auction->id}}">
                            <input type="hidden" name="bidder_id" value="{{Auth::user()->id ?? ''}}">
                            <input type="hidden" name="bid_price" id="bid_price" value="{{$highest_bid + $bid_increment }}">

                            @if(isset(Auth::user()->id))
                                @if(\Illuminate\Support\Facades\Auth::user()->admin_verified == TRUE)
                                    <button type="submit" class="custom-button submit_bid" data-style="zoom-in">{{__('Bid ')}} <span id="bid_price_view">{{getMoney( $highest_bid + $bid_increment )}}</span></button>
                                @else
                                    @if(getUserProfileImage() == '' || Auth::user()->nid_picture == '')
                                        <button class="custom-button profile_update_page">{{__('Update Profile to bid')}}</button>
                                    @else
                                        <button type="button" class="custom-button admin_verification" data-style="zoom-in">{{__('Bid ')}} <span id="bid_price_view">{{getMoney( $highest_bid + $bid_increment )}}</span></button>
                                    @endif
                                @endif
                            @else
                                <button type="button" class="custom-button login" data-style="zoom-in"><i class="fa fa-sign-in-alt"></i> {{__('Login to bid')}}</button>
                            @endif
                        </form>
                    @endif
                @endif

            <div class="buy-now-area justify-center">
                <div class="share-area">
                    <span>{{__('Share with ')}}</span>
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/sharer/sharer.php?u={{route('auction',['slug'=>$auction->slug])}}"><i class="fab fa-facebook-f"></i></a>
                        </li>
                        <li>
                            <a href="https://twitter.com/intent/tweet?text=my share text&amp;url={{route('auction',['slug'=>$auction->slug])}}"><i class="fab fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{route('auction',['slug'=>$auction->slug])}}&amp;"><i class="fab fa-linkedin-in"></i></a>
                        </li>
                        <li>
                            <a href="https://wa.me/?text={{route('auction',['slug'=>$auction->slug])}}"><i class="fab fa-whatsapp"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@include('web.auth.login_modal')

@push('custom-script')
<script>
    $(document).ready(function (){
        const auction_bid_channel_name = 'live_auction_message_'+"{{$auction->id ?? ''}}"
        receiveNotification(auction_bid_channel_name,'bid_event',function (response){
            const body = response.body;
            const next_bid = body.next_bid;
            const next_bid_view = body.next_bid_format;
            const current_bid = body.current_bid;
            const bid_counts = body.bid_count;

            $('#bid_count').html(bid_counts);
            $('#bid_price').val(next_bid);
            $('#bid_price_view').html(next_bid_view);
            $('#current_bid').html(current_bid);
        });

        submitOperation(submitResponse,'submit_bid');
        function submitResponse(response){
            if (response.success == true){
                swalRedirect("{{Request::url()}}",response.message,true);
            }else {
                swalError(response.message);
            }
        }
    });



</script>
@endpush

