<div class="tab-pane fade show" id="delevery">
    <div class="shipping-wrapper">
        @if(!empty($auction->shipping_payment))
            <div class="item">
                <h5 class="title">{{__('Shipping & Payments')}}</h5>
                {!! $auction->shipping_payment !!}
            </div>
        @endif
        @if(!empty($auction->terms_condition))
            <div class="item">
                <h5 class="title">{{__('Terms & Conditions')}}</h5>
                {!! $auction->terms_condition !!}
            </div>
        @endif
    </div>
</div>
