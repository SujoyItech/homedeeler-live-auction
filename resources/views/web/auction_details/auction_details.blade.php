@extends('web.layouts.app') @section('content')
<!--============= Hero Section Starts Here =============-->
<div class="hero-section">
    <div
        class="bg_img hero-bg"
        data-background="{{ webAsset('images/banner/auction-bg.jpg') }}"
    ></div>
</div>
<!--============= Hero Section Ends Here =============-->

<!--============= Product Details Section Starts Here =============-->
<section class="product-details padding-bottom mt--240">
    <div
        class="container check_for_auction_details_page_{{$auction->id}}"
        data-id="{{$auction->id}}"
    >
         @if($auction->status == AUCTION_IN_LIVE) @if($auction->is_streaming ==
                TRUE && !empty($auction->streaming_token))
                <div class="row">
                    <div class="col-lg-7 col-md-7">
                        @include('web.auction_details.live_video_chat_client')
                    </div>
                    <div class="col-lg-5 col-md-5">
                        @include('web.auction_details.live_streaming.product_images')
                    </div>
                    <div class="col-lg-12 col-md-12">
                        @include('web.auction_details.live_streaming.product_details')
                    </div>
                </div>
                @else
                <div class="row">
                    <div class="col-lg-8 col-md-8">
                        @include('web.auction_details.product_images_for_live')
                    </div>
                    <div class="col-lg-4 col-md-4">
                        @include('web.auction_details.product_details_for_live')
                    </div>
                </div>
                @endif @else
                {{--    FOR LIVE--}}
                <div class="row">
                    <div class="col-lg-5 col-md-5">
                        @include('web.auction_details.product_images')
                    </div>
                    <div class="col-lg-7 col-md-7">
                        @include('web.auction_details.product_details')
                    </div>
                </div>
            @endif
        {{-- For Ofline --}}

    </div>
    <div class="product-tab-menu-area mb-3 mt-3">
        <div class="container">
            <ul class="product-tab-menu nav nav-tabs">
                <li>
                    <a href="#details" class="active" data-toggle="tab">
                        <div class="thumb">
                            <img
                                src="{{ webAsset('images/product/tab1.png') }}"
                                alt="product"
                            />
                        </div>
                        <div class="content">{{ __("Description") }}</div>
                    </a>
                </li>
                <li>
                    <a href="#delevery" data-toggle="tab">
                        <div class="thumb">
                            <img
                                src="{{ webAsset('images/product/tab2.png') }}"
                                alt="product"
                            />
                        </div>
                        <div class="content">{{ __("Delivery Options") }}</div>
                    </a>
                </li>
                <li>
                    <a href="#history" data-toggle="tab" id="bid_history_tab">
                        <div class="thumb">
                            <img
                                src="{{ webAsset('images/product/tab3.png') }}"
                                alt="product"
                            />
                        </div>
                        <div class="content">
                            {{ __("Bid History") }}
                            ({{$auction->auction_details->count()}})
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#questions" data-toggle="tab" id="question_tab">
                        <div class="thumb">
                            <img
                                src="{{ webAsset('images/product/tab4.png') }}"
                                alt="product"
                            />
                        </div>
                        <div class="content">{{ __("Questions") }}</div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="tab-content">
            <div class="tab-pane fade show active" id="details">
                <div class="tab-details-content">
                    <div class="header-area">
                        {!! $auction->product_description !!}
                    </div>
                </div>
            </div>
            <div class="tab-pane fade show" id="delevery">
                <div class="shipping-wrapper">
                    @if(!empty($auction->shipping_payment))
                    <div class="item">
                        <h5 class="title">{{ __("Shipping & Payments") }}</h5>
                        {!! $auction->shipping_payment !!}
                    </div>
                    @endif @if(!empty($auction->terms_condition))
                    <div class="item">
                        <h5 class="title">{{ __("Terms & Conditions") }}</h5>
                        {!! $auction->terms_condition !!}
                    </div>
                    @endif
                </div>
            </div>
            <div class="tab-pane fade show" id="history">
                @include('web.auction_details.bid_history')
            </div>
            <div class="tab-pane fade show" id="questions">
                @if(isset(Auth::user()->id))
                @include('web.auction_details.auction_chats') @endif
            </div>
        </div>
    </div>
</section>
<!--============= Product Details Section Ends Here =============-->
@endsection
@section('script')
<script>
    $(document).on('click','.profile_update_page',function(){
        window.location.href("{{route('editUserProfile',['type'=>'personal'])}}");
    });

    $(document).ready(function () {
        const liveStreamingChannel = "live_auction_message_{{$auction->id}}";
        const liveStreamingStartEvent = "auction_live_streaming_start";
        const liveStreamingEndEvent = "auction_live_streaming_end";
        countDownEndAuctionDetails();
        receiveNotification(
            liveStreamingChannel,
            liveStreamingStartEvent,
            (response) => {
                window.location.reload();
            }
        );
        receiveNotification(
            liveStreamingChannel,
            liveStreamingEndEvent,
            (response) => {
                window.location.reload();
            }
        );
    });
    function countDownEndAuctionDetails() {
        let endDate = $(".auction_end_date").data("value");
        if (endDate.length > 0 && typeof endDate == "string") {
            let counterElement = document.querySelector("#auction_end_date_id");
            let myCountDown = new ysCountDown(endDate, function (
                remaining,
                finished
            ) {
                let message = "";
                if (finished) {
                    message = "Expired";
                } else {
                    var re_days = remaining.totalDays;
                    var re_hours = remaining.hours;
                    message += re_days + "d  : ";
                    message += re_hours + "h  : ";
                    message += remaining.minutes + "m  : ";
                    message += remaining.seconds + "s";
                }
                counterElement.textContent = message;
            });
        }
    }

    var ENDPOINT = "{{ url('/') }}";
    var page = 1;
    infinteLoadMore(page);

    $(document).on("click", ".load_more", function () {
        page++;
        infinteLoadMore(page);
    });

    function infinteLoadMore(page) {
        $.ajax({
            url:
                ENDPOINT +
                "/auction-bidding-list/{{$auction->id}}?page=" +
                page,
            datatype: "html",
            type: "get",
            beforeSend: function () {
                $(".auto-load").addClass("text-success");
                $(".auto-load").show();
            },
        })
            .done(function (response) {
                if (response.length == 0) {
                    $(".auto-load").html("We don't have more data to display ");
                    $(".auto-load").removeClass("text-success");
                    $(".auto-load").addClass("text-danger");
                    $(".load_more").hide();
                    return;
                }
                $(".auto-load").hide();
                $("#data-wrapper").append(response);
            })
            .fail(function (jqXHR, ajaxOptions, thrownError) {
                console.log("Server error occured");
            });
    }

    function sendMessageResponse(response) {
        if (response.success == true) {
            const messaging_id = response.data.message.messaging_id;
            $("#messaging_id").val(messaging_id);
            $("#message").val("");
            getMessageList(messaging_id);
        } else {
            swalError(response.message);
        }
    }

    $(document).on("click", "#question_tab", function () {
        submitOperation(sendMessageResponse, "send_message");
        getMessageList($("#messaging_id").val());
    });

    function getMessageList(messaging_id) {
        let message_data = {
            id: messaging_id,
            auction_id: "{{$auction->id}}",
        };
        makeAjaxPostText(
            message_data,
            "{{route('getUserAuctionMessageList')}}"
        ).done(function (response) {
            $(".show_message_list").html(response);
            $("#auctionMessageListId").animate({ scrollTop: 9999 });
        });
    }

    $(document).on("click", ".admin_verification", function () {
        Swal.fire({
            title: "You are not verified!!",
            html: "Please update your profile with all information",
            type: "warning",
            showConfirmButton: false,
        });
    });
</script>

@stack('custom-script'); @endsection
