<div class="text-center mb-3">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="card_payment" value="card_payment" name="payment_type" class="custom-control-input check_payment_type" checked>
        <label class="custom-control-label" for="card_payment">{{__('Card')}}</label>
    </div>
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="btc_payment" value="btc_payment" name="payment_type" class="custom-control-input check_payment_type">
        <label class="custom-control-label" for="btc_payment">{{__('BTC')}}</label>
    </div>
</div>
<div class="card_payment">
    @include('web.payment.card')
</div>
<div class="btc_payment d-none">
    @include('web.payment.coin_payment')
</div>
<script>
    $(document).on('change','.check_payment_type',function (){
        if ($(this).val() == 'card_payment'){
            $('.card_payment').removeClass('d-none');
            $('.btc_payment').addClass('d-none');
        }else {
            $('.btc_payment').removeClass('d-none');
            $('.card_payment').addClass('d-none');
            if ($('.amount_in_btc').val().length == 0){
                let data = {
                    price : $('.amount').val()
                }
                $('.loading').removeClass('d-none');
                makeAjaxPostText(data,"{{route('convertPriceToBtcPrice')}}").done(function (response){
                    if (response.success == true){
                        $('.amount_in_btc').val(response.data.btc_price);
                        $('#btc_price_show').html(response.data.btc_price);
                        $('.loading').addClass('d-none');
                    }else {
                        swalError(response.message);
                        $('.loading').addClass('d-none');
                    }
                })
            }
        }
    })
</script>
