<form class="paypal_payment_class" id="paypal_payment_id" action="{{ route('productPurchasePaymentByPaypal') }}" method="post">
    @csrf
    <div class="card-box">
        <div class="">
            <div class="row">
                <input type="hidden" name="auction_id" class="auction_id_payment" value="{{$auction_id}}">
                <input type="hidden" name="payment_type" value="{{PRODUCT_PURCHASE_PAYMENT}}">
                <div class="col-12 text-center">
                    <button class="btn btn-primary btn-block submit_paypal_payment" type="submit"><i class="fab fa-paypal"></i> {{__('Pay With Paypal')}}</button>
                </div>
            </div>
        </div>
    </div>
</form>

