
<div class="text-center mb-3">
    <h6>{{__('Total Charge ')}}: {{getMoney($total_charge)}}</h6>
</div>
<div class="text-center card_payment_btn mb-3">
    <button class="btn btn-primary" id="pay_with_card">{{__('Pay With Card')}}</button>
</div>
<div class="text-center mb-3" id="card_payment_body">
    <div class="text-center d-none loading">
        <div class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
</div>

<script>
    $(document).on('click','#pay_with_card',function (){
        Ladda.bind(this);
        let load = $(this).ladda();
        let data = {
            auction_id : "{{$auction_id}}",
            total_charge : "{{$total_charge}}"
        }
        $('.loading').removeClass('d-none');
        makeAjaxPostText(data,"{{route('loadProductPurchasePaymentBody')}}",load).done(function (response){
            $('#card_payment_body').html(response);
            $('.card_payment_btn').addClass('d-none');
            $('.loading').addClass('d-none');
        })
    })
</script>
