@php($settings = __options(['payment_settings']))
@if(isset($settings->payment_method))
    @if($settings->payment_method == 'stripe')
        <div class="stripe-payment">
            @include('web.payment.stripe')
        </div>
    @elseif($settings->payment_method == 'brain_tree')
        <div class="braintree-payment">
            @include('web.payment.brain_tree')
        </div>
    @elseif($settings->payment_method == 'paypal')
        <div class="paypal-payment">
            @include('web.payment.paypal')
        </div>
    @endif
@endif
