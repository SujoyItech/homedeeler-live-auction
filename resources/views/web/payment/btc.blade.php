<div class="text-center">
    <h5 class="mb-2 mt-2">{{__('Address')}}</h5>
    <h6 class="text-success mb-2" id="btc_address">{{ $btc_data['btc_address'] ?? ''}}</h6>
    <h5 id="btc_address" class="mb-2">{{__('QR Code')}}</h5>
    @if(!empty($btc_data['btc_address']))
        <h6 id="btc_address mb-1">{!!\SimpleSoftwareIO\QrCode\Facades\QrCode::size(150)->generate($btc_data['btc_address']); !!}</h6>
    @endif
    <div class="input-group expiry_countdown mt-1">
        <div class="input-group-prepend">
            <span class="input-group-text">{{__('Expired Within')}}</span>
        </div>
        <input type="text" class="form-control" id="expiry_minute">
        <input type="text" class="form-control" id="expiry_sec">
    </div>
    <div class="expired_show d-none">
        <span class="badge badge-danger" id="show_expired"></span>
    </div>
</div>
<script>

    $(document).ready(function (){
        CountDownTimer("{{Carbon\Carbon::parse($btc_data['expiry_date'])->format('m/d/Y H:i:s')}}","{{Carbon\Carbon::now()->format('m/d/Y H:i:s')}}");
    })

    function CountDownTimer(expired_date,current_date)
    {
        const expiredDate = new Date(expired_date).getTime();
        const currentDate = new Date(current_date).getTime();
        var distance = Math.abs(expiredDate - currentDate);
        // Time calculations for days, hours, minutes and seconds
        let minutes = Math.floor(distance / (1000 * 60));
        let seconds = Math.floor(minutes % 60);
// Update the count down every 1 second
        setInterval(function() {
            // Get today's date and time
            // Find the distance between now and the count down date
            if (seconds > 0){
                seconds --;
            }else {
                seconds = 60;
                minutes = minutes - 1;
            }

            // Output the result in an element with id="demo"
            $('#expiry_minute').val(minutes);
            $('#expiry_sec').val(seconds);

            // If the count down is over, write some text
            if (minutes === 0 && seconds === 0) {
                clearInterval();
                $('.expiry_countdown').addClass('d-none');
                $('.expired_show').removeClass('d-none');
                $('#show_expired').html(`<span class="badge badge-danger p-1 font-14">Expired</span>`);
            }
        }, 1000);
    }
</script>
