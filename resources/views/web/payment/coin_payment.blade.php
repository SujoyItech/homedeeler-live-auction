
<div class="text-center mb-3">
    <div class="text-center d-none loading">
        <div class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
    <h6>{{__('Total Charge: ')}} <span id="btc_price_show"></span> BTC</h6>
</div>
<div class="card-box text-center">
    <input type="hidden" name="auction_id" class="auction_id_payment" value="{{$auction_id}}">
    <input type="hidden" name="amount" class="amount" value="{{$total_charge}}">
    <input type="hidden" name="amount_in_btc" class="amount_in_btc" value="">
    <input type="hidden" name="payment_type" value="{{PRODUCT_PURCHASE_PAYMENT}}" class="payment_type">
    <button class="btn btn-primary" id="btc_payment_submit" data-style="zoom-in">{{__('Pay With BTC Balance')}}</button>
    <button class="btn btn-info" id="add_btc_balance" data-style="zoom-in">{{__('Add BTC Balance')}}</button>
</div>
<div class="card-box text-center" id="btc_address_show">
</div>


<script>
    $(document).on('click','#btc_payment_submit',function (){
        Ladda.bind(this);
        let load = $(this).ladda();
        let data = {
            auction_id : $('.auction_id_payment').val(),
            amount : $('.amount').val(),
            amount_in_btc : $('.amount_in_btc').val(),
            payment_type : $('.payment_type').val()
        }
        makeAjaxPostText(data,"{{route('productPurchasePaymentWithBtcBalance')}}",load).done(function (response){
            if (response.success == true){
                swalRedirect("{{route('myWonItems')}}",response.message,'success')
            }else {
                swalError(response.message);
            }
        });
    });

    $(document).on('click','#add_btc_balance',function (){
        Ladda.bind(this);
        let load = $(this).ladda();
        let data = {
            auction_id : "{{$auction_id}}",
            type : "{{PRODUCT_PURCHASE_PAYMENT}}",
        }
        makeAjaxPostText(data,"{{route('generateCoinPaymentAddress')}}",load).done(function (response){
            $('#btc_address_show').html(response);
        });
    })

</script>

