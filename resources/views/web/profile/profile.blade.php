@extends('web.layouts.app')
@section('content')
    <!--============= Hero Section Starts Here =============-->
    <div class="hero-section">
        <div class="bg_img hero-bg" data-background="{{webAsset('images/banner/auction-bg.jpg')}}"></div>
    </div>
    <!--============= Hero Section Ends Here =============-->
    <!--============= Dashboard Section Starts Here =============-->
    <section class="dashboard-section mt--240 mb-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-10 col-md-7 col-lg-4">
                    @include('web.profile.profile_sidebar',['tab'=> $tab ??''])
                </div>
                <div class="col-lg-8">
                    @yield('profile-content')
                </div>
            </div>
        </div>
    </section>
    <!--============= Dashboard Section Ends Here =============-->
@endsection
