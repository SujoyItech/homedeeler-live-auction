@extends('web.profile.profile',['tab'=>'profile'])

@section('profile-content')
<div class="row">
    @if($type == 'personal')
        @include('web.profile.profile_edit.personal')
    @elseif($type == 'address')
        @include('web.profile.profile_edit.address')
    @elseif($type =='account')
        @include('web.profile.profile_edit.account')
    @elseif($type == 'password')
        @include('web.profile.profile_edit.password')
    @endif
</div>

@endsection
@section('script')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css" integrity="sha512-In/+MILhf6UMDJU4ZhDL0R0fEpsp4D3Le23m6+ujDWXwl3whwpucJG1PEmI3B07nyJx+875ccs+yX2CqQJUxUw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        $('.dropify').dropify();
        $(document).ready(function (){
            submitOperation(submitResponse);
        });
        function submitResponse(response){
            if (response.success == true){
                swalRedirect("{{route('myProfile')}}",response.message,'success');
            }else {
                swalError(response.message);
            }
        }
    </script>
@endsection
