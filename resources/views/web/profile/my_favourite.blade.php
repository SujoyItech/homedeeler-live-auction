@extends('web.profile.profile',['tab'=>$tab ?? ''])
@section('profile-content')
    <div class="dashboard-widget">
        <table class="purchasing-table">
            <thead>
            <th>{{__('Item')}}</th>
            <th>{{__('Highest Bid')}}</th>
            <th>{{__('Expires')}}</th>
            </thead>
            <tbody>
            @if(isset($my_favourites))
                @foreach($my_favourites as $my_favourite)
                    @if(!empty($my_favourite->name))
                        <tr>
                            <td data-purchase="item"><a href="{{route('auction',['slug'=>$my_favourite->slug])}}">{{$my_favourite->name}}</a></td>
                            <td data-purchase="highest bid">{{getMoney($my_favourite->highest_bid)}} {{ACTIVE}}</td>
                            <td data-purchase="expires">{{!empty($my_favourite->live_final_end_time) ? \Carbon\Carbon::parse($my_favourite->live_final_end_time)->format('d/m/Y') : 'DD-MM-YYYY'}}</td>
                        </tr>
                    @endif
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            const a = 11;
            console.log(a);
        });

        $(document).on('click',function(){

        })

    </script>
@endsection



