<div class="col-12">
    <div class="dash-pro-item mb-30 dashboard-widget">
        <div class="header">
            <h5 class="title">{{__('Account Settings')}}</h5>
        </div>
        <form method="post" id="account-profile-id" class="account-profile-form" action="{{route('userProfileUpdate')}}">
            <div class="form-group">
                <label for="language">{{__('Language')}}</label>
                <select class="form-control" name="language">
                    <option value="">{{__('Select')}}</option>
                    @foreach(languages() as $key=>$lang)
                        <option value="{{$key}}" {{is_selected($key,$profile->language)}}>{{$lang}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="time_zone">{{__('Time Zone')}}</label>
                <select class="form-control" name="time_zone">
                    <option value="">{{__('Select')}}</option>
                    @foreach(timezones() as $key=>$value)
                        <option value="{{$key}}" {{is_selected($key,$profile->time_zone)}}>{{$value}}</option>
                    @endforeach
                </select>
            </div>
            <button class="btn btn-dark btn-xs basic_submit" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
        </form>
    </div>
</div>
