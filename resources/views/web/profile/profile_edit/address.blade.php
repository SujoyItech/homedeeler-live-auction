<div class="col-12">
    <div class="dash-pro-item mb-30 dashboard-widget">
        <div class="header">
            <h5 class="title">{{__('Address Details')}}</h5>
        </div>
        <form method="post" id="personal-profile-id" class="personal-profile-form" action="{{route('userProfileUpdate')}}">
            <div class="form-group">
                <label for="country">{{__('Country')}}</label>
                <select class="form-control" name="country">
                    <option value="">{{__('Select')}}</option>
                    @foreach(countries() as $key=>$value)
                        <option value="{{$value}}" {{is_selected($value,$profile->country)}}>{{$value}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="city">{{__('City')}}</label>
                <input type="text" class="form-control" name="city" id="city" value="{{$profile->city}}">
            </div>
            <div class="form-group">
                <label for="state">{{__('State')}}</label>
                <input type="text" class="form-control" name="state" id="state" value="{{$profile->state}}">
            </div>
            <div class="form-group">
                <label for="post_code">{{__('Post Code')}}</label>
                <input type="text" class="form-control" name="post_code" id="post_code" value="{{$profile->post_code}}">
            </div>
            <div class="form-group">
                <label for="address">{{__('Address')}}</label>
                <input type="text" class="form-control" name="address" id="address" value="{{$profile->address}}">
            </div>
            <div class="form-group">
                <label for="address_secondary">{{__('Address secondary')}}</label>
                <input type="text" class="form-control" name="address_secondary" id="address_secondary" value="{{$profile->address_secondary}}">
            </div>
            <button class="btn btn-dark btn-xs basic_submit" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
        </form>
    </div>
</div>
