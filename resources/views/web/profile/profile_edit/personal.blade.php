<div class="col-12">
    <div class="dash-pro-item mb-30 dashboard-widget">
        <div class="header">
            <h5 class="title">{{__('Personal Details')}}</h5>
        </div>
        <form method="post" id="personal-profile-id" class="personal-profile-form" action="{{route('userProfileUpdate')}}">
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="name">{{__('Name')}}</label>
                    <input type="text" class="form-control" name="name" id="name" value="{{$profile->name}}">
                </div>
                <div class="form-group col-md-6">
                    <label for="email">{{__('Email')}}</label>
                    <input type="email" class="form-control" name="email" id="email" readonly value="{{$profile->email}}">
                </div>
                <div class="form-group col-md-6">
                    <label for="phone">{{__('Phone')}}</label>
                    <input type="text" class="form-control" name="phone" id="phone" value="{{$profile->phone}}">
                </div>
                <div class="form-group col-md-6">
                    <label for="phone">{{__('Nid')}}</label>
                    <input type="text" class="form-control" name="nid" id="nid" value="{{$profile->nid}}">
                </div>
                <div class="form-group col-md-6">
                    <label for="phone">{{__('Tin')}}</label>
                    <input type="text" class="form-control" name="tin" id="tin" value="{{$profile->tin}}">
                </div>
                <div class="col-md-6">
                    <input type="hidden" name="profile_photo_path" id="profile_photo_path_id">
                    <label>{{__('Profile Picture')}}</label>
                    <button class="btn btn-dark btn-block btn-sm" id="cameraOpen" type="button"><i class="fa fa-camera"></i> {{__('Take a photo')}}</button>
                </div>
                <div class="form-group col-md-6">
                    <label>{{__('Nid Picture')}}</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="nid_picture" id="nid_picture">
                            <label class="custom-file-label nid_picture_label" for="nid_picture"><i class="fa fa-paperclip"></i> {{__('Choose file')}}</label>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-dark btn-xs basic_submit" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
        </form>
    </div>
</div>
@include('web.auth.image_capture_modal')
<script type="text/javascript" src="https://unpkg.com/webcam-easy/dist/webcam-easy.min.js"></script>
<script>
    $(document).ready(function (){
        const webcamElement = document.getElementById('webcam');
        const canvasElement = document.getElementById('canvas');
        const snapSoundElement = document.getElementById('snapSound');
        const webcam = new Webcam(webcamElement, 'user', canvasElement, snapSoundElement);
        $(document).on('click','#cameraOpen',function (){
            $('#image-capture-modal').modal('show');
            webcam.start()
                .then(result =>{
                    console.log("webcam started");
                })
                .catch(err => {
                    console.log(err);
                });
        })
        $(document).on('click','#click_shot',function (){
            let picture = webcam.snap();
            $('#profile_photo_path_id').val(picture);
            $('#image-capture-modal').modal('hide');
            $('#cameraOpen').html(`<i class="fa fa-check"></i> Selected`);
            webcam.stop();
        });
        $(document).on('click','#close',function (){
            webcam.stop();
            $('#image-capture-modal').modal('hide');
        });
        $(document).on('click','#flip',function (){
            webcam.flip();
            webcam.start();
        });
    });

    $(document).on('change','#nid_picture',function (){
        $('.nid_picture_label').html(`<i class="fa fa-check"></i> Selected`);
    });
</script>
