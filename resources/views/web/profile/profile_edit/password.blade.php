<div class="col-12">
    <div class="dash-pro-item dashboard-widget">
        <div class="header">
            <h5 class="title">{{__('Change Password')}}</h5>
        </div>
        <form method="post" id="change-password-form-id" class="change-password-form" action="{{route('userPasswordUpdate')}}">
            <div class="form-group">
                <label for="old_password">{{__('Old Password')}}</label>
                <input type="password" class="form-control" name="old_password" id="old_password">
            </div>
            <div class="form-group">
                <label for="password">{{__('New Password')}}</label>
                <input type="password" class="form-control" name="password" id="password">
            </div>
            <div class="form-group">
                <label for="password_confirmation">{{__('Password Confirmation')}}</label>
                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation">
            </div>
            <button class="btn btn-dark btn-xs basic_submit" type="submit"><i class="fa fa-save"></i> {{__('Change Password')}}</button>
        </form>
    </div>
</div>
