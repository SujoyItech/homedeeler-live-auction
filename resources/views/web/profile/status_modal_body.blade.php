<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
<link rel="stylesheet" href="{{webAsset('css/order.css')}}">
<div class="container padding-bottom-3x mb-1">
    <div class="card mb-3">
        <div class="p-4 text-center text-white text-lg bg-dark rounded-top">
            <h5 class="text-uppercase text-light">{{__('Tracking Order No')}} - <span class="">#{{str_pad($auction_details->id,6,0,STR_PAD_LEFT)}}</span></h5>
        </div>
        <div class="d-flex flex-wrap flex-sm-nowrap justify-content-between py-3 px-2 bg-secondary">
            <div class="w-100 text-center py-1 px-2"><span class="text-medium"><strong>{{__('Product Title')}} :</strong></span> {{$auction_details->name}}</div>
            <div class="w-100 text-center py-1 px-2"><span class="text-medium"><strong>{{__('Total Price')}} : </strong></span> {{getMoney($auction_details->auction_winning_price)}} + {{getMoney($auction_details->processing_fee) }} = {{getMoney($auction_details->total_price)}}</div>
            <div class="w-100 text-center py-1 px-2"><span class="text-medium"><strong>{{__('Status')}} : </strong></span> {{AUCTION_STATUS[$auction_details->status]}}</div>
{{--            <div class="w-100 text-center py-1 px-2"><span class="text-medium">Expected Date:</span> SEP 09, 2017</div>--}}
        </div>
        <div class="card-body">
            <div class="steps d-flex flex-wrap flex-sm-nowrap justify-content-between padding-top-2x padding-bottom-1x">
                @if(isset($auction_status_list))
                    @foreach($auction_status_list as $status_list)
                        @if ($status_list['status'] < SELLER_PAID )
                            <div class="step @if(isset($status_list['auction']) && $status_list['auction']->status != NULL) completed @endif">
                                <div class="step-icon-wrap">
                                    <div class="step-icon">
                                        @if(isset($status_list['auction']))
                                            <i class="fa fa-check"></i>
                                        @else
                                            <i class="fa fa-circle"></i>
                                        @endif
                                    </div>
                                </div>
                                <h4 class="step-title">{{$status_list['status_title']}}</h4>
                                @if(isset($status_list['auction']))
                                    <h4 class="step-title">{{\Carbon\Carbon::parse($status_list['auction']->created_at)->format('jS F Y g:i A')}}</h4>
                                    <span class="text-sm">{{$status_list['auction']->message}}</span>
                                    <br/>
                                    <br/>
                                    @if(!empty($status_list['auction']->file) && file_exists(public_path(get_image_path('auction/auction_status'))))
                                        <a class="btn btn-dark btn-sm" target="_blank" href="{{asset(get_image_path('auction/auction_status').'/'.$status_list['auction']->file)}}"><i class="fa fa-download"></i> {{__('Shipment Document')}}</a>
                                    @endif
                                @endif
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
