@extends('web.profile.profile',['tab'=>'profile'])
@section('profile-content')
<div class="row">
    <div class="col-md-12">
        <div class="dash-pro-item mb-30 dashboard-widget">
            <div class="header">
                <h5 class="title">{{__('Personal Details')}}</h5>
                <a href="{{route('editUserProfile',['type'=>'personal'])}}"><span class="edit"><i class="flaticon-edit"></i> {{__('Edit')}}</span></a>
            </div>
            <ul class="dash-pro-body">
                <li>
                    <div class="info-name">{{__('Name')}}</div>
                    <div class="info-value">{{$profile->name}}</div>
                </li>
                <li>
                    <div class="info-name">{{__('Email')}}</div>
                    <div class="info-value">{{$profile->email}}</div>
                </li>
                <li>
                    <div class="info-name">{{__('Mobile')}}</div>
                    <div class="info-value">{{$profile->phone}}</div>
                </li>
                <li>
                    <div class="info-name">{{__('Nid')}}</div>
                    <div class="info-value">{{$profile->nid}}</div>
                </li>
                <li>
                    <div class="info-name">{{__('Tin')}}</div>
                    <div class="info-value">{{$profile->tin}}</div>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-md-12">
        <div class="dash-pro-item mb-30 dashboard-widget">
            <div class="header">
                <h5 class="title">{{__('Balance')}}</h5>
            </div>
            <ul class="dash-pro-body">
                <li>
                    <div class="info-name">{{__('Balance')}}</div>
                    <div class="info-value">{{ getMoney($profile->balance) }}</div>
                </li>
                <li>
                    <div class="info-name">{{__('BTC Balance')}}</div>
                    <div class="info-value">{{$profile->btc_balance}} {{__('BTC')}}</div>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-md-12">
        <div class="dash-pro-item mb-30 dashboard-widget">
            <div class="header">
                <h5 class="title">{{__('Account Settings')}}</h5>
                <a href="{{route('editUserProfile',['type'=>'account'])}}"><span class="edit"><i class="flaticon-edit"></i> {{__('Edit')}}</span></a>
            </div>
            <ul class="dash-pro-body">
                <li>
                    <div class="info-name">{{__('Language')}}</div>
                    <div class="info-value">{{ !empty($profile->language) ? languages($profile->language) : ''}}</div>
                </li>
                <li>
                    <div class="info-name">{{__('Time Zone')}}</div>
                    <div class="info-value">{{!empty($profile->time_zone) ? timezones($profile->time_zone) : ''}}</div>
                </li>
                <li>
                    <div class="info-name">{{__('Status')}}</div>
                    <div class="info-value"><i class="flaticon-check text-success"></i> {{__('Active')}}</div>
                </li>
            </ul>
        </div>
    </div>

    <div class="col-md-12">
        <div class="dash-pro-item mb-30 dashboard-widget">
            <div class="header">
                <h5 class="title">{{__('Address Details')}}</h5>
                <a href="{{route('editUserProfile',['type'=>'address'])}}"><span class="edit"><i class="flaticon-edit"></i> {{__('Edit')}}</span></a>
            </div>
            <ul class="dash-pro-body">
                <li>
                    <div class="info-name">{{__('Country')}}</div>
                    <div class="info-value">{{$profile->country}}</div>
                </li>
                <li>
                    <div class="info-name">{{__('City')}}</div>
                    <div class="info-value">{{$profile->country}}</div>
                </li>
                <li>
                    <div class="info-name">{{__('State')}}</div>
                    <div class="info-value">{{$profile->state}}</div>
                </li>
                <li>
                    <div class="info-name">{{__('Post Code')}}</div>
                    <div class="info-value">{{$profile->post_code}}</div>
                </li>
                <li>
                    <div class="info-name">{{__('Address')}}</div>
                    <div class="info-value">{{$profile->address}}</div>
                </li>
                <li>
                    <div class="info-name">{{__('Address Secondary')}}</div>
                    <div class="info-value">{{$profile->address_secondary}}</div>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-md-12">
        <div class="dash-pro-item dashboard-widget">
            <div class="header">
                <h5 class="title">{{__('Security')}}</h5>
                <a href="{{route('editUserProfile',['type'=>'password'])}}"><span class="edit"><i class="flaticon-edit"></i> {{__('Edit')}}</span></a>
            </div>
            <ul class="dash-pro-body">
                <li>
                    <div class="info-name">{{__('Password')}}</div>
                    <div class="info-value">xxxxxxxxxxxxxxxx</div>
                </li>
            </ul>
        </div>
    </div>
</div>

@endsection
