<form method="post" id="profile-update-form-id" class="profile-update-form-class" action="{{route('userProfileUpdate')}}">
    <div class="row">
        <div class="form-group">
            <label for="nid">{{__('Nid')}} <span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="nid" id="nid" value="" required>
        </div>
        <div class="form-group">
            <input type="hidden" name="profile_photo_path" id="profile_photo_path_id" required>
            <label>{{__('Profile Picture')}}</label>
            <button class="btn btn-dark btn-block btn-sm" id="cameraOpen" type="button">
                <i class="fa fa-camera"></i> {{__('Take a photo')}}</button>
        </div>
        <div class="form-group">
            <label>{{__('Nid Picture')}}</label>
            <div class="input-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="nid_picture" id="nid_picture" required>
                    <label class="custom-file-label nid_picture_label" for="nid_picture"><i class="fa fa-paperclip"></i> {{__('Choose file')}}
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
        <button type="submit" class="btn btn-dark submit_profile">{{__('Save')}}</button>
    </div>
</form>
@include('web.auth.image_capture_modal')
<script type="text/javascript" src="https://unpkg.com/webcam-easy/dist/webcam-easy.min.js"></script>
<script>
    $(document).ready(function () {
        const webcamElement = document.getElementById('webcam');
        const canvasElement = document.getElementById('canvas');
        const snapSoundElement = document.getElementById('snapSound');
        const webcam = new Webcam(webcamElement, 'user', canvasElement, snapSoundElement);
        $(document).on('click', '#cameraOpen', function () {
            $('#image-capture-modal').modal('show');
            webcam.start()
                .then(result => {
                    console.log("webcam started");
                })
                .catch(err => {
                    console.log(err);
                });
        })
        $(document).on('click', '#click_shot', function () {
            let picture = webcam.snap();
            let audioElement = document.createElement('audio');
            audioElement.setAttribute('src', '{{adminAsset('audio/audio.mp3')}}');
            audioElement.play();
            $('#profile_photo_path_id').val(picture);
            $('#image-capture-modal').modal('hide');
            $('#cameraOpen').html(`<i class="fa fa-check"></i> Selected`);
            webcam.stop();
        });
        $(document).on('click', '#close', function () {
            webcam.stop();
            $('#image-capture-modal').modal('hide');
        });
        $(document).on('click', '#flip', function () {
            webcam.flip();
            webcam.start();
        });

        $(document).on('change','#nid_picture',function (){
            $('.nid_picture_label').html(`<i class="fa fa-check"></i> Selected`);
        });
    });
</script>
