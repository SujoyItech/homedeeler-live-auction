@extends('web.profile.profile',['tab'=>$tab ?? ''])
@section('profile-content')
    <div class="dashboard-widget">
        <table class="purchasing-table">
        <thead>
        <th>{{__('Image')}}</th>
        <th>{{__('Name')}}</th>
        <th>{{__('Title')}}</th>
        <th>{{__('Notification')}}</th>
        </thead>
        <tbody>
        @if(isset($notifications))
        <tr>
            @foreach($notifications as $notification)
                @php($body = !empty($notification->body) ? json_decode($notification->body) : [])
                <td data-purchase="item"><img class="img-thumbnail" src="{{asset(get_image_path().$body->user_image)}}" onerror='this.src="{{asset(get_image_path('user').'avatar.png')}}"' width="50"></td>
                <td data-purchase="bid price">{{$body->username ?? ''}}</td>
                <td data-purchase="highest bid">{{$body->title ?? ''}}</td>
                <td data-purchase="lowest bid">{{$body->body ?? ''}}</td>
            @endforeach
        </tr>
        @endif
        </tbody>
    </table>
    </div>
@endsection




