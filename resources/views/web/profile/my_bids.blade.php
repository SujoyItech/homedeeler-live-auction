@extends('web.profile.profile',['tab'=>$tab ?? ''])
@section('profile-content')
    <div class="dashboard-widget">
        <table class="purchasing-table">
            <thead>
            <th>{{__('Item')}}</th>
            <th>{{__('Bid Price')}}</th>
            <th>{{__('Highest Bid')}}</th>
            <th>{{__('Expires')}}</th>
            </thead>
            <tbody>
            @if(isset($my_bids))
                @foreach($my_bids as $my_bid)
                    @if(!empty($my_bid->name))
                        <tr>
                            <td data-purchase="item"><a href="{{route('auction',['slug'=>$my_bid->slug])}}">{{$my_bid->name}}</a></td>
                            <td data-purchase="bid price">{{getMoney($my_bid->bid_price)}}</td>
                            <td data-purchase="highest bid">{{getMoney($my_bid->bid_price)}}</td>
                            <td data-purchase="expires">{{!empty($my_bid->end_date) ? \Carbon\Carbon::parse($my_bid->end_date)->format('d/m/Y') : 'DD-MM-YYYY'}}</td>
                        </tr>
                    @endif
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
@endsection
