

<div class="dashboard-widget mb-30 mb-lg-0">
        <div class="user">
            <div class="thumb-area">
                <div class="thumb">
                    @if(!empty(\Illuminate\Support\Facades\Auth::user()->profile_photo_path))
                        <img src="{{asset(get_image_path('user').'/'.\Illuminate\Support\Facades\Auth::user()->profile_photo_path)}}" class="profile_image" alt="user" onerror='this.src="{{asset(get_web_image_path('users').'/'.'avatar.png')}}"'>
                    @else
                        @if(\Illuminate\Support\Facades\Auth::user()->is_social_login == TRUE)
                        <img src="{{\Illuminate\Support\Facades\Auth::user()->social_image_link}}" class="profile_image" alt="user" onerror='this.src="{{asset(get_image_path('user').'/'.'avatar.png')}}"'>
                        @else
                            <img src="{{asset(get_image_path('user').'/'.'avatar.png')}}" class="profile_image" alt="user">
                        @endif
                    @endif
                </div>
                <label for="profile-pic" class="profile-pic-edit"><i class="flaticon-pencil"></i></label>
                <input type="file" id="profile-pic" class="d-none">
            </div>
            <div class="content">
                <h5 class="title"><a href="javascript:void(0);">{{\Illuminate\Support\Facades\Auth::user()->name}}</a></h5>
                <span class="username">{{\Illuminate\Support\Facades\Auth::user()->email}}</span><br>
            </div>
        </div>
        <ul class="dashboard-menu">
            <li>
                <a href="{{route('userDashboard')}}" class="@if(isset($tab) && $tab == 'dashboard') active @endif"><i class="flaticon-dashboard"></i>{{__('Dashboard')}}</a>
            </li>
            @if (\Illuminate\Support\Facades\Auth::user()->role === USER_SELLER && \Illuminate\Support\Facades\Auth::user()->is_seller === ACTIVE)
            <li>
                <a href="{{route('sellerHome')}}" class="@if(isset($tab) && $tab == 'seller-dashboard') active @endif"><i class="flaticon-dashboard"></i>{{__('Seller Dashboard')}}</a>
            </li>
            @endif
            <li>
                <a href="{{route('myProfile')}}" class="@if(isset($tab) && $tab == 'profile') active @endif"><i class="flaticon-settings"></i>{{__('Personal Profile')}} </a>
            </li>
            <li>
                <a href="{{route('myBids')}}" class="@if(isset($tab) && $tab == 'my-bids') active @endif"><i class="flaticon-auction"></i>{{__('My Bids')}}</a>
            </li>
            <li>
                <a href="{{route('myWonItems')}}" class="@if(isset($tab) && $tab == 'my-won-items') active @endif"><i class="flaticon-best-seller"></i>{{__('Winning Bids')}}</a>
            </li>
            <li>
                <a href="{{route('myFavourite')}}" class="@if(isset($tab) && $tab == 'my-favourite') active @endif"><i class="flaticon-alarm"></i>{{__('My Favourite')}}</a>
            </li>
            <li class="dropdown d-none d-lg-inline-block topbar-dropdown">
                <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    @if(\Illuminate\Support\Facades\Auth::user()->language == 'en')
                        <img src="{{webAsset('images/flags/us.jpg')}}" alt="user-image" height="16"> <span class="align-middle">{{__('English')}}</span>
                    @elseif(\Illuminate\Support\Facades\Auth::user()->language == 'no')
                        <img src="{{webAsset('images/flags/no.png')}}" alt="user-image" class="mr-1" height="12"> <span class="align-middle">{{__('Norway')}}</span>
                    @else
                        <img src="{{webAsset('images/flags/us.jpg')}}" alt="user-image" height="16"> <span class="align-middle">{{__('English')}}</span>
                    @endif
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <!-- item-->
                    <a href="{{route('setLang',['code'=>'en'])}}" class="dropdown-item">
                        <img src="{{webAsset('images/flags/us.jpg')}}" alt="user-image" class="mr-1" height="12"> <span class="align-middle">{{__('English')}}</span>
                    </a>
                    <a href="{{route('setLang',['code'=>'no'])}}" class="dropdown-item">
                        <img src="{{webAsset('images/flags/no.png')}}" alt="user-image" class="mr-1" height="12"> <span class="align-middle">{{__('Norway')}}</span>
                    </a>

                </div>
            </li>
            <li>
                <a href="{{route('userLogOut')}}" class="@if(isset($tab) && $tab == 'logout') active @endif text-danger"><img src="{{webAsset('images/logout_red.svg')}}">&nbsp;{{__('Logout')}}</a>
            </li>
        </ul>
    </div>

    <script>
        $('#profile-pic').on('change',function (){
            let formData = new FormData();
            let file = $(this)[0].files;
            if (file.length > 0){
                formData.append('picture',file[0]);
                makeAjaxPostFile(formData,"{{route('userProfilePictureUpdate')}}").done(function (response){
                    if (response.success == true){
                        let user = response.data.user;
                        let image_link = "{{asset(get_image_path('user'))}}"+'/'+user.profile_photo_path;
                        $('.profile_image').attr('src',image_link);
                    }
                })
            }
        })
    </script>
