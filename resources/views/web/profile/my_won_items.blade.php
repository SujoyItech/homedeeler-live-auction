@extends('web.profile.profile',['tab'=>$tab ?? ''])
@section('profile-content')
    <div class="dashboard-widget">
        <table class="purchasing-table">
            <thead>
            <th>{{__('Item')}}</th>
            <th>{{__('Status')}}</th>
            <th>{{__('Action')}}</th>
            <th>{{__('Details')}}</th>
            </thead>
            <tbody>
            @if(isset($my_own_bids))
                @foreach($my_own_bids as $my_bid)
                    @if(!empty($my_bid->name))
                    <tr>
                        <td>
                            <a href="{{route('auction',['slug'=>$my_bid->slug])}}">
                                <span>{{__('Title')}}: {{$my_bid->name}}</span><br>
                                <span>{{__('Price')}}: {{getMoney($my_bid->total_price)}}</span>
                            </a>
                        </td>
                        @if($my_bid->status == AUCTION_LIVE_END || $my_bid->status == AUCTION_END )
                            <td><span class="badge badge-warning">{{__('Payment Incomplete')}}</span></td>
                        @elseif($my_bid->status == PAYMENT_COMPLETED)
                            <td><span class="badge badge-success">{{__('Payment Completed')}}</span></td>
                        @elseif($my_bid->status == DELIVERY_REQUESTED)
                            <td><span class="badge badge-primary">{{__('Delivery Requested')}}</span></td>
                        @elseif($my_bid->status == DELIVERY_IN_SHIPMENT)
                            <td><span class="badge badge-warning">{{__('In Shipment')}}</span></td>
                        @elseif($my_bid->status == DELIVERY_COMPLETED)
                            <td><span class="badge badge-success">{{__('Delivery Completed')}}</span></td>
                        @elseif($my_bid->status == PRODUCT_RECEIVED)
                            <td><span class="badge badge-success">{{__('Product received')}}</span></td>
                        @endif

                        @if($my_bid->status == AUCTION_LIVE_END || $my_bid->status == AUCTION_END)
                            <td><button class="btn btn-sm btn-info make_payment" data-id="{{$my_bid->auction_id}}" data-style="zoom-in"><i class="fa fa-credit-card"></i> {{__('Pay Now')}}</button></td>
                        @elseif($my_bid->status == PAYMENT_COMPLETED)
                            <td><button class="btn btn-sm btn-primary delivery_request" data-id="{{$my_bid->auction_id}}" data-style="zoom-in" data-status="{{DELIVERY_REQUESTED}}">{{__('Delivery Request')}}</button></td>
                        @elseif($my_bid->status == DELIVERY_REQUESTED)
                            <td><span class="badge badge-warning">{{__('Delivery Requested')}}</span></td>
                        @elseif($my_bid->status == DELIVERY_IN_SHIPMENT)
                            <td><span class="badge badge-warning">{{__('In Shipment')}}</span></td>
                        @elseif($my_bid->status == DELIVERY_COMPLETED)
                            <td><button class="btn btn-sm btn-success product_received" data-id="{{$my_bid->auction_id}}" data-style="zoom-in" data-status="{{PRODUCT_RECEIVED}}">{{__('Product Received?')}}</button></td>
                        @elseif($my_bid->status == PRODUCT_RECEIVED)
                            <td><span class="badge badge-success">{{__('Product received')}}</span></td>
                        @endif
                        <td><button class="btn btn-sm btn-info view_details" data-style="zoom-in" data-id="{{$my_bid->auction_id}}"><i class="fa fa-eye"></i></button></td>
                    </tr>
                    @endif
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
    @include('web.payment.add_payment_modal')
    @include('web.profile.view_status_modal')
@endsection
@section('script')
    <script>
        $(document).ready(function (){
            statusChangeOperation(deliveryRequestStatusResponse,'delivery_request',"{{route('sendDeliveryRequest')}}","{{__('Are you really want to send delivery request ?')}}");
            statusChangeOperation(productReceiveConfirmationResponse,'product_received',"{{route('sendProductReceiveConfirmation')}}","{{__('Are you really received your product ?')}}");
            submitOperation(function (response){
                if (response.success == true){
                    swalRedirect("{{Request::url()}}",response.message,'success');
                }else {
                    swalError(response.message);
                }
            },'make_payment');
        });

        function deliveryRequestStatusResponse(response){
            if (response.success == true){
                let location = window.location;
                swalRedirect(location,response.message,'success');
            }else {
                swalError(response.message);
            }
        }
        function productReceiveConfirmationResponse(response){
            if (response.success == true){
                swalRedirect("{{Request::url()}}",response.message,'success');
            }else {
                swalError(response.message);
            }
        }


        $(document).on('click','.make_payment',function (){
            Ladda.bind(this);
            let load = $(this).ladda();
            const auction_data = {
                auction_id : $(this).data('id')
            }
            makeAjaxPostText(auction_data,"{{route('viewOwnAuctionPaymentPage')}}",load).done(function (response){
                $('#payment_body').html(response);
                $('#paymentModal').modal('show');
                load.ladda('stop');
            });

        });

        $(document).on('click','.view_details',function (){
            Ladda.bind(this);
            let load = $(this).ladda();
            const data ={
                id : $(this).data('id')
            }
            makeAjaxPostText(data,"{{route('loadAuctionStatusBody')}}",load).done(function (response){
                $('#status_modal_body').html(response);
                $('#viewStatusModal').modal('show');
            });
        })
    </script>
@endsection
