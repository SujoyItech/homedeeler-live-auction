@extends('web.profile.profile')
@section('profile-content')
    <div class="dashboard-widget mb-40">
        <div class="dashboard-title mb-30">
            <h5 class="title">My Activity</h5>
        </div>
        <div class="row justify-content-center mb-30-none">
            <div class="col-md-4 col-sm-6">
                <div class="dashboard-item">
                    <a href="{{route('myBids')}}">
                        <div class="thumb">
                            <img src="{{webAsset('images/dashboard/01.png')}}" alt="dashboard">
                        </div>
                        <div class="content">
                            <h2 class="title"><span class="counter">{{$total_bids->total_bids ?? 0}}</span></h2>
                            <h6 class="info">{{__('Active Bids')}}</h6>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="dashboard-item">
                    <a href="{{route('myWonItems')}}">
                        <div class="thumb">
                            <img src="{{webAsset('images/dashboard/02.png')}}" alt="dashboard">
                        </div>
                        <div class="content">
                            <h2 class="title"><span class="counter">{{$won_bids->won_bids ?? 0}}</span></h2>
                            <h6 class="info">{{__('Items Won')}}</h6>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="dashboard-item">
                    <a href="{{route('myFavourite')}}">
                        <div class="thumb">
                            <img src="{{webAsset('images/dashboard/03.png')}}" alt="dashboard">
                        </div>
                        <div class="content">
                            <h2 class="title"><span class="counter">{{$favourite_bids->favourite_bids ?? 0}}</span></h2>
                            <h6 class="info">{{__('Favorites')}}</h6>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
