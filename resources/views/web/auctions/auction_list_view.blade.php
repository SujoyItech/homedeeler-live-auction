@if(isset($auction_lists) && !empty($auction_lists[0]))
<div class="row mb-30-none">
    @foreach($auction_lists as $auction_list)
        <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3">
            @include('web.auctions.auction_card')
        </div>
    @endforeach
</div>
{{ $auction_lists->links('vendor.pagination.front-end') }}
@else
    <div class="row mb-30-none">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="auction-item-2 text-center"><span class="badge badge-warning p-2">{{__('No items found!')}}</span></div>
        </div>
    </div>
@endif

