<div class="product-header mb-40 style-2 bg-white">
    <div class="product-header-item">
        <div class="item">{{__('Sort By :')}} </div>
        <select name="sort-by" class="select-bar sort_by_class">
            <option value="all">{{__('All')}}</option>
            <option value="name">{{__('Name')}}</option>
            <option value="date">{{__('Date')}}</option>
            <option value="type">{{__('Type')}}</option>
            <option value="car">{{__('Car')}}</option>
        </select>
    </div>
    <div class="product-header-item">
        <div class="item">{{__('Show :')}} </div>
        <select name="pagination" class="select-bar pagination_class">
            <option value="12">{{__('12')}}</option>
            <option value="24">{{__('24')}}</option>
            <option value="36">{{__('36')}}</option>
            <option value="48">{{__('48')}}</option>
            <option value="60">{{__('60')}}</option>
        </select>
    </div>
    <form class="product-search ml-auto">
        <input type="text" name="search_key" placeholder="{{__('Search Items')}}">
        <button type="submit"><i class="fas fa-search"></i></button>
    </form>
</div>
