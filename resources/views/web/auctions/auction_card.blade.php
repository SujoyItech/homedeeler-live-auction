<div class="auction-item-2">
    {{--    @dd($auction_list)--}}
    <div
        class="upcoming-badge auction_in_live_{{$auction_list->id}}  {{ $auction_list->status == AUCTION_IN_LIVE ? '' : 'd-none'}}"
        title="Upcoming Auction">
        <i class="flaticon-auction"></i>
    </div>
    <div class="auction-thumb">
        <a href="{{route('auction',['slug'=>$auction_list->slug])}}"><img
                src="{{check_storage_image_exists($auction_list->product_image)}}"
                onerror='this.src="{{adminAsset('images/no-image.jpg')}}"' alt="product"></a>
        <a href="{{route('addAuctionToFavourite',['auction_id'=>$auction_list->id])}}"
           class="rating">@if($auction_list->is_favourite == TRUE) <i class="fa fa-star"></i> @else <i
                class="far fa-star"></i> @endif</a>
    </div>
    <div class="auction-content">
        <h6 class="title">
            <a href="{{route('auction',['slug'=>$auction_list->slug])}}">{{$auction_list->name}}</a>
        </h6>
        <div>
            <i class="fas fa-money-bill"></i> {{getMoney($auction_list->price_range_from)}} - {{getMoney($auction_list->price_range_to)}}
            {!! $auction_list->is_new ? '<span class="badge badge-dark float-right">NEW</span>' : '' !!}
        </div>
        <h6 class="title">
            <i class="fa fa-gavel" aria-hidden="true"></i> {{getMoney($auction_list->highest_bid??0)}}
            <span class="text-danger float-right"><i class="fa fa-users" aria-hidden="true"></i> {{$auction_list->total_bids ?? 0}}</span>
        </h6>
        <h6 class="title text-danger">
            <span><i class="fa fa-clock"></i> {{$auction_list->live_final_end_time}}</span>
            {!! $auction_list->status == AUCTION_IN_LIVE ? '<span class="badge badge-danger text-light float-right">LIVE</span>' : '' !!}
        </h6>

    <!--        <div class="countdown text-left">
            <div class="auction_end title" id="counter-{{$auction_list->id}}" data-value="{{$auction_list->live_final_end_time}}">
                {{$auction_list->live_final_end_time}}
        </div>
    </div>-->
    </div>
</div>
