<div class="auction-item-1">
{{--    @if($auction_list->status == AUCTION_IN_LIVE)--}}
        <div class="upcoming-badge-1" title="Upcoming Auction">
            <i class="flaticon-auction"></i>
        </div>
{{--    @endif--}}
    <div class="auction-thumb">
        <a href="{{route('auction',['slug'=>$auction_list->slug])}}"><img src="{{check_storage_image_exists($auction_list->product_image)}}" onerror='this.src="{{webAsset('images/auction/product/10.png')}}"' alt="product"></a>
        <a href="javascript:void(0);" class="rating"><i class="far fa-star"></i></a>
    </div>
    <div class="auction-content">
        <h6 class="title"><a href="{{route('auction',['slug'=>$auction_list->slug])}}">{{$auction_list->name}}</a></h6>
        <div>
            {{getMoney($auction_list->price_range_from)}} - {{getMoney($auction_list->price_range_to)}}
            {!! $auction_list->is_new ? '<span class="badge badge-dark float-right">NEW</span>' : '' !!}
        </div>
        <div class="countdown text-left">
            <div class="auction_end" id="counter-{{$auction_list->id}}" data-value="{{$auction_list->live_final_end_time}}">{{$auction_list->live_final_end_time}}</div>
        </div>
        <h6 class="my-2 text-success"><label>{{__('Current Bid')}}</label> {{getMoney($auction_list->highest_bid??0)}}</h6>
        <p class="">{{__('Total Bids ')}} {{$auction_list->total_bids ?? 0}}</p>
    </div>
</div>
