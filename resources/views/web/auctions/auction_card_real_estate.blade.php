<div class="auction-item-4">
    <div class="auction-thumb">
        <a href="{{route('auction',['slug'=>$auction_list->slug])}}"><img src="{{check_storage_image_exists($auction_list->product_image)}}" onerror='this.src="{{webAsset('images/auction/product/10.png')}}"' alt="product"></a>
        <a href="javascript:void(0);" class="bid"><i class="flaticon-auction"></i></a>
    </div>
    <div class="auction-content">
        <h4 class="title">
            <a href="{{route('auction',['slug'=>$auction_list->slug])}}">{{$auction_list->name}}</a>
        </h4>
        <div class="bid-area">
            <div class="bid-amount">
                <div class="icon">
                    <i class="flaticon-auction"></i>
                </div>
                <div class="amount-content">
                    <div class="current">{{__('Current Bid')}}</div>
                    <div class="amount">{{getMoney($auction_list->highest_bid)}}</div>
                </div>
            </div>
            <div class="bid-amount">
                <div class="icon">
                    <i class="flaticon-money"></i>
                </div>
                <div class="amount-content">
                    <div class="current">{{__('Total Bid')}}</div>
                    <div class="amount">{{$auction_list->total_bids ?? 0}} {{__('Bids')}}</div>
                </div>
            </div>
        </div>
        <div class="countdown-area">
            <div class="countdown">
                <div id="bid_counter30"></div>
            </div>
        </div>
        <div class="text-center">
            <a href="{{route('auction',['slug'=>$auction_list->slug])}}" class="custom-button">{{__('Submit a bid')}}</a>
        </div>
    </div>
</div>
