@extends('web.layouts.app')
@section('content')
    <!--============= Hero Section Starts Here =============-->
    <div class="hero-section">
        <div class="bg_img hero-bg" data-background="{{webAsset('images/banner/auction-bg.jpg')}}"></div>
    </div>
    <!--============= Hero Section Ends Here =============-->

    <!--============= Product Auction Section Starts Here =============-->
    <div class="product-auction padding-bottom mt--240">
        <div class="container-fluid px-5">
            <div class="row mb--50">
                <div class="col-lg-3 col-md-4 col-xl-3 col-sm-4 mb-50">
                    <div class="widget">
                        <h5 class="title">{{__('Categories')}}</h5>
                        @if(isset($parent_breadcrumb))
                            <h6 class="text-center"><a href="{{route('auctionList')}}">{{__('All')}}</a> <i class="fa fa-caret-right"></i>&ensp;{!! $parent_breadcrumb !!} </h6>
                            <hr>
                        @endif
                        <div class="widget-body">
                            @if(isset($category_id))
                                <input type="checkbox" hidden value="{{$category_id}}" name="category_id" class="category_id" id="category_{{$category_id}}" checked>
                            @endif
                            @if(isset($categories))
                                    @foreach($categories as $category)
                                        <div class="widget-form-group">
                                            <input type="checkbox" value="{{$category->id}}" name="category_id" class="category_id" id="category_{{$category->id}}" {{!empty($category_id) && $category_id == $category->id ? 'checked': ''}}>
                                            <label for="category_{{$category->id}}">{{$category->name}}</label>
                                        </div>
                                    @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="widget">
                        <h5 class="title">{{__('Filter by Price')}}</h5>
                        <div class="widget-body">
                            <div id="slider-range"></div>
                            <div class="price-range">
                                <label for="amount">{{__('Price')}} :</label>
                                <input type="text" id="amount" readonly>
                            </div>
                        </div>
                        <div class="text-center mt-20">
                            <a href="javascript:void(0)" class="custom-button price_filter">{{__('Filter')}}</a>
                        </div>
                    </div>
                    <div class="widget">
                        <h5 class="title">{{__('Ending Within')}}</h5>
                        <div class="widget-body">
                            <div class="widget-form-group">
                                <input type="checkbox" name="ending-type" class="ending_type" value="day" id="day">
                                <label for="day">{{__('1 Day')}}</label>
                            </div>
                            <div class="widget-form-group">
                                <input type="checkbox" name="ending-type" class="ending_type"  value="week" id="week">
                                <label for="week">{{__('1 Week')}}</label>
                            </div>
                            <div class="widget-form-group">
                                <input type="checkbox" name="ending-type" class="ending_type"  value="month1" id="month1">
                                <label for="month1">{{__('1 Month')}}</label>
                            </div>
                            <div class="widget-form-group">
                                <input type="checkbox" name="ending-type" class="ending_type" value="month3" id="month3">
                                <label for="month3">{{__('3 Month')}}</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 mb-50">
                    @include('web.auctions.auction_filtering')
                    <div class="auction-list-view" id="auction_list_view_id">
                        @if(isset($auction_lists) && !empty($auction_lists[0]))
                            <div class="row mb-30-none">
                                @foreach($auction_lists as $auction_list)
                                    <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3">
                                        @include('web.auctions.auction_card')
                                    </div>
                                @endforeach
                            </div>
                            {{ $auction_lists->links('vendor.pagination.front-end') }}
                        @else
                            <div class="row mb-30-none">
                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="auction-item-2 text-center"><span class="badge badge-warning p-2">{{__('No items found!')}}</span></div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--============= Product Auction Section Ends Here =============-->

@endsection
@section('script')
    <script>
        // $(document).ready(function (){
        //     loadAuctionListData(generateSearchArray());
        // })
        $(document).on('click','.category_id,.ending_type',function(){
            loadAuctionListData(generateSearchArray());
        });

        $(document).on('click','.price_filter',function(){
            let search_array = generateSearchArray();
            search_array['price_range'] = getPriceRange();
            loadAuctionListData(search_array);
        });

        $('.sort_by_class,.pagination_class').change(function(){
            loadAuctionListData(generateSearchArray());
        });

        function generateSearchArray(){
            let search_array = {
                category_id : getCheckedCategory(),
                pagination : getPaginateData(),
                order_by : getOrderData(),
                ending_type : getEndingTypeData(),
                search_key : "{{$search_key ?? ''}}"
            };
            return search_array;
        }

        function getCheckedCategory(){
            let category_id = [];
            $('input:checkbox[name=category_id]:checked').each(function(){
                category_id.push($(this).val());
            });
            return category_id;
        }

        function getPaginateData(){
           return  $('select[name="pagination"]').val()
        }

        function getOrderData(){
           return  $('select[name="sort-by"]').val()
        }

        function getEndingTypeData(){
            let ending_type = [];
            $('input:checkbox[name="ending-type"]:checked').each(function(){
                ending_type.push($(this).val());
            });
            return ending_type;
        }

        function getPriceRange(){
            let price_range = {
                min : $('#slider-range').slider("values")[0],
                max : $('#slider-range').slider("values")[1],
            };
            return price_range;
        }

        function loadAuctionListData(search_array){
            makeAjaxPostText(search_array,"{{route('auctionList',['page'=>$page ?? '','type'=>$type ?? ''])}}").done(function (response){
                $('#auction_list_view_id').html(response);
                countDownEndAuction();
            });
        }
    </script>
@endsection
