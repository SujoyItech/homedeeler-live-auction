@extends('web.layouts.app')
@section('content')
    <!--============= Hero Section Starts Here =============-->
    <div class="hero-section">
        <div class="bg_img hero-bg" data-background="{{webAsset('images/banner/auction-bg.jpg')}}"></div>
    </div>
    <!--============= Hero Section Ends Here =============-->
    <!--============= Contact Section Starts Here =============-->
    <section class="contact-section padding-bottom mt--240">
        <div class="container">
            <div class="contact-wrapper padding-top padding-bottom">
                <div class="section-header">
                    <h5 class="cate">{{__('Faqs')}}</h5>
                    <p>{{__('Some of the common questions about home deeler showing below.')}}</p>
                </div>
                <div class="row">
                    <div class="col-md-11">
                        <div class="accordion">
                            @if(isset($faqs))
                                @php($flag = 0)
                                @foreach($faqs as $faq)
                                    <div class="card">
                                        <div class="card-header">
                                            <h6>
                                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-{{$faq->id}}"
                                                        aria-expanded="false" aria-controls="accordion-{{$faq->id}}">{{$faq->question}}</button>
                                            </h6>
                                        </div>
                                        <div class="collapse {{$flag == 0 ? 'show' : ''}}" id="accordion-{{$faq->id}}"
                                             aria-labelledby="accordion-{{$faq->id}}" data-parent="#accordion-{{$faq->id}}">
                                            <div class="card-body">
                                                <p>{{$faq->answer}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    @php($flag = 1)
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--============= Contact Section Ends Here =============-->
@endsection
