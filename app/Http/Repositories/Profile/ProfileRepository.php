<?php

namespace App\Http\Repositories\Profile;


use App\Http\Repositories\BaseRepository;
use App\Models\User;

class ProfileRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param Profile/Profile $model
       */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository
}
