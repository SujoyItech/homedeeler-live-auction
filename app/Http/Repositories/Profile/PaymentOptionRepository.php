<?php

namespace App\Http\Repositories\Profile;


use App\Http\Repositories\BaseRepository;

use App\Models\UserPaymentOption;
use Illuminate\Support\Facades\DB;

class PaymentOptionRepository extends BaseRepository {
    public function __construct(UserPaymentOption $model) {
        parent::__construct($model);
    }
    // Your methods for repository

    public function userPaymentStatusChange(array $requestArray){
        $this->model::where(['user_id'=>$requestArray['user_id']])->update(['active'=>INACTIVE]);
        $this->model::where('id',$requestArray['id'])->update(['active'=>STATUS_ACTIVE]);
        return TRUE;
    }
}
