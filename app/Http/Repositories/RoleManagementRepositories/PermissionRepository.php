<?php


namespace App\Http\Repositories\RoleManagementRepositories;

use App\Http\Repositories\BaseRepository;
use App\Models\Permission;


class PermissionRepository extends BaseRepository {

    /**
     * PermissionRepository constructor.
     *
     * @param Permission $model
     */
    public function __construct(Permission $model) {
        parent::__construct($model);
    }

    /**
     * @return mixed
     */
    public function permissionWithRole() {
        $data = [];
        $groups = $this->model->select('permissions.route_group')->distinct('permissions.route_group')->get();
        foreach ($groups as $group) {
            $data[$group->route_group] = $this->model
                ->select(
                    'permissions.id',
                    'permissions.route_name',
                    'permissions.route_group',
                    'permission_role.permission_id',
                    'permission_role.role_id'
                )
                ->where(['route_group' => $group->route_group])
                ->leftJoin('permission_role', ['permissions.id' => 'permission_role.permission_id'])
                ->get();
        }
        
        return $data;
    }
}
