<?php


namespace App\Http\Repositories\RoleManagementRepositories;


use App\Http\Repositories\BaseRepository;
use App\Models\PermissionRole;

class RolePermissionRepository extends BaseRepository {

    /**
     * RolePermissionRepository constructor.
     *
     * @param PermissionRole $model
     */
    public function __construct(PermissionRole $model) {
        parent::__construct($model);
    }
}
