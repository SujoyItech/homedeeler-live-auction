<?php

namespace App\Http\Repositories\Auction;


use App\Http\Repositories\BaseRepository;
use App\Models\Auction\Auction;
use Illuminate\Support\Facades\DB;

class AuctionRepository extends BaseRepository
{
    /**
     * Instantiate repository
     *
     * @param Auction/Auction $model
     */
    public function __construct(Auction $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository

    public function getAuctionList($search_array, $where = [])
    {
        $query = $this->model::select(
            'auctions.*',
            'products.name',
            'products.slug',
            'products.description as product_description',
            'products.additional_info as product_additional_info',
            'products.seller_id',
            'users.name as seller_name',
            'product_combinations.media_url as product_image'
        );
        $query->join('products', 'auctions.product_id', '=', 'products.id');
        $query->leftJoin('product_combinations', function ($join) {
            $join->on('products.id', 'product_combinations.product_id')
                ->where('product_combinations.is_featured', ACTIVE);
        });
        $query->leftJoin('users', 'products.seller_id', '=', 'users.id');
        if (!empty($where)) {
            $query->where($where);
        }

        if (isset($search_array) && !empty($search_array)) {
            foreach ($search_array as $search) {
                if ($search['value'] !== NULL) {
                    if ($search['name'] == 'name_description') {
                        $query->where(function ($inner_query) use ($search) {
                            $inner_query->where('products.name', 'like', '%' . $search['value'] . '%')
                                ->orWhere('products.description', 'like', '%' . $search['value'] . '%');
                        });
                    } else if ($search['name'] == 'special_case') {

                        if ($search['value'] == 'n_ct') {
                            $query->leftJoin('product_categories', 'products.id', 'product_categories.product_id');
                            $query->where('product_categories.category_id', NULL);
                        } else if ($search['value'] == 'n_t') {
                            $query->leftJoin('product_tags', 'products.id', 'product_tags.product_id');
                            $query->where('product_tags.tag_id', NULL);
                        } else if ($search['value'] == 'n_cr') {
                            $query->whereNotIn('products.id', function ($subQuery) use ($search) {
                                $subQuery->select(DB::raw('DISTINCT product_id'))
                                    ->from('product_combinations')
                                    ->where('combination_type_id', COLOR_ID);
                            });
                        } else if ($search['value'] == 'n_m') {
                            $query->whereNotIn('products.id', function ($subQuery) use ($search) {
                                $subQuery->select(DB::raw('DISTINCT product_id'))
                                    ->from('product_combinations')
                                    ->where('media_type', '!=', NULL);
                            });
                        }
                    } elseif ($search['name'] == 'status') {
                        $query->where('auctions.status', $search['value']);
                    } else {
                        $query->where('products.' . $search['name'], 'like', '%' . $search['value'] . '%');
                    }
                }
            }
        }
        return $query->groupBy('products.id');
    }

    public function getAuctionDetails($slug)
    {
        return $this->model::select(
            'auctions.*',
            'products.name',
            'products.slug',
            'products.description',
            'products.additional_info',
            'products.is_new',
            'products.meta_title',
            'products.meta_keywords',
            'products.meta_description',
            'products.price_range_from',
            'products.price_range_to',
            'users.name as winner name'
        )
            ->join('products', function ($join) use ($slug) {
                $join->on('auctions.product_id', 'products.id')->where('products.slug', $slug);
            })->leftJoin('users', 'auctions.winner_id', 'users.id')->first();
    }

    public function getAuctionBiddingDetailsData($slug)
    {
        return DB::table('auction_details')
            ->select('auction_details.*', 'products.name as product_name', 'users.name as bidder_name', 'users.profile_photo_path as bidder_image')
            ->join('auctions', 'auction_details.auction_id', '=', 'auctions.id')
            ->join('products', 'auctions.product_id', '=', 'products.id')
            ->join('users', 'auction_details.bidder_id', '=', 'users.id')
            ->where('products.slug', $slug)
            ->get();
    }
}
