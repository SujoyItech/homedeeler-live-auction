<?php

namespace App\Http\Repositories;

use App\Models\ApiEndpoint;

class ApiEndpointRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param ApiEndpoint $model
       */
    public function __construct(ApiEndpoint $model)
    {
        $this->model = $model;
    }

    // Your methods for repository
}
