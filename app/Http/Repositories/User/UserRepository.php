<?php

namespace App\Http\Repositories\User;

use App\Http\Repositories\BaseRepository;
use App\Http\Services\MailService;
use App\Jobs\SendMailJob;
use App\Models\User;
use App\Models\UserBrand;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param User/User $model
       */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository

    public function getUserList(){
       return $this->model::select('users.*','roles.title as role_name')
                          ->leftJoin('roles','users.role','roles.id')
                          ->where('users.module_id','=',MODULE_USER_ADMIN)
                          ->where('users.id','<>',Auth::user()->id)
                          ->orderBy('users.created_at','desc');
    }

    public function getSellerList(){
       return $this->model::select('users.*')->where(['users.module_id'=>MODULE_USER])->where('role',USER_SELLER)->get();
    }

    public function getBidderList(){
       return $this->model::select('users.*')->where(['users.module_id'=>MODULE_USER,'role'=>USER_BIDDER])->get();
    }

    public function getUserDetails($id){
        return $this->model::where('id',$id)->first();
    }

    public function userPasswordChangeMail($user) {
        if ($user) {
            $userName = $user->name;
            $userEmail = $user->email;
            $subject = __('Home Deeler Password Change Mail');
            $data['name'] = $userName;
            $data['remember_token'] = $user->remember_token;
            dispatch(new SendMailJob('admin.mail.email.reset_password_mail_web', $userEmail, $data, $subject));
        }
    }
}
