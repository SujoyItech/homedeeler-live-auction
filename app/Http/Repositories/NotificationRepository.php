<?php

namespace App\Http\Repositories;

use App\Models\Notification;

class NotificationRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param Notification $model
       */
    public function __construct(Notification $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository
}
