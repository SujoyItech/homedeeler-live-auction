<?php

namespace App\Http\Repositories\Product;

use App\Http\Repositories\BaseRepository;
use App\Models\Product\Combination;

class CombinationRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param Product/Combination $model
       */
    public function __construct(Combination $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository

    public function getCombinationDetails($id){
        return $this->model::where(['id'=>$id])->first();
    }

    public function getCombinationQuery() {
        return $this->model::leftJoin('combination_types','combinations.combination_type_id','=','combination_types.id')->select('combinations.*','combination_types.name as combination_type')->get();
    }
}
