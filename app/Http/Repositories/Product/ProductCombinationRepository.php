<?php

namespace App\Http\Repositories\Product;

use App\Http\Repositories\BaseRepository;
use App\Models\Product\ProductCombination;
use Illuminate\Support\Facades\DB;

class ProductCombinationRepository extends BaseRepository
{
    /**
     * ProductCombinationRepository constructor.
     * @param ProductCombination $model
     */
    public function __construct(ProductCombination $model)
    {
        parent::__construct($model);
    }

    public function getProductImagesByColor($product_id,$is_featured=FALSE){
        if ($is_featured){
            return $this->model::where(['product_id'=>$product_id])->where('is_featured','=',TRUE)->first();
        }else{
            return $this->model::where(['product_id'=>$product_id])->where('media_url','<>',NULL)->get();
        }
    }

    public function saveFeatured($productId, $productCombinationId) {
        return $this->model::where('product_id', $productId)
            ->where(function ($query){
                $query->where('combination_type_id', COLOR_ID);
                $query->orWhere('combination_type_id', null);
            })
            ->update(['is_featured'=>DB::raw('CASE WHEN id = '.$productCombinationId.' THEN '.ACTIVE.' ELSE '.INACTIVE.' END')]);
    }

    public function duplicateProductCombinations($newProductId, $oldProductId) {
        $query = "INSERT INTO `product_combinations` (`product_id`, `combination_type_id`, `combination_id`, `media_type`, `media_url`, `status`,`is_featured`,
                    `created_at`,`updated_at`) SELECT :product_id, `combination_type_id`, `combination_id`, `media_type`, `media_url`,
                    `status`,`is_featured`, now(), now() FROM `product_combinations` WHERE `product_id`=:old_product_id;";
        return DB::statement($query, ['product_id'=>$newProductId, 'old_product_id'=>$oldProductId]);
    }
}
