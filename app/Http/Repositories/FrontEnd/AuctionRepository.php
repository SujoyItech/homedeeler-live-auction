<?php

namespace App\Http\Repositories\FrontEnd;


use App\Http\Repositories\BaseRepository;
use App\Models\Auction\Auction;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuctionRepository extends BaseRepository
{
    /**
     * AuctionRepository constructor.
     *
     * @param Auction $model
     */
    public function __construct(Auction $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository

    public function getAuctionList($search_array = [], $where = [], $type = NULL, $order_by = 'id', $paginate = NULL, $limit = NULL, $page = NULL)
    {
        $query = $this->model::select(
            'auctions.*',
            'products.name',
            'products.slug',
            'products.description as product_description',
            'products.additional_info as product_additional_info',
            'products.seller_id',
            'users.name as seller_name',
            'products.price_range_from',
            'products.price_range_to',
            'products.is_new',
            'product_combinations.media_url as product_image',
            'auction_favourites.is_favourite',
            DB::raw("COUNT(auction_details.auction_id) as total_bids")
        );

        $query->join('products', 'auctions.product_id', '=', 'products.id');
        $query->leftJoin('users', 'products.seller_id', '=', 'users.id');
        $query->leftJoin('auction_favourites', 'auctions.id', '=', 'auction_favourites.auction_id');
        $query->leftJoin('product_categories', 'products.id', '=', 'product_categories.product_id');
        $query->leftJoin('categories', 'product_categories.category_id', '=', 'categories.id');
        $query->leftJoin('product_combinations', function ($join) {
            $join->on('products.id', 'product_combinations.product_id')
                ->where('product_combinations.is_featured', ACTIVE);
        });
        $query->leftJoin('auction_details', 'auctions.id', '=', 'auction_details.auction_id');
        $query->where('auctions.is_approved', ACTIVE);
        $query->where('auctions.status', '>=', STATUS_ACTIVE)
            ->where('auctions.status', '<=', AUCTION_LIVE_END)
            ->where('auctions.live_final_end_time', '>=', Carbon::now());

        if (!empty($type)) {
            $this->prepareQueryByType($query, $type);
        }
        $this->prepareSearchQuery($search_array, $query);
        if (!empty($where)) {
            $query->where($where);
        }
        $this->prepareQueryByOrder($order_by, $query);
        $query->groupBy('products.id');
        if (!empty($paginate)) {
            if (!empty($page)) {
                return $query->paginate($paginate, ['*'], 'page', $page);
            } else {
                return $query->paginate($paginate);
            }
        } else if (!empty($limit)) {
            return $query->limit($limit)->get();
        } else {
            return $query->get();
        }
    }

    private function prepareSearchQuery($search_array, $query)
    {
        if (isset($search_array['search_key'])) {
            $search_key = strtolower($search_array['search_key']);
            $query->leftJoin('product_tags', 'products.id', '=', 'product_tags.product_id');
            $query->leftJoin('tags', 'product_tags.tag_id', '=', 'tags.id');
            $query->leftJoin('product_brands', 'products.id', '=', 'product_brands.product_id');
            $query->leftJoin('brands', 'product_brands.brand_id', '=', 'brands.id');
            $query->where(function ($inner_query) use ($search_key) {
                $inner_query->where('products.name', 'like', '%' . $search_key . '%')
                    ->orWhere('products.description', 'like', '%' . $search_key . '%')
                    ->orWhereRaw("LOWER(json_unquote( json_extract( categories.name, '$.en' ))) LIKE '%{$search_key}%'")
                    ->orWhere('brands.name', 'like', '%' . $search_key . '%')
                    ->orWhereRaw("LOWER(json_unquote( json_extract( tags.name, '$.en' ))) LIKE '%{$search_key}%'");
            });
        }
        if (isset($search_array['category_id']) && !empty($search_array['category_id'])) {
            $query->whereIn('product_categories.category_id', $search_array['category_id']);
        }
        if (isset($search_array['price_range']) && !empty($search_array['price_range'])) {
            $price_range = $search_array['price_range'];
            $query->where(function ($condition) use ($price_range) {
                $condition->where(function ($inner_query) use ($price_range) {
                    $inner_query->where('products.price_range_from', '>=', $price_range['min'])->where('products.price_range_from', '<=', $price_range['max']);
                })->orWhere(function ($inner_query) use ($price_range) {
                    $inner_query->where('products.price_range_to', '>=', $price_range['min'])->where('products.price_range_to', '<=', $price_range['max']);
                });
            });
        }
        if (isset($search_array['ending_type']) && !empty($search_array['ending_type'])) {
            $ending_date = $search_array['ending_type'];
            foreach ($ending_date as $value) {
                if ($value == 'day') {
                    $today = Carbon::now()->toDateString();
                    $query->where('auctions.live_final_end_time', '=', $today);
                } else if ($value == 'week') {
                    $week = Carbon::now()->addWeek();
                    $query->where('auctions.live_final_end_time', '=<', $week);
                } else if ($value == 'month1') {
                    $month = Carbon::now()->addMonth();
                    $query->where('auctions.live_final_end_time', '<', $month);
                } else if ($value == 'month3') {
                    $three_month = Carbon::now()->addMonth(3);
                    $query->where('auctions.live_final_end_time', '<', $three_month);
                }
            }
        }
    }

    private function prepareQueryByOrder($order_by, $query)
    {
        if ($order_by == 'name') {
            $query->orderBy('products.name', 'asc');
        } else if ($order_by == 'date') {
            $query->orderBy('auctions.created_at', 'desc');
        } else {
            $query->orderBy('auctions.id', 'desc');
        }
    }

    private function prepareQueryByType($query, $type)
    {
        if (!empty($type)) {
            if ($type == 'today_auctions') {
                $query->where(DB::raw("DATE(auctions.live_start_time)"), date('Y-m-d'));
            } else if ($type == 'featured') {
                $query->where(['auctions.is_featured' => TRUE]);
            } else if ($type == 'is_new') {
                $query->where(['products.is_new' => TRUE]);
            } else if ($type == 'is_favourite') {
                $query->where(['auction_favourites.user_id' => Auth::user()->id, 'is_favourite' => TRUE]);
            } else if ($type == 'my_active_bids') {
                $dt = Carbon::now()->toDateString();
                $query->join('auction_details', function ($inner_join) {
                    $inner_join->on('auctions.id', 'auction_details.auction_id')
                        ->where('auction_details.bidder_id', Auth::user()->id);
                });
                $query->where('auctions.live_end_time', '>=', $dt);
            } else if ($type == 'my_expired_bids') {
                $dt = Carbon::now()->toDateString();
                $query->join('auction_details', function ($inner_join) {
                    $inner_join->on('auctions.id', 'auction_details.auction_id')
                        ->where('auction_details.bidder_id', Auth::user()->id);
                });
                $query->where('auctions.live_end_time', '<', $dt);
            } else if ($type == 'my_own_bids') {
                $query->where('auctions.winner_id', Auth::user()->id);
            } else if ($type == 'my_approved_auctions') {
                $query->where(['products.seller_id' => Auth::user()->id, 'auctions.status' => ACTIVE]);
            } else if ($type == 'my_pending_auctions') {
                $query->where(['products.seller_id' => Auth::user()->id, 'auctions.status' => INACTIVE]);
            } else if ($type == 'my_live_items') {
                $query->where(['products.seller_id' => Auth::user()->id, 'auctions.status' => AUCTION_IN_LIVE]);
            } else if ($type == 'my_sold_items') {
                $query->where(['products.seller_id' => Auth::user()->id])->where('auctions.status', '>=', SOLD);
            }
        }
    }

    public function getAuctionDetails($slug)
    {
        return $this->model::select(
            'auctions.*',
            'products.name',
            'products.seller_id',
            'products.slug',
            'products.description as product_description',
            'products.additional_info as product_additional_info',
            'products.seller_id',
            'products.is_new',
            'users.name as seller_name',
            'brands.name as brand_name',
            'auction_favourites.is_favourite',
            'products.price_range_from',
            'products.price_range_to',
            'product_combinations.media_url as product_image',
            DB::raw("GROUP_CONCAT(categories.id) AS category_id")
        )->join('products', function ($join) use ($slug) {
            $join->on('auctions.product_id', 'products.id')->where('products.slug', $slug);
        })
            ->leftJoin('product_brands', 'products.id', '=', 'product_brands.product_id')
            ->leftJoin('brands', 'product_brands.product_id', '=', 'brands.id')
            ->leftJoin('users', 'products.seller_id', '=', 'users.id')
            ->leftJoin('auction_favourites', 'auctions.id', '=', 'auction_favourites.auction_id')
            ->leftJoin('product_categories', 'products.id', '=', 'product_categories.product_id')
            ->leftJoin('categories', function ($join) {
                $join->on('product_categories.category_id', '=', 'categories.id')
                    ->where('categories.parent_id', ACTIVE);
            })
            ->leftJoin('product_combinations', function ($join) {
                $join->on('products.id', 'product_combinations.product_id')
                    ->where('product_combinations.is_featured', ACTIVE);
            })->first();
    }
}
