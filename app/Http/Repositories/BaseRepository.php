<?php

namespace App\Http\Repositories;

use App\Http\Traits\Sortable;
use App\Http\Traits\Relationable;

abstract class BaseRepository
{
    use Sortable, Relationable;

    /**
     * @var
     */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param $model
     */
    public function __construct($model) {
        $this->model = $model;
    }

    /**
     * @return  mixed
     */
    public function all()
    {
        return $this->model
            ->with($this->relations)
            ->orderBy($this->sortBy, $this->sortOrder)
            ->get();
    }

    /**
     * @param mixed $paginate
     *
     * @return mixed
     */
    public function paginated($paginate)
    {
        return $this
            ->model
            ->with($this->relations)
            ->orderBy($this->sortBy, $this->sortOrder)
            ->paginate($paginate);
    }

    /**
     * @param array $input
     *
     * @return mixed
     */
    public function create(array $input)
    {
        return $this->model->create($input);
    }

    /**
     * @param array $input
     *
     * @return mixed
     */
    public function insert(array $input) {
        return $this->model->insert($input);
    }

    /**
     * @param mixed $id
     *
     * @return mixed
     */
    public function find($id)
    {
        return $this->model->where('id', $id)->first();
    }

    /**
     * @param mixed $id
     *
     * @return mixed
     */
    public function destroy($id)
    {
        return $this->find($id)->delete();
    }

    /**
     * @param mixed $id
     * @param array $input
     *
     * @return mixed
     */
    public function update($id, array $input)
    {
        return $this->model->where(['id' => $id])->update($input);
    }


    //for triggering setter method in Model
    public function updateModel($id, array $input)
    {
        return $this->model->find($id)->update($input);
    }

    /**
     * @param $columnToSort
     * @param $orderBy
     * @param $rowPerPage
     *
     * @return mixed
     */
    public function paginateForDataTable($columnToSort, $orderBy, $rowPerPage) {
        return $this->model->orderBy($columnToSort, $orderBy)->paginate($rowPerPage);
    }

    /**
     * @param array $where
     * @param array $update
     * @return mixed
     */
    public function updateWhere(array $where=[], array $update=[])
    {
        $query = $this->model::query();
        foreach($where as $key => $value) {
            if(is_array($value)){
                $query->where($key,$value[0],$value[1]);
            }else{
                $query->where($key,'=',$value);
            }
        }
        return $query->update($update);
    }

    /**
     * @param array $where
     * @param bool $isForce
     * @return mixed
     */
    public function deleteWhere($where=[], $isForce = false)
    {
        $query = $this->model::query();
        foreach($where as $key => $value) {
            if(is_array($value)){
                $query->where($key,$value[0],$value[1]);
            }else{
                $query->where($key,'=',$value);
            }
        }

        if ($isForce) {
            return $query->forceDelete();
        }
        return $query->delete();
    }

    public function getData(array $where=[],array $select=null,array $orderBy=[],array $with=[], $paginate=0){
        if($select == null){
            $select = ['*'];
        }
        $query = $this->model::select($select);
        foreach($with as $wt) {
            $query = $query->with($wt);
        }
        foreach($where as $key => $value) {
            if(is_array($value)){
                $query->where($key,$value[0],$value[1]);
            }else{
                $query->where($key,'=',$value);
            }
        }
        foreach($orderBy as $key => $value) {
            $query->orderBy($key,$value);
        }

        if ($paginate) {
            return $query->paginate($paginate);
        }
        return $query->get();
    }

    /**
     * @param array $where
     *
     * @return mixed
     */
    public function firstWhere(array $where) {
        return $this->model->where($where)->first();
    }

    public function getSingleRowData(array $where=[],array $select=null,array $orderBy=[],array $with=[]){
        if($select == null){
            $select = ['*'];
        }
        $query = $this->model::select($select);
        foreach($with as $wt) {
            $query = $query->with($wt);
        }
        foreach($where as $key => $value) {
            if(is_array($value)){
                $query->where($key,$value[0],$value[1]);
            }else{
                $query->where($key,'=',$value);
            }
        }
        foreach($orderBy as $key => $value) {
            $query->orderBy($key,$value);
        }

        return $query->first();
    }

    public function updateOrCreate(array $where, array $data) {
        return $this->model->updateOrCreate($where, $data);
    }

    public function paginateWhere(array $where) {
        return $this->model->where($where)->paginate();
    }

    public function getQuery(array $where=[],array $select=null, array $with=[]){
        if($select == null){
            $select = ['*'];
        }
        $query = $this->model::select($select);
        foreach($with as $wt) {
            $query = $query->with($wt);
        }
        foreach($where as $key => $value) {
            if(is_array($value)){
                $query->where($key,$value[0],$value[1]);
            }else{
                $query->where($key,'=',$value);
            }
        }

        return $query;
    }

    public function updateWhereNotInAndWhere($notInColumn, array $whereNotIn, array $where, array $data) {
        return $this->model->whereNotIn($notInColumn, $whereNotIn)->where($where)->update($data);
    }

    public function updateWhereInAndWhere($inColumnArray, array $inWhere, array $where, array $data) {
        return $this->model->whereIn($inColumnArray, $inWhere)->where($where)->update($data);
    }

    public function getDataWhereNotInAndWhere($notInColumn, array $whereNotInArray, array $where) {
        return $this->model::whereNotIn($notInColumn, $whereNotInArray)->where($where)->get();
        //$query = vsprintf(str_replace(array('?'), array('\'%s\''), $query->toSql()), $query->getBindings());
    }

    public function getDataWhereIn($key,array $whereInArray) {
        return $this->model::whereIn($key, $whereInArray)->get();
    }

    public function getSlug($slug,$id){
        return $this->model::where('slug',$slug)->where('id','<>',$id)->first();
    }

    public function getIdBySlug($slug){
        return $this->model::where('slug','=',$slug)->first()->id ?? '';
    }


}
