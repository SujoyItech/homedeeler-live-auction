<?php

namespace App\Http\Repositories\Message;

use App\Http\Repositories\BaseRepository;
use App\Models\Message\Messaging;

class MessagingRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param Message/Messaging $model
       */
    public function __construct(Messaging $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository

    public function getAuctionMessages($user_id){
        return $this->model::select('messaging.*','products.name as auction_name','receiver.name as receiver_name',
            'sender.name as sender_name','sender.profile_photo_path as sender_image')
               ->join('auctions','messaging.event_id','auctions.id')
               ->join('products','auctions.product_id','products.id')
               ->join('users as receiver','messaging.receiver','receiver.id')
               ->join('users as sender','messaging.sender','sender.id')
               ->where('messaging.receiver',$user_id)
               ->where('messaging.event_type','auction')
               ->orderBy('id','desc')
               ->get();
    }

    public function getLastAuctionMessages($user_id){
        return $this->model::select('messaging.*','products.name as auction_name','receiver.name as receiver_name',
            'sender.name as sender_name','sender.profile_photo_path as sender_image')
               ->join('auctions','messaging.event_id','auctions.id')
               ->join('products','auctions.product_id','products.id')
               ->join('users as receiver','messaging.receiver','receiver.id')
               ->join('users as sender','messaging.sender','sender.id')
               ->where('messaging.receiver',$user_id)
               ->orderBy('id','desc')
               ->first();
    }

    public function getAuctionMessageById($message_id){
        return $this->model::select('messaging.*','products.name as auction_name','receiver.name as receiver_name',
            'sender.name as sender_name','sender.profile_photo_path as sender_image')
               ->join('auctions','messaging.event_id','auctions.id')
               ->join('products','auctions.product_id','products.id')
               ->join('users as receiver','messaging.receiver','receiver.id')
               ->join('users as sender','messaging.sender','sender.id')
               ->where('messaging.id',$message_id)
               ->first();
    }


    public function getAdminMessages($user_id){
        return $this->model::select('messaging.*','products.name as auction_name','receiver.name as receiver_name',
            'sender.name as sender_name','sender.profile_photo_path as sender_image')
            ->join('auctions','messaging.event_id','auctions.id')
            ->join('products','auctions.product_id','products.id')
            ->join('users as receiver','messaging.receiver','receiver.id')
            ->join('users as sender','messaging.sender','sender.id')
            ->where('messaging.receiver',$user_id)
            ->where('messaging.event_type','admin')
            ->orderBy('id','desc')
            ->get();
    }

    public function getLastAdminMessages($user_id){
        return $this->model::select('messaging.*','products.name as auction_name','receiver.name as receiver_name',
            'sender.name as sender_name','sender.profile_photo_path as sender_image')
            ->join('auctions','messaging.event_id','auctions.id')
            ->join('products','auctions.product_id','products.id')
            ->join('users as receiver','messaging.receiver','receiver.id')
            ->join('users as sender','messaging.sender','sender.id')
            ->where('messaging.receiver',$user_id)
            ->orderBy('id','desc')
            ->first();
    }

    public function getAdminMessageById($message_id){
        return $this->model::select('messaging.*','products.name as auction_name','seller.name as seller_name',
            'sender.name as sender_name','sender.profile_photo_path as sender_image')
            ->join('auctions','messaging.event_id','auctions.id')
            ->join('products','auctions.product_id','products.id')
            ->join('users as receiver','messaging.receiver','receiver.id')
            ->join('users as sender','messaging.sender','sender.id')
            ->where('messaging.id',$message_id)
            ->first();
    }
}
