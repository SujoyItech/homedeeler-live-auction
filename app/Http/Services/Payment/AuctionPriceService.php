<?php

namespace App\Http\Services\Payment;


use App\Models\Auction\Auction;
use Carbon\Carbon;
use DateTime;

class AuctionPriceService
{

    public function calculateLivePaymentCharge($requestArray)
    {
        $settings = __options(['commission_settings']);
        $live_time = Carbon::parse($requestArray['live_start_time'])->format('H:i');

        if ($this->isTimeBetween($settings->pick_start, $settings->pick_end, $live_time)) {
            $duration = 'pick_' . $requestArray['live_duration'] . '_min';
            if (isset($settings->$duration)) {
                $charge = $settings->$duration;
            } else {
                return FALSE;
            }
        } else {
            $duration = 'off_pick_' . $requestArray['live_duration'] . '_min';
            if (isset($settings->$duration)) {
                $charge = $settings->$duration;
            } else {
                return FALSE;
            }
        }
        return floatval($charge);
    }

    private function isTimeBetween($from, $till, $input)
    {
        $f = DateTime::createFromFormat('!H:i', $from);
        $t = DateTime::createFromFormat('!H:i', $till);
        $i = DateTime::createFromFormat('!H:i', $input);
        if ($f > $t) $t->modify('+1 day');
        return ($f <= $i && $i <= $t) || ($f <= $i->modify('+1 day') && $i <= $t);
    }

    public function calculateProductPaymentCharge($auction_id)
    {
        $auction = Auction::where('id', $auction_id)->first();
        $total_price = (float)$auction->highest_bid + getPercentageValue($auction->highest_bid, $auction->service_charge);
        return $total_price;
    }
}
