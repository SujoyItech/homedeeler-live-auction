<?php

namespace App\Http\Services\Message;

use App\Http\Repositories\Message\MessagingRepository;
use App\Http\Services\BaseService;
use App\Models\Message\Messaging;
use App\Models\Message\MessagingDetails;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class MessagingService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param Message/MessagingRepository $repository
     */
    public function __construct(MessagingRepository $repository)
    {
        $this->repo = $repository;
    }

    public function getAuctionMessages($user_id){
        $data['event_messages'] = $this->repo->getAuctionMessages($user_id);
        return $data;
    }

    public function getAdminMessages($user_id){
        $data['event_messages'] = $this->repo->getAdminMessages($user_id);
        return $data;
    }

    public function getLastAuctionMessage($user_id){
        return $this->repo->getLastAuctionMessages($user_id);
    }


    public function getAuctionMessageById($message_id){
        return  $this->repo->getAuctionMessageById($message_id);
    }

    public function getMessageDetails($message_id){
        return MessagingDetails::select('messaging_details.*','users.name as sender_name','users.profile_photo_path as sender_image')
                        ->join('users','messaging_details.sender','users.id')
                        ->where('messaging_id',$message_id)->get();
    }

    public function prepareMessageDetailsData($message_details_array,$auction_id,$count=0){
        $flag = 1;
        while ($flag){
            $total_count = count($message_details_array);
            if ($total_count < 10){
                $old_message = $this->getMessageFromStorage($auction_id, $count);
                if (count($old_message) == 0){
                    $flag = 0;
                    break;
                }else{
                    $message_details_array = array_merge($old_message,$message_details_array);
                }
            }else{
                $flag = 0;
                break;
            }
            $count ++ ;
        }

        $data['message_details'] = $message_details_array;
        $data['count'] = $count;
        return $data;
    }

    public function prepareAdminMessageDetailsData($message_details_array,$auction_id,$count=0){
        $flag = 1;
        while ($flag){
            $total_count = count($message_details_array);
            if ($total_count < 10){
                $old_message = $this->getAdminMessageFromStorage($auction_id, $count);
                if (count($old_message) == 0){
                    $flag = 0;
                    break;
                }else{
                    $message_details_array = array_merge($old_message,$message_details_array);
                }
            }else{
                $flag = 0;
                break;
            }
            $count ++ ;
        }

        $data['message_details'] = $message_details_array;
        $data['count'] = $count;
        return $data;
    }

    public function sendMessage(array $requestArray){
        try {
            $requestArray['created_at'] = Carbon::now();
            MessagingDetails::insert($requestArray);
            Messaging::where('id',$requestArray['messaging_id'])->update(['synced'=>INACTIVE]);
            return jsonResponse(TRUE)->message(__('Message sent successfully.'));
        }catch (\Exception $exception){
            return jsonResponse(TRUE)->default();
        }
    }

    public function getAllMessagesFromStorage($auction_id,$index){
        try {
            $data['messages'] = $this->getMessageFromStorage($auction_id, $index);
            $data['count'] = $index;
            $data['image_path'] = asset(get_image_path('user'));
            return jsonResponse(TRUE)->message(__('Message get successfully.'))->data($data);
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }
    public function getAllAdminMessagesFromStorage($auction_id,$index){
        try {
            $data['messages'] = $this->getAdminMessageFromStorage($auction_id, $index);
            $data['count'] = $index;
            $data['image_path'] = asset(get_image_path('user'));
            return jsonResponse(TRUE)->message(__('Message get successfully.'))->data($data);
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    public function getMessageFromStorage($auction_id,$index){
        $directory = 'auction_message/auction_'.$auction_id;
        $files = Storage::disk('public')->listContents($directory);
        arsort($files);
        $files = array_values($files);
        if (isset($files[$index])){
            $message_array = Storage::get($files[$index]['path']);
            $message = json_decode($message_array,TRUE);
        }else{
            $message = [];
        }
        return $message;
    }

    public function getAdminMessageFromStorage($auction_id,$index){
        $directory = 'admin_message/admin_'.$auction_id;
        $files = Storage::disk('public')->listContents($directory);
        arsort($files);
        $files = array_values($files);
        if (isset($files[$index])){
            $message_array = Storage::get($files[$index]['path']);
            $message = json_decode($message_array,TRUE);
        }else{
            $message = [];
        }
        return $message;
    }

    public static function processAuctionMessage(){
        try {
            $messaging = Messaging::where(['synced'=>INACTIVE,'type'=>'auction'])->get();
            if (isset($messaging) && !empty($messaging[0])){
                foreach ($messaging as $message){
                    $message_chat = MessagingDetails::select('messaging_details.*','users.name as sender_name','users.profile_photo_path as sender_image')
                                                    ->join('users','messaging_details.sender','users.id')
                                                    ->where('messaging_details.messaging_id',$message->id)
                                                    ->get()->toArray();
                    $message_chat['auction_name'] = $message->auction_name;
                    if (isset($message_chat) && !empty($message_chat[0])){
                        $today = date("Y-m-d");
                        $directory = 'auction_message/auction_'.$message->event_id;
                        $path = $directory.'/'.$today;
                        Storage::disk('public')->put($path.'.json',json_encode($message_chat));
                        Messaging::where('id',$message->id)->update(['synced'=>ACTIVE]);
                        MessagingDetails::where('messaging_id',$message->id)->delete();
                    }
                }
            }
        }catch (\Exception $exception){
        }

    }

    public static function processAdminMessage(){
        try {
            $messaging = Messaging::where(['synced'=>INACTIVE,'type'=>'admin'])->get();
            if (isset($messaging) && !empty($messaging[0])){
                foreach ($messaging as $message){
                    $message_chat = MessagingDetails::select('messaging_details.*','users.name as sender_name','users.profile_photo_path as sender_image')
                                                    ->join('users','messaging_details.sender','users.id')
                                                    ->where('messaging_details.messaging_id',$message->id)->get()->toArray();
                    if (isset($message_chat) && !empty($message_chat[0])){
                        $today = date("Y-m-d");
                        $directory = 'admin_message/admin_'.$message->event_id;
                        $path = $directory.'/'.$today;
                        Storage::disk('public')->put($path.'.json',json_encode($message_chat));
                        Messaging::where('id',$message->id)->update(['synced'=>ACTIVE]);
                        MessagingDetails::where('messaging_id',$message->id)->delete();
                    }
                }
            }
        }catch (\Exception $exception){
        }

    }

}
