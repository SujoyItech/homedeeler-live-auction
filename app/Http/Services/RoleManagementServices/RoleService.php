<?php


namespace App\Http\Services\RoleManagementServices;


use App\Http\Boilerplate\CustomResponse;
use App\Http\Repositories\RoleManagementRepositories\RoleRepository;
use App\Http\Services\BaseService;
use App\Http\Services\JQGridService;
use App\Models\Role\Role;
use App\Models\Role\RoleRoute;

class RoleService extends BaseService {

    /**
     * RoleService constructor.
     *
     * @param RoleRepository $roleRepository
     */
    public function __construct(RoleRepository $roleRepository) {
        $this->repo = $roleRepository;
    }

    public function getRoleData($id){
        $data['role'] = Role::where('id',$id)->first();
        $data['routes'] = RoleRoute::where('module_id',$data['role']->module_id)->get();
        return $data;
    }


    /**
     * @param array $data
     *
     * @return CustomResponse|mixed|string
     */
    public function create(array $data) {
        try {
            $response = $this->repo->create($data);
            return is_null($response) ?
                jsonResponse(false)->message(__('Role create failed')) :
                jsonResponse(true)->message(__('Role has been created successfully'));
        } catch (\Exception $e) {
            return jsonResponse(false)->message($e->getMessage());
        }
    }

    /**
     * @param int $id
     * @param array $data
     *
     * @return CustomResponse|mixed|string
     */
    public function update(int $id, array $data) {
        try {
            $response = $this->repo->update($id, $data);
            return $response ?
                jsonResponse(true)->message(__('Role has been updated successfully')) :
                jsonResponse(false)->default();
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    public function delete($id){
        try {
            $response = $this->repo->destroy($id);
            if ($response){
                return jsonResponse(TRUE)->message('Role deleted successfully.');
            }else{
                return jsonResponse(TRUE)->message('Role delete failed.');
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }

    }
}
