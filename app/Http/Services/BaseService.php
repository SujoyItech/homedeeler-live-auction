<?php

namespace App\Http\Services;

abstract class BaseService
{
    public $repo;

    /**
     * @return mixed
     */
    public function all()
    {
        return $this->repo->all();
    }

    /**
     * @return mixed
     */
    public function paginated()
    {
        return $this->repo->paginated(config('paginate'));
    }

    /**
     * @param array $input
     *
     * @return mixed
     */
    public function create(array $input)
    {
        return $this->repo->create($input);
    }

    /**
     * @param mixed $id
     *
     * @return mixed
     */
    public function find($id)
    {
        return $this->repo->find($id);
    }

    /**
     * @param int $id
     * @param array $input
     *
     * @return  mixed
     */
    public function update(int $id, array $input)
    {
        return $this->repo->update($id, $input);
    }

    /**
     * @param array $where
     * @param array $update
     * @return  mixed
     */
    public function updateWhere(array $where, array $update)
    {
        return $this->repo->updateWhere($where, $update);
    }

    /**
     * @param mixed $id
     *
     * @return mixed
     */
    public function destroy($id)
    {
        return $this->repo->destroy($id);
    }

    /**
     * @param array $where
     *
     * @return mixed
     */
    public function firstWhere(array $where) {
        return $this->repo->firstWhere($where);
    }

    /**
     * @param array $where
     * @param array $data
     * @return mixed
     */
    public function updateOrCreate(array $where, array $data) {
        return $this->repo->updateOrCreate($where, $data);
    }

    public function paginateWhere(array $where) {
        return $this->repo->paginateWhere($where);
    }

    public function getData(array $where=[],array $select=null,array $orderBy=[],array $with=[], $paginate=0){
        return $this->repo->getData($where, $select, $orderBy, $with, $paginate);
    }

    public function getSingleRowData(array $where=[],array $select=null,array $orderBy=[],array $with=[], $paginate=0){
        return $this->repo->getSingleRowData($where, $select, $orderBy, $with, $paginate);
    }

    public function getQuery(array $where=[],array $select=null, array $with=[]){
        return $this->repo->getQuery($where, $select, $with);
    }

    public function getDataWhereIn($key,array $whereInArray){
        return $this->repo->getDataWhereIn($key,$whereInArray);
    }

    public function getIdBySlug($slug){
        return $this->repo->getIdBySlug($slug);
    }

}
