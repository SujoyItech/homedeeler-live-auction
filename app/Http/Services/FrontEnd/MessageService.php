<?php

namespace App\Http\Services\FrontEnd;

use App\Http\Services\NotificationService;
use App\Models\Message\Messaging;
use App\Models\Message\MessagingDetails;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class MessageService
{
    public function sendAuctionMessage($auction_id,$message_details){
        try {
                $message_details_created = MessagingDetails::create($message_details);
                $message_details['auction_id'] = $auction_id;
                $message_details['sender_name'] = Auth::user()->name;
                $this->updateMessageSyncStatus($message_details['messaging_id']);
                $this->sendAuctionMessageNotification($message_details);
                $data['message'] = $message_details_created;
                return jsonResponse(TRUE)->message(__('Message sent successfully.'))->data($data);

        }catch (\Exception $exception){
            return  jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    public function sendAdminMessage($message_details){
        try {
                $message_details_created = MessagingDetails::create($message_details);
                $message_details['sender_name'] = Auth::user()->name;
                $this->updateMessageSyncStatus($message_details['messaging_id']);
                $this->sendAdminMessageNotification($message_details);
                $data['message'] = $message_details_created;
                return jsonResponse(TRUE)->message(__('Message sent successfully.'))->data($data);

        }catch (\Exception $exception){
            return  jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    private function updateMessageSyncStatus($messaging_id){
        Messaging::where('id',$messaging_id)->update(['synced'=>INACTIVE]);
    }

    public function loadLiveAuctionMessage($auction_id){
       return $this->checkChatExists($auction_id) ? json_decode(Storage::disk('live_chat')->get($auction_id.'.json')) :[];
    }
    public function sendLiveMessageReply($auction_id,$message){
        try {
            if ($this->checkChatExists($auction_id)){
                $message_array = json_decode(Storage::disk('live_chat')->get($auction_id.'.json'));
                array_push($message_array,$message);
                Storage::disk('live_chat')->put($auction_id.'.json',json_encode($message_array));
            }else{
                $message_array [] = $message;
                Storage::disk('live_chat')->put($auction_id.'.json',json_encode($message_array));
            }
            $channel = 'live_auction_message_'.$auction_id;
            NotificationService::triggerNotification($channel,'auction_message',__('Live auction message'),$message);
            return jsonResponse(TRUE)->message(__('Message sent successfully'));
        } catch (\Exception $e) {
            return jsonResponse(FALSE)->message(__('Message send failed'));
        }
    }

    public function checkChatExists($auction_id){
        return Storage::disk('live_chat')->exists($auction_id.'.json') ? TRUE : FALSE;
    }

    private function sendAuctionMessageNotification($message_details){
        $channel_name = 'auction_message_'.$message_details['auction_id'];
        $title = $message_details['sender_name'].__(' sent a new message');
        $body = $message_details;
        NotificationService::triggerNotification($channel_name,'auction_message',$title,$body);
    }

    private function sendAdminMessageNotification($message_details){
        $channel_name = 'admin_message_'.$message_details['messaging_id'];
        $title = $message_details['sender_name'].__(' sent a new message');
        $body = $message_details;
        NotificationService::triggerNotification($channel_name,'auction_message',$title,$body);
    }
}
