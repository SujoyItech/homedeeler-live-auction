<?php

namespace App\Http\Services\FrontEnd;


use App\Models\Auction\Auction;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ProfileService
{
    public function dashboardData()
    {
        $id = Auth::user()->id;
        $data['total_bids'] = Auction::select(DB::raw("COUNT(auction_details.auction_id) AS total_bids"))
            ->join('auction_details', 'auctions.id', '=', 'auction_details.auction_id')
            ->where('auction_details.bidder_id', '=', $id)
            ->first();
        $data['won_bids'] = Auction::select(DB::raw("COUNT(auctions.id) AS won_bids"))
            ->join('auction_details', 'auctions.id', '=', 'auction_details.auction_id')
            ->where('auction_details.bidder_id', '=', $id)
            ->where(['auctions.winner_id' => $id])
            ->where('auctions.status', '>=', AUCTION_END)
            ->first();
        $data['favourite_bids'] = Auction::select(DB::raw("COUNT(auction_favourites.auction_id) AS favourite_bids"))
            ->join('auction_favourites', 'auctions.id', '=', 'auction_favourites.auction_id')
            ->where('auction_favourites.user_id', '=', $id)
            ->where('auction_favourites.is_favourite', ACTIVE)
            ->first();
        return $data;
    }

    public function myBids()
    {
        return Auction::select(
            'products.name',
            'products.slug',
            'auctions.highest_bid',
            'auctions.end_date',
            'auction_payments.auction_winning_price',
            'auction_payments.processing_fee',
            'auction_payments.total_price',
            'auction_details.*'
        )
            ->join('products', 'auctions.product_id', '=', 'products.id')
            ->join('auction_details', 'auctions.id', '=', 'auction_details.auction_id')
            ->leftJoin('auction_payments', 'auctions.id', '=', 'auction_payments.auction_id')
            ->where('auction_details.bidder_id', '=', Auth::user()->id)
            ->orderBy('auction_details.created_at', 'desc')->get();
    }

    public function myWonBids()
    {
        return Auction::select(
            'products.name',
            'products.slug',
            'auctions.highest_bid',
            'auctions.service_charge',
            'auctions.status',
            'auction_payments.auction_winning_price',
            'auction_payments.processing_fee',
            'auction_payments.total_price',
            'auction_details.*'
        )
            ->join('products', 'auctions.product_id', '=', 'products.id')
            ->join('auction_details', 'auctions.id', '=', 'auction_details.auction_id')
            ->leftJoin('auction_payments', 'auctions.id', '=', 'auction_payments.auction_id')
            ->where('auction_details.bidder_id', '=', Auth::user()->id)
            ->where('auctions.winner_id', Auth::user()->id)
            ->where('auctions.status', '>=', AUCTION_END)
            ->orderBy('auction_details.created_at', 'desc')
            ->get();
    }

    public function myFavouriteAuctionList()
    {
        return Auction::select(
            'products.name',
            'products.slug',
            'auctions.highest_bid',
            'auctions.end_date',
            'auction_payments.auction_winning_price',
            'auction_payments.processing_fee',
            'auction_payments.total_price',
        )
            ->join('products', 'auctions.product_id', '=', 'products.id')
            ->join('auction_favourites', 'auctions.id', '=', 'auction_favourites.auction_id')
            ->leftJoin('auction_payments', 'auctions.id', '=', 'auction_payments.auction_id')
            ->where('auction_favourites.user_id', '=', Auth::user()->id)
            ->where('auction_favourites.is_favourite', '=', ACTIVE)
            ->orderBy('auctions.created_at', 'desc')
            ->get();
    }

    public function updateProfilePicture(array $requestArray)
    {
        try {
            if (isset($requestArray['picture'])) {
                $user = Auth::user();
                if (!empty($user->profile_photo_path)) {
                    deleteOnlyImage(get_image_path('user'), $user->profile_photo_path);
                }
                $image_name = uploadImage($requestArray['picture'], get_image_path('user'));
                User::where('id', $user->id)->update(['profile_photo_path' => $image_name]);
                $data['user'] = User::where('id', $user->id)->first();
                return jsonResponse(TRUE)->message(__('Profile picture changed successfully.'))->data($data);
            }
            return jsonResponse(FALSE)->message(__('Profile picture change failed.'));
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->default();
        }
    }

    public function userProfileUpdate(array $requestArray)
    {
        // tEST VSCODE
        try {
            $user = Auth::user();
            $update_data = $this->prepareProfileUpdateData($requestArray, $user);
            User::where('id', $user->id)->update($update_data);
            return jsonResponse(TRUE)->message(__('Profile updated successfully.'));
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    private function prepareProfileUpdateData($requestArray, $user)
    {
        if (isset($requestArray['profile_photo_path'])) {
            if (!empty($user->profile_photo_path)) {
                deleteOnlyImage(get_image_path('user'), $user->profile_photo_path);
            }
            $requestArray['profile_photo_path'] = uploadImageByCamera($requestArray['profile_photo_path'], get_image_path('user'));
        }
        if (isset($requestArray['nid_picture'])) {
            if (!empty($user->nid_picture)) {
                deleteOnlyImage(get_image_path('user'), $user->nid_picture);
            }
            $requestArray['nid_picture'] = uploadImage($requestArray['nid_picture'], get_image_path('user'));
        }
        return $requestArray;
    }

    public function userPasswordUpdate(array $requestArray)
    {
        try {
            $user = Auth::user();
            if (Hash::check($requestArray['old_password'], $user->password)) {
                $data['password'] = bcrypt($requestArray['password']);
                $response = User::where('id', $user->id)->update($data);
                if ($response) {
                    return jsonResponse(true)->message(__("Password changed successfully."));
                } else {
                    return jsonResponse(false)->message(__("Password change failed."));
                }
            } else {
                return jsonResponse(false)->message(__("Incorrect old password!."));
            }
        } catch (\Exception $exception) {
            return jsonResponse(false)->message($exception->getMessage());
        }
    }
}
