<?php

namespace App\Http\Services\FrontEnd;

use App\Http\Repositories\FrontEnd\AuctionRepository;
use App\Http\Services\BaseService;
use App\Http\Services\NotificationService;
use App\Models\Auction\Auction;
use App\Models\Auction\AuctionDetail;
use App\Models\Auction\AuctionFavourite;
use App\Models\Auction\AuctionStatus;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Monolog\Handler\IFTTTHandler;

class AuctionService extends BaseService
{
    /**
     * AuctionService constructor.
     * @param AuctionRepository $repository
     */
    public $repo;
    public function __construct(AuctionRepository $repository)
    {
        $this->repo = $repository;
    }

    public function addAuctionToFavourite($id)
    {
        try {
            $update_data = [];
            $message = '';
            $auction_fav = AuctionFavourite::where(['auction_id' => $id, 'user_id' => Auth::user()->id])->first();
            if (isset($auction_fav)) {
                if ($auction_fav->is_favourite == TRUE) {
                    $update_data['is_favourite'] = FALSE;
                    $message = __('Auction removed from favourite list');
                } else {
                    $update_data['is_favourite'] = TRUE;
                    $message = __('Auction added to favourite list');
                }
                AuctionFavourite::where(['auction_id' => $id, 'user_id' => Auth::user()->id])->update($update_data);
                return jsonResponse(TRUE)->message($message);
            } else {
                $insert_data = [
                    'auction_id' => $id,
                    'user_id' => Auth::user()->id,
                    'is_favourite' => TRUE
                ];
                AuctionFavourite::insert($insert_data);
                $message = __('Auction added to favourite list');
                return jsonResponse(TRUE)->message($message);
            }
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->default();
        }
    }

    public function submitBid(array $requestArray)
    {
        try {
            DB::beginTransaction();
            $requestArray['bidder_id'] = Auth::user()->id;
            $requestArray['created_at'] = Carbon::now();
            $new_bid = AuctionDetail::create($requestArray);
            if ($new_bid) {
                $auction_settings = __options(['commission_settings']);
                $last_bidding_extends_duration = $auction_settings->last_bidding_extends_duration ?? 0;
                $auction = $this->getAuctionData($new_bid->auction_id, $auction_settings);
                $update_array = [
                    'winner_id'  => Auth::user()->id,
                    'highest_bid' => $new_bid->bid_price
                ];
                if ($auction->check_extends == 1) {
                    if (!empty($auction->live_final_end_time)) {
                        $update_array['live_extends_count'] = $auction->live_extends_count == NULL ? 1 : $auction->live_extends_count + 1;
                        $update_array['live_extend_minutes'] = $auction->live_extend_minutes == NULL ? $last_bidding_extends_duration : $auction->live_extend_minutes + $last_bidding_extends_duration;
                        $time = new DateTime($auction->live_final_end_time);
                        $time->modify("+{$last_bidding_extends_duration} minutes");
                        $update_array['live_final_end_time'] = $time->format('Y-m-d h:i:s');
                    }
                }
                Auction::where('id', $new_bid->auction_id)->update($update_array);
                $new_auction = Auction::where('id', $auction->id)->first();
                $this->bidNotification($new_auction, $new_bid);
                DB::commit();
                return jsonResponse(TRUE)->message(__('New bid created successfully.'));
            }
            DB::rollBack();
            return jsonResponse(FALSE)->message(__('Your bid failed'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return jsonResponse(FALSE)->default();
        }
    }

    private function getAuctionData($auction_id, $auction_settings)
    {
        $last_bidding_extends_times = $auction_settings->last_bidding_extends_times;
        return Auction::select('auctions.*', 'products.name as product_name', 'products.seller_id')
            ->selectRaw("CASE WHEN (auctions.live_final_end_time BETWEEN TIMESTAMPADD(MINUTE,-1,NOW()) AND NOW()) AND auctions.live_extends_count < {$last_bidding_extends_times} THEN 1 ELSE 0 END AS check_extends")
            ->join('products', 'auctions.product_id', 'products.id')
            ->where('auctions.id', $auction_id)
            ->first();
    }

    private function bidNotification($auction, $response)
    {
        $notification_data = [
            'auction_id' => (int) $auction->id,
            'user_id' => (int) Auth::user()->id,
            'user_name' => Auth::user()->name,
            'user_image' => getUserAvatar(Auth::user()),
            'message' => Auth::user()->name . __(' bid for ') . $response->bid_price,
            'created_at' => Carbon::now()->format('d-m-Y H:i:s')
        ];

        if ($auction->status == AUCTION_IN_LIVE) {
            $message_service = new MessageService();
            $message_service->sendLiveMessageReply($auction->id, $notification_data);
        }
        $highest_bid = (float)$auction->highest_bid;
        $next_bid = (float) $auction->highest_bid + (float) $auction->bid_increment;
        $notification_data['highest_bid'] = $highest_bid;
        $notification_data['current_bid'] = getMoney($highest_bid);
        $notification_data['next_bid'] = $next_bid;
        $notification_data['next_bid_format'] = getMoney($next_bid);
        $notification_data['bid_count'] = AuctionDetail::where('auction_id', $auction->id)->count();
        $this->sendBidNotification($notification_data);
    }

    private function saveLiveAuctionMessageToStorage()
    {
    }

    // Your methods for repository
    public function getAuctionList($search_array = [], $where = [], $type = NULL, $order_by = NULL, $paginate = NULL, $limit = NULL, $page = NULL)
    {
        return $this->repo->getAuctionList($search_array, $where, $type, $order_by, $paginate, $limit, $page);
    }

    public function getAuctionDetails($slug)
    {
        return $this->repo->getAuctionDetails($slug);
    }

    private function sendBidNotification($data)
    {
        $title = __('Bidding notification.');
        $new_channel = 'live_auction_message_' . $data['auction_id'];
        NotificationService::triggerNotification($new_channel, 'bid_event', $title, $data);
    }

    public function sendDeliveryRequest(array $requestArray)
    {
        try {
            Auction::where('id', $requestArray['id'])->update(['status' => DELIVERY_REQUESTED]);
            $auction_status = [
                'auction_id' => $requestArray['id'],
                'status' => DELIVERY_REQUESTED,
                'message' => __('Product is requested for delivery.')
            ];
            $check_exist = AuctionStatus::where(['auction_id' => $requestArray['id'], 'status' => PRODUCT_RECEIVED])->first();
            if (!isset($check_exist)) {
                AuctionStatus::create($auction_status);
            }
            return jsonResponse(TRUE)->message(__('Delivery request sent successfully.'));
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    public function sendProductReceiveConfirmation(array $requestArray)
    {
        try {
            Auction::where('id', $requestArray['id'])->update(['status' => PRODUCT_RECEIVED]);
            $auction_status = [
                'auction_id' => $requestArray['id'],
                'status' => PRODUCT_RECEIVED,
                'message' => __('Product is received by user.')
            ];
            $check_exist = AuctionStatus::where(['auction_id' => $requestArray['id'], 'status' => PRODUCT_RECEIVED])->first();
            if (!isset($check_exist)) {
                AuctionStatus::create($auction_status);
            }
            return jsonResponse(TRUE)->message(__('Product received successfully.'));
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }
}
