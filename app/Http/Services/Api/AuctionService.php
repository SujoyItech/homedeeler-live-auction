<?php

namespace App\Http\Services\Api;

use App\Http\Repositories\Api\AuctionRepository;
use App\Http\Services\BaseService;
use App\Http\Services\FrontEnd\MessageService;
use App\Http\Services\NotificationService;
use App\Models\Auction\Auction;
use App\Models\Auction\AuctionDetail;
use App\Models\Auction\AuctionFavourite;
use App\Models\Product\Category;
use App\Models\Product\ProductCombination;
use App\Models\Product\ProductTag;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use function MongoDB\BSON\fromJSON;
use function React\Promise\map;

class AuctionService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param Api/AuctionRepository $repository
     */
    public $repo;
    public function __construct(AuctionRepository $repository)
    {
        $this->repo = $repository;
    }

    // Your methods for repository

    public function getCategoryList()
    {
        try {
            $categories = Category::where('parent_id', INACTIVE)->get();
            $data['categories'] = $this->prepareCategoryList($categories);
            return jsonResponse(TRUE)->message(__('Category list get successfully'))->data($data);
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    private function prepareCategoryList($categories)
    {
        $local = App::getLocale();
        if (isset($categories)) {
            foreach ($categories as $key => $category) {
                if (!empty($categories[$key]->icon)) {
                    $categories[$key]->icon = asset(get_image_path('category') . $categories[$key]->icon);
                }
            }
        }
        return $categories;
    }

    public function getAuctionList($search_array = [], $where = [], $type = NULL)
    {
        try {
            $auction_list = $this->repo->getAuctionList($search_array, $where, $type);
            $auction_list_data = $this->prepareAuctionList($auction_list);
            $data['base_image_url'] = Storage::url('');
            $data['auction_lists'] = $auction_list_data;
            return jsonResponse(TRUE)->message(__('Auction get successfully'))->data($data);
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    private function prepareAuctionList($auction_list)
    {
        $local = App::getLocale();
        foreach ($auction_list as $key => $auction) {
            if (isset($auction_list[$key]->category_name)) {
                $name = json_decode($auction->category_name);
                $auction_list[$key]->category_name = $name->$local ?? $name->en;
            }
        }
        return $auction_list;
    }

    public function auctionDetails($slug)
    {
        try {
            $lang = App::getLocale();
            $auction = $this->repo->getAuctionDetails($slug);
            if (isset($auction)) {
                $auction = $this->prepareAuctionDetails($auction);
                $data['my_bid'] = [];
                if (isset(Auth::user()->id)) {
                    $bid = AuctionDetail::where(['auction_id' => $auction->id, 'bidder_id' => Auth::user()->id])->first();
                    $data['my_bid'] = $bid;
                }
                $data['base_image_url'] = Storage::url('');
                $data['auction'] = $auction;
                $data['product_images'] = ProductCombination::select('media_type', 'media_url')
                    ->where(['product_id' => $auction->product_id])->where('media_url', '<>', NULL)
                    ->get();
                $product_tags = ProductTag::join('tags', 'product_tags.tag_id', 'tags.id')
                    ->where('product_id', $auction->product_id)
                    ->pluck('tags.name')
                    ->toArray();
                $data['product_tags'] = array_map(function ($item) use ($lang) {
                    return json_decode('[' . $item . ']')[0]->$lang ?? json_decode('[' . $item . ']')[0]->en ?? '';
                }, $product_tags);
                $message_service = new MessageService();
                $data['auction_messages'] = $message_service->checkChatExists($auction->id) ? json_decode(Storage::disk('live_chat')->get($auction->id . '.json')) : [];
                return jsonResponse(TRUE)->message(__('Auction details get successfully'))->data($data);
            }
            return jsonResponse(FALSE)->message(__('Auction not found'));
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }
    private function prepareAuctionDetails($auction)
    {
        $local = App::getLocale();
        if (isset($auction->category_name)) {
            $name = json_decode($auction->category_name);
            $auction->category_name = $name->$local ?? $name->en ?? '';
        }
        $auction->bid_counts = $auction->auction_details->count();
        unset($auction->auction_details);
        return $auction;
    }

    public function makeAuctionFavourite($user_id, $auction_id, $action)
    {
        try {
            $insert_data = [
                'user_id' => $user_id,
                'auction_id' => $auction_id,
                'is_favourite' => $action
            ];
            $message = $action == ACTIVE ? __('Product added to favourite list') :  __('Product remove from favourite list');
            AuctionFavourite::updateOrInsert(['user_id' => $user_id, 'auction_id' => $auction_id], $insert_data);
            return jsonResponse(TRUE)->message($message);
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->default();
        }
    }


    public function bid(array $requestArray)
    {
        try {
            if (Auth::user()->admin_verified === ACTIVE) {
                $auction = Auction::where('id', $requestArray['auction_id'])->first();
                if (isset($auction) && $auction->status >= ACTIVE && $auction->status < AUCTION_END && $auction->live_final_end_time >= Carbon::now()) {
                    $requestArray['created_at'] = Carbon::now();
                    $new_bid = AuctionDetail::create($requestArray);

                    if ($new_bid) {
                        $auction_settings = __options(['commission_settings']);
                        $last_bidding_extends_duration = $auction_settings->last_bidding_extends_duration ?? 0;
                        $auction = $this->getAuctionData($new_bid->auction_id, $auction_settings);
                        $update_array = [
                            'winner_id'  => Auth::user()->id,
                            'highest_bid' => $new_bid->bid_price
                        ];
                        if ($auction->check_extends == 1) {
                            if (!empty($auction->live_final_end_time)) {
                                $update_array['live_extends_count'] = $auction->live_extends_count == NULL ? 1 : $auction->live_extends_count + 1;
                                $update_array['live_extend_minutes'] = $auction->live_extend_minutes == NULL ? $last_bidding_extends_duration : $auction->live_extend_minutes + $last_bidding_extends_duration;
                                $time = new DateTime($auction->live_final_end_time);
                                $time->modify("+{$last_bidding_extends_duration} minutes");
                                $update_array['live_final_end_time'] = $time->format('Y-m-d h:i:s');
                            }
                        }
                        $this->repo->updateModel($new_bid->auction_id, $update_array);
                        $new_auction = Auction::where('id', $auction->id)->first();
                        $this->bidNotification($new_auction, $new_bid);
                        return jsonResponse(TRUE)->message(__('New bid created successfully.'));
                    }
                    return jsonResponse(FALSE)->message(__('New bid create failed.'));
                } else {
                    return jsonResponse(FALSE)->message(__('Auction already ended.'));
                }
            } else {
                return jsonResponse(FALSE)->message(__('You are not approved by admin.'));
            }
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }


    private function getAuctionData($auction_id, $auction_settings)
    {
        $last_bidding_extends_times = $auction_settings->last_bidding_extends_times;
        return Auction::select('auctions.*', 'products.name as product_name', 'products.seller_id')
            ->selectRaw("CASE WHEN (auctions.live_final_end_time BETWEEN TIMESTAMPADD(MINUTE,-1,NOW()) AND NOW()) AND auctions.live_extends_count < {$last_bidding_extends_times} THEN 1 ELSE 0 END AS check_extends")
            ->join('products', 'auctions.product_id', 'products.id')
            ->where('auctions.id', $auction_id)
            ->first();
    }

    private function bidNotification($auction, $response)
    {
        $notification_data = [
            'auction_id' => (int)$auction->id,
            'user_id' => (int)Auth::user()->id,
            'user_name' => Auth::user()->name,
            'user_image' => getUserAvatar(Auth::user()),
            'message' => Auth::user()->name . __(' bid for ') . getMoney($response->bid_price),
            'created_at' => Carbon::now()->format('d-m-Y H:i:s')
        ];

        if ($auction->status == AUCTION_IN_LIVE) {
            $message_service = new MessageService();
            $message_service->sendLiveMessageReply($auction->id, $notification_data);
        }

        $highest_bid = (float) $auction->highest_bid;
        $next_bid = (float) $auction->highest_bid + (float)$auction->bid_increment;

        $notification_data['highest_bid'] = $highest_bid;
        $notification_data['current_bid'] = getMoney($highest_bid);
        $notification_data['next_bid'] = $next_bid;
        $notification_data['next_bid_format'] = getMoney($next_bid);
        $notification_data['bid_count'] = AuctionDetail::where('auction_id', $auction->id)->count();
        $this->sendBidNotification($notification_data);
    }

    private function sendBidNotification($data)
    {
        $title = __('Bidding notification.');
        $event = 'bid_event';
        $new_channel = 'live_auction_message_' . $data['auction_id'];
        NotificationService::triggerNotification($new_channel, $event, $title, $data);
    }
}
