<?php

namespace App\Http\Services\Auction;

use App\Http\Services\NotificationService;
use App\Models\Auction\Auction;
use App\Models\Auction\AuctionPayment;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class LiveAuctionService
{
    private $auction_service;
    public function __construct(AuctionService $auction_service)
    {
        $this->auction_service = $auction_service;
    }

    public function makeAuctionLive()
    {
        $live_auctions_id = Auction::where(['has_live' => STATUS_ACTIVE, 'status' => STATUS_ACTIVE])
            ->whereRaw('live_start_time BETWEEN TIMESTAMPADD(MINUTE, -1, NOW()) AND NOW()')
            ->pluck('id')->toArray();
        if (!empty($live_auctions_id)) {
            Auction::whereIn('id', $live_auctions_id)->update(['status' => AUCTION_IN_LIVE]);
            foreach ($live_auctions_id as $auction_id) {
                $this->auction_service->auctionStatusUpdateHistory($auction_id, AUCTION_IN_LIVE);
            }
            $title = __('New auctions added to live.');
            $data['auction_id'] = $live_auctions_id;
            NotificationService::triggerNotification('auction_send_in_live', 'auction-live', $title, $data);
        }
    }

    public function makeAuctionLiveEnd()
    {

        $live_end_auctions_id = Auction::where(['has_live' => STATUS_ACTIVE, 'status' => AUCTION_IN_LIVE])
            ->whereRaw('live_start_time BETWEEN TIMESTAMPADD(MINUTE, -1, NOW()) AND NOW()')
            ->pluck('id')->toArray();
        if (!empty($live_end_auctions_id)) {
            Auction::whereIn('id', $live_end_auctions_id)->update(['status' => AUCTION_END, 'is_streaming' => INACTIVE]);
            foreach ($live_end_auctions_id as $auction_id) {
                $this->auction_service->auctionStatusUpdateHistory($auction_id, AUCTION_LIVE_END);
                $this->auction_service->makeAuctionEnd($auction_id);
                $this->auction_service->auctionStatusUpdateHistory($auction_id, AUCTION_END);
            }
            $title = __('Some auction live ended.');
            $data['auction_id'] = $live_end_auctions_id;
            NotificationService::triggerNotification('auction_live_end', 'auction-live', $title, $data);
        }
    }


    public function auctionLiveDataUpdate(array $requestArray, $amount, $paid_amount)
    {
        $live_auction_data = [
            'live_start_time' => $requestArray['live_start_time'],
            'live_duration' => $requestArray['live_duration'],
            'has_live' => ACTIVE,
            'is_approved' => ACTIVE,
            'status' => ACTIVE,
            'live_end_time' => Carbon::parse($requestArray['live_start_time'])->addMinutes($requestArray['live_duration']),
            'live_final_end_time' => Carbon::parse($requestArray['live_start_time'])->addMinutes($requestArray['live_duration']),
            'live_extend_minutes' => 0,
            'live_extends_count' => 0
        ];

        $live_payment_data = [
            'live_auction_charge' => $amount,
            'live_auction_charge_paid' => $paid_amount,
            'is_live_charge_paid' => ACTIVE
        ];

        Auction::where('id', $requestArray['auction_id'])->update($live_auction_data);
        AuctionPayment::where('auction_id', $requestArray['auction_id'])->update($live_payment_data);
        $this->auction_service->auctionStatusUpdateHistory($requestArray['auction_id'], ACTIVE);
    }
}
