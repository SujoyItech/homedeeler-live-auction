<?php

namespace App\Http\Services\Auction;

use App\Http\Repositories\Auction\AuctionRepository;
use App\Http\Repositories\Product\BrandRepository;
use App\Http\Repositories\Product\CategoryRepository;
use App\Http\Repositories\Product\CombinationRepository;
use App\Http\Repositories\Product\CombinationTypeRepository;
use App\Http\Repositories\Product\ProductRepository;
use App\Http\Repositories\Product\TagRepository;
use App\Http\Services\BaseService;
use App\Http\Services\NotificationService;
use App\Models\Auction\Auction;
use App\Models\Auction\AuctionDetail;
use App\Models\Auction\AuctionPayment;
use App\Models\Auction\AuctionPaymentTransaction;
use App\Models\Auction\AuctionStatus;
use App\Models\Product\Brand;
use App\Models\Product\Category;
use App\Models\Product\Combination;
use App\Models\Product\CombinationType;
use App\Models\Product\ProductCategory;
use App\Models\Product\ProductCombination;
use App\Models\Product\ProductPricing;
use App\Models\Product\ProductTag;
use App\Models\Product\Tag;
use App\Models\User;
use Carbon\Carbon;
use DateInterval;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AuctionService extends BaseService
{
    private $product_repository, $category_repository;

    /**
     * Instantiate repository
     *
     * @param Auction/AuctionRepository $repository
     */
    public $repo;
    public function __construct(
        AuctionRepository $repository,
        ProductRepository $product_repository,
        CategoryRepository $category_repository
    ) {
        $this->repo = $repository;
        $this->product_repository = $product_repository;
        $this->category_repository = $category_repository;
    }

    public function update($id, array $requestArray)
    {
        try {

            if (isset($requestArray['description'])) {
                Auction::join('products', 'auctions.product_id', 'products.id')->where('auctions.id', $id)->update(['products.description' => $requestArray['description']]);
            } else {
                $this->repo->update($id, $requestArray);
            }
            return jsonResponse(TRUE)->message(__('Auction info saved successfully.'));
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->default();
        }
    }

    public function auctionApprove(array $requestArray)
    {
        try {
            Auction::where('id', $requestArray['id'])->update(['is_approved' => ACTIVE]);
            return jsonResponse(true)->message(__('Auction approved successfully!'));
        } catch (\Exception $exception) {
            return jsonResponse(false)->default();
        }
    }

    public function getAuctionList(array $search_array = [], $where = [])
    {
        return $this->repo->getAuctionList($search_array, $where);
    }

    public function getMyAuctionList(array $search_array = [], $where = [])
    {
        return $this->repo->getAuctionList($search_array, $where);
    }

    public function getAuctionDetailsData($slug)
    {
        return $this->repo->getAuctionDetails($slug);
    }

    public function getAuctionDetailsById($id)
    {
        return $this->repo->getAuctionDetails($id);
    }

    public function getAuctionBiddingDetailsData($slug)
    {
        return $this->repo->getAuctionBiddingDetailsData($slug);
    }

    public function getAuctionAllInformation($slug)
    {
        $auction = $this->repo->getAuctionDetails($slug);
        return $this->getAuctionAllInformationData($auction);
    }

    private function getAuctionAllInformationData($auction)
    {
        $categories = Category::where('status', STATUS_ACTIVE)->get()->toArray();
        $category_tree = buildTreeForArray($categories);

        $tags = Tag::where('status', STATUS_ACTIVE)->get();
        $brands = Brand::where('status', STATUS_ACTIVE)->get();
        $combinationTypes = CombinationType::all();
        $combinations = Combination::all();
        $productCombinations = ProductCombination::where(['product_id' => $auction->product_id, 'status' => ACTIVE])
            ->orderBy('combination_type_id', 'desc')
            ->get();
        $productCategories = ProductCategory::where(['product_id' => $auction->product_id])
            ->pluck('category_id')
            ->toArray();
        $productTags = ProductTag::where(['product_id' => $auction->product_id])->get();
        return [
            'auction'             => $auction,
            'categories'          => $category_tree,
            'tags'                => $tags,
            'productCategories'   => $productCategories,
            'productTags'         => $productTags,
            'brands'              => $brands,
            'combinationTypes'    => $combinationTypes,
            'combinations'        => $combinations,
            'productCombinations' => $productCombinations,
        ];
    }

    public function initAuction(array $requestArray)
    {
        DB::beginTransaction();
        try {
            $success = FALSE;
            $requestArray['seller_id'] = Auth::user()->id;
            $init_product = $this->product_repository->create($requestArray);
            if ($init_product) {
                $init_auction = $this->repo->create(['product_id' => $init_product->id]);
                if ($init_auction) {
                    AuctionPayment::create(['auction_id' => $init_auction->id]);
                    $success = TRUE;
                }
            }
            DB::commit();
            if ($success == TRUE) {
                return jsonResponse(TRUE)
                    ->message(__('New auction created successfully.'))
                    ->data(['slug' => $init_product->slug]);
            } else {
                return jsonResponse(FALSE)->message(__('New auction create failed.'));
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            return jsonResponse(FALSE)->default();
        }
    }

    public function saveAuctionPricingInfo($id, array $requestArray)
    {
        try {
            $requestArray['live_end_time'] = $requestArray['end_date'];
            $requestArray['live_final_end_time'] = $requestArray['end_date'];
            $this->repo->updateModel($id, $requestArray);
            return jsonResponse(TRUE)->message(__('Auction price info updated successfully.'));
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    public function makeAuctionPayment(array $requestArray)
    {
        try {
            $payment_data = [
                'auction_id'          => $requestArray['auction_id'],
                'is_live_charge_paid' => TRUE
            ];
            AuctionPayment::updateOrInsert(['auction_id' => $requestArray['auction_id']], $payment_data);

            $update_auction = [
                'live_start_time' => $requestArray['live_start_time'],
                'live_end_time' => $requestArray['live_start_time'],
                'live_final_end_time' => $requestArray['live_start_time'],
                'end_date' => $requestArray['live_start_time'],
                'live_duration'   => $requestArray['live_duration'],
                'has_live'        => TRUE
            ];
            $this->repo->updateModel($requestArray['auction_id'], $update_auction);
            return jsonResponse(TRUE)->message(__('Payment Successful.'));
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    public function auctionStatusChange(array $requestArray)
    {
        try {
            if (Auth::user()->module_id == MODULE_SUPER_ADMIN || Auth::user()->module_id == MODULE_USER_ADMIN) {
                return $this->auctionStatusUpdate($requestArray);
            } else {
                $auction_all_status = AuctionStatus::where('auction_id', $requestArray['id'])->pluck('status')->toArray();
                if (in_array($requestArray['status'], $auction_all_status)) {
                    return jsonResponse(FALSE)->message(__('Sorry! you can not change this status again!'));
                } else {
                    return $this->auctionStatusUpdate($requestArray);
                }
            }
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    private function auctionStatusUpdate($requestArray)
    {
        if ($requestArray['status'] == ACTIVE) {
            $check_media_exist = ProductCombination::where(['product_id' => $requestArray['id'], 'is_featured' => ACTIVE, 'status' => ACTIVE])->first();
            if (isset($check_media_exist)) {
                $updated_status = ['status' => $requestArray['status']];
                if ($requestArray['status'] == AUCTION_IN_LIVE) {
                    $updated_status['has_live'] = ACTIVE;
                }
                $this->repo->updateModel($requestArray['id'], $updated_status);
                $auction_status = $this->auctionStatusUpdateHistory($requestArray['id'], $requestArray['status']);
                $data['auction_status'] = $auction_status;
                return jsonResponse(TRUE)->message(__('Status changed successful.'))->data($data);
            } else {
                return jsonResponse(false)->message(__('You must upload atleast one image to active this.'));
            }
        } else {
            $updated_status = ['status' => $requestArray['status']];
            if ($requestArray['status'] == AUCTION_IN_LIVE) {
                $updated_status['has_live'] = ACTIVE;
            }
            if ($requestArray['status'] == AUCTION_END) {
                $this->makeAuctionEnd($requestArray['id']);
            }
            $this->repo->updateModel($requestArray['id'], $updated_status);
            $auction_status = $this->auctionStatusUpdateHistory($requestArray['id'], $requestArray['status']);
            $data['auction_status'] = $auction_status;
            return jsonResponse(TRUE)->message(__('Status changed successful.'))->data($data);
        }
    }

    public function updateAuctionStatusHistory(array $requestArray)
    {
        try {
            $update_data = [];
            if (isset($requestArray['file'])) {
                $update_data['file'] = uploadFile($requestArray['file'], get_image_path('auction/auction_status'));
            }
            if (isset($requestArray['image'])) {
                $update_data['image'] = uploadImage($requestArray['image'], get_image_path('auction/auction_status'));
            }
            $update_data['extra_message'] = $requestArray['message'];
            AuctionStatus::where('id', $requestArray['id'])->update($update_data);
            return jsonResponse(TRUE)->message(__('Status changed successful.'));
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    public function auctionStatusUpdateHistory($auction_id, $status)
    {
        $insert_data = [
            'auction_id' => $auction_id,
            'status' => $status
        ];
        if ($status == ACTIVE) {
            $insert_data['message'] = __('Auction activated successfully.');
        } elseif ($status == AUCTION_IN_LIVE) {
            $insert_data['message'] = __('Auction is in live.');
        } elseif ($status == AUCTION_LIVE_END) {
            $insert_data['message'] = __('Auction live ended.');
        } elseif ($status == AUCTION_END) {
            $insert_data['message'] = __('Auction ended.');
        } elseif ($status == PAYMENT_COMPLETED) {
            $insert_data['message'] = __('Auction payment completed confirmed by seller.');
        } elseif ($status == DELIVERY_REQUESTED) {
            $insert_data['message'] = __('Product is requested for delivery confirmed by seller.');
        } elseif ($status == DELIVERY_IN_SHIPMENT) {
            $insert_data['message'] = __('Product sent to shipment successfully.');
        } elseif ($status == DELIVERY_COMPLETED) {
            $insert_data['message'] = __('Product delivered successfully.');
        } elseif ($status == PRODUCT_RECEIVED) {
            $insert_data['message'] = __('Product is received by user confirmed by seller.');
        } elseif ($status == SELLER_PAID) {
            $insert_data['message'] = __('Seller payment successful.');
        }
        $auction_status = AuctionStatus::create($insert_data);
        return  $auction_status;
    }

    public function makeAuctionLive(array $requestArray)
    {
        try {
            $data = [];
            $message = '';
            if ($requestArray['type'] == 'enable') {
                $data['has_live'] = TRUE;
                $message = __('Live enabled successful.');
            } else {
                $data['has_live'] = FALSE;
                $message = __('Live disabled successful.');
            }
            $this->repo->updateModel($requestArray['id'], $data);
            return jsonResponse(TRUE)->message($message);
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    public function makeAuctionEnd($auction_id)
    {
        try {
            $auction = Auction::where('id', $auction_id)->first();
            if (!empty($auction->winner_id)) {
                $commssion_settings = __options(['commission_settings']);
                $commission_price_perchantage = $commssion_settings->commission_rate ?? 0;
                $heighest_bid = $auction->highest_bid;
                $processing_fee = ($heighest_bid * $auction->service_charge) / 100;
                $admin_commission_price = ($heighest_bid * $commission_price_perchantage) / 100;
                $total_price = $heighest_bid + $processing_fee;
                $seller_paying_amount = $heighest_bid - $admin_commission_price;
                $data['auction_winning_price'] = $heighest_bid;
                $data['commission_price'] = $admin_commission_price;
                $data['processing_fee'] = $processing_fee;
                $data['total_price'] = $total_price;
                $data['seller_paying_amount'] = $seller_paying_amount;
                $auction_payment = AuctionPayment::where('auction_id', $auction->id)->first();
                if (isset($auction_payment)) {
                    AuctionPayment::where('auction_id', $auction->id)->update($data);
                } else {
                    AuctionPayment::create($data);
                }
            }
        } catch (\Exception $exception) {
        }
    }

    public function payAuctionPriceToSeller(array $requestArray)
    {
        try {
            DB::beginTransaction();
            if (!empty($requestArray['amount'])) {
                $auction = Auction::select('auctions.*', 'products.seller_id')
                    ->join('products', 'auctions.product_id', 'products.id')
                    ->where('auctions.id', $requestArray['auction_id'])
                    ->first();
                if (isset($auction)) {
                    $this->updateSellerPayingStatus($requestArray);
                    $this->addTransactionHistory($requestArray);
                    $this->updateBalance($auction, $requestArray);
                    DB::commit();
                    return jsonResponse(TRUE)->message(__('Seller payment successful'));
                } else {
                    DB::rollBack();
                    return jsonResponse(false)->message(__('Seller payment failed'));
                }
            } else {
                DB::rollBack();
                return jsonResponse(false)->message(__('Amount must be greater than zero.'));
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    private function addTransactionHistory($requestArray)
    {
        $paymentHistory = [
            'auction_id' => $requestArray['auction_id'],
            'user_id' => Auth::user()->id,
            'purpose' => SELLER_PAYMENT,
            'payment_method' => OFFLINE_PAYMENT,
            'amount' => $requestArray['amount'],
            'created_at' => Carbon::now(),
            'payment_status' => ACTIVE
        ];
        $transaction_ref = [
            'success' => TRUE,
            'title' => __('Seller auction payment'),
            'amount' => $requestArray['amount']
        ];
        if (isset($requestArray['document'])) {
            $path = 'auction/' . $requestArray['auction_id'] . '/payment';
            $transaction_ref['document'] = Storage::put($path, $requestArray['document']);
        }
        $paymentHistory['transaction_reference'] = json_encode($transaction_ref);
        AuctionPaymentTransaction::create($paymentHistory);
    }

    private function updateBalance($auction, $requestArray)
    {
        $admin = User::where(['module_id' => USER_ADMIN, 'status' => ACTIVE])->first();
        User::where('id', $admin->id)->decrement('balance', $requestArray['amount']);
        User::where('id', $auction->seller_id)->increment('balance', $requestArray['amount']);
    }

    private function updateSellerPayingStatus($requestArray)
    {
        $updateData = [
            'seller_paying_amount' => 0,
            'is_seller_paid' => ACTIVE,
            'seller_paying_amount_time' => Carbon::now(),
        ];
        Auction::where('id', $requestArray['auction_id'])->update(['is_seller_paid' => ACTIVE]);
        AuctionPayment::where('auction_id', $requestArray['auction_id'])->update($updateData);
        $this->auctionStatusUpdateHistory($requestArray['auction_id'], SELLER_PAID);
    }
}
