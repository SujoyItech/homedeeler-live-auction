<?php

namespace App\Http\Services;

use App\Http\Repositories\PaymentRepository;

class PaymentService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param PaymentRepository $repository
     */
    public function __construct(PaymentRepository $repository)
    {
        $this->repo = $repository;
    }

    // Your methods for repository
}
