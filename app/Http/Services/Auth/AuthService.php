<?php

namespace App\Http\Services\Auth;

use App\Http\Repositories\Auth\AuthRepository;
use App\Http\Requests\Web\Auth\ForgetPasswordRequest;
use App\Http\Services\BaseService;
use App\Http\Services\MailService;
use App\Jobs\SendMailJob;
use App\Models\User;
use App\Models\UserVerificationCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param Auth/AuthRepository $repository
     */
    public function __construct(AuthRepository $repository)
    {
        $this->repo = $repository;
    }

    // Your methods for repository

    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return $this->statusCheck(auth()->user());
        } else {
            return jsonResponse(FALSE)->message(__('Email or Password Not matched!'));
        }
    }

    public function loginWithSocial($user, $driver)
    {
        if ($driver == 'facebook') {
            return $this->socialLoginAttempt($user, 'fb_id');
        } elseif ($driver == 'google') {
            return $this->socialLoginAttempt($user, 'google_id');
        }
    }

    private function socialLoginAttempt($user, $driver_key)
    {
        $isUser = User::where('email', $user->email)->orWhere($driver_key, $user->id)->first();
        if ($isUser) {
            Auth::login($isUser);
            return $this->statusCheck(auth()->user());
        } else {
            $createUserData = [
                'name' => $user->name,
                'email' => $user->email,
                'is_social_login' => TRUE,
                'social_image_url' => $user->avatar,
                $driver_key => $user->id,
                'password' => bcrypt('Pass.1234'),
                'remember_token' => md5($user->email . uniqid() . randomString(5)),
                'email_verified' => TRUE,
                'admin_verified' => FALSE,
                'is_seller' => FALSE,
                'module_id' => MODULE_USER,
                'role' => USER_BIDDER,
                'status' => ACTIVE
            ];
            $createUser = $this->repo->create($createUserData);
            Auth::login($createUser);
            return jsonResponse(true)->message(__('Login successful'));
        }
    }

    public function statusCheck($user)
    {
        if ($user->status == ACTIVE) {
            return jsonResponse(TRUE)->message(__('Login successful.'));
        } elseif ($user->status == INACTIVE) {
            Auth::logout();
            return jsonResponse(FALSE)->message(__('Your account is inactive. Please change your password or contact with admin.'));
        } elseif ($user->status == USER_BLOCKED) {
            Auth::logout();
            return jsonResponse(FALSE)->message(__('You are blocked. Contact with admin.'));
        } elseif ($user->status == USER_SUSPENDED) {
            Auth::logout();
            return jsonResponse(FALSE)->message(__('Your Account has been suspended. please contact with admin to active again!'));
        } else {
            Auth::logout();
            return jsonResponse(FALSE)->default();
        }
    }

    public function register(array $requestArray)
    {
        DB::beginTransaction();
        try {
            $user = User::create($this->getRegistrationData($requestArray));
            if (isset($user)) {
                $this->sendUserVerificationMail($user);
            }
            DB::commit();
            return jsonResponse(TRUE)->message(__('Registration successful.Please verify your email to login.'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    public function sellerRequestSend(array $requestArray)
    {

        DB::beginTransaction();
        try {
            $user = User::where('id', $requestArray['id'])->first();
            if (isset($user)) {
                $requestArray['role'] = USER_SELLER;
                User::where('id', $user->id)->update($requestArray);
                DB::commit();
                return jsonResponse(TRUE)->message(__('Seller request sent.'));
            } else {
                return jsonResponse(FALSE)->message(__('User not found.'));
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    private function getRegistrationData(array $requestArray)
    {
        $user_data = [
            'name' => $requestArray['name'],
            'email' => $requestArray['email'],
            'nid' => $requestArray['nid'] ?? '',
            'is_seller' => isset($requestArray['is_seller']) && $requestArray['is_seller'] == ACTIVE ? IS_SELLER : IS_USER,
            'module_id' => MODULE_USER,
            'role' => isset($requestArray['is_seller']) && $requestArray['is_seller'] == ACTIVE ? USER_SELLER  : USER_BIDDER,
            'password' => bcrypt($requestArray['password']),
            'remember_token' => md5($requestArray['email'] . uniqid() . randomString(5)),
            'status' => INACTIVE
        ];
        if (isset($requestArray['nid_picture'])) {
            $user_data['nid_picture'] = uploadImage($requestArray['nid_picture'], get_image_path('user'));
        }
        if (isset($requestArray['profile_photo_path'])) {
            $user_data['profile_photo_path'] = uploadImageByCamera($requestArray['profile_photo_path'], get_image_path('user'));;
        }
        return $user_data;
    }

    public function sendUserVerificationMail($user)
    {
        $mail_key = randomNumber(6);
        $insert_code = [
            'user_id' => $user->id,
            'code' => $mail_key,
            'type' => 1,
            'status' => INACTIVE,
            'expired_at' => date('Y-m-d', strtotime('+15 days'))
        ];
        UserVerificationCode::create($insert_code);
        $userName = $user->name;
        $userEmail = $user->email;
        $subject = __('Home Deeler Email Verification.');
        $userData['message'] = __('Hello! ') . $userName . __(' Please Verify Your Email.');
        $userData['verification_code'] = $mail_key;
        $userData['email'] = $userEmail;
        $userData['name'] = $userName;
        dispatch(new SendMailJob('admin.mail.email.send_verification_mail_web', $userEmail, $userData, $subject));
    }

    public function userVerifyEmail($code)
    {
        if (!empty($code)) {
            $user_verification = UserVerificationCode::where(['code' => $code])->where('status', INACTIVE)->first();
            if ($user_verification) {
                DB::beginTransaction();
                try {
                    UserVerificationCode::where(['id' => $user_verification->id])->update(['status' => STATUS_SUCCESS]);
                    $user = User::where('id', $user_verification->user_id)->first();
                    if (!empty($user)) {
                        if ($user->email_verified == INACTIVE) {
                            User::where('id', $user->id)->update(['email_verified' => STATUS_ACTIVE, 'status' => STATUS_ACTIVE]);
                            $response = jsonResponse(TRUE)->message(__('Email successfully verified.'));
                        } else {
                            $response = jsonResponse(TRUE)->message(__('You already verified email!'));
                        }
                    } else {
                        $response = jsonResponse(FALSE)->message(__('Email Verification Failed'));
                    }
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollBack();
                    $response =  jsonResponse(FALSE)->message($e->getMessage());
                }
            } else {
                $response =  jsonResponse(FALSE)->message(__('Verification Code Not Found!'));
            }
            return $response;
        } else {
            return jsonResponse(FALSE)->message(__('Verification Code Not Found!'));
        }
    }

    public function sendForgetPasswordMail($request)
    {
        try {
            $user = User::where(['email' => $request->email])->first();
            if ($user) {
                $this->repo->sendForgotPasswordMail($user);
                $data['user'] = $user;
                return jsonResponse(true)->message(__("Please check your email to recover password."))->data($data);
            } else {
                return jsonResponse(FALSE)->message(__("Your email is not correct!."));
            }
        } catch (\Exception $e) {
            return jsonResponse(FALSE)->default();
        }
    }

    public function changePassword(Request $request)
    {
        try {
            $user = User::where(['remember_token' => $request->remember_token])->first();
            if ($user) {
                $updated = $this->repo->changePassword($user, $request->password);
                if ($updated) {
                    if ($user->status == INACTIVE) {
                        $this->repo->changeStatus($user, STATUS_ACTIVE);
                    }
                    $data['user'] = $user;
                    return jsonResponse(true)->message(__("Password changed successfully."))->data($data);
                } else {
                    return jsonResponse(FALSE)->message(__("Password not changed try again."));
                }
            } else {
                return jsonResponse(FALSE)->message(__("Sorry! user not found."));
            }
        } catch (\Exception $e) {
            return jsonResponse(FALSE)->default();
        }
    }

    public function logOut(Request $request)
    {
        try {
            if (Auth::check()) {
                Auth::user()->AauthAcessToken()->delete();
                return jsonResponse(TRUE)->message(__('Logout successful!'));
            } else {
                return jsonResponse(FALSE)->message(__('You are not authenticated!'));
            }
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }
}
