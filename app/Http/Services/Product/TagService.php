<?php

namespace App\Http\Services\Product;

use App\Http\Repositories\Product\TagRepository;
use App\Http\Services\BaseService;

class TagService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param Product/TagRepository $repository
     */
    public function __construct(TagRepository $repository)
    {
        $this->repo = $repository;
    }

    // Your methods for repository

    public function getTagData($id){

        if ($id !== NULL){
            $data['tag'] = $this->repo->getTagDetails($id);
        }else{
            $data['tag'] = [];
        }
        return $data;
    }

    public function create(array $requestArray) {

        try {
            $tag = $this->repo->create($requestArray);
            if ( $tag) {
                return jsonResponse(true)->message(__("Tag has been created successfully."));
            }
            return jsonResponse(false)->message(__("Tag create failed."));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    public function update(int $id, array $requestArray) {
        try {
            $response = $this->repo->updateModel($id, $requestArray);
            return !$response ? jsonResponse(false)->default() :
                jsonResponse(true)->message(__("Tag has been updated successfully"));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    public function delete($id){
        try {
            $supplier = $this->repo->destroy($id);
            if ($supplier){
                return jsonResponse(TRUE)->message(__('Tag deleted successfully.'));
            }else{
                return jsonResponse(TRUE)->message(__('Tag delete failed.'));
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }

    }
}
