<?php

namespace App\Http\Services\Product;

use App\Http\Repositories\Product\CombinationTypeRepository;
use App\Http\Services\BaseService;

class CombinationTypeService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param Product/CombinationTypeRepository $repository
     */
    public function __construct(CombinationTypeRepository $repository)
    {
        $this->repo = $repository;
    }

    // Your methods for repository

    public function getCombinationTypeData($id){

        if ($id !== NULL){
            $data['combination_type'] = $this->repo->getCombinationTypeDetails($id);
        }else{
            $data['combination_type'] = [];
        }
        return $data;
    }

    public function create(array $requestArray) {

        try {
            $combination_type = $this->repo->create($requestArray);
            if ( $combination_type) {
                return jsonResponse(true)->message(__("Combination type has been created successfully."));
            }
            return jsonResponse(false)->message(__("Combination type create failed."));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    public function update(int $id, array $requestArray) {
        if ($id < SIZE_ID) {
            return sendResponseError('',__('You can\'t change it.'));
        }
        try {
            $response = $this->repo->updateModel($id, $requestArray);
            return !$response ? jsonResponse(false)->default() :
                jsonResponse(true)->message(__("Combination type has been updated successfully"));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    public function delete($id){
        try {
            $response = $this->repo->destroy($id);
            if ($response){
                return jsonResponse(TRUE)->message(__('Combination type deleted successfully.'));
            }else{
                return jsonResponse(TRUE)->message(__('Combination type delete failed.'));
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }

    }
}
