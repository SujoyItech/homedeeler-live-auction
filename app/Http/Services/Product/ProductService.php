<?php

namespace App\Http\Services\Product;

use App\Http\Repositories\Product\BrandRepository;
use App\Http\Repositories\Product\CategoryRepository;
use App\Http\Repositories\Product\CombinationRepository;
use App\Http\Repositories\Product\CombinationTypeRepository;
use App\Http\Repositories\Product\TagRepository;
use App\Models\Auction\Auction;
use App\Models\Product\ProductBrand;
use App\Models\Product\ProductPricing;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Services\BaseService;
use App\Http\Repositories\Product\ProductRepository;
use App\Http\Repositories\Product\ProductTagRepository;
use App\Http\Repositories\Product\ProductBrandRepository;
use App\Http\Repositories\Product\ProductCategoryRepository;
use App\Http\Repositories\Product\ProductCombinationRepository;
use App\Models\Product\ProductCombination;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Util\Filesystem;


class ProductService extends BaseService
{
    private $productBrandRepo, $productCategoryRepo, $productCombinationRepo, $productTagRepo, $combinationTypeRepository;
    private $categoryRepository, $tagRepository, $brandRepo, $combinationRepository;

    public function __construct(
        ProductRepository $repository,
        ProductBrandRepository $productBrandRepository,
        ProductCategoryRepository $productCategoryRepository,
        ProductCombinationRepository $productCombinationRepository,
        ProductTagRepository $productTagRepository,
        CategoryRepository $categoryRepository,
        TagRepository $tagRepository,
        BrandRepository $brandRepo,
        CombinationTypeRepository $combinationTypeRepository,
        CombinationRepository $combinationRepository
    ) {
        $this->repo = $repository;
        $this->productBrandRepo = $productBrandRepository;
        $this->productCategoryRepo = $productCategoryRepository;
        $this->productCombinationRepo = $productCombinationRepository;
        $this->productTagRepo = $productTagRepository;
        $this->categoryRepository = $categoryRepository;
        $this->tagRepository = $tagRepository;
        $this->brandRepo = $brandRepo;
        $this->combinationRepository = $combinationRepository;
        $this->combinationTypeRepository = $combinationTypeRepository;
    }

    public function getProductData($search_array = [])
    {
        return $this->repo->getProductData($search_array);
    }

    public function getProductImages($product_id, $is_featured = FALSE)
    {
        return $this->productCombinationRepo->getProductImagesByColor($product_id, $is_featured);
    }


    public function checkSlug(Request $request)
    {
        $slug = $this->repo->getSlug($request->slug, $request->id);
        try {
            if ($slug) {
                return jsonResponse(FALSE)->message(__('Unique and valid Slug required'));
            } else {
                return jsonResponse(TRUE)->message(__('Valid Slug'));
            }
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->default();
        }
    }

    public function checkReference(Request $request)
    {
        $reference = $this->repo->getReference($request->reference, $request->id);
        try {
            if ($reference) {
                return sendResponseError('', __('Unique and valid Reference required.'));
            } else {
                return sendResponse('', __('Valid Reference.'));
            }
        } catch (\Exception $exception) {
            return sendResponseError();
        }
    }

    public function saveBrands(array $requestArray)
    {
        try {
            $product = $this->productBrandRepo->create($requestArray);
            if ($product) {
                return jsonResponse(TRUE)->message(__("Product Brands has been saved successfully."));
            }
            return jsonResponse(FALSE)->message(__("Product Brands save failed."));
        } catch (\Exception $e) {
            return jsonResponse(FALSE)->default();
        }
    }

    public function saveCategories(array $requestArray)
    {
        try {
            $product = $this->productCategoryRepo->create($requestArray);
            if ($product) {
                return jsonResponse(TRUE)->message(__("Product Categories has been saved successfully."));
            }
            return jsonResponse(FALSE)->message(__("Product Categories save failed."));
        } catch (\Exception $e) {
            return jsonResponse(FALSE)->default();
        }
    }

    public function saveCombinations(array $requestArray)
    {
        try {
            $product = $this->productCombinationRepo->create($requestArray);
            if ($product) {
                return jsonResponse(TRUE)->message(__("Product Combinations has been saved successfully."));
            }
            return jsonResponse(FALSE)->message(__("Product Combinations save failed."));
        } catch (\Exception $e) {
            return jsonResponse(FALSE)->default();
        }
    }

    public function saveTags(array $requestArray)
    {
        try {
            $product = $this->productTagRepo->create($requestArray);
            if ($product) {
                return jsonResponse(TRUE)->message(__("Product Tags has been saved successfully."));
            }
            return jsonResponse(FALSE)->message(__("Product Tags save failed."));
        } catch (\Exception $e) {
            return jsonResponse(FALSE)->default();
        }
    }

    public function statusChange(array $requestArray)
    {
        try {
            $product = $this->repo->changeStatus($requestArray);
            if ($product) {
                return jsonResponse(TRUE)->message(__("Product status has been changed successfully."));
            }
            return jsonResponse(FALSE)->message(__("Product status change failed."));
        } catch (\Exception $e) {
            return jsonResponse(FALSE)->default();
        }
    }

    public function initProduct($request): \Illuminate\Http\JsonResponse
    {
        try {
            $product = $this->prepareDataAndCreateProduct($request);
            if (!$product) throw new \Exception();
            return sendResponse(['slug' => $product->slug], __('Product has been initiated successfully.'));
        } catch (\Exception $e) {
            return sendResponseError('', __('Product initialization failed. ') . $e->getMessage());
        }
    }

    private function prepareDataAndCreateProduct($request)
    {
        $data = [
            'seller_id'      => Auth::user()->id,
            'name'      => $request->name,
            'slug'      => $request->slug,
            'status'    => PRODUCT_DRAFT
        ];
        return $this->repo->create($data);
    }


    /**
     * @param $request
     *
     * @return array
     */
    public function getProductEditPageData($request): array
    {
        $product = $this->repo->firstWhere(['slug' => $request->slug]);
        if (!$product) {
            abort(404);
        }
        $categories = buildTree($this->categoryRepository->getData(['status' => STATUS_ACTIVE], NULL, ['sort_number' => 'asc']));
        dd($categories);
        $tags = $this->tagRepository->getData(['status' => STATUS_ACTIVE]);
        $brands = $this->brandRepo->getData(['status' => ACTIVE]);
        $combinationTypes = $this->combinationTypeRepository->getData();
        $combinations = $this->combinationRepository->getData();
        $productCombinations = $this->productCombinationRepo->getData(['product_id' => $product->id, 'status' => ACTIVE], NULL, ['combination_type_id' => 'desc']);
        $productCategories = $this->productCategoryRepo->getQuery(['product_id' => $product->id])->pluck('category_id')->toArray();
        $productPricing = ProductPricing::where('product_id', $product->id)->first();
        return [
            'product'             => $product,
            'categories'          => $categories,
            'tags'                => $tags,
            'productCategories'   => $productCategories,
            'productTags'         => $product->product_tags,
            'brands'              => $brands,
            'combinationTypes'    => $combinationTypes,
            'combinations'        => $combinations,
            'productCombinations' => $productCombinations,
            'productPricing' => $productPricing
        ];
    }

    public function saveProduct($request): \Illuminate\Http\JsonResponse
    {
        try {
            $product = $this->repo->updateWhere(['id' => $request->id], [
                'name'             => $request->name,
                'description'      => $request->description,
                'additional_info'  => $request->additional_info,
                'is_new'  => isset($request->is_new) ? ACTIVE : INACTIVE,
                'price_range_from'  => $request->price_range_from,
                'price_range_to'  => $request->price_range_to
            ]);
            if (!empty($request->brand_id)) {
                $product_brands = $this->prepareProductBrandData($request->id, $request->brand_id);
                ProductBrand::where('product_id', $request->id)->delete();
                ProductBrand::insert($product_brands);
            }
            $auction_data = [];
            // $auction_data['status'] =  isset($request->status) ? ACTIVE : INACTIVE;
            $auction_data['is_featured'] = isset($request->is_featured) ? ACTIVE : INACTIVE;
            $auction_data['service_charge'] = $request->service_charge;
            Auction::where('id', $request->auction_id)->update($auction_data);
            if (!$product) throw new \Exception();
            return sendResponse('', __('Saved successfully.'));
        } catch (\Exception $e) {
            return sendResponseError('', __('Save failed. ') . $e->getMessage());
        }
    }

    private function prepareProductBrandData($product_id, $brands)
    {
        $product_brand = [];
        foreach ($brands as $key => $brand_id) {
            $product_brand[$key]['product_id'] = $product_id;
            $product_brand[$key]['brand_id'] = $brand_id;
        }
        return $product_brand;
    }

    public function saveProductCategoriesAndTags($request): \Illuminate\Http\JsonResponse
    {
        $categoryIds = $request->category_id;
        $tagIds = $request->tag_id;
        $productId = $request->id;
        try {
            if (!$categoryIds && !$tagIds) return sendResponseError('', __('No data send.'));
            $response = $this->saveProductCategories($categoryIds, $productId);
            if (!$response['success']) throw new \Exception($response['message']);
            $response = $this->saveProductTags($tagIds, $productId);
            if (!$response['success']) throw new \Exception($response['message']);
            return sendResponse('', __('Saved successfully.'));
        } catch (\Exception $e) {
            return sendResponseError('', __('Something went wrong. ') . $e->getMessage());
        }
    }

    private function saveProductCategories($categoryIds, $productId)
    {
        if (!$categoryIds) return ['success' => TRUE];
        $productCategories = [];
        foreach ($categoryIds as $categoryId) {
            $productCategories[] = ['product_id' => $productId, 'category_id' => $categoryId];
        }
        try {
            DB::beginTransaction();
            $this->productCategoryRepo->deleteWhere(['product_id' => $productId]);
            $this->productCategoryRepo->insert($productCategories);
            DB::commit();
            return ['success' => TRUE];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['success' => FALSE, 'message' => $e->getMessage()];
        }
    }

    private function saveProductTags($tagIds, $productId)
    {
        if (!$tagIds) return ['success' => TRUE];
        $productTags = [];
        foreach ($tagIds as $tagId) {
            $productTags[] = ['product_id' => $productId, 'tag_id' => $tagId];
        }
        try {
            DB::beginTransaction();
            $this->productTagRepo->deleteWhere(['product_id' => $productId]);
            $this->productTagRepo->insert($productTags);
            DB::commit();
            return ['success' => TRUE];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['success' => FALSE, 'message' => $e->getMessage()];
        }
    }

    public function saveProductCombinations($request)
    {
        $productId = $request->product_id;
        $combinationTypeId = $request->combination_type_id;
        $combinationIds = is_array($request->combination_ids) ? $request->combination_ids : [];
        if ($combinationTypeId == COLOR_ID) {
            return $this->saveColorCombinations($productId, $combinationIds);
        } else {
            return $this->saveOtherCombinations($productId, $combinationTypeId, $combinationIds);
        }
    }

    private function saveColorCombinations($productId, $reqCombinationIds)
    {
        $newProductCombinations = [];
        $existingCombinationIds = $this->productCombinationRepo->getData(['product_id' => $productId, 'combination_type_id' => COLOR_ID], ['combination_id'])
            ->pluck('combination_id')->toArray();
        $newCombinationIds = array_diff($reqCombinationIds, $existingCombinationIds);
        foreach ($newCombinationIds as $combinationId) {
            $newProductCombinations[] = [
                'product_id' => $productId, 'combination_type_id' => COLOR_ID, 'combination_id' => $combinationId,
                'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
            ];
        }
        try {
            DB::beginTransaction();
            $this->productCombinationRepo->updateWhereNotInAndWhere(
                'combination_id',
                array_values($reqCombinationIds),
                ['product_id' => $productId, 'combination_type_id' => COLOR_ID],
                ['status' => INACTIVE]
            );
            $this->productCombinationRepo->updateWhereInAndWhere(
                'combination_id',
                array_values($reqCombinationIds),
                ['product_id' => $productId, 'combination_type_id' => COLOR_ID],
                ['status' => ACTIVE]
            );
            $this->productCombinationRepo->insert($newProductCombinations);
            DB::commit();
            return $this->renderMediaCombinations($productId);
        } catch (\Exception $e) {
            DB::rollBack();
            return sendResponseError('', __('Something went wrong.') . $e->getMessage());
        }
    }

    private function renderMediaCombinations($productId)
    {
        $updatedMediaCombinations = $this->productCombinationRepo->getData(['product_id' => $productId, 'status' => ACTIVE], NULL, ['combination_type_id' => 'desc']);
        $html = view('admin.auction.add_product.combination.media_combinations', ['productCombinations' => $updatedMediaCombinations])->render();
        return sendResponse(['html' => $html], __('Saved successfully.'));
    }

    private function saveOtherCombinations($productId, $combinationTypeId, $combinationIds)
    {
        $productCombinations = [];
        foreach ($combinationIds as $combinationId) {
            $productCombinations[] = [
                'product_id' => $productId, 'combination_type_id' => $combinationTypeId, 'combination_id' => $combinationId,
                'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
            ];
        }
        try {
            DB::beginTransaction();
            $this->productCombinationRepo->deleteWhere(['product_id' => $productId, 'combination_type_id' => $combinationTypeId]);
            $this->productCombinationRepo->insert($productCombinations);
            DB::commit();
            return sendResponse('', __('Saved successfully.'));
        } catch (\Exception $e) {
            DB::rollBack();
            return sendResponseError('', __('Something went wrong.') . $e->getMessage());
        }
    }

    public function saveProductMedia($request)
    {
        $data['product_id'] = $request->product_id;
        $data['media_type'] = $request->media_type;
        $data['id'] = $request->id;
        if ($request->image_url) {
            // $response = $this->uploadCombinationImage($request->file('image'));
            $path = uploadBase64ImageToStorage($request->image_url, 'products / images', 'public');
            if ($path) {
                $data['media_url'] = $path;
            } else {
                return sendResponseError('', __('Image upload failed!'));
            }
        } else if ($request->media_url) {
            $data['media_url'] = $request->media_url;
        }

        try {
            $productCombination = $this->productCombinationRepo->firstWhere(['id' => $data['id'], 'media_type' => INTERNAL_IMAGE]);

            $ok = $this->productCombinationRepo->updateOrCreate(['id' => $data['id']], $data);
            if (!$ok) throw new \Exception('Operation Failed');

            //remove if previous image exist
            if (($request->image && $productCombination) || ($request->media_url && $productCombination)) {
                $this->removePreviousCombinationImageFile($productCombination);
            }
            //

            return $this->renderMediaCombinations($data['product_id']);
        } catch (\Exception $e) {
            return sendResponseError('', __('Something went wrong. ') . $e->getMessage());
        }
    }

    private function uploadCombinationImage($image)
    {
        try {
            if (isset($image)) {
                $folder = 'products/images';
                $url = $image->store($folder,);
                return ['success' => TRUE, 'url' => $url];
            }
            return ['success' => TRUE];
        } catch (\Exception $e) {
            return ['success' => FALSE, 'message' => $e->getMessage()];
        }
    }

    public function removePreviousCombinationImageFile($productCombination)
    {
        if ($productCombination->media_url) {
            Storage::delete($productCombination->media_url);
        }
    }

    public function saveProductFeaturedMedia($request)
    {
        $productId = $request->product_id;
        $productCombinationId = $request->product_combination_id;
        try {
            $this->productCombinationRepo->saveFeatured($productId, $productCombinationId);
            return $this->renderMediaCombinations($productId);
        } catch (\Exception $e) {
            return sendResponseError('', '', $e->getMessage());
        }
    }

    public function saveProductPricingInfo($product_id, array $requestArray)
    {
        try {
            ProductPricing::updateOrInsert(['product_id' => $product_id], $requestArray);
            return jsonResponse(TRUE)->message(__('Product price updated successfully'));
        } catch (\Exception $e) {
            return jsonResponse(FALSE)->message($e->getMessage());
        }
    }

    public function getProductSearchByCategory($category_id, $searchItem)
    {
        return $this->repo->getProductSearchByCategory($category_id, $searchItem);
    }

    public function getProductByCategory($category_id)
    {
        return $this->repo->getProductByCategory($category_id);
    }

    public function getProductListByWhereIn($id, $slug)
    {
        return $this->repo->getProductListByWhereIn($id, $slug);
    }
}
