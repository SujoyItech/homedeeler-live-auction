<?php

namespace App\Http\Services;

use App\Models\Auction\AuctionPaymentTransaction;

class TransactionService
{
    public function getTransactionHistoryList($where=[]){
        $query = AuctionPaymentTransaction::select('auction_payment_transactions.*','products.name as auction_name','users.name as user_name')
            ->join('auctions','auction_payment_transactions.auction_id','auctions.id')
            ->join('products','auctions.product_id','products.id')
            ->join('users','auction_payment_transactions.user_id','users.id')
            ->orderBy('auction_payment_transactions.created_at','desc');
        if (!empty($where)){
            $query->where($where);
        }
        return $query;
    }
}
