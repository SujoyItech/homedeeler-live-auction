<?php

namespace App\Http\Controllers;

use App\Classes\AgoraDynamicKey\RtcTokenBuilder;
use App\Events\MakeAgoraCall;
use App\Http\Services\NotificationService;
use App\Models\Auction\Auction;
use App\Models\Product\ProductCombination;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AgoraVideoController extends Controller
{
    public function index(Request $request)
    {
        // fetch all users apart from the authenticated user
        $users = User::where('id', '<>', Auth::id())->get();
        return view('agora-chat', ['users' => $users]);
    }
    public function broadcastLive($slug)
    {
        $data['auction'] = Auction::select('auctions.*', 'products.slug')
            ->join('products', 'auctions.product_id', '=', 'products.id')
            ->where('products.slug', $slug)
            ->first();
        return view('admin.auction.live_streaming.live_video_chat_broadcast', $data);
    }

    public function clientLive()
    {
        return view('admin.auction.live_streaming.live_video_chat_client');
    }

    public function createToken(Request $request)
    {
        try {
            if (Auth::user()->module_id == MODULE_SUPER_ADMIN || (Auth::user()->module_id == MODULE_USER && Auth::user()->is_seller == ACTIVE)) {
                $auction = Auction::select('auctions.*', 'products.slug')
                    ->join('products', 'auctions.product_id', 'products.id')
                    ->where('auctions.id', $request->auction_id)
                    ->first();
                if (isset($auction)) {
                    if ($auction->has_live == ACTIVE && $auction->status == AUCTION_IN_LIVE) {
                        if (!empty($auction->live_duration)) {
                            $appID = env('AGORA_APP_ID');
                            $appCertificate = env('AGORA_APP_CERTIFICATE');
                            $channelName = $auction->slug;
                            $user = NULL;
                            $role = $request->role;
                            $expireTimeInSeconds = (int) $auction->live_duration * 60;
                            $currentTimestamp = now()->getTimestamp();
                            $privilegeExpiredTs = $currentTimestamp + $expireTimeInSeconds;
                            $token = RtcTokenBuilder::buildTokenWithUserAccount($appID, $appCertificate, $channelName, $user, $role, $privilegeExpiredTs);
                            Auction::where('id', $auction->id)->update(['is_streaming' => ACTIVE, 'streaming_token' => $token]);
                            $this->sendLiveStreamingNotification($auction->id, $token);
                            return jsonResponse(TRUE)->message(__('Token generated successfully.'))->data(['token' => $token]);
                        } else {
                            return jsonResponse(FALSE)->message(__('Invalid live duration.'));
                        }
                    } else {
                        return jsonResponse(FALSE)->message(__('This auction has no live now.'));
                    }
                } else {
                    return jsonResponse(FALSE)->message('No auction found using this id');
                }
            } else {
                return jsonResponse(FALSE)->message('Sorry! You are not a seller. Only seller can go to live.');
            }
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    private function sendLiveStreamingNotification($auction_id, $token)
    {
        $channel = 'live_auction_message_' . $auction_id;
        $event = 'auction_live_streaming_start';
        $title = __('Live auction streaming started');
        $body = [
            'is_livestreaming' => TRUE,
            'auction_id' => $auction_id,
            'streaming_uid' => $auction_id,
            'token' => $token,
        ];
        NotificationService::triggerNotification($channel, $event, $title, $body);
    }

    public function endStreaming(Request $request)
    {
        try {
            Auction::where('id', $request->auction_id)->update(['is_streaming' => INACTIVE, 'streaming_token' => '']);
            return jsonResponse(TRUE)->message(__('Streaming ended successfully'));
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message(__('Streaming end failed!'));
        }
    }

    public function auctionLiveStreaming($slug)
    {
        $auction_details = Auction::select(
            'auctions.*',
            'products.name',
            'products.slug',
            'products.description',
            'products.additional_info',
            'products.is_new',
            'products.meta_title',
            'products.meta_keywords',
            'products.meta_description',
            'products.price_range_from',
            'products.price_range_to',
            'users.name as winner name'
        )
            ->join('products', function ($join) use ($slug) {
                $join->on('auctions.product_id', 'products.id')->where('products.slug', $slug);
            })->leftJoin('users', 'auctions.winner_id', 'users.id')->first();
        $data['auction_details'] = $auction_details;
        $data['product_images'] = ProductCombination::where(['product_id' => $auction_details->product_id])->where('is_featured', '=', TRUE)->first();
        return view('admin.auction.live_streaming.auction_details_live_streaming', $data);
    }

    public function audienceLive($slug)
    {
        $data['auction'] = Auction::select('auctions.*', 'products.slug')
            ->join('products', 'auctions.product_id', '=', 'products.id')
            ->where('products.slug', $slug)
            ->first();
        return view('admin.auction.live_streaming.audience', $data);
    }

    public function auctionLiveStreamingStop(Request $request)
    {
        try {
            $auction = Auction::where('id', $request->auction_id)->first();
            if (isset($auction)) {
                Auction::where('id', $request->auction_id)->update(['is_streaming' => INACTIVE]);
                $channel = 'live_auction_message_' . $request->auction_id;
                $event = 'auction_live_streaming_end';
                $title = __('Auction live streaming ended.');
                $body = [
                    'is_livestreaming' => FALSE,
                    'auction_id' => $auction->id,
                ];
                NotificationService::triggerNotification($channel, $event, $title, $body);
                return jsonResponse(TRUE)->message(__('Auction streaming ended successfully!'));
            } else {
                return jsonResponse(FALSE)->message(__('Auction streaming end failed!'));
            }
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }
}
