<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\Auction\Auction;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Services\CoinPaymentsAPI;
use App\Http\Services\HomeDeelerService;
use App\Http\Services\Payment\PaymentService;
use App\Models\Payment\BtcPaymentAddressMapping;

class PaymentController extends Controller
{
    private $payment_service;

    public function __construct(PaymentService $payment_service)
    {
        $this->payment_service = $payment_service;
    }

    public function checkActiveCard()
    {
        try {
            $payment_setting = __options(['payment_settings']);
            $data = [
                'active_card' => $payment_setting->payment_method
            ];
            return jsonResponse(TRUE)->data($data)->message(__('Get active card data'));
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message(__('Something went wrong!'));
        }
    }

    public function paymentSettings()
    {
        try {
            $payment_setting = __options(['payment_settings']);
            $data['payment_settings'] = $payment_setting;
            return jsonResponse(TRUE)->data($data)->message(__('Get payment setting data'));
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message(__('Something went wrong!'));
        }
    }

    public function auctionPaymentWithPaypal(Request $request)
    {
    }

    public function auctionPaymentWithStripe(Request $request)
    {
        try {
            DB::beginTransaction();
            $stripe_response = $request->response;;
            if ($stripe_response->status == 'succeeded') {
                $paid_amount = !empty($stripe_response->amount) ? $stripe_response->amount / 100 : 0;
                $auction = Auction::where('id', $request->auction_id)->first();
                $this->payment_service->addToTransactionHistory($request->auction_id, PRODUCT_PURCHASE_PAYMENT, CARD_STRIPE, $paid_amount, json_encode($stripe_response));
                $this->payment_service->productPurchaseBalanceUpdate($auction, $paid_amount);
                $this->userPaymentStatusUpdated($auction->id);
                DB::commit();
                return jsonResponse(TRUE)->message(__('Payment successful.'));
            } else {
                DB::rollBack();
                return jsonResponse(FALSE)->message(__('Payment failed!'));
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    public function payWithBtcBalance(Request $request)
    {
        try {
            DB::beginTransaction();
            $user_id = Auth::user()->id;
            $user = User::where('id', $user_id)->first();
            $user_btc_balance =  (float)$user->btc_balance;
            $pay_btc_balance = (float)$request->amount_in_btc;
            if ($user_btc_balance >= $pay_btc_balance) {
                $new_balance = $user_btc_balance - $pay_btc_balance;
                $amount = $request->amount;
                $status = [
                    'payment_type' => 'BTC Payment',
                    'btc_amount' => $pay_btc_balance,
                    'amount' => $amount
                ];
                User::where('id', $user_id)->update(['btc_balance' => $new_balance]);
                $auction = Auction::where('id', $request->auction_id)->first();
                $this->payment_service->addToTransactionHistory($auction->id, PRODUCT_PURCHASE_PAYMENT, BTC, $amount, json_encode($status));
                $this->payment_service->productPurchaseBalanceUpdate($auction, $amount);
                $this->userPaymentStatusUpdated($auction->id);
                DB::commit();
                return jsonResponse(TRUE)->message(__('Payment with btc successful.'));
            } else {
                DB::rollBack();
                return jsonResponse(FALSE)->message(__('Insufficient Balance !'));
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            return jsonResponse(FALSE)->default();
        }
    }

    public function addAuctionBtcBalance(Request $request)
    {
        try {
            $data['btc_data'] = $this->getBtcAddress($request->auction_id, PRODUCT_PURCHASE_PAYMENT, 'LTCT');
            $data['btc_data']['user_balance'] = Auth::user()->btc_balance;
            return jsonResponse(TRUE)->message(__('Btc address generated successfully.'))->data($data);
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message(__('Btc address generate failed.'));
        }
    }

    public function getBtcAddress($auction_id, $type = PRODUCT_PURCHASE_PAYMENT, $currency = 'BTC')
    {
        $coin_payment_settings = __options(['coin_payment_settings']);
        $coin_payment_service = new CoinPaymentsAPI();
        $user_id = Auth::user()->id;
        $btc_data = [];
        $conditions = [
            'auction_id' => $auction_id,
            'user_id' => $user_id,
            'type' => $type,
            'status' => BTC_ADDRESS_ACTIVE
        ];
        $check_address = BtcPaymentAddressMapping::where($conditions)->first();
        if (isset($check_address)) {
            if ($check_address->expired_at >= Carbon::now()) {
                $btc_data['btc_address'] = $check_address->btc_address;
                $btc_data['expiry_date'] = $check_address->expired_at;
            } else {
                BtcPaymentAddressMapping::where('id', $check_address->id)->update(['status' => BTC_ADDRESS_EXPIRED]);
                $address = $coin_payment_service->GetCallbackAddress($currency, route('userBtcBalanceUpdate'));
                $btc_address = isset($address) && $address['error'] == 'ok' ? $address['result']['address'] : '';
                $insert_data = [
                    'auction_id' => $auction_id,
                    'user_id' => $user_id,
                    'type' => $type,
                    'btc_address' => $btc_address,
                    'status' => BTC_ADDRESS_ACTIVE,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'expired_at' => Carbon::now()->addMinutes($coin_payment_settings->coin_payment_expiration_time)
                ];
                $bit_rates = $coin_payment_service->GetRates();
                if (isset($bit_rates) && $bit_rates['error'] == 'ok') {
                    $insert_data['btc_rates'] = $bit_rates['result']['USD']['rate_btc'];
                }
                $btc_map = BtcPaymentAddressMapping::create($insert_data);
                $btc_data['btc_address'] = $btc_map->btc_address;
                $btc_data['expiry_date'] = $btc_map->expired_at;
            }
        } else {
            $address = $coin_payment_service->GetCallbackAddress($currency, route('userBtcBalanceUpdate'));
            $btc_address = isset($address) && $address['error'] == 'ok' ? $address['result']['address'] : '';
            $insert_data = [
                'auction_id' => $auction_id,
                'user_id' => $user_id,
                'type' => $type,
                'btc_address' => $btc_address,
                'status' => BTC_ADDRESS_ACTIVE,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'expired_at' => Carbon::now()->addMinutes($coin_payment_settings->coin_payment_expiration_time)
            ];
            $btc_map = BtcPaymentAddressMapping::create($insert_data);
            $btc_data['btc_address'] = $btc_map->btc_address;
            $btc_data['expiry_date'] = $btc_map->expired_at;
        }
        return $btc_data;
    }

    private function userPaymentStatusUpdated($auction_id)
    {
        $status_data = [
            'auction_id' => $auction_id,
            'status' => PAYMENT_COMPLETED,
            'message' => __('User payment completed.')
        ];
        $home_deeler_service = new HomeDeelerService();
        $home_deeler_service->auctionStatusUpdated($status_data);
    }
}
