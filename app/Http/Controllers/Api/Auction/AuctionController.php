<?php

namespace App\Http\Controllers\Api\Auction;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auction\BidRequest;

use App\Http\Services\Api\AuctionService;
use App\Models\Auction\Auction;
use App\Models\Auction\AuctionFavourite;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuctionController extends Controller
{
    private $auction_service;

    public function __construct(AuctionService $auction_service)
    {
        $this->auction_service = $auction_service;
    }

    public function getCategoryList()
    {
        return $this->auction_service->getCategoryList();
    }

    public function auctionList(Request $request, $type = NULL)
    {
        $search_data = [];
        $where = [];
        if ($request->has('search_data')) {
            $search_data = $request->search_data;
        }
        return $this->auction_service->getAuctionList($search_data, $where, $type);
    }
    public function auctionListByCategory($category_id)
    {
        $search_data = [];
        $where = ['product_categories.category_id' => $category_id];
        return $this->auction_service->getAuctionList($search_data, $where);
    }

    public function searchAuction(Request $request)
    {
        $search_data = [];
        $where = [];
        if ($request->has('keywords')) {
            $search_data['keywords'] = $request->keywords;
        }
        return $this->auction_service->getAuctionList($search_data);
    }
    public function liveAuctionByDate(Request $request)
    {
        $search_data = [];
        $where = [];
        if ($request->has('live_start_time')) {
            $search_data['live_start_time'] = $request->live_start_time;
        }
        return $this->auction_service->getAuctionList($search_data);
    }

    public function auctionDetails($slug)
    {
        return $this->auction_service->auctionDetails($slug);
    }

    public function makeAuctionFavourite($auction_id)
    {
        $user_id = Auth::user()->id;
        $check_favourite = AuctionFavourite::where(['auction_id' => $auction_id, 'user_id' => $user_id])->first();
        $action = isset($check_favourite) ? ($check_favourite->is_favourite == TRUE ? FALSE : TRUE) : TRUE;
        return $this->auction_service->makeAuctionFavourite($user_id, $auction_id, $action);
    }

    public function myFavouriteList()
    {
        $search_data = [];
        $where = [];
        return $this->auction_service->getAuctionList($search_data, $where, 'is_favourite');
    }

    public function bid(BidRequest $request)
    {
        return $this->auction_service->bid($request->all());
    }

    public function myActiveBid()
    {
        return $this->auction_service->getAuctionList([], [], 'my_active_bids');
    }
    public function myExpiredBid()
    {
        return $this->auction_service->getAuctionList([], [], 'my_expired_bids');
    }
    public function myWinningBid()
    {
        return $this->auction_service->getAuctionList([], [], 'my_own_bids');
    }

    public function myAuctions()
    {
        return $this->auction_service->getAuctionList([], [], 'my_auctions');
    }
    public function myAuctionApproved()
    {
        return $this->auction_service->getAuctionList([], [], 'my_approved_auctions');
    }
    public function myAuctionPending()
    {
        return $this->auction_service->getAuctionList([], [], 'my_pending_auctions');
    }
    public function myItemsOnLive()
    {
        return $this->auction_service->getAuctionList([], [], 'my_live_items');
    }
    public function myItemsSold()
    {
        return $this->auction_service->getAuctionList([], [], 'my_sold_items');
    }
}
