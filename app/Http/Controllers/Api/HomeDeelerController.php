<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeDeelerController extends Controller
{
    public function generalInfo(){
        try {
            $settings = __options(['web_content_settings']);
            $data= [
                'about_us' => $settings->about_us ?? '',
                'privacy_policy' => $settings->privacy_policy ?? '',
                'terms_conditions' => $settings->terms_condition ?? '',
                'help_center' => $settings->help_center ?? ''
            ];
            return jsonResponse(TRUE)->message(__('General settings data'))->data($data);
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }
    }
}
