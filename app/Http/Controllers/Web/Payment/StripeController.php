<?php

namespace App\Http\Controllers\Web\Payment;

use App\Http\Controllers\Controller;
use App\Http\Services\Auction\LiveAuctionService;
use App\Http\Services\HomeDeelerService;
use App\Http\Services\Payment\AuctionPriceService;
use App\Http\Services\Payment\PaymentService;
use App\Models\Auction\Auction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Stripe\Charge;
use Stripe\Stripe;

class StripeController extends Controller
{
    private $live_auction_service,$payment_service,$auction_price_service;

    public function __construct(LiveAuctionService $live_auction_service,
                                PaymentService $payment_service,
                                AuctionPriceService $auction_price_service){
        $this->live_auction_service = $live_auction_service;
        $this->payment_service = $payment_service;
        $this->auction_price_service = $auction_price_service;
    }

    public function liveChargePaymentByStripe(Request $request){
        try {
            DB::beginTransaction();
            $requestArray = $request->all();
            $amount = $this->auction_price_service->calculateLivePaymentCharge($requestArray);
            if (!$amount){
                return redirect()->back()->with(['dismiss'=>__('Payment settings error!')]);
            }
            $stripe_response = $this->makePaymentByStripe($request, $amount, __('Live charge Payment'));

            if ($stripe_response->status == 'succeeded') {
                $paid_amount = !empty($stripe_response->amount) ? $stripe_response->amount/100 : 0 ;
                $this->live_auction_service->auctionLiveDataUpdate($requestArray,$amount,$paid_amount);
                $this->payment_service->addToTransactionHistory($requestArray['auction_id'],LIVE_AUCTION_CHARGE_PAYMENT,CARD_STRIPE,$paid_amount,json_encode($stripe_response));
                $this->payment_service->adminBalanceUpdate($paid_amount);
                DB::commit();
                return redirect()->back()->with(['success'=>__('Payment successful.')]);
            }
            return redirect()->back()->with(['dismiss'=>__('Payment failed!')]);

        }catch (\Exception $exception){
            DB::rollBack();
            return redirect()->back()->with(['dismiss'=>$exception->getMessage()]);
        }
    }

    public function productPurchasePaymentByStripe(Request $request){
        try {
            DB::beginTransaction();
            $requestArray = $request->all();
            $amount = $this->auction_price_service->calculateProductPaymentCharge($requestArray['auction_id']);
            if (!$amount){
                return redirect()->back()->with(['dismiss'=>__('Payment settings error!')]);
            }
            $stripe_response = $this->makePaymentByStripe($request, $amount, __('Product purchase Payment'));
            if ($stripe_response->status == 'succeeded') {
                $paid_amount = !empty($stripe_response->amount) ? $stripe_response->amount/100 : 0 ;
                $auction = Auction::where('id',$requestArray['auction_id'])->first() ;
                $this->payment_service->addToTransactionHistory($requestArray['auction_id'],PRODUCT_PURCHASE_PAYMENT,CARD_STRIPE,$paid_amount,json_encode($stripe_response));
                $this->payment_service->productPurchaseBalanceUpdate($auction,$paid_amount);
                $this->userPaymentStatusUpdated($auction->id);
                DB::commit();
                return redirect()->back()->with(['success'=>__('Payment successful.')]);
            }
            return redirect()->back()->with(['dismiss'=>__('Payment failed!')]);

        }catch (\Exception $exception){
            DB::rollBack();
            return redirect()->back()->with(['dismiss'=>__('Something went wrong!')]);
        }
    }

    public function makePaymentByStripe($request,$amount,$description){
        $settings = __options(['payment_settings']);
        Stripe::setApiKey($settings->stripe_secret);
        $stripe_response = Charge::create ([
            "amount" => 100 * floatval($amount),
            "currency" => "usd",
            "source" => $request->stripeToken,
            "description" => $description
        ]);
        return $stripe_response;
    }

    private function userPaymentStatusUpdated($auction_id){
        $status_data = [
            'auction_id' => $auction_id,
            'status' => PAYMENT_COMPLETED,
            'message' => __('User payment completed.')
        ];
        $home_deeler_service = new HomeDeelerService();
        $home_deeler_service->auctionStatusUpdated($status_data);
    }

}
