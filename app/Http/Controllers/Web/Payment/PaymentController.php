<?php

namespace App\Http\Controllers\Web\Payment;

use App\Http\Controllers\Controller;
use App\Models\Auction\Auction;
use Illuminate\Http\Request;


class PaymentController extends Controller
{
    public function loadProductPurchasePaymentBody(Request $request){
        $data['auction_id'] = $request->auction_id;
        $data['auction'] = Auction::select('auctions.*','products.name','products.slug')->join('products','auctions.product_id','products.id')->where('auctions.id',$request->auction_id)->first();
        $data['total_charge'] = $request->total_charge;
        return view('web.payment.card_payment_body',$data);
    }

    public function loadAuctionPaymentBody(Request $request){
        $data['auction'] = Auction::select('auctions.*','products.name','products.slug')->join('products','auctions.product_id','products.id')->where('auctions.id',$request->auction_id)->first();
        $data['auction_id'] = $request->auction_id;
        $data['live_start_time'] = $request->live_start_time;
        $data['live_duration'] = $request->live_duration;
        $data['total_charge'] = $request->total_charge;
        return view('admin.auction.add_product.price.payment.card_payment_body',$data);
    }

}
