<?php

namespace App\Http\Controllers\Web\Payment;

use App\Http\Controllers\Controller;
use App\Http\Services\Auction\LiveAuctionService;
use App\Http\Services\HomeDeelerService;
use App\Http\Services\Payment\AuctionPriceService;
use App\Http\Services\Payment\PaymentService;
use App\Models\Auction\Auction;
use Braintree\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BrainTreeController extends Controller
{
    private $live_auction_service, $payment_service, $auction_price_service;

    public function __construct(
        LiveAuctionService $live_auction_service,
        PaymentService $payment_service,
        AuctionPriceService $auction_price_service
    ) {
        $this->live_auction_service = $live_auction_service;
        $this->payment_service = $payment_service;
        $this->auction_price_service = $auction_price_service;
    }

    public function liveChargePaymentByBrainTree(Request $request)
    {
        try {
            DB::beginTransaction();
            $requestArray = $request->except('payload');
            $amount = $this->auction_price_service->calculateLivePaymentCharge($requestArray);
            if (!$amount) {
                return jsonResponse(FALSE)->message(__('Payment settings error!'));
            }
            $status = $this->makePaymentByBrainTree($request, $amount);
            if ($status->success == TRUE) {
                $this->live_auction_service->auctionLiveDataUpdate($requestArray, $amount, $amount);
                $this->payment_service->addToTransactionHistory($requestArray['auction_id'], LIVE_AUCTION_CHARGE_PAYMENT, CARD_BRAIN_TREE, $amount, json_encode($status));
                $this->payment_service->adminBalanceUpdate($amount);
                DB::commit();
                return jsonResponse(TRUE)->message(__('Payment successful.'));
            }
            return jsonResponse(FALSE)->message(__('Payment failed.'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    public function productPurchasePaymentByBrainTree(Request $request)
    {
        try {
            DB::beginTransaction();
            $requestArray = $request->except('payload');
            $amount = $this->auction_price_service->calculateProductPaymentCharge($requestArray);
            if (!$amount) {
                return jsonResponse(FALSE)->message(__('Payment settings error!'));
            }
            $status = $this->makePaymentByBrainTree($request, $amount);
            if ($status->success == TRUE) {
                $auction = Auction::where('id', $requestArray['auction_id'])->first();
                $this->payment_service->addToTransactionHistory($auction->id, PRODUCT_PURCHASE_PAYMENT, CARD_BRAIN_TREE, $amount, json_encode($status));
                $this->payment_service->productPurchaseBalanceUpdate($auction, $amount);
                $this->userPaymentStatusUpdated($auction->id);
                DB::commit();
                return jsonResponse(TRUE)->message(__('Payment successful.'));
            }
            return jsonResponse(FALSE)->message(__('Payment failed.'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return jsonResponse(FALSE)->default();
        }
    }

    private function makePaymentByBrainTree($request, $amount)
    {
        $payload = $request->input('payload', false);
        $nonce = $payload['nonce'];
        $status = Transaction::sale([
            'amount' => (float)$amount,
            'paymentMethodNonce' => $nonce,
            'options' => [
                'submitForSettlement' => True
            ]
        ]);
        return $status;
    }

    private function userPaymentStatusUpdated($auction_id)
    {
        $status_data = [
            'auction_id' => $auction_id,
            'status' => PAYMENT_COMPLETED,
            'message' => __('User payment completed.')
        ];
        $home_deeler_service = new HomeDeelerService();
        $home_deeler_service->auctionStatusUpdated($status_data);
    }
}
