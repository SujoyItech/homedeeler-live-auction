<?php

namespace App\Http\Controllers\Web\Payment;

use App\Http\Controllers\Controller;
use App\Http\Services\Auction\LiveAuctionService;
use App\Http\Services\Payment\AuctionPriceService;
use App\Http\Services\Payment\PaymentService;
use App\Models\Auction\Auction;
use App\Models\Auction\AuctionPayment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Rest\ApiContext;
use Srmklive\PayPal\Services\ExpressCheckout;

class PaypalController extends Controller
{
    private $live_auction_service, $payment_service, $auction_price_service;

    public function __construct(
        LiveAuctionService $live_auction_service,
        PaymentService $payment_service,
        AuctionPriceService $auction_price_service
    ) {
        $this->live_auction_service = $live_auction_service;
        $this->payment_service = $payment_service;
        $this->auction_price_service = $auction_price_service;
    }
    public function liveChargePaymentByPaypal(Request $request)
    {
        $requestArray = $request->all();
        $product = $this->prepareLiveAuctionChargeForPayapl($requestArray);
        $paypalModule = new ExpressCheckout;
        $res = $paypalModule->setExpressCheckout($product, true);
        return redirect($res['paypal_link']);
    }

    public function productPurchasePaymentByPaypal(Request $request)
    {
        $requestArray = $request->all();
        $product = $this->prepareProductPaymentChargeForPayapl($requestArray);
        $paypalModule = new ExpressCheckout;
        $res = $paypalModule->setExpressCheckout($product, true);
        return redirect($res['paypal_link']);
    }

    public function cancelLiveChargePaypalPayment($slug)
    {
        return redirect()->route('auctionDetails', ['slug' => $slug])->with(['dismiss' => __('Your payment has been declined!')]);
    }

    public function successLiveChargePaypalPayment(Request $request)
    {
        try {
            DB::beginTransaction();
            $paypalModule = new ExpressCheckout;
            $response = $paypalModule->getExpressCheckoutDetails($request->token);
            if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
                $auction = $this->getAuctionDetails($response['PAYMENTREQUEST_0_INVNUM']);
                $this->liveChargePaymentUpdate($response);
                DB::commit();
                return redirect()->route('editAuction', ['slug' => $auction->slug])->with(['success' => __('Payment was successful.')]);
            }
            return redirect()->route('auctions')->with(['dismiss' => __('Error occured!')]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->route('auctions')->with(['dismiss' => __('Error occured!')]);
        }
    }

    private function liveChargePaymentUpdate($response)
    {
        $live_charge_payment = [
            'live_auction_charge_paid' => $response['PAYMENTREQUEST_0_AMT'],
            'live_auction_charge_paid_time' => Carbon::now(),
            'is_live_charge_paid' => ACTIVE
        ];
        AuctionPayment::where('auction_id', $response['PAYMENTREQUEST_0_INVNUM'])->update($live_charge_payment);
        Auction::where('id', $response['PAYMENTREQUEST_0_INVNUM'])->update(['has_live' => ACTIVE]);
        $this->payment_service->addToTransactionHistory($response['PAYMENTREQUEST_0_INVNUM'], LIVE_AUCTION_CHARGE_PAYMENT, CARD_PAYPAL, $response['PAYMENTREQUEST_0_AMT'], json_encode($response));
        $this->payment_service->adminBalanceUpdate($response['PAYMENTREQUEST_0_AMT']);
    }

    private function getAuctionDetails($auction_id)
    {
        return Auction::select(
            'auctions.*',
            'products.name',
            'products.slug',
            'products.description',
            'products.additional_info',
            'products.is_new',
            'products.meta_title',
            'products.meta_keywords',
            'products.meta_description'
        )
            ->join('products', 'auctions.product_id', 'products.id')
            ->where('auctions.id', $auction_id)
            ->first();
    }


    private function prepareLiveAuctionChargeForPayapl($requestArray)
    {
        $amount = $this->auction_price_service->calculateLivePaymentCharge($requestArray);
        $auction = $this->getAuctionDetails($requestArray['auction_id']);
        $product = [];
        $product['items'] = [
            [
                'name' => $auction->name,
                'price' => $amount,
                'des' => __('Live auction payment charge'),
                'qty' => 1
            ]
        ];

        $product['invoice_id'] = $auction->id;
        $product['invoice_description'] = __("live_charge");
        $product['return_url'] = route('successLiveChargePaypalPayment');
        $product['cancel_url'] = route('cancelLiveChargePaypalPayment');
        $product['total'] = $amount;

        $live_auction_data = [
            'live_start_time' => $requestArray['live_start_time'],
            'live_duration' => $requestArray['live_duration'],
            'live_extend_minutes' => 0,
            'live_extends_count' => 0
        ];

        $live_payment_data = [
            'auction_id' => $requestArray['auction_id'],
            'live_auction_charge' => $amount,
            'live_auction_charge_paid' => 0,
            'is_live_charge_paid' => 0
        ];

        Auction::where('id', $requestArray['auction_id'])->update($live_auction_data);
        AuctionPayment::create($live_payment_data);

        return $product;
    }

    private function prepareProductPaymentChargeForPayapl($requestArray)
    {
        $amount = $this->auction_price_service->calculateProductPaymentCharge($requestArray['auction_id']);
        $auction = $this->getAuctionDetails($requestArray['auction_id']);
        $product = [];
        $product['items'] = [
            [
                'name' => $auction->name,
                'price' => $amount,
                'des' => __('Product purchase payment charge'),
                'qty' => 1
            ]
        ];

        $product['invoice_id'] = $auction->id;
        $product['invoice_description'] = __("Product purchase payment charge");
        $product['return_url'] = route('successProductPurchasePaypalPayment');
        $product['cancel_url'] = route('cancelProductPurchasePaypalPayment');
        $product['total'] = $amount;

        return $product;
    }

    public function cancelProductPurchasePaypalPayment()
    {
        return redirect()->route('/')->with(['dismiss' => __('Your payment has been declined!')]);
    }

    public function successProductPurchasePaypalPayment(Request $request)
    {
        try {
            DB::beginTransaction();
            $paypalModule = new ExpressCheckout;
            $response = $paypalModule->getExpressCheckoutDetails($request->token);
            if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
                $auction = $this->getAuctionDetails($response['PAYMENTREQUEST_0_INVNUM']);
                $this->p($response);
                DB::commit();
                return redirect()->route('editAuction', ['slug' => $auction->slug])->with(['success' => __('Payment was successful.')]);
            }
            return redirect()->route('auctions')->with(['dismiss' => __('Error occured!')]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->route('auctions')->with(['dismiss' => __('Error occured!')]);
        }
    }
}
