<?php

namespace App\Http\Controllers\Web\FrontEnd;

use App\Http\Controllers\Controller;
use App\Http\Services\FrontEnd\AuctionService;
use App\Models\Faq;
use App\Models\GeneralMessage;
use App\Models\Product\Category;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $auction_service;

    public function __construct(AuctionService $auction_service)
    {
        $this->auction_service = $auction_service;
    }

    public function home()
    {
        $categories = Category::where('parent_id', INACTIVE)->get();
        $data['categories'] = $categories;
        $data['today_auctions'] = $this->auction_service->getAuctionList([], [], 'today_auctions', NULL, NULL, 6);
        $data['featured_items'] = $this->auction_service->getAuctionList([], [], 'featured', NULL, NULL, 4);
        return view('web.home.home', $data);
    }

    public function showCategoryMenuToWeb()
    {
        $categories = Category::where(['parent_id' => 0, 'status' => STATUS_ACTIVE])->orderBy('sort_number', 'asc')->get();
        $data['categoryTrees'] = $categories;
        return view('web.includes.category_tree_generate', $data);
    }

    public function contact()
    {
        return view('web.contact');
    }

    public function sendUserGeneralMessage(Request $request)
    {
        $requestArray = $request->all();
        try {
            GeneralMessage::create($requestArray);
            return redirect()->back()->with(['success' => __('Message sent successfully!')]);
        } catch (\Exception $e) {
            return redirect()->back()->with(['dismiss' => __('Message send failed!')]);
        }
    }

    public function faqs()
    {
        $data['faqs'] = Faq::where('status', ACTIVE)->orderBy('order', 'asc')->get();
        return view('web.faqs', $data);
    }
}
