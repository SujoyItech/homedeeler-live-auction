<?php

namespace App\Http\Controllers\Web\FrontEnd;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auction\BidRequest;
use App\Http\Services\CoinPaymentsAPI;
use App\Http\Services\FrontEnd\AuctionService;
use App\Http\Services\Payment\AuctionPriceService;
use App\Models\Auction\Auction;
use App\Models\Auction\AuctionDetail;
use App\Models\Auction\AuctionStatus;
use App\Models\Message\Messaging;
use App\Models\Product\Category;
use App\Models\Product\ProductCombination;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuctionController extends Controller
{
    private $auction_service;
    public function __construct(AuctionService $auction_service)
    {
        $this->auction_service = $auction_service;
    }

    public function submitBid(BidRequest $request)
    {
        return $this->auction_service->submitBid($request->all());
    }

    public function addAuctionToFavourite($id)
    {
        $response = $this->auction_service->addAuctionToFavourite($id);
        if ($response->getStatus() == TRUE) {
            return redirect()->back()->with(['success' => $response->getMessage()]);
        } else {
            return redirect()->back()->with(['dismiss' => $response->getMessage()]);
        }
    }

    public function auction($slug)
    {
        $auction = $this->auction_service->getAuctionDetails($slug);
        $data['auction'] = $auction;
        $data['product_images'] = ProductCombination::where('product_id', $auction->product_id)->get();
        if (isset(Auth::user()->id)) {
            $data['auction_messaging'] = Messaging::where([
                'event_id' => $auction->id,
                'event_type' => 'auction',
                'sender' => Auth::user()->id,
                'receiver' => $auction->seller_id
            ])->first();
        }
        $data['bid_counts'] = AuctionDetail::where('auction_id', $auction->id)->count();
        return view('web.auction_details.auction_details', $data);
    }

    public function auctionBiddingList(Request $request, $id)
    {
        $results = AuctionDetail::select('auction_details.*', 'users.name', 'users.profile_photo_path')
            ->join('auctions', 'auction_details.auction_id', '=', 'auctions.id')
            ->join('users', 'auction_details.bidder_id', '=', 'users.id')
            ->where('auction_details.auction_id', $id)
            ->groupBy('auction_details.auction_id')
            ->paginate(10);
        $artilces = '';
        if ($request->ajax()) {
            foreach ($results as $result) {
                $name = $result->name;
                $default_image = asset(get_image_path('user') . '/' . 'avatar.png');
                $image = !empty($result->profile_photo_path) ? asset(get_image_path('user')) . '/' . $result->profile_photo_path : asset(get_image_path('user') . '/' . 'avatar.png');
                $date = !empty($result->created_at) ? Carbon::parse($result->created_at)->format('d/m/Y') : '';
                $time = !empty($result->created_at) ? Carbon::parse($result->created_at)->format('g:i A') : '';
                $price = getMoney($result->bid_price ?? 0);
                $artilces .= '<tr>
                                <td data-history="bidder">
                                    <div class="user-info">
                                        <div class="thumb">
                                            <img src="' . $image . '" alt="history" onerror=\'this.src="' . $default_image . '"\'>
                                        </div>
                                        <div class="content">
                                            ' . $name . '
                                        </div>
                                    </div>
                                </td>
                                <td data-history="date">' . $date . '</td>
                                <td data-history="time">' . $time . '</td>
                                <td data-history="unit price">' . $price . '</td>
                            </tr>';
            }
            return $artilces;
        }
    }

    public function auctionList(Request $request)
    {
        if ($request->ajax()) {
            $where = [];
            $type = [];
            $search_array = [];
            if (!empty($request->category_id)) {
                $search_array['category_id'] = $request->category_id;
            }
            if (!empty($request->price_range)) {
                $search_array['price_range'] = $request->price_range;
            }
            if (!empty($request->search_key)) {
                $search_array['search_key'] = $request->search_key;
            }
            if (!empty($request->ending_type)) {
                $search_array['ending_type'] = $request->ending_type;
            }
            $page = isset($_GET['page']) ? $_GET['page'] : NULL;
            $data['auction_lists'] = $this->auction_service->getAuctionList($search_array, $where, $type, $request->order_by, $request->pagination, NULL, $page);
            return view('web.auctions.auction_list_view', $data);
        } else {
            $type = isset($_GET['type']) ? $_GET['type'] : NULL;
            $page = isset($_GET['page']) ? $_GET['page'] : NULL;
            $category_id = NULL;
            if (isset($_GET['category_id'])) {
                $category_id = $_GET['category_id'];
                $search_array['category_id'][] = $category_id;
            }
            if (isset($_GET['search_key'])) {
                $data['search_key'] = $_GET['search_key'];
                $search_array['search_key'] = $_GET['search_key'];
            }
            if (!empty($category_id)) {
                $data['categories'] = Category::where(['parent_id' => $category_id, 'status' => ACTIVE])->get();
                $elements = Category::where('status', STATUS_ACTIVE)->get();
                $parent_category_data = generateParentTree($elements, $category_id);
                $data['parent_breadcrumb']  = $this->generateParentBreadcrumb($parent_category_data[0]);
            } else {
                $data['categories'] = Category::where(['parent_id' => 0, 'status' => ACTIVE])->get();
            }
            $data['type'] = $type;
            $data['page'] = $page;
            $data['category_id'] = $category_id;
            $search_array['type'] = $type;
            $search_array['page'] = $page;
            $where = [];
            $order_by = NULL;
            $paginate = AUCTION_PAGINATE_WEB;
            $data['auction_lists'] = $this->auction_service->getAuctionList($search_array, $where, $type, $order_by, $paginate);
            return view('web.auctions.auction_list', $data);
        }
    }

    private function generateParentBreadcrumb($element, $slug = '')
    {
        if (isset($element->parent[0])) {
            $slug = $this->generateParentBreadcrumb($element->parent[0], $slug) . '<a href="' . route('auctionList', ['category_id' => $element->id]) . '">' . $element->name . '</a> &ensp;';
        } else {
            $slug = '<a href="' . route('auctionList', ['category_id' => $element->id]) . '">' . $element->name . '</a> <i class="fa fa-caret-right"></i>&ensp;';
        }
        return $slug;
    }

    public function getPrimaryCategoryAuctionList()
    {
        $categories = Category::where(['status' => STATUS_ACTIVE, 'parent_id' => INACTIVE])->whereNotIn('slug', ['electronics', 'art', 'real-estate'])->get();
        $product_by_categories = [];
        foreach ($categories as $key => $category) {
            $product_by_categories[$key]['category_id'] = $category->id;
            $product_by_categories[$key]['category_slug'] = $category->slug;
            $product_by_categories[$key]['category_name'] = $category->name;
            $product_by_categories[$key]['category_image'] = $category->icon;
            $product_by_categories[$key]['products'] = $this->auction_service->getAuctionList([], ['product_categories.category_id' => $category->id], NULL, NULL, NULL, 3);
        }
        $data['category_products'] = $product_by_categories;
        return view('web.home.products_by_category', $data);
    }
    public function getFeaturedAuctionList()
    {
        $data['auction_lists'] = $this->auction_service->getAuctionList([], [], 'featured', NULL, NULL, 4);
        return view('web.home.list.featured_auction_list', $data);
    }
    public function getTodayAuctionList()
    {
        $data['auction_lists'] = $this->auction_service->getAuctionList([], [], 'today_auctions', NULL, NULL, 6);
        return view('web.home.list.todays_auction_list', $data);
    }
    public function getElectronicAndArtAuctionList()
    {
        $categories = Category::where(['status' => STATUS_ACTIVE, 'parent_id' => INACTIVE])->whereIn('slug', ['electronics', 'art'])->get();
        $electronics_categories = [];
        $art_categories = [];
        foreach ($categories as $key => $category) {
            if ($category->slug == 'electronics') {
                $electronics_categories['category_id'] = $category->id;
                $electronics_categories['category_slug'] = $category->slug;
                $electronics_categories['category_name'] = $category->name;
                $electronics_categories['category_image'] = $category->icon;
                $electronics_categories['products'] = $this->auction_service->getAuctionList([], ['product_categories.category_id' => $category->id], NULL, NULL, NULL, 8);
            } elseif ($category->slug == 'art') {
                $art_categories['category_id'] = $category->id;
                $art_categories['category_slug'] = $category->slug;
                $art_categories['category_name'] = $category->name;
                $art_categories['category_image'] = $category->icon;
                $art_categories['products'] = $this->auction_service->getAuctionList([], ['product_categories.category_id' => $category->id], NULL, NULL, NULL, 8);
            }
        }
        $data['electronic_categories'] = $electronics_categories;
        $data['art_categories'] = $art_categories;
        return view('web.home.list.electronics_art_auction_list', $data);
    }
    public function getRealEstateAuctionList()
    {
        $category = Category::where(['status' => STATUS_ACTIVE, 'parent_id' => INACTIVE])->where('slug', 'real-estate')->first();

        if (isset($category)) {
            $real_estate['category_id'] = $category->id;
            $real_estate['category_slug'] = $category->slug;
            $real_estate['category_name'] = $category->name;
            $real_estate['category_image'] = $category->icon;
            $real_estate['products'] = $this->auction_service->getAuctionList([], ['product_categories.category_id' => $category->id], NULL, NULL, NULL, 5);
            return view('web.home.list.real_estate_list', $real_estate);
        }
    }

    public function sendDeliveryRequest(Request $request)
    {
        return $this->auction_service->sendDeliveryRequest($request->all());
    }

    public function sendProductReceiveConfirmation(Request $request)
    {
        return $this->auction_service->sendProductReceiveConfirmation($request->all());
    }

    public function viewOwnAuctionPaymentPage(Request $request)
    {
        $payment_serve = new AuctionPriceService();
        $data['auction_id'] = $request->auction_id;
        $total_charge = $payment_serve->calculateProductPaymentCharge($request->auction_id);
        $data['total_charge'] = $total_charge;
        return view('web.payment.own_auction_payment_body', $data);
    }

    public function loadAuctionStatusBody(Request $request)
    {
        $auction_id = $request->id;
        $auction_status = AuctionStatus::where('auction_id', $auction_id)->get();
        $all_status = AUCTION_STATUS;
        $auction_data = [];
        $index = 0;
        foreach ($all_status as $key => $status) {
            if ($key >= PAYMENT_COMPLETED) {
                $auction_data[$index]['status'] = $key;
                $auction_data[$index]['status_title'] = $status;
                foreach ($auction_status as $auction) {
                    if ($auction->status == $key) {
                        $auction_data[$index]['auction'] = $auction;
                    }
                }
                $index++;
            }
        }
        $data['auction_status_list'] = $auction_data;
        $data['auction_details'] =
            Auction::select(
                'products.name',
                'products.slug',
                'auctions.*',
                'auction_payments.auction_winning_price',
                'auction_payments.processing_fee',
                'auction_payments.total_price',
            )
            ->join('products', 'auctions.product_id', '=', 'products.id')
            ->leftJoin('auction_payments', 'auctions.id', '=', 'auction_payments.auction_id')
            ->where('auctions.id', $auction_id)->first();

        return view('web.profile.status_modal_body', $data);
    }
}
