<?php

namespace App\Http\Controllers\Web\FrontEnd;

use App\Http\Controllers\Controller;
use App\Http\Services\FrontEnd\MessageService;
use App\Http\Services\Message\MessagingService;
use App\Http\Services\NotificationService;
use App\Models\Message\Messaging;
use App\Models\Message\MessagingDetails;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class MessageController extends Controller
{
    private $message_service, $admin_message_service;
    public function __construct(MessageService $message_service, MessagingService $admin_message_service)
    {
        $this->message_service = $message_service;
        $this->admin_message_service = $admin_message_service;
    }

    public function getUserAuctionMessageList(Request $request)
    {
        if (!empty($request->id)) {
            $messages = MessagingDetails::select('messaging_details.*', 'users.name', 'users.profile_photo_path')
                ->join('users', 'messaging_details.sender', '=', 'users.id')
                ->where('messaging_details.messaging_id', $request->id)
                ->get()->toArray();
            $details = $this->admin_message_service->prepareMessageDetailsData($messages, $request->auction_id);
            $data['messages'] = $details['message_details'];
            $data['count'] = $details['count'];
            return view('web.auction_details.message.auction_messaging_list', $data);
        }
    }


    public function sendAuctionMessage(Request $request)
    {
        if (!empty($request->id)) {
            $message_details = [
                'messaging_id' => $request->id,
                'sender' => Auth::user()->id,
                'message' => $request->message,
                'created_at' => Carbon::now()
            ];
            return $this->message_service->sendAuctionMessage($request->event_id, $message_details);
        } else {
            $chat_data = [
                'event_type' => 'auction',
                'event_id' => $request->event_id,
                'receiver' => $request->receiver,
                'sender' => Auth::user()->id
            ];
            $message = Messaging::create($chat_data);
            $message_details = [
                'messaging_id' => $message->id,
                'sender' => Auth::user()->id,
                'message' => $request->message,
                'created_at' => Carbon::now()
            ];
            return $this->message_service->sendAuctionMessage($request->event_id, $message_details);
        }
    }


    public function loadLiveAuctionMessage(Request $request)
    {
        $data['live_messages'] = $this->message_service->loadLiveAuctionMessage($request->auction_id);
        return view('web.auction_details.message.live_auction_message', $data);
    }

    public function sendLiveMessageReply(Request $request)
    {
        $auction_id = (int)$request->auction_id;
        $message = [
            'auction_id' => $auction_id,
            'user_id' => (int)$request->user_id,
            'user_name' => $request->user_name,
            'user_image' => $request->user_image,
            'message' => $request->message,
            'created_at' => Carbon::now()->format('d-m-Y H:i:s')
        ];
        return $this->message_service->sendLiveMessageReply($auction_id, $message);
    }
}
