<?php

namespace App\Http\Controllers\Web\FrontEnd;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\Profile\PasswordUpdateRequest;
use App\Http\Services\FrontEnd\ProfileService;
use App\Models\Auction\Auction;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    private $profile_service;

    public function __construct(ProfileService $profile_service)
    {
        $this->profile_service = $profile_service;
    }

    public function dashboard()
    {
        $data = $this->profile_service->dashboardData();
        return view('web.profile.dashboard', $data);
    }

    public function myProfile()
    {
        $data['profile'] = User::where('id', Auth::user()->id)->first();
        return view('web.profile.profile_info', $data);
    }

    public function myWonItems()
    {
        $data['tab'] = 'my-won-items';
        $my_own_bids = $this->profile_service->myWonBids();
        foreach ($my_own_bids as $key => $my_own_bid) {
            $my_own_bids[$key]->service_charge = getPercentageValue($my_own_bid->highest_bid, $my_own_bid->service_charge);
        }
        $data['my_own_bids'] = $my_own_bids;
        return view('web.profile.my_won_items', $data);
    }

    public function myBids()
    {
        $data['tab'] = 'my-bids';
        $data['my_bids'] = $this->profile_service->myBids();
        return view('web.profile.my_bids', $data);
    }

    public function myFavourite()
    {
        $data['tab'] = 'my-favourite';
        $data['my_favourites'] = $this->profile_service->myFavouriteAuctionList();
        return view('web.profile.my_favourite', $data);
    }

    public function myNotification()
    {
        $data['notifications'] = Notification::where('user_id', Auth::user()->id)->get();
        $data['tab'] = 'my-notifications';
        return view('web.profile.my_notifications', $data);
    }

    public function editUserProfile($type)
    {
        $data['profile'] = Auth::user();
        $data['type'] = $type;
        return view('web.profile.edit_profile_info', $data);
    }

    public function userProfilePictureUpdate(Request $request)
    {
        return $this->profile_service->updateProfilePicture($request->all());
    }

    public function userProfileUpdate(Request $request)
    {
        return $this->profile_service->userProfileUpdate($request->all());
    }

    public function userPasswordUpdate(PasswordUpdateRequest $request)
    {
        return $this->profile_service->userPasswordUpdate($request->all());
    }
}
