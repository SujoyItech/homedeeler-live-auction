<?php

namespace App\Http\Controllers\Web\Admin;


use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Auction\Auction;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Auction\AuctionPayment;
use App\Http\Services\DashboardService;


class DashboardController extends Controller
{

    protected $dashboardService;

    public function __construct(DashboardService $dashboardService)
    {
        $this->dashboardService = $dashboardService;
    }

    public function adminHome()
    {
        $data = $this->adminDashboardData();
        return view('admin.home', $data);
    }

    public function permissionDenied()
    {
        return view('admin.errors.403');
    }

    private function adminDashboardData()
    {
        $data['user_count'] = User::select(DB::raw("SUM(CASE WHEN role = 20 THEN 1 ELSE 0 END) as total_seller,
        SUM(CASE WHEN (role = 20 AND admin_verified = 0) THEN 1 ELSE 0 END) as unapproved_seller,
        SUM(CASE WHEN role = 21 THEN 1 ELSE 0 END) as total_bidder,
        SUM(CASE WHEN (role = 21 AND admin_verified = 0) THEN 1 ELSE 0 END) as unapproved_bidder"))->first();

        $data['auction_count'] = Auction::select(DB::raw("SUM(id) as total_product,
        SUM(CASE WHEN status = 11 THEN 1 ELSE 0 END) AS total_product_in_live,
        SUM(CASE WHEN (status >= 1 AND status <= 11) THEN 1 ELSE 0 END) AS total_active_product,
        SUM(CASE WHEN (status >= 20 AND status <= 30) THEN 1 ELSE 0 END) AS total_deliverable_product"))->first();

        $data['balance_count'] = AuctionPayment::select(DB::raw("SUM(total_price) as total_price,
        SUM(seller_paying_amount) AS total_seller_payable"))->first();

        return $data;
    }
}
