<?php

namespace App\Http\Controllers\Web\Admin\Messaging;

use App\Http\Controllers\Controller;
use App\Jobs\SendMailJob;
use App\Models\GeneralMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class GeneralMessagingController extends Controller
{
    public function generalMessages(Request $request)
    {
        if ($request->ajax()) {
            $list = GeneralMessage::all();
            return datatables($list)
                ->editColumn('created_at', function ($item) {
                    return Carbon::parse($item->created_at)->format('Y-m-d H:i:s');
                })->editColumn('status', function ($item) {
                    return $item->status == STATUS_ACTIVE ? '<span class="badge badge-success">' . __('Replied') . '</span>' : '<span class="badge badge-primary">' . __('New') . '</span>';
                })->editColumn('action', function ($item) {
                    $html = '<a href="javascript:void(0)" class="text-primary p-1 reply_message" data-style="zoom-in" data-id="' . $item->id . '"><i class="fa fa-reply"></i></a>';
                    $html .= '<a href="javascript:void(0)" class="text-danger p-1 delete_item" data-style="zoom-in" data-id="' . $item->id . '"><i class="fa fa-trash"></i></a>';
                    return $html;
                })->rawColumns(['status', 'action'])
                ->make(TRUE);
        }
        return view('admin.messaging.general_messaging');
    }

    public function deleteGeneralMessage(Request $request)
    {
        try {
            GeneralMessage::where('id', $request->id)->delete();
            return jsonResponse(TRUE)->message(__('Message deleted successfully.'));
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->default();
        }
    }
    public function replyGeneralMessage(Request $request)
    {
        try {
            $message = GeneralMessage::where('id', $request->id)->first();
            $subject = $request->subject ?? '';
            $email =   $message->email;
            $data['message'] = $request->message ?? '';
            dispatch(new SendMailJob('admin.mail.email.subscriber_mail', $email, $data, $subject));
            GeneralMessage::where('id', $message->id)->update(['status' => ACTIVE]);
            return jsonResponse(TRUE)->message(__('Message replied successfully.'));
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }
}
