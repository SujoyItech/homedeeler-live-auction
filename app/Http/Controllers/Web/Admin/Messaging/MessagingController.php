<?php

namespace App\Http\Controllers\Web\Admin\Messaging;

use App\Http\Controllers\Controller;
use App\Http\Services\FrontEnd\MessageService;
use App\Http\Services\Message\MessagingService;
use App\Models\Message\Messaging;
use App\Models\Message\MessagingDetails;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MessagingController extends Controller
{
    private $messaging_service;
    public function __construct(MessagingService $messaging_service)
    {
        $this->messaging_service = $messaging_service;
    }

    public function messaging()
    {
        $last_messagae = $this->messaging_service->getLastAuctionMessage(Auth::id());
        if (isset($last_messagae)) {
            $data['message'] = $last_messagae;
            $data['message_details'] = $this->messaging_service->getMessageDetails($last_messagae->message_id);
        }
        $data['my_id'] = Auth::user()->id;
        return view('admin.messaging.messaging', $data);
    }

    public function getEventMessages(Request $request)
    {
        if ($request->type == 'auction') {
            return $this->messaging_service->getAuctionMessages(Auth::id());
        }
        if ($request->type == 'admin') {
            return $this->messaging_service->getAdminMessages(Auth::id());
        }
    }

    public function showMessageDetails(Request $request)
    {
        $auction_message = $this->messaging_service->getAuctionMessageById($request->message_id);
        if (isset($auction_message)) {
            $data['message'] = $auction_message;
            $message_details_array = $this->messaging_service->getMessageDetails($request->message_id)->toArray();
            $details = $this->messaging_service->prepareMessageDetailsData($message_details_array, $auction_message->event_id);
            $data['count'] = $details['count'];
            $data['message_details'] = $details['message_details'];
        }
        $data['my_id'] = Auth::user()->id;
        return view('admin.messaging.message_details', $data);
    }

    public function loadAuctionMessagesByScroll(Request $request)
    {
        return $this->messaging_service->getAllMessagesFromStorage($request->auction_id, $request->page);
    }

    public function sendMessage(Request $request)
    {
        return $this->messaging_service->sendMessage($request->all());
    }

    public function loadAdminLiveAuctionMessage(Request $request)
    {
        $message_service = new MessageService();
        $data['live_messages'] = $message_service->loadLiveAuctionMessage($request->auction_id);
        return view('admin.auction.auction_details.live.live_chat_view', $data);
    }

    public function sendAdminLiveAuctionReply(Request $request)
    {
        $auction_id = (int) $request->auction_id;
        $message = [
            'auction_id' => $auction_id,
            'user_id' => (int) $request->user_id,
            'user_name' => $request->user_name,
            'user_image' => $request->user_image,
            'message' => $request->message,
            'type' => 'text',
            'created_at' => Carbon::now()->format('d-m-Y H:i:s')
        ];
        if ($request->hasFile('message_image')) {
            $message_image = uploadImage($request->message_image, get_image_path('auction_message'));
            $message['message_image'] = asset(get_image_path('auction_message') . $message_image);
            $message['type'] = 'image';
        }
        $message_service = new MessageService();
        return $message_service->sendLiveMessageReply($auction_id, $message);
    }
}
