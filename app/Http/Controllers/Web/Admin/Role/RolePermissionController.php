<?php

namespace App\Http\Controllers\Web\Admin\Role;

use App\Http\Controllers\Controller;
use App\Http\Services\RoleManagementServices\RolePermissionService;
use App\Http\Services\RoleManagementServices\RoleService;
use App\Models\Role\Role;
use App\Models\Role\RoleRoute;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RolePermissionController extends Controller
{
    private $role_service;

    public function __construct(RoleService $role_service)
    {
        $this->role_service = $role_service;
    }
    public function index(Request $request)
    {
        $role_list = $this->role_service->getData([], [], ['module_id' => 'asc']);
        if ($request->ajax()) {
            return datatables($role_list)
                ->editColumn('created_at', function ($item) {
                    return Carbon::parse($item->created_at)->format('d-m-Y h:iA');
                })->editColumn('module', function ($item) {
                    foreach (MODULES as $module_id => $module_name) {
                        if ($item->module_id == $module_id) {
                            return __($module_name);
                            break;
                        }
                    }
                })
                ->editColumn('action', function ($item) {
                    $html = '<a href="javascript:void(0)" class="text-info p-1 edit_item" data-id="' . $item->id . '"><i class="fa fa-edit"></i></a>';
                    return $html;
                })
                ->rawColumns(['action', 'module'])
                ->make(TRUE);
        }
        return view('admin.role.roles');
    }

    public function store(Request $request)
    {
        $data['title'] = $request->title;
        $data['module_id'] = $request->module_id;
        $data['actions'] = implode('|', $request->routes);
        if (!empty($request->id)) {
            return $this->role_service->update($request->id, $data);
        } else {
            return $this->role_service->create($data);
        }
    }

    public function edit(Request $request)
    {
        return view('admin.role.create', $this->role_service->getRoleData($request->id));
    }

    public function delete(Request $request)
    {
        return $this->role_service->delete($request->id);
    }
}
