<?php

namespace App\Http\Controllers\Web\Admin\Auction;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\Product\ProductInitRequest;
use App\Http\Services\Auction\AuctionService;
use App\Http\Services\Payment\AuctionPriceService;
use App\Http\Services\Product\ProductService;
use App\Models\Auction\Auction;
use App\Models\Auction\AuctionStatus;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Monolog\Handler\IFTTTHandler;

class AuctionController extends Controller
{
    private $service, $product_service;

    public function __construct(AuctionService $service, ProductService $product_service)
    {
        $this->service = $service;
        $this->product_service = $product_service;
    }
    public function auction(Request $request)
    {
        if ($request->has('search_data')) {
            $list = $this->service->getAuctionList($request->search_data);
        } else {
            $list = $this->service->getAuctionList();
        }
        if ($request->ajax()) {
            return $this->showAuctionTable($list);
        }
        return view('admin.auction.auction_list_admin');
    }

    public function myAuctionList(Request $request)
    {
        if ($request->has('search_data')) {
            $list = $this->service->getMyAuctionList($request->search_data, ['products.seller_id' => Auth::user()->id]);
        } else {
            $list = $this->service->getMyAuctionList([], ['products.seller_id' => Auth::user()->id]);
        }
        if ($request->ajax()) {
            return $this->showAuctionTable($list);
        }
        return view('admin.auction.auction_list_user');
    }

    public function showAuctionTable($product_list)
    {
        return datatables($product_list)
            ->editColumn('icon', function ($item) {
                return '<img src="' . get_product_featured_media($item->product_id)['thumbnail'] . '" onerror=\'this.src="' . adminAsset('images/no-image.png') . '"\' class="img-circle zoom-sm" width="60">';
            })
            ->editColumn('name', function ($item) {
                $html = '<a href="' . route('auctionDetails', ['slug' => $item->slug]) . '">';
                $html .= '<span><strong>' . $item->name . '</strong></span><br>';
                $html .= '<span>(' . $item->slug . ')</span><br>';
                $html .= '</a>';
                return $html;
            })->editColumn('seller_name', function ($item) {
                return $item->seller_name;
            })
            ->addColumn('duration', function ($item) {
                $html = '';
                if ($item->end_date >= Carbon::now()) {
                    if (!empty($item->start_date)) {
                        $html .= '<span><strong>' . __('Start Date: ') . '</strong>' . Carbon::parse($item->start_date)->format('d-m-Y H:i:s') . '</span><br>';
                    }
                    if (!empty($item->end_date)) {
                        $html .= '<span><strong>' . __('End Date: ') . '</strong>' . Carbon::parse($item->end_date)->format('d-m-Y H:i:s') . '</span><br>';
                    }
                } else {
                    $html .= '<span class="badge badge-danger">' . __('Offline ended') . '</span><br>';
                }
                return $html;
            })->addColumn('features', function ($item) {
                $html = '<span>' . __('Is New: ') . '</span>' . checkYesNo($item->product->is_new) . '<br>';
                $html .= '<span>' . __('Is Featured: ') . '</span>' . checkYesNo($item->is_featured) . '<br>';
                return $html;
            })->addColumn('live', function ($item) {
                $html = '';
                if ($item->live_final_end_time >= Carbon::now()) {
                    if ($item->has_live == TRUE) {
                        $html = '<span><strong>' . __('Start Time: ') . '</strong>' . $item->live_start_time . '</span><br>';
                        $html .= '<span><strong>' . __('Duration: ') . '</strong>' . $item->live_duration . ' mins</span><br>';
                    } else {
                        $html = '<span class="badge badge-warning">' . __('Payment Incomplete') . '</span><br>';
                    }
                } else {
                    $html .= '<span class="badge badge-danger">' . __('Live ended') . '</span><br>';
                }
                return $html;
            })
            ->editColumn('status', function ($item) {
                return $this->auctionTableStatus($item);
            })
            ->editColumn('action', function ($item) {
                $html = '<a href="' . route('editAuction', $item->slug) . '" class="btn btn-xs btn-outline-info waves-effect waves-light m-1 edit_item" data-id="' . $item->id . '">' . __('Modify') . '</a>';
                if ($item->status == AUCTION_IN_LIVE) {
                    $html .= '<a href="' . route('auctionLiveStreaming', $item->slug) . '" class="btn btn-xs btn-outline-danger waves-effect waves-light m-1">' . __('Go Live!') . '</a>';
                }
                if (Auth::user()->module_id === MODULE_SUPER_ADMIN || Auth::user()->module_id === MODULE_USER_ADMIN) {
                    if ($item->is_approved == INACTIVE) {
                        $html .= '<a href="javascript:void(0);" class="btn btn-xs btn-outline-success waves-effect waves-light m-1 approve_auction" data-id="' . $item->id . '">' . __('Approve') . '</a>';
                    }
                    if ($item->status >= DELIVERY_COMPLETED) {
                        if ($item->is_seller_paid == ACTIVE) {
                            $html .= '<span class="badge badge-warning p-1 m-1">' . __('Seller Paid') . '</span>';
                        } else {
                            $html .= '<a href="javascript:void(0);" class="btn btn-xs btn-outline-warning waves-effect waves-light m-1 pay_to_seller" data-id="' . $item->id . '">' . __('Pay To Seller') . '</a>';
                        }
                    }
                } else {
                    if ($item->is_approved == INACTIVE) {
                        $html .= '<span class="badge badge-danger p-1 m-1">' . __('Approval Pending') . '</span>';
                    }
                }
                if ($item->is_approved == ACTIVE) {
                    $html .= '<span class="badge badge-success p-1 m-1">' . __('Approved') . '</span>';
                }
                return $html;
            })
            ->rawColumns(['name', 'duration', 'icon', 'live', 'features', 'status', 'action'])
            ->make(TRUE);
    }

    private function auctionTableStatus($item)
    {
        $statusText = '';
        $statusClass = '';
        if ($item->status == ACTIVE) {
            $statusText = __('Active');
            $statusClass = 'success';
        } else if ($item->status == AUCTION_IN_LIVE) {
            $statusText = __('In Live');
            $statusClass = 'danger';
        } else if ($item->status == AUCTION_LIVE_END) {
            $statusText = __('Live End');
            $statusClass = 'warning';
        } else if ($item->status == AUCTION_END) {
            $statusText = __('Auction End');
            $statusClass = 'warning';
        } else if ($item->status == PAYMENT_COMPLETED) {
            $statusText = __('Payment completed');
            $statusClass = 'success';
        } else if ($item->status == DELIVERY_REQUESTED) {
            $statusText = __('Delivery requested');
            $statusClass = 'primary';
        } else if ($item->status == DELIVERY_IN_SHIPMENT) {
            $statusText = __('In Shipment');
            $statusClass = 'warning';
        } else if ($item->status == DELIVERY_COMPLETED) {
            $statusText = __('Delivery completed');
            $statusClass = 'success';
        } else if ($item->status == PRODUCT_RECEIVED) {
            $statusText = __('Product Received');
            $statusClass = 'success';
        } else {
            $statusText = __('Inactive');
            $statusClass = 'primary';
        }
        $html = '<div class="btn-group mb-2">
                    <button type="button" class="btn btn-xs btn-' . $statusClass . ' dropdown-toggle dropdown-toggle-split"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        ' . $statusText . '&nbsp<i class="fas fa-caret-down"></i>
                    </button>
                    <div class="dropdown-menu" style="">
                        <a class="dropdown-item status_change" data-has-live ="' . $item->has_live . '" data-type="' . INACTIVE . '" data-id="' . $item->id . '" href="javascript:">' . __('Inactive') . '</a>
                        <a class="dropdown-item status_change" data-has-live ="' . $item->has_live . '" data-type="' . ACTIVE . '" data-id="' . $item->id . '" href="javascript:">' . __('Active') . '</a>';
        if (Auth::user()->module_id == MODULE_USER_ADMIN || Auth::user()->module_id == MODULE_SUPER_ADMIN || (Auth::user()->module_id == MODULE_USER && $item->has_live === STATUS_ACTIVE)) {
            $html .= '<a class="dropdown-item status_change" data-has-live ="' . $item->has_live . '" data-type="' . AUCTION_IN_LIVE . '" data-id="' . $item->id . '" href="javascript:">' . __('In Live') . '</a>
                     <a class="dropdown-item status_change" data-has-live ="' . $item->has_live . '" data-type="' . AUCTION_LIVE_END . '" data-id="' . $item->id . '" href="javascript:">' . __('Live End') . '</a>';
        }
        $html .= '<a class="dropdown-item status_change" data-has-live ="' . $item->has_live . '" data-type="' . AUCTION_END . '" data-id="' . $item->id . '" href="javascript:">' . __('Auction End') . '</a>
                        <a class="dropdown-item status_change" data-has-live ="' . $item->has_live . '" data-type="' . PAYMENT_COMPLETED . '" data-id="' . $item->id . '" href="javascript:">' . __('Payment Completed') . '</a>
                        <a class="dropdown-item status_change" data-has-live ="' . $item->has_live . '" data-type="' . DELIVERY_REQUESTED . '" data-id="' . $item->id . '" href="javascript:">' . __('Delivery Requested') . '</a>
                        <a class="dropdown-item status_change" data-has-live ="' . $item->has_live . '" data-type="' . DELIVERY_IN_SHIPMENT . '" data-id="' . $item->id . '" href="javascript:">' . __('In Shipment') . '</a>
                        <a class="dropdown-item status_change" data-has-live ="' . $item->has_live . '" data-type="' . DELIVERY_COMPLETED . '" data-id="' . $item->id . '" href="javascript:">' . __('Delivery Completed') . '</a>
                        <a class="dropdown-item status_change" data-has-live ="' . $item->has_live . '" data-type="' . PRODUCT_RECEIVED . '" data-id="' . $item->id . '" href="javascript:">' . __('Product Received') . '</a>
                    </div>
                </div>';

        return $html;
    }

    public function auctionDetails(Request $request, $slug)
    {
        $auction_details = $this->service->getAuctionDetailsData($slug);
        $data['auction_details'] = $auction_details;
        $data['auction_status_histories'] = AuctionStatus::where('auction_id', $data['auction_details']->id)->orderBy('status', 'asc')->get();
        if ($request->ajax()) {
            $auction_bidding_details = $this->service->getAuctionBiddingDetailsData($slug);
            return $this->showBiddingTable($auction_bidding_details);
        }
        if ($auction_details->status == AUCTION_IN_LIVE) {
            $data['product_images'] = $this->product_service->getProductImages($data['auction_details']->product_id, TRUE);
            return view('admin.auction.auction_details.auction_details_live', $data);
        } else {
            if ($auction_details->status >= AUCTION_END && !empty($auction_details->winner_id)) {
                $data['winner'] = User::select('name', 'email', 'phone', 'country', 'city', 'address')->where('id', $auction_details->winner_id)->first();
            }
            $data['product_images'] = $this->product_service->getProductImages($data['auction_details']->product_id);
            return view('admin.auction.auction_details.auction_details', $data);
        }
    }

    private function showBiddingTable($bidding_list)
    {
        return datatables($bidding_list)
            ->editColumn('icon', function ($item) {
                return '<img src="' . asset(get_image_path() . '/' . $item->bidder_image) . '" onerror=\'this.src="' . adminAsset('images/users/avatar.png') . '"\' class="img-circle zoom-sm" width="60">';
            })->editColumn('bid_price', function ($item) {
                return getMoney($item->bid_price);
            })->editColumn('created_at', function ($item) {
                return Carbon::parse($item->created_at)->format('d-m-Y h:i:s');
            })->rawColumns(['icon'])
            ->make(TRUE);
    }

    public function initAuction(ProductInitRequest $request)
    {
        return $this->service->initAuction($request->all());
    }

    public function saveAuction(Request $request)
    {
        return $this->service->update($request->id, $request->except('id'));
    }

    public function edit($slug)
    {
        $data = $this->service->getAuctionAllInformation($slug);
        return view('admin.auction.create_auction', $data);
    }

    public function saveAuctionPricingInfo(Request $request)
    {
        return $this->service->saveAuctionPricingInfo($request->id, $request->except('id'));
    }

    public function auctionApprove(Request $request)
    {
        return $this->service->auctionApprove($request->all());
    }

    public function viewLiveAuctionPaymentPage(Request $request)
    {
        $auction = Auction::select('auctions.*', 'products.slug')->join('products', 'auctions.product_id', 'products.id')->where('auctions.id', $request->auction_id)->first();
        $price_service = new AuctionPriceService();
        $requestArray['live_start_time'] = $request->live_start_time;
        $requestArray['live_duration'] = $request->live_duration;
        $data['auction_id'] = $request->auction_id;
        $data['live_start_time'] = $request->live_start_time;
        $data['live_duration'] = $request->live_duration;
        $live_charge = $price_service->calculateLivePaymentCharge($requestArray);
        $data['live_charge'] = $live_charge;
        $data['auction'] = $auction;
        return view('admin.auction.add_product.price.live_charge_payment_body', $data);
    }

    public function liveChargePaymentByBtc(Request $request)
    {
        dd($request->all());
    }

    public function auctionPaymentMake(Request $request)
    {
        return $this->service->makeAuctionPayment($request->all());
    }

    public function auctionStatusChange(Request $request)
    {
        return $this->service->auctionStatusChange($request->all());
    }

    public function updateAuctionStatusHistory(Request $request)
    {
        return $this->service->updateAuctionStatusHistory($request->all());
    }

    public function makeAuctionLive(Request $request)
    {
        return $this->service->makeAuctionLive($request->all());
    }

    public function auctionList()
    {
        return view('admin.auction.auction_list');
    }
    public function bidderList()
    {
        return view('admin.auction.bidder_list');
    }
    public function currentAuctionList(Request $request)
    {
        if ($request->ajax()) {
            $serach_data = $request->has('search_data') ? $request->search_data : [];
            $conditions = ['auctions.status' => ACTIVE];
            if (Auth::user()->module_id == MODULE_USER) {
                $conditions['products.seller_id'] = Auth::user()->id;
            }
            $list = $this->service->getAuctionList($serach_data, $conditions);
            return $this->showAuctionTable($list);
        }
        return view('admin.auction.current_auction_list');
    }
    public function liveAuctionList(Request $request)
    {
        if ($request->ajax()) {
            $serach_data = $request->has('search_data') ? $request->search_data : [];
            $conditions = ['auctions.status' => AUCTION_IN_LIVE];
            if (Auth::user()->module_id == MODULE_USER) {
                $conditions['products.seller_id'] = Auth::user()->id;
            }
            $list = $this->service->getAuctionList($serach_data, $conditions);
            return $this->showAuctionTable($list);
        }
        return view('admin.auction.live_auction_list');
    }
    public function completeAuctionList(Request $request)
    {
        if ($request->ajax()) {
            $serach_data = $request->has('search_data') ? $request->search_data : [];
            $conditions = ['auctions.status' => DELIVERY_COMPLETED];
            if (Auth::user()->module_id == MODULE_USER) {
                $conditions['products.seller_id'] = Auth::user()->id;
            }
            $list = $this->service->getAuctionList($serach_data, $conditions);
            return $this->showAuctionTable($list);
        }
        return view('admin.auction.complete_auction_list');
    }


    public function createAuction()
    {
        return view('admin.auction.create_auction');
    }

    public function loadSellerPaymentModalBody(Request $request)
    {
        $data['auction_details'] = Auction::select(
            'auctions.*',
            'auction_payments.auction_winning_price',
            'auction_payments.commission_price',
            'auction_payments.processing_fee',
            'auction_payments.total_price',
            'auction_payments.recieved_amount_by_winner',
            'auction_payments.seller_paying_amount'
        )
            ->leftJoin('auction_payments', 'auctions.id', 'auction_payments.auction_id')
            ->where('auctions.id', $request->auction_id)
            ->first();
        return view('admin.auction.seller_payment_modal_body', $data);
    }

    public function payAuctionPriceToSeller(Request $request)
    {
        return $this->service->payAuctionPriceToSeller($request->all());
    }
}
