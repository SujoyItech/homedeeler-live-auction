<?php

namespace App\Http\Controllers\Web\Admin\Auction;

use App\Http\Controllers\Controller;
use App\Http\Services\Auction\AuctionService;
use App\Http\Services\Auction\LiveAuctionService;
use App\Models\Auction\Auction;
use App\Models\Auction\AuctionPayment;
use App\Models\Auction\AuctionPaymentTransaction;
use App\Models\User;
use Braintree\Transaction;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Srmklive\PayPal\Services\ExpressCheckout;
use Stripe\Charge;
use Stripe\Stripe;


class PaymentController extends Controller
{
    private $live_auction_service;

    public function __construct(LiveAuctionService $live_auction_service){
        $this->live_auction_service = $live_auction_service;
    }
    public function makePaymentByStripe(Request $request){
        try {
            DB::beginTransaction();
            $requestArray = $request->all();
            $amount = $this->calculatePrice($requestArray);
            if (!$amount){
                return redirect()->back()->with(['dismiss'=>__('Payment settings error!')]);
            }
            $settings = __options(['payment_settings']);
            Stripe::setApiKey($settings->stripe_secret);
            $stripe_response = Charge::create ([
                "amount" => 100 * floatval($amount),
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Payment for live auction."
            ]);

            if ($stripe_response->status == 'succeeded') {
                $paid_amount = !empty($stripe_response->amount) ? $stripe_response->amount/100 : 0 ;
                $this->live_auction_service->auctionLiveDataUpdate($requestArray,$amount,$paid_amount);
                $this->addToTransactionHistory($requestArray['auction_id'],LIVE_AUCTION_CHARGE_PAYMENT,CARD_STRIPE,$paid_amount,json_encode($stripe_response));
                $this->adminBalanceUpdate($paid_amount);
                DB::commit();
                return redirect()->back()->with(['success'=>__('Payment successful.')]);
            }
            return redirect()->back()->with(['dismiss'=>__('Payment failed!')]);

        }catch (\Exception $exception){
            DB::rollBack();
            return redirect()->back()->with(['dismiss'=>__('Something went wrong!')]);
        }
    }

    public function makePaymentByBrainTree(Request $request){

        try {
//            DB::beginTransaction();
            $requestArray = $request->except('payload');
            $payload = $request->input('payload', false);
            $nonce = $payload['nonce'];
            $amount = $this->calculatePrice($requestArray);
            if (!$amount){
                return jsonResponse(FALSE)->message(__('Payment settings error!'));
            }
            $status = Transaction::sale([
                'amount' => floatval($amount),
                'paymentMethodNonce' => $nonce,
                'options' => [
                    'submitForSettlement' => True
                ]
            ]);

            if ($status->success == TRUE) {
                $this->live_auction_service->auctionLiveDataUpdate($requestArray,$amount,$amount);
                $this->addToTransactionHistory($requestArray['auction_id'],LIVE_AUCTION_CHARGE_PAYMENT,CARD_BRAIN_TREE,$amount,json_encode($status));
                $this->adminBalanceUpdate($amount);
//                DB::commit();
                return jsonResponse(TRUE)->message(__('Payment successful.'));
            }
            return jsonResponse(FALSE)->message(__('Payment failed.'));
        }catch (\Exception $exception){
//            DB::rollBack();
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }







    private function addToTransactionHistory($auction_id,$purpose,$payment_method,$amount,$ref,$status=PAYMENT_DONE){
        try {
            $insert = [
                'auction_id'=>$auction_id,
                'user_id'=>Auth::user()->id,
                'purpose'=>$purpose,
                'payment_method'=>$payment_method,
                'amount'=>$amount,
                'created_at'=>Carbon::now(),
                'created_by'=>Auth::user()->id,
                'transaction_reference'=>$ref,
                'payment_status'=>$status,
            ];
            AuctionPaymentTransaction::create($insert);
        }catch (\Exception $exception){

        }
    }

    private function adminBalanceUpdate($amount){
       $admin = User::where(['module_id'=>USER_ADMIN,'status'=>ACTIVE])->first();
       $new_balance = $admin->balance + $amount;
       User::where('id',$admin->id)->update(['balance'=>$new_balance]);
    }


}

