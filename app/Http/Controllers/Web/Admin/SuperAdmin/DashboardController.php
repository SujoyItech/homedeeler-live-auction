<?php

namespace App\Http\Controllers\Web\Admin\SuperAdmin;


use App\Http\Controllers\Controller;
use App\Http\Services\DashboardService;
use App\Models\Auction\Auction;
use App\Models\Auction\AuctionPayment;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    public function index()
    {


        return view('admin.super_admin.dashboard.home', $data);
    }
}
