<?php

namespace App\Http\Controllers\Web\Admin\Seller;

use Illuminate\Http\Request;
use App\Models\Auction\Auction;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Auction\AuctionPayment;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {

        $data['auction_count'] = Auction::join('products', 'auctions.product_id', 'products.id')
            ->join('auction_payments', 'auctions.id', 'auction_payments.auction_id')
            ->select(DB::raw("SUM(auctions.id) as total_product,
        SUM(CASE WHEN auctions.status = 11 THEN 1 ELSE 0 END) AS total_product_in_live,
        SUM(CASE WHEN (auctions.status >= 1 AND auctions.status <= 11) THEN 1 ELSE 0 END) AS total_active_product,
        SUM(CASE WHEN (auctions.status >= 20 AND auctions.status <= 30) THEN 1 ELSE 0 END) AS total_deliverable_product,
        SUM(auction_payments.seller_paying_amount) AS total_pending_amount"))
            ->where('products.seller_id', Auth::user()->id)
            ->first();

        return view('admin.seller.dashboard.home');
    }
}
