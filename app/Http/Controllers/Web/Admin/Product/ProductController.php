<?php

namespace App\Http\Controllers\Web\Admin\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\Product\ProductInitRequest;
use App\Http\Requests\Web\Admin\Product\ProductMediaRequest;
use App\Http\Requests\Web\Admin\Product\ProductSaveRequest;
use App\Http\Services\Product\BrandService;
use App\Http\Services\Product\ProductService;
use App\Models\Product\ProductBrand;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $productService, $brand_service;

    public function __construct(ProductService $service, BrandService $brand_service)
    {
        $this->productService = $service;
        $this->brand_service = $brand_service;
    }
    public function index(Request $request)
    {
        if ($request->has('search_data')) {
            $product_list = $this->productService->getProductData($request->search_data);
        } else {
            $product_list = $this->productService->getProductData();
        }
        if ($request->ajax()) {
            return $this->showProductTable($product_list);
        }
        return view('admin.products.products.product_list');
    }
    public function showProductTable($product_list)
    {
        return datatables($product_list)
            ->editColumn('icon', function ($item) {
                return '<img src="' . get_product_featured_media($item->id)['thumbnail'] . '" onerror=\'this.src="' . adminAsset('images/no-image.png') . '"\' class="img-circle zoom-sm" width="60">';
            })
            ->editColumn('description', function ($item) {
                return substr($item->description, 0, 200);
            })
            ->editColumn('tag_names', function ($item) {
                $tags = [];
                $local = app()->getLocale();
                $all_tags = json_decode('[' . $item->tag_names . ']');
                foreach ($all_tags as $all_tag) {
                    $tags[] = '<span class="badge badge-secondary text-lg">' . $all_tag->$local . '</span>';
                }
                return implode(' ', $tags);
            })
            ->editColumn('colors', function ($item) {
                if (isset($item->colors)) {
                    $colorNameString = '';
                    $colorCodes = [];
                    $colorData = explode('***', $item->colors);
                    foreach ($colorData as $key => $data) {
                        $arr = explode('--', $data);
                        if ($key > 0) {
                            $colorNameString .= ',';
                        }
                        $colorNameString .= $arr[2];
                        $colorCodes[] = $arr[1];
                    }

                    //$local = app()->getLocale();
                    $colors = json_decode('[' . $colorNameString . ']');
                    $colorBlocks = [];
                    foreach ($colors as $key => $color) {
                        // $color_name =  . $color->$local;
                        if ($colorCodes[$key] != 'NO_COLOR') {
                            // '<span class="badge badge-secondary text-lg">'.$color->$local.'</span>';
                            $colorBlocks[] = '<span class="badge mr-1" style="background-color:' . $colorCodes[$key] . '">&nbsp;&nbsp;</span>';
                        }
                        /*else {
                            $colorBlocks[] = $color_name;
                        }*/
                    }
                    return implode(' ', $colorBlocks);
                } else {
                    return 'N/A';
                }
            })
            ->editColumn('status', function ($item) {
                $statusText = '';
                $statusClass = '';
                if ($item->status == ACTIVE) {
                    $statusText = __('Active');
                    $statusClass = 'success';
                } else if ($item->status == INACTIVE) {
                    $statusText = __('Inactive');
                    $statusClass = 'danger';
                } else if ($item->status == PRODUCT_DRAFT) {
                    $statusText = __('In draft');
                    $statusClass = 'secondary';
                }
                return '<div class="btn-group mb-2">
                                <button type="button" class="btn btn-xs btn-' . $statusClass . ' dropdown-toggle dropdown-toggle-split"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ' . $statusText . '&nbsp<i class="fas fa-caret-down"></i>
                                </button>
                                <div class="dropdown-menu" style="">
                                    <a class="dropdown-item status_change" data-type="' . ACTIVE . '" data-id="' . $item->id . '" href="javascript:">' . __('Active') . '</a>
                                    <a class="dropdown-item status_change" data-type="' . INACTIVE . '" data-id="' . $item->id . '" href="javascript:">' . __('Inactive') . '</a>
                                </div>
                            </div>';
            })
            ->editColumn('action', function ($item) {
                $html = '<a href="' . route('editProduct', $item->slug) . '" class="btn btn-xs btn-outline-info waves-effect waves-light m-1 edit_item" data-id="' . $item->id . '">' . __('Add/Edit') . '</a>';
                return $html;
            })

            ->rawColumns(['tag_names', 'icon', 'colors', 'status', 'action'])

            ->make(TRUE);
    }

    public function initProduct(ProductInitRequest $request): \Illuminate\Http\JsonResponse
    {
        return $this->productService->initProduct($request);
    }

    public function saveProduct(ProductSaveRequest $request)
    {
        return $this->productService->saveProduct($request);
    }

    public function productSlugCheck(Request $request)
    {
        return $this->productService->checkSlug($request);
    }

    public function productReferenceCheck(Request $request)
    {
        return $this->productService->checkReference($request);
    }

    public function edit($slug, Request $request)
    {
        $data = $this->productService->getProductEditPageData($request);
        $data['brands'] = $this->brand_service->getData(['status' => ACTIVE]);
        $product = $this->productService->firstWhere(['slug' => $slug]);
        $data['product_brands'] = ProductBrand::where('product_id', $product->id)->pluck('brand_id')->toArray();
        return view('admin.products.products.add_product', $data);
    }

    public function saveProductCategoriesAndTags(Request $request)
    {
        return $this->productService->saveProductCategoriesAndTags($request);
    }

    public function productStatusChange(Request $request)
    {
        return $this->productService->statusChange($request->all());
    }

    public function getProductSupplier(Request $request)
    {
        $product = $this->productService->firstWhere(['id' => $request->id]);
        if ($product && $product->supplier) {
            return ['success' => true, 'supplier' => $product->supplier];
        } else {
            return ['success' => false];
        }
    }

    public function saveProductCombinations(Request $request)
    {
        return $this->productService->saveProductCombinations($request);
    }

    public function saveProductMedia(ProductMediaRequest $request)
    {
        return $this->productService->saveProductMedia($request);
    }

    public function saveProductFeaturedMedia(Request $request)
    {
        return $this->productService->saveProductFeaturedMedia($request);
    }

    public function saveProductPricingInfo(Request $request)
    {
        return $this->productService->saveProductPricingInfo($request->product_id, $request->except('product_id'));
    }
}
