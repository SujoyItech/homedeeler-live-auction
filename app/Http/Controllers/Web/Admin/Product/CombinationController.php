<?php

namespace App\Http\Controllers\Web\Admin\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\Product\CombinationRequest;
use App\Http\Services\Product\CombinationService;
use App\Http\Services\Product\CombinationTypeService;
use Illuminate\Http\Request;

class CombinationController extends Controller
{
    private $combinationService,$combinationTypeService;

    public function __construct(CombinationService $service,CombinationTypeService $combination_type_service){
        $this->combinationService= $service;
        $this->combinationTypeService= $combination_type_service;
    }

    public function index(Request $request){
        $combination_lists = $this->combinationService->getCombinationQuery();
        $data['combination_types'] =$this->combinationTypeService->getData();
        if ($request->ajax()){
            return datatables($combination_lists)
                ->editColumn('combination_type',function ($item){
                     return $item->combination_type;
                })->editColumn('name',function ($item){
                   return $item->name;
                })->editColumn('color_code',function ($item){
                    if ($item->combination_type_id == 1) {
                        return '<span style="display: block; width: 30px; height: 20px; background-color:'.$item->color_code.'"></span>';
                    }else{
                        return '';
                    }
                })->editColumn('action',function ($item){
                    $html = '<a href="javascript:void(0);" class="text-info p-1 edit_item" data-id="'.$item->id.'"><i class="fa fa-edit"></i></a>';
                    $html .='<a href="javascript:void(0);" class="text-danger p-1 delete_item" data-id="'.$item->id.'"><i class="fa fa-trash"></i></a>';
                    return $html;
                })->rawColumns(['status','action', 'color_code'])
                ->make(TRUE);
        }
        return view('admin.products.combination.combination',$data);
    }

    public function edit(Request $request){
        $data = $this->combinationService->getCombinationData($request->id);
        $data['combination_types'] =$this->combinationTypeService->getData();
        return view('admin.products.combination.combination_add',$data);
    }

    public function store(CombinationRequest $request){
        if(!empty($request->id)){
            return $this->combinationService->update($request->id,$request->except('id'));
        }else{
            return $this->combinationService->create($request->except('id'));
        }
    }

    public function delete(Request $request){
        return $this->combinationService->delete($request->id);
    }
}
