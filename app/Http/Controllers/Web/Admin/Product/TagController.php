<?php

namespace App\Http\Controllers\Web\Admin\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\Product\TagRequest;
use App\Http\Services\Product\TagService;
use Illuminate\Http\Request;

class TagController extends Controller
{
    private $tagService;

    public function __construct(TagService $service){
        $this->tagService = $service;
    }

    public function index(Request $request){

        $tag_list = $this->tagService->getData([],[],['created_at'=>'desc']);
        if ($request->ajax()){
            return datatables($tag_list)
               ->editColumn('name',function ($item){
                  return $item->name;
                })->editColumn('status',function ($item){
                    return $item->status == STATUS_ACTIVE ? '<span class="badge badge-success">'.__('Active').'</span>':'<span class="badge badge-warning">'.__('Inactive').'</span>';
                }) ->editColumn('action',function ($item){
                    $html = '<a href="javascript:void(0)" class="text-info p-1 edit_item" data-id="'.$item->id.'"><i class="fa fa-edit"></i></a>';
                    $html .='<a href="javascript:void(0)" class="text-danger p-1 delete_item" data-id="'.$item->id.'"><i class="fa fa-trash"></i></a>';
                    return $html;
                })->rawColumns(['name','status','action'])
                ->make(TRUE);
        }

        return view('admin.products.tags.tags');
    }

    public function store(TagRequest $request){
        if(!empty($request->id)){
            return $this->tagService->update($request->id,$request->except('id'));
        }else{
            return $this->tagService->create($request->except('id'));
        }
    }

    public function edit(Request $request){
        return view('admin.products.tags.tags_add',$this->tagService->getTagData($request->id));
    }

    public function delete(Request $request){
        return $this->tagService->delete($request->id);
    }
}
