<?php

namespace App\Http\Controllers\Web\Admin\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\Product\CombinationTypeRequest;
use App\Http\Services\Product\CombinationTypeService;
use Illuminate\Http\Request;

class CombinationTypeController extends Controller
{
    private $combinationTypeService;

    public function __construct(CombinationTypeService $service){
        $this->combinationTypeService = $service;
    }

    public function index(Request $request){
        $combination_type_lists = $this->combinationTypeService->getData([],[],['created_at'=>'desc']);
        if ($request->ajax()){
            return datatables($combination_type_lists)
            ->editColumn('name',function ($item){
                return $item->name;
            })->editColumn('action',function ($item){
                $html = '';
                if($item->id > SIZE_ID) {
                    $html = '<a href="javascript:void(0);" class="text-info p-1 edit_item" data-id="' . $item->id . '"><i class="fa fa-edit"></i></a>';
                    $html .= '<a href="javascript:void(0);" class="text-danger p-1 delete_item" data-id="' . $item->id . '"><i class="fa fa-trash"></i></a>';
                }
                    return $html;
                })->rawColumns(['name','action'])
                ->make(TRUE);
        }
        return view('admin.products.combination_type.combination_type');
    }

    public function edit(Request $request){
        return view('admin.products.combination_type.combination_type_add',$this->combinationTypeService->getCombinationTypeData($request->id));
    }

    public function store(CombinationTypeRequest $request){
        if(!empty($request->id)){
            return $this->combinationTypeService->update($request->id,$request->except('id'));
        }else{
            return $this->combinationTypeService->create($request->except('id'));
        }

    }

    public function delete(Request $request){
        return $this->combinationTypeService->delete($request->id);
    }
}
