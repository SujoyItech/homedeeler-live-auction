<?php

namespace App\Http\Controllers\Web\Admin\Product;


use App\Http\Controllers\Controller;
use App\Http\Services\DashboardService;
use App\Http\Services\Product\ProductService;
use App\Http\Services\Production\ProductionService;
use Illuminate\Http\Request;


class DashboardController extends Controller {

    public function index(Request $request){
        return view('admin.products.dashboard.home');
    }

}
