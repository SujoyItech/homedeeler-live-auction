<?php

namespace App\Http\Controllers\Web\Pdf;


use App\Http\Services\BaseService;
use Barryvdh\DomPDF\Facade as PDF;

class PdfService
{
    // Your methods for repository

    public function create_pdf($template,$data=[]){
        return PDF::loadView('pdf.'.$template, $data);
    }
}
