<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SellerCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()->module_id === MODULE_SUPER_ADMIN || Auth::user()->module_id === MODULE_USER_ADMIN) {
            return $next($request);
        } else {
            if (Auth::user()->role === USER_SELLER && Auth::user()->is_seller === ACTIVE && Auth::user()->admin_verified === ACTIVE) {
                return $next($request);
            } else {
                return redirect()->route('profile')->with(['message' => __('You are not verified as seller. Please update your profile')]);
            }
        }
    }
}
