<?php

namespace App\Http\Requests\Api\Auction;

use App\Models\Auction\Auction;
use App\Models\Auction\AuctionDetail;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class BidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        $auction = Auction::where('id', $this->auction_id)->first();
        $bid_increment = $auction->bid_increment ?? 1;
        $highest_bid = !empty($auction->highest_bid) ? $auction->highest_bid : $auction->base_price;
        $highest_bid_price = $highest_bid + $bid_increment;
        return [
            'auction_id' => 'required',
            'bidder_id'  => 'required',
            'bid_price'  => 'required|numeric|min:' . $highest_bid_price,
        ];
    }

    public function messages()
    {
        return [
            'auction_id.required' => __('Auction field is required.'),
            'bidder_id.required'  => __('Bidder field is required.'),
            'bid_price.required'  => __('Bid price field is required.'),
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->header('accept') == "application/json") {
            $errors = [];
            if ($validator->fails()) {
                $e = $validator->errors()->all();
                foreach ($e as $error) {
                    $errors[] = $error;
                }
            }
            $json = [
                'success' => FALSE,
                'data'    => [],
                'message' => $errors[0],
            ];
            $response = new JsonResponse($json, 200);

            throw (new ValidationException($validator, $response))->errorBag($this->errorBag)
                ->redirectTo($this->getRedirectUrl());
        } else {
            throw (new ValidationException($validator))
                ->errorBag($this->errorBag)
                ->redirectTo($this->getRedirectUrl());
        }
    }
}
