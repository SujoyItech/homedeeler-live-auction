<?php

namespace App\Http\Requests\Web\Admin\Product;

use App\Http\Requests\BaseValidation;
use Illuminate\Foundation\Http\FormRequest;

class ProductMediaRequest extends BaseValidation
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        if ($this->has('media_url')) {
            $rules['media_url'] = 'required|max:255';
        }
        $rules['media_type'] = 'required|in:' . implode(',', array_keys(get_product_media_type()));
        return $rules;
    }
}
