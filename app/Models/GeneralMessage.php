<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GeneralMessage extends Model
{
    protected $fillable = ['name', 'email', 'message', 'replied_message', 'replied_by', 'status'];
}
