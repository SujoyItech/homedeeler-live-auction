<?php

namespace App\Models\Auction;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuctionPaymentTransaction extends Model
{
    protected $table = 'auction_payment_transactions';
    public $timestamps = FALSE;

    protected $fillable = array('auction_id', 'user_id', 'purpose','payment_method',
                                'start_date', 'end_date', 'winnig_position',
                                'amount', 'created_at', 'created_by',
                                'transaction_reference', 'payment_status');
}
