<?php

namespace App\Models\Auction;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuctionStatus extends Model
{
    public $timestamps = TRUE;
    protected $fillable = ['auction_id','status','message','extra_message','image','file'];
}
