<?php

namespace App\Models\Auction;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuctionPayment extends Model
{
    protected $table = 'auction_payments';
    public $timestamps = FALSE;

    protected $fillable = array('auction_id', 'offline_auction_charge', 'offline_auction_charge_paid','offline_auction_charge_paid_time',
                                'live_auction_charge', 'live_auction_charge_paid_time', 'live_auction_charge_paid',
                                'auction_winning_price', 'commission_price', 'processing_fee',
                                'total_price', 'recieved_amount_by_winner', 'recieved_amount_by_winner_time',
                                'seller_paying_amount', 'seller_paying_amount_time', 'is_live_charge_paid',
                                'is_winner_paid', 'is_seller_paid');

    public function auction(){
        return $this->belongsTo(Auction::class);
    }
}
