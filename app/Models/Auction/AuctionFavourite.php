<?php

namespace App\Models\Auction;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuctionFavourite extends Model
{
    protected $table = 'auction_favourites';
    public $timestamps = false;
    protected $fillable = array('auction_id', 'user_id','is_favourite');
}
