<?php

namespace App\Models\Auction;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuctionDetail extends Model
{
    protected $table = 'auction_details';
    public $timestamps = FALSE;
    protected $fillable = array('auction_id', 'bidder_id', 'bid_price', 'created_at');

    public function auction()
    {
        return $this->belongsTo(Auction::class);
    }
}
