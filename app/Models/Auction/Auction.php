<?php

namespace App\Models\Auction;

use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Model;

class Auction extends Model
{
    protected $table = 'auctions';
    public $timestamps = FALSE;

    protected $fillable = array(
        'product_id', 'description', 'base_price', 'bid_increment',
        'start_date', 'end_date', 'winnig_position',
        'winner_id', 'highest_bid', 'commission_price',
        'processing_fee', 'service_charge', 'total_price', 'has_live',
        'live_start_time', 'live_duration', 'live_extend_minutes',
        'live_extends_count', 'is_featured', 'created_by',
        'created_at', 'status', 'live_final_end_time', 'live_end_time',
        'terms_condition', 'shipping_payment', 'is_streaming', 'streaming_token', 'is_seller_paid', 'is_winner_paid', 'is_approved'
    );

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function auction_payment()
    {
        return $this->hasOne(AuctionPayment::class);
    }

    public function auction_details()
    {
        return $this->hasMany(AuctionDetail::class);
    }
}
