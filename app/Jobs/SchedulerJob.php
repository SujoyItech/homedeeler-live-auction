<?php

namespace App\Jobs;

use App\Http\Services\Auction\LiveAuctionService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SchedulerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $live_auction_service;
    public function __construct(LiveAuctionService $live_auction_service)
    {
        $this->live_auction_service = $live_auction_service;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->live_auction_service->makeAuctionLive();
        $this->live_auction_service->makeAuctionLiveEnd();
    }
}
