<?php

use App\Http\Services\Production\ProductionService;
use App\Models\CRM\CrmPrintingOption;
use App\Models\CRM\CrmQuotation;
use App\Models\Product\ProductCombination;
use App\Models\Product\ProductPricing;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


function getMoney($digit): string {
    $fmt = new NumberFormatter('en', NumberFormatter::CURRENCY);
    return $fmt->formatCurrency($digit, "USD");
}


function formatQuoteReference($brand_slug = '', $pk): string {
    return $brand_slug . '-' . date('Ym') . '-' . $pk;
}

function formatBudgetReference($brand_slug = '', $salesman_slug = '', $ym = '', $pk): string {
    return $brand_slug . '-' . $salesman_slug . '-' . $ym . '-' . $pk;
}

function formatCustomerReference($customer_slug = '', $pk): string {
    return $customer_slug . '-' . date('Ym') . '-' . $pk;
}

function customerReferenceWithSalesman($salesman, $customer_slug, $ym, $pk): string {
    return $salesman . '-' . $customer_slug . '-' . $ym . '-' . $pk;
}

function formatOrderReference($pk): string {
    return 'ENC' . '-' . date('Ym') . '-' . $pk;
}

function formatPurchaseReference($pk): string {
    return 'PUR' . '-' . date('Ym') . '-' . $pk;
}

function formatProductionReference($pk): string {
    return 'PRD' . '-' . date('Ym') . '-' . $pk;
}


function getCustomerReferenceCode($code) {
    $result = $code;
    $code = explode('-', $code);
    $size = sizeof($code);
    if ($size > 3) {
        $result = $code[1] . '-' . $code[2] . '-' . $code[3];
    }
    else {
        $result = $code[0] . '-' . $code[1] . '-' . $code[2];
    }

    return $result;
}

function get_product_combination_thumbnail_and_media_url($productCombination): array {
    if ($productCombination->media_type == INTERNAL_IMAGE) {
        return ['media_url' => Storage::url($productCombination->media_url), 'thumbnail' => Storage::url($productCombination->media_url)];
    }
    else {
        if ($productCombination->media_type == EXTERNAL_IMAGE) {
            return ['media_url' => $productCombination->media_url, 'thumbnail' => $productCombination->media_url];
        }
        else {
            if ($productCombination->media_type == VIDEO_URL) {
                return ['media_url' => $productCombination->media_url, 'thumbnail' => adminAsset('images/play.png')];
            }
            else {
                if ($productCombination->media_type == _360_URL) {
                    return ['media_url' => Storage::url($productCombination->media_url), 'thumbnail' => adminAsset('images/360.png')];
                }
            }
        }
    }
    return ['media_url' => '', 'thumbnail' => ''];
}

function get_product_media_type($input = NULL) {
    $output = [
        INTERNAL_IMAGE => __('Upload Image'),
        EXTERNAL_IMAGE => __('External Image Url'),
        VIDEO_URL => __('External Video Url'),
        _360_URL => __('External 360 Image Url')
    ];
    return is_null($input) ? $output : $output[$input];
}

// check customer
function check_customer($email) {
    $customer = DB::table('crm_customers')->where(['email' => $email])->first();
    if (isset($customer)) {
        return TRUE;
    }
    return FALSE;
}

if (!function_exists('sendResponse')) {
    function sendResponse($data = '', $message = '') {
        if (!empty($data)) {
            $response = [
                'success' => TRUE,
                'message' => $message,
                'data' => $data,
            ];
        }
        else {
            $response = [
                'success' => TRUE,
                'message' => $message,
            ];
        }

        return response()->json($response, 200);
    }
}

if (!function_exists('sendResponseError')) {
    /**
     * @param $errorCode
     * @param $error
     * @param array $errorMessages
     * @param int $code
     *
     * @return \Illuminate\Http\JsonResponse
     */
    function sendResponseError($errorCode = '', $errorMessage = '', $exceptionMessage = '', $errorMessages = [], $code = 200) {
        if (!$errorMessage) {
            $errorMessage = __('Something went wrong.');
        }
        if ($exceptionMessage) {
            $errorMessage .= ' ' . $exceptionMessage;
        }
        $response = [
            'success' => FALSE,
            'code' => intval($errorCode),
            'message' => $errorMessage
        ];

        if (!empty($errorMessages)) {
            $response['data'] = $errorMessages;
        }

        return response()->json($response, $code);
    }
}


if (!function_exists('get_product_featured_media')) {
    function get_product_featured_media($productId) {
        $productCombination = DB::table('product_combinations')->where(['product_id' => $productId, 'is_featured' => ACTIVE, 'status' => ACTIVE])->first();
        if ($productCombination) {
            return get_product_combination_thumbnail_and_media_url($productCombination);
        }
        else {
            return ['media_url' => '', 'thumbnail' => ''];
        }
    }
}

function get_product_combination($productId, $combinationId, $combination_type_id) {
    $data = [
        'image' => adminAsset('images/no-image.png'),
        'combination_type' => '',
        'combination' => '',
    ];
    $productCombination = ProductCombination::where(['product_id' => $productId, 'combination_id' => $combinationId, 'combination_type_id' => $combination_type_id, 'status' => STATUS_ACTIVE])->first();
    if (isset($productCombination)) {
        $data = [
            'image' => get_product_combination_thumbnail_and_media_url($productCombination)['thumbnail'],
            'combination_type' => isset($productCombination->combination_type) ? $productCombination->combination_type->name : '',
            'combination' => isset($productCombination->combination) ? $productCombination->combination->name : '',
        ];
    }

    return $data;
}

function get_product_pricing($product_id) {
    return DB::table('product_pricings')->where(['product_id' => $product_id, 'status' => STATUS_ACTIVE])->get();
}

function createCRMLog($id) {
    //
}

//purchased product arrival status

if (!function_exists('getPermittedBrands')) {
    function getPermittedBrands($user_id = NULL) {
        $user_id = !is_null($user_id) ? $user_id : \Illuminate\Support\Facades\Auth::id();
        $user = \App\Models\User::where('id', $user_id)->first();
        if ($user->module_id == MODULE_SUPER_ADMIN || $user->module_id == MODULE_USER_ADMIN) {
            return \App\Models\UserBrand::join('brands', 'user_brands.brand_id', '=', 'brands.id')
                ->where('brands.status', STATUS_ACTIVE)->pluck('user_brands.brand_id')->toArray();
        }
        else {
            return \App\Models\UserBrand::join('brands', 'user_brands.brand_id', '=', 'brands.id')
                ->where(['user_brands.user_id' => $user_id])
                ->where('brands.status', STATUS_ACTIVE)
                ->pluck('brand_id')->toArray();
        }
    }
}

if (!function_exists('checkYesNo')) {
    function checkYesNo($status) {
        if ($status == STATUS_ACTIVE) {
            return '<span class="badge badge-success">' . __('Yes') . '</span>';
        }
        else {
            return '<span class="badge badge-danger">' . __('No') . '</span>';
        }
    }
}

if (!function_exists('getPercentageValue')) {
    function getPercentageValue($value, $percentage) {
        return (float)($value * $percentage) / 100;
    }
}

if (!function_exists('checkPreviousBid')) {
    function checkPreviousBid($auction_id, $user_id) {
        $bid_check = \App\Models\Auction\AuctionDetail::where(['auction_id' => $auction_id, 'bidder_id' => $user_id])->first();
        if (isset($bid_check)) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
}
