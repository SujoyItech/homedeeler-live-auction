<?php

use App\Http\Boilerplate\CustomResponse;
use App\Models\Role\Role;
use App\Models\Role\RoleRoute;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/***********************************************************************************/
/***********************************************************************************/

function __options(array $option_group = ['application_settings'], string $option_key = NULL) {
    $option_value = \App\Models\Setting::select('option_key', 'option_value');
    if (is_null($option_key)) {
        $option_value->whereIn('option_group', $option_group);
        $options = $option_value->get();
        $result = [];
        if (!empty($options)) {
            foreach ($options as $data) {
                $result[$data->option_key] = $data->option_value;
            }
        }
        return (object)$result;
    } else {
        $option_value->value('option_value');
        $option_value->where('option_key', '=', $option_key);
        return !empty($option_value->first()) ? $option_value->first()->option_value : '';
    }
}
function __setOptions($option_group = '', $option_key = '', $option_value = '') {
    DB::table('sys_system_settings')
      ->updateOrInsert(
          ['option_group' => $option_group, 'option_key' => $option_key],
          ['option_value' => $option_value]
      );
}


function is_selected($key, $value): string {
    return $key === $value ? 'selected' : '';
}

function buildTree($elements, $parentId = 0, $branch = []): array {
    foreach ($elements as $element) {
        if ($element->parent_id == $parentId) {
            $children = buildTree($elements, $element->id);
            if ($children) {
                $element->children = $children;
            }
            $branch[] = $element;
        }
    }
    return $branch;
}

function generateParentTree($elements,$element_id, $branch = []): array {
    foreach ($elements as $element) {
        if ($element->id == $element_id) {
            $new_parent_id = $element->parent_id;
            if ($new_parent_id != 0) {
                $element->parent = generateParentTree($elements,$new_parent_id,$branch);
            }
            $branch[] = $element;
        }
    }
    return $branch;
}


function buildTreeForArray($elements=[], $parentId = 0, $branch = []): array {
    foreach ($elements as $element) {
        if ($element['parent_id'] == $parentId) {
            $children = buildTreeForArray($elements, $element['id']);
            if ($children) {
                $element['children'] = $children;
            }
            $branch[] = $element;
        }
    }
    return $branch;
}

function language() {
    $lang = [];
    $path = base_path('resources/lang');
    foreach (glob($path . '/*.json') as $file) {
        $langName = basename($file, '.json');
        $lang[$langName] = $langName;
    }
    return empty($lang) ? FALSE : $lang;
}

function randomString($a, $only_number = FALSE) {
    $x = $only_number ? '0123456789' : 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    $c = strlen($x) - 1;
    $z = '';
    for ($i = 0; $i < $a; $i++) {
        $y = rand(0, $c);
        $z .= substr($x, $y, 1);
    }
    return $z;
}

function randomNumber($a = 10) {
    $x = '123456789';
    $c = strlen($x) - 1;
    $z = '';
    for ($i = 0; $i < $a; $i++) {
        $y = rand(0, $c);
        $z .= substr($x, $y, 1);
    }
    return $z;
}


function getNextPrimaryId($table_name) {
    $lastId = sprintf('%07d', 1);
    $item = DB::table($table_name)->orderBy('id', 'desc')->first();
    if (isset($item)) {
        $lastId = $item->id + $lastId;
    }
    return sprintf('%07d', $lastId);
}

/****************** ROUTING AND MENU ***********************/

/**
 * @param $module_to_check
 *
 * @return bool
 */
function check_permission($module_to_check) {
    $user_module_id = Auth::user()->module_id;
    $response = FALSE;
    foreach (MODULES as $module_id => $module_name){
        if ($module_to_check == $module_id) {
            if($user_module_id === $module_id){
                $response = TRUE;
                break;
            }
        }
    }
    return $response;
}

function check_role($role_to_check) {
    $user_role_id = Auth::user()->role;
    $response = FALSE;
    foreach (ROLES as $role_id => $role_name){
        if ($role_to_check == $role_id) {
            if($user_role_id === $role_id){
                $response = TRUE;
                break;
            }
        }
    }
    return $response;
}

/**
 * @param $route_name
 * @param bool $admin_permission
 *
 * @return bool
 */
function check_route_permission($route_name, $admin_permission = FALSE): bool {
    $route_for_all = ["profile","viewAllNotifications"];
    if (in_array($route_name,$route_for_all)) {
        return TRUE;
    }
    if (Auth::user()->module_id == MODULE_SUPER_ADMIN) {
        return TRUE;
    } else {
        if (Auth::user()->module_id == MODULE_USER_ADMIN && $admin_permission == TRUE) {
            return TRUE;
        } else {
            $action = RoleRoute::where('url', $route_name)->first();
            $role = Role::where('id', Auth::user()->role)->first();
            if (!empty($role->actions) && !empty($action)) {
                if (!empty($role->actions)) {
                    $tasks = array_filter(explode('|', $role->actions));
                    if (isset($tasks)) {
                        return in_array($action->id, $tasks);
                    }
                }else{
                    return FALSE;
                }

            }else{
                return FALSE;
            }
        }
        return FALSE;
    }
}
/**
 * @param $route_name
 * @param $menu_title
 * @param string $menu_icon
 *
 * @param string $submenu
 * @param string $submenu_to_compare
 * @param bool $admin_permission
 *
 * @return string
 */
function menuLiAppend($route_name, $menu_title, $menu_icon='fas fa-align-right', $submenu = '', $submenu_to_compare = '', $admin_permission = TRUE, $badge_class = NULL): string {
    $html = '';
    if(substr($route_name, 0, 1 ) !== "#"){
        if(check_route_permission($route_name, $admin_permission)){
            $submenu_to_compare = $submenu_to_compare != '' ? $submenu_to_compare : $route_name;
            $li_a_active = !empty($submenu) && $submenu == $submenu_to_compare ? 'menuitem-active' : '';
            $a_active = !empty($submenu) && $submenu == $submenu_to_compare ? 'active' : '';
            $html .= '<li class="nav-item '.$li_a_active.'">';
            $html .= '<a class="nav-link '.$a_active.'" href="'. route($route_name) .'">';
            $html .= '<i class="'. $menu_icon .'"></i> '. __($menu_title);

            if(!is_null($badge_class)){
                $html .= '<span class="badge '.$badge_class.' float-right">0</span>';
            }

            $html .= '</a>';
            $html .= '</li>';
        }
    }else{
        $html .= '<li class="nav-item">';
        $html .= '<a class="nav-link" href="'.$route_name.'">';
        $html .= '<i class="'. $menu_icon .'"></i> '. __($menu_title);
        $html .= '</a>';
        $html .= '</li>';
    }
    return $html;
}

/**
 * @param $route_name
 * @param $menu_title
 * @param string $menu_icon
 * @param string $menu
 * @param string $menu_to_compare
 * @param array $module_permission
 *
 * @return string
 */
function mainMenuAppend($route_name, $menu_title, $menu_icon='fas fa-align-right 2x', $menu = '', $menu_to_compare = '', $module_permission = []): string {
    $html = '';
    $permitted = FALSE;
    $is_active = !empty($menu) && $menu == $menu_to_compare ? 'active' : '';
    if(substr($route_name, 0, 1 ) === "#"){
        if(!empty($module_permission)){
            foreach ($module_permission as $module_id){
                if(check_permission($module_id) == TRUE) {
                    $permitted = TRUE;
                    break;
                }
            }
            if($permitted){
                $html .= generateMainMenuLink($is_active, $route_name, $menu_title, $menu_icon);
            }
        }else{
            $html .= generateMainMenuLink($is_active, $route_name, $menu_title, $menu_icon);
        }
    }else{
        $route_name = route($route_name);
        $html .= generateMainMenuLink($is_active, $route_name, $menu_title, $menu_icon);
    }
    return $html;
}

/**
 * @param $is_active
 * @param $route_name
 * @param $menu_title
 * @param $menu_icon
 *
 * @return string
 */
function generateMainMenuLink($is_active, $route_name, $menu_title, $menu_icon): string {
    $html = '';
    $html .= '<a class="nav-link '. $is_active .'" href="'.$route_name.'"';
    $html .= 'data-plugin="tippy" data-tippy-followCursor="true" data-tippy-arrow="true" data-placement="right" data-tippy-animation="fade"';
    $html .= 'title="'.$menu_title.'">';
    $html .= '<i class="'.$menu_icon.'"></i>';
    $html .= '</a>';
    return $html;
}

/****************** END OF ROUTING AND MENU ************************/

/**
 * @param $text
 *
 * @return string
 */
function textSlugify($text): string {
    $text_array = explode(' ', $text);
    $slug = '';
    foreach ($text_array as $i => $split_text) {
        $slug = $slug . strtoupper(substr(trim($split_text), 0, 1));
    }
    return $slug;
}

/**
 * @param $code
 * @param null $index
 *
 * @return false|mixed|string|string[]
 */
function referenceCodeEdit($code, $index = NULL) {
    try {
        $code = explode('-', $code);
        return is_null($index) ? $code : $code[$index];
    } catch (\Exception $e) {
        return FALSE;
    }
}

/**
 * @param string $str
 *
 * @return string|string[]|null
 */
function create_slug($str = '') {
    $str = preg_replace('/\s\s+/', ' ', $str);
    $str = str_replace(' ', '-', $str);                // Replaces all spaces with hyphens.
    $str = preg_replace('/[^A-Za-z0-9\-]/', '', $str); // Removes special chars.
    $str = preg_replace('/-+/', '-', $str);
    $a = substr($str, -1);
    if ($a == '-') {
        $str = rtrim($str, '-');
        //$str = clean_fields($str);
    }
    return $str;
}

/**
 * @param string $str
 *
 * @return string|string[]|null
 */
function clean_reference($str = '') {
    $str = str_replace(' ', '', $str);                 // Replaces all spaces with hyphens.
    $str = preg_replace('/[^A-Za-z0-9\-]/', '', $str); // Removes special chars.
    return $str;
}

/**
 * @param bool $type
 *
 * @return CustomResponse
 */
function jsonResponse($type = TRUE) {
    return new CustomResponse($type);
}

function getResponseMessage(bool $type = FALSE, string $message = 'Something went wrong') {
    return [
        'success' => $type,
        'message' => $message,
        'data'    => []
    ];
}

function sendError($error, $errorMessages = [], $code = 404) {
    $response = [
        'success' => FALSE,
        'message' => $error,
    ];
    if (!empty($errorMessages)) {
        $response['data'] = $errorMessages;
    }
    return response()->json($response, $code);
}

function countUnreadNotification(){
    return \App\Models\Notification::where(['user_id'=>Auth::user()->id,'status'=>PROCESSING])->count();
}
