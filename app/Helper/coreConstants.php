<?php

/*================================= Role ============================================= */
const SUPER_ADMIN = 0;
const USER_ADMIN = 1;
const USER_SELLER = 20;
const USER_BIDDER = 21;
/*================================= End Of Role ============================================= */

/*================================= Module ============================================= */
const MODULE_SUPER_ADMIN = 0;
const MODULE_USER_ADMIN = 1;
const MODULE_USER = 2;

const MODULES = array(
    MODULE_SUPER_ADMIN => 'Super Admin Module',
    MODULE_USER_ADMIN => 'Admin Module',
    MODULE_USER => 'User',
);

const ROLES = array(
    SUPER_ADMIN => 'Super Admin',
    USER_ADMIN => 'Admin',
    USER_SELLER => 'Seller',
    USER_BIDDER => 'Bidder',
);

const IS_USER = 0;
const IS_SELLER = 1;
const IS_SELLER_PENDING = 2;
/*================================= End Of Module ===================================== */


/*================================= Status ===================================== */
const INACTIVE = 0;
const ACTIVE = 1;
const AUCTION_IN_LIVE = 11;
const AUCTION_LIVE_END = 12;
const AUCTION_END = 15;
const PAYMENT_COMPLETED = 20;
const DELIVERY_REQUESTED = 30;
const DELIVERY_IN_SHIPMENT = 31;
const DELIVERY_COMPLETED = 35;
const PRODUCT_RECEIVED = 40;
const SELLER_PAID = 50;

const SOLD = 30;
const CLOSED = 40;
const USER_SUSPENDED = 2;
const USER_BLOCKED = 3;

const STATUS_PROCESSING = 0;
const STATUS_ACTIVE = 1;
const STATUS_SUCCESS = 1;
const STATUS_ADMIN_APPROVAL = 2;
const STATUS_CANCELLED = 3;
const STATUS_FAILED = 4;
const STATUS_APPROVED_BY_CUSTOMER = 1;
const STATUS_APPROVED_BY_MANAGER = 2;

//transaction status
const PROCESSING = 0;
const CONFIRMED = 1;
const NOTIFIED = 2;
const COMPLETED = 3;
const FAILED = 4;
const REJECTED = 5;
const EXPIRED = 6;
const BLOCKED = 7;
const REFUNDED = 8;
const VOIDED = 9;

//product statuses
const PRODUCT_DRAFT = 2;

/*================================= End Of Status ===================================== */


/*================================= Combination Type ===================================== */
const COLOR_ID = 1;
const SIZE_ID = 2;

//product combination media type enums
const INTERNAL_IMAGE = 'internal_image';
const EXTERNAL_IMAGE = 'external_image';
const VIDEO_URL = 'video_url';
const _360_URL = '360_url';

/*================================= End Of Combination Type ===================================== */

// shipped product
const IS_SHIPPED_ITEM = 1;

/*================================= Payment Status ===================================== */
const PAYMENT_NOT_DONE = 0;
const PAYMENT_DONE = 1;
const PAYMENT_IN_PROGRESS = 2;
const PAYMENT_CANCEL = 3;

const PAYMENT_TYPE_LIVE_CHARGE = 1;
const PAYMENT_TYPE_OFFLINE_CHARGE = 2;
const PRODUCT_PURCHASE_PAYMENT = 3;
const SELLER_PAYMENT = 4;

const LIVE_AUCTION_CHARGE_PAYMENT = 1;

const CARD_BRAIN_TREE = 1;
const CARD_STRIPE = 2;
const CARD_PAYPAL = 3;
const BTC = 10;
const OFFLINE_PAYMENT = 15;

const PAYMENT_PURPOSES = array(
    PAYMENT_TYPE_LIVE_CHARGE => 'Live Charge Payment',
    PAYMENT_TYPE_OFFLINE_CHARGE => 'Offline Payment',
    PRODUCT_PURCHASE_PAYMENT => 'Product Purchase Payment',
    SELLER_PAYMENT => 'Seller Payment',
);

const PAYMENT_METHODS = array(
    CARD_BRAIN_TREE => 'Braintree',
    CARD_STRIPE => 'Stripe',
    CARD_PAYPAL => 'Paypal',
    BTC => 'BTC',
    OFFLINE_PAYMENT => 'Offline Payment',
);

const AUCTION_STATUS = array(
    INACTIVE => 'Inactive',
    ACTIVE => 'Active',
    AUCTION_IN_LIVE => 'In Live',
    AUCTION_LIVE_END => 'Live End',
    AUCTION_END => 'Auction End',
    PAYMENT_COMPLETED => 'Payment Completed',
    DELIVERY_REQUESTED => 'Delivery Requested',
    DELIVERY_IN_SHIPMENT => 'In Shipment',
    DELIVERY_COMPLETED => 'Delivery Completed',
    PRODUCT_RECEIVED => 'Product Received',
    SELLER_PAID => 'Seller Paid',
);
/*================================= Payment Status ===================================== */

/*================================= Settings ===================================== */
const SORT_BY_LOWEST_PRICE = 1;
const SORT_BY_HIGHEST_PRICE = 2;
const SORT_BY_NEWEST = 3;

const GLOBAL_PAGINATION = 20;
const MYSQL_DATE_FORMAT = 'Y-m-d h:i:s';
const MYSQL_DATE_WITHOUT_TIME = 'Y-m-d';

//users table created_by column value definition
const SEEDUSER = 0;
/*================================= Settings ===================================== */

/*================================= Notification ===================================== */
const NOTIFICATION_BIDDING = 10;
/*================================= Notification ===================================== */

/*================================= Auction Paginate ===================================== */
const AUCTION_PAGINATE = 10;
const AUCTION_PAGINATE_WEB = 12;
/*================================= Auction Paginate ===================================== */

const BTC_ADDRESS_ACTIVE = 1;
const BTC_ADDRESS_EXPIRED = 2;
