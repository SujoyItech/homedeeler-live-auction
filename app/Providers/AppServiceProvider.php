<?php

namespace App\Providers;

use Braintree\Configuration;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        date_default_timezone_set('Asia/Dhaka');

        try {
            $payment_settings = __options(['payment_settings']);
            if (!empty($payment_settings)) {
                Configuration::environment($payment_settings->brain_tree_env ?? '');
                Configuration::merchantId($payment_settings->brain_tree_merchant_id ?? '');
                Configuration::publicKey($payment_settings->brain_tree_public_key ?? '');
                Configuration::privateKey($payment_settings->brain_tree_private_key ?? '');

                Config::set('paypal.client_id', $payment_settings->paypal_client_id);
                Config::set('paypal.secret', $payment_settings->paypal_secret);
                Config::set('paypal.settings.mode', $payment_settings->paypal_env);
            }
        } catch (\Exception $exception) {
            Configuration::environment(config('services.braintree.environment'));
            Configuration::merchantId(config('services.braintree.merchant_id'));
            Configuration::publicKey(config('services.braintree.public_key'));
            Configuration::privateKey(config('services.braintree.private_key'));
        }
    }
}
